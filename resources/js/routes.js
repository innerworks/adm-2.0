import web from './views/Web/Index.vue';
// auth
import login from './views/Admin/Auth/Login.vue';
import forgotPassword from './views/Admin/Auth/ForgotPassword.vue';
import adminResetPassword from './views/Admin/Auth/ResetPassword.vue';

// cms components pages
import admin from './views/AdminLayout/TheMaster.vue';
import dashboard from './views/Admin/Dashboard/Index.vue';
import accountSettings from './views/Admin/MyAccount/Index.vue';
import customFields from './views/Admin/CustomFields/Index.vue';
import addFieldGroup from './views/Admin/CustomFields/Add.vue';
import comingSoon from './views/ComingSoon/Index.vue';
// end cms components pages

//users
import users from './views/Admin/User/Index.vue';
import addUser from './views/Admin/User/Add.vue';
import editUser from './views/Admin/User/Edit.vue';

import roles from './views/Admin/Role/Index.vue';
import addRole from './views/Admin/Role/Add.vue';
import editRole from './views/Admin/Role/Edit.vue';
import grantPermissions from './views/Admin/Role/Permissions.vue';

import categories from './views/Admin/Categories/Index.vue';
import addCategory from './views/Admin/Categories/Add.vue';
import editCategory from './views/Admin/Categories/Edit.vue';

import brands from './views/Admin/Brands/Index.vue';
import addBrand from './views/Admin/Brands/Add.vue';
import editBrand from './views/Admin/Brands/Edit.vue';

import merchants from './views/Admin/Merchant/Index.vue';
import addMerchant from './views/Admin/Merchant/Add.vue';
import editMerchant from './views/Admin/Merchant/Edit.vue';

import productSizes from './views/Admin/ProductSizes/Index.vue';
import addProductSize from './views/Admin/ProductSizes/Add.vue';
import editProductSize from './views/Admin/ProductSizes/Edit.vue';

import products from './views/Admin/Product/Index.vue';
import addProduct from './views/Admin/Product/Add.vue';
import editProduct from './views/Admin/Product/Edit.vue';

import bestSeller from './views/Admin/Product/BestSeller.vue';

import bannerAds from './views/Admin/BannerAds/Index.vue';
import addBannerAd from './views/Admin/BannerAds/Add.vue';
import editBannerAd from './views/Admin/BannerAds/Edit.vue';

import Ads from './views/Admin/Ads/Index.vue';
import addAd from './views/Admin/Ads/Add.vue';
import editAd from './views/Admin/Ads/Edit.vue';

import FlashSale from './views/Admin/FlashSales/Index.vue';
import addFlashSale from './views/Admin/FlashSales/Add.vue';
import editFlashSale from './views/Admin/FlashSales/Edit.vue';

import CommissionRate from './views/Admin/CommissionRate/Index.vue';
import addCommissionRate from './views/Admin/CommissionRate/Add.vue';
import editCommissionRate from './views/Admin/CommissionRate/Edit.vue';

import Voucher from './views/Admin/Voucher/Index.vue';
import addVoucher from './views/Admin/Voucher/Add.vue';
import editVoucher from './views/Admin/Voucher/Edit.vue';

import Orders from './views/Admin/Orders/Index.vue';
import editOrders from './views/Admin/Orders/Edit.vue';
import editView from './views/Admin/Orders/View.vue';

import salesReport from './views/Admin/Reports/Sales.vue';
import statusReport from './views/Admin/Reports/Status.vue';

import viewTracker from './views/Admin/ViewTracker/Index.vue';

import couriers from './views/Admin/Courier/Index.vue';
import new_couriers from './views/Admin/Courier/Add.vue';

import merchantLayout from './views/MerchantLayout/TheMaster.vue';
import merchantDashboard from './views/Merchant/Dashboard/Index.vue';

import merchantLogin from './views/Merchant/Auth/Login.vue';
import merchanForgotPassword from './views/Merchant/Auth/ForgotPassword.vue';

import merchantOrders from './views/Merchant/Orders/Index.vue';

import merchantCategories from './views/Merchant/Categories/Index.vue';
import merchantDddCategory from './views/Merchant/Categories/Add.vue';
import merchantEditCategory from './views/Merchant/Categories/Edit.vue';

import merchantProductSizes from './views/Merchant/ProductSizes/Index.vue';
import merchantAddProductSize from './views/Merchant/ProductSizes/Add.vue';
import merchantEditProductSize from './views/Merchant/ProductSizes/Edit.vue';

import merchantProducts from './views/Merchant/Product/Index.vue';
import merchantAddProduct from './views/Merchant/Product/Add.vue';
import merchantEditProduct from './views/Merchant/Product/Edit.vue';

import merchantSettings from './views/Merchant/Settings/Index.vue';

import merchantCouriers from './views/Merchant/Courier/Index.vue';
import merchantNew_couriers from './views/Merchant/Courier/Add.vue';

import merchantSalesReport from './views/Merchant/Reports/Sales.vue';
import merchantStatusReport from './views/Merchant/Reports/Status.vue';


import socialLogin from './components/Social.vue';
import customerLogin from './views/Web/Auth/Login.vue';
import customerRegister from './views/Web/Auth/Register.vue';
import customerForgotPassword from './views/Web/Auth/ForgotPassword.vue';
import customerPasswordReset from './views/Web/Auth/PasswordReset.vue';
import customer from './views/CustomerLayout/TheMaster.vue';
import customerHomepage from './views/Web/Index.vue';
import customerOnSale from './views/Web/OnSale.vue';
import customerNew from './views/Web/New.vue';
import customerBestSeller from './views/Web/BestSeller.vue';
import customerBrand from './views/Web/ShopBrand.vue';
import customerAllBrand from './views/Web/Brands.vue';
import customerSearchAll from './views/Web/SearchAll.vue';
import customerSearchList from './views/Web/SearchList.vue';
import customerCategory from './views/Web/ShopCategory.vue';
import customerProductDetails from './views/Web/ProductDetails.vue';
import merchantStore from './views/Web/MerchantStore.vue';
import customerCart from './views/Web/Cart.vue';
import customerShipping from './views/Web/Shipping.vue'
import customerShippingCartCheckout from './views/Web/ShippingCartCheckout.vue';
import customerSelectPayment from './views/Web/SelectPayment.vue';
import customerSelectPaymentSolo from './views/Web/SelectPaymentSolo.vue';
import customerPaymentStatus from './views/Web/PaymentStatus.vue';
import customerOrders from './views/Web/Profile/Orders.vue';
import customerOrderDetails from './views/Web/Profile/OrderDetails.vue';
import customerWishlist from './views/Web/Profile/Wishlists.vue';
import customerSettings from './views/Web/Profile/Settings.vue';
import customerAddress from './views/Web/Profile/CustomerAddress.vue';

import contactUs from './views/Web/ContactUs.vue';
import privacyPolicy from './views/Web/PrivacyPolicy.vue';
import termsAndCondition from './views/Web/TermsAndCondition.vue';
import faq from './views/Web/Faq.vue';

export const routes = [
    // customer
    // {
    //     path: '/',
    //     component: web,
    // },
    {
        path: '/register',
        component: customerRegister,
    },
    {
        path: '/login',
        name: 'customer-login',
        component: customerLogin,
    },
    {
        path: '/social/:token',
        component: socialLogin
    },    
    {
        path: '/password/create',
        component: customerForgotPassword,
    },
    {
        path: '/password/reset/:token',
        component: customerPasswordReset,
    },
    {
        path: '/',
        component: customer,
        children: [
            {
                path: '/',
                component: customerHomepage
            },
            {
                path: '/on-sale/sort/:sort',
                component: customerOnSale
            },
            {
                path: '/new',
                component: customerNew
            },
            {
                path: '/best-sellers',
                component: customerBestSeller
            },
            {
                path: '/brand',
                component: customerAllBrand
            },
            {
                path: '/brand/:name/sort/:sort',
                component: customerBrand
            },
            {
                path: '/category/:category/sort/:sort',
                component: customerCategory
            },
            {
                path: '/search-all',
                component: customerSearchAll
            },
            {
                path: '/search',
                component: customerSearchList,
                name: 'search'
            },
            {
                path: '/product/:name',
                component: customerProductDetails
            },
            {
                path: '/merchant-:id/:store_name/sort/:sort',
                component: merchantStore
            },
            {
                path: '/orders',
                component: customerOrders,
            },
            {
                path: '/wishlist',
                component: customerWishlist,
            },
            {
                path: '/order-details/:id',
                component: customerOrderDetails,
            },
            {
                path: '/cart',
                component: customerCart,
            },
            {
                path: '/shipping/:product_name',
                component: customerShipping,
            },
            {
                path: '/shipping-cart-checkout',
                component: customerShippingCartCheckout,
            },
            {
                path: '/select-payment',
                component: customerSelectPayment,
            },
            {
                path: '/select-payment-solo/:permalink',
                component: customerSelectPaymentSolo,
            },
            {
                path: '/payment-status',
                component: customerPaymentStatus,
            },
            {
                path: '/profile',
                component: customerSettings,
            },
            {
                path: '/address',
                component: customerAddress,
            },
            {
            path: '/contact-us',
                component: contactUs,
            },
            {
                path: '/privacy-policy',
                component: privacyPolicy,
            },
            {
                path: '/terms-and-conditions',
                component: termsAndCondition,
            },
            {
                path: '/faq',
                component: faq,
            },
        ]
    },
    // customer end
    {
        path: '/admin/login',
        component: login,
        name: 'login'
    },
    {
        path: '/admin/forgot-password',
        component: forgotPassword,
        name: 'forgot-password'
    },
    {
        path: '/admin/reset-password/:token',
        component: adminResetPassword,
        name: 'reset-password'
    },
    {
        path: '/admin/dashboard',
        component: admin,
        meta: {
            requiresAuth: true
        },
        children: [          
            {
                path: '/',
                component: dashboard
            },            
            {
                path: '/admin/comingSoon',
                component: comingSoon,
                name: 'comming-soon'
            },  
            {
                path: '/admin/users',
                component: users,
                name: 'users'
            },            
            {
                path: '/admin/user/add',
                component: addUser,
                name: 'user.add'
            },            
            {
                path: '/admin/user/edit/:id',
                component: editUser,
                name: 'user.edit'
            },            
            {
                path: '/admin/roles',
                component: roles,
                name: 'roles'
            },            
            {
                path: '/admin/role/add',
                component: addRole,
                name: 'role.add'
            },            
            {
                path: '/admin/role/edit/:id',
                component: editRole,
                name: 'role.edit'
            },            
            {
                path: '/admin/role/permissions/:id',
                component: grantPermissions,
                name: 'grant.permissions'
            },            
            {
                path: '/admin/categories',
                component: categories,
                name: 'categories'
            },            
            {
                path: '/admin/categories/add',
                component: addCategory,
                name: 'category.add'
            },            
            {
                path: '/admin/categories/edit/:id',
                component: editCategory,
                name: 'category.edit'
            },        
            {
                path: '/admin/brands',
                component: brands,
                name: 'brands'
            },            
            {
                path: '/admin/brands/add',
                component: addBrand,
                name: 'brand.add'
            },            
            {
                path: '/admin/brands/edit/:id',
                component: editBrand,
                name: 'brand.edit'
            },                  
            {
                path: '/admin/merchants',
                component: merchants,
                name: 'merchants'
            },            
            {
                path: '/admin/merchant/add',
                component: addMerchant,
                name: 'merchant.add'
            },            
            {
                path: '/admin/merchant/edit/:id',
                component: editMerchant,
                name: 'merchant.edit'
            },            
            {
                path: '/admin/product-size',
                component: productSizes,
                name: 'product-sizes'
            },            
            {
                path: '/admin/product-size/add',
                component: addProductSize,
                name: 'product-size.add'
            },            
            {
                path: '/admin/product-size/edit/:id',
                component: editProductSize,
                name: 'product-size.edit'
            },            
            {
                path: '/admin/products',
                component: products,
                name: 'products'
            },            
            {
                path: '/admin/product/add',
                component: addProduct,
                name: 'product.add'
            },            
            {
                path: '/admin/product/edit/:id',
                component: editProduct,
                name: 'product.edit'
            },
            {
                path: '/admin/product/best-seller',
                component: bestSeller,
                name: 'product.best-seller'
            },            
            {
                path: '/admin/banner-ads',
                component: bannerAds,
                name: 'banner-ads'
            },            
            {
                path: '/admin/banner-ads/add',
                component: addBannerAd,
                name: 'banner-ad.add'
            },            
            {
                path: '/admin/banner-ads/edit/:id',
                component: editBannerAd,
                name: 'banner-ad.edit'
            },  
            {
                path: '/admin/ads',
                component: Ads,
                name: 'ads'
            },            
            {
                path: '/admin/ads/add',
                component: addAd,
                name: 'ads.add'
            },            
            {
                path: '/admin/ads/edit/:id',
                component: editAd,
                name: 'ads.edit'
            },  
            {
                path: '/admin/view-tracker',
                component: viewTracker,
                name: 'view-tracker'
            },
            {
                path: '/admin/courier',
                component: couriers,
                name: 'courier'
            },
            {
                path: '/admin/courier-add',
                component: new_couriers,
                name: 'courier.add'
            },
            {
                path: '/admin/courier-edit/:id',
                component: new_couriers,
                name: 'courier.edit'
            },            
            {
                path: '/admin/rush-sale',
                component: FlashSale,
                name: 'rush-sale'
            },            
            {
                path: '/admin/rush-sale/add',
                component: addFlashSale,
                name: 'rush-sale.add'
            },            
            {
                path: '/admin/rush-sale/edit/:id',
                component: editFlashSale,
                name: 'rush-sale.edit'
            },  
            {
                path: '/admin/commission-rate',
                component: CommissionRate,
                name: 'commission-rate'
            },            
            {
                path: '/admin/commission-rate/add',
                component: addCommissionRate,
                name: 'commission-rate.add'
            },            
            {
                path: '/admin/commission-rate/edit/:id',
                component: editCommissionRate,
                name: 'commission-rate.edit'
            },  
            {
                path: '/admin/voucher',
                component: Voucher,
                name: 'voucher'
            },            
            {
                path: '/admin/voucher/add',
                component: addVoucher,
                name: 'voucher.add'
            },            
            {
                path: '/admin/voucher/edit/:id',
                component: editVoucher,
                name: 'voucher.edit'
            },  
            {
                path: '/admin/orders',
                component: Orders,
                name: 'orders'
            },            
            {
                path: '/admin/orders/edit/:id',
                component: editOrders,
                name: 'order.edit'
            },  
            {
                path: '/admin/orders/view/:id',
                component: editView,
                name: 'order.view'
            },
            {
                path: '/admin/reports/sales',
                component: salesReport,
                name: 'sales.report'
            },  
            {
                path: '/admin/reports/status',
                component: statusReport,
                name: 'status.report'
            }, 
            // {
            //     path: '/account-settings',
            //     component: accountSettings
            // },            
            // {
            //     path: '/admin/custom-fields',
            //     component: customFields
            // },            
            // {
            //     path: '/admin/field-group',
            //     component: addFieldGroup
            // },            
        ]
    },
    {
        path: '/merchant/login',
        component: merchantLogin
    },
    {
        path: '/merchant/dashboard',
        component: merchantLayout,
        name: 'merchant.dashboard',
        meta: {
            requiresAuth: true
        },
        children: [{
                path: '/',
                component: merchantDashboard
            },            
            {
                path: '/merchant/comingSoon',
                component: comingSoon,
                name: 'merchant.comming-soon'
            },
            {
                path: '/merchant/categories',
                component: merchantCategories,
                name: 'merchant.categories'
            },            
            {
                path: '/merchant/categories/add',
                component: merchantDddCategory,
                name: 'merchant.category.add'
            },            
            {
                path: '/merchant/categories/edit/:id',
                component: merchantEditCategory,
                name: 'merchant.category.edit'
            },
            {
                path: '/merchant/product-size',
                component: merchantProductSizes,
                name: 'merchant.product-sizes'
            },            
            {
                path: '/merchant/product-size/add',
                component: merchantAddProductSize,
                name: 'merchant.product-size.add'
            },            
            {
                path: '/merchant/product-size/edit/:id',
                component: merchantEditProductSize,
                name: 'merchant.product-size.edit'
            },
            {
                path: '/merchant/products',
                component: merchantProducts,
                name: 'merchant.products'
            },            
            {
                path: '/merchant/product/add',
                component: merchantAddProduct,
                name: 'merchant.product.add'
            },            
            {
                path: '/merchant/product/edit/:id',
                component: merchantEditProduct,
                name: 'merchant.product.edit'
            },
            {
                path: '/merchant/orders',
                component: merchantOrders,
                name: 'merchant.orders'
            },            
            {
                path: '/merchant/settings',
                component: merchantSettings,
                name: 'merchant.settings'
            },
            {
                path: '/merchant/reports/sales',
                component: merchantSalesReport,
                name: 'merchant-report.sales'
            },  
            {
                path: '/merchant/reports/status',
                component: merchantStatusReport,
                name: 'merchant-report.status'
            },  
            {
                path: '/merchant/courier',
                component: merchantCouriers,
                name: 'merchant-courier'
            },
            {
                path: '/merchant/courier-add',
                component: merchantNew_couriers,
                name: 'merchant-courier.add'
            },
            {
                path: '/merchant/courier-edit/:id',
                component: merchantNew_couriers,
                name: 'merchant-courier.edit'
            },  
        ]
    }

];