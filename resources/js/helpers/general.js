export function initialize(store, router) {
    // var pathArray = window.location.pathname.split('/');
    // const dashboard = pathArray[1];
    // if((pathArray[1] == 'admin' || pathArray[1] == 'merchant') && !pathArray[2] && !currentUser) {
    //     window.location.href = '/'+dashboard+'/login';
    // }

    router.beforeEach((to, from, next) => {
        const currentUser = store.state.currentUser;
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const dashboard = store.state.dashboard;

        if(requiresAuth && !currentUser) {
            next('/'+dashboard+'/login');
        } 
        else if(to.path == '/'+dashboard+'/login' && currentUser) {
            next('/'+dashboard+'/dashboard');
        } 
        else if(to.path == '/login' && currentUser) {
            next('/');
        }
        else {
            next();
        }
    });

    if (store.getters.currentUser) {
        setAuthorization(store.getters.currentUser.token);
    }
}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`

    console.log(axios.defaults.headers.common["Authorization"]);
}