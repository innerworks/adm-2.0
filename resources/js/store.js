import { getLocalUser } from "./helpers/auth";

const user = getLocalUser();

export default {
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        registration_error: null,
        dashboard: '',
    },
    getters: {
        isLoading(state) {
            return state.loading;
        },
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        currentUser(state) {
            return state.currentUser;
        },
        authError(state) {
            return state.auth_error;
        },
        registrationError(state) {
            return state.registration_error;
        },
    },
    mutations: {
        login(state) {
            state.loading = true;
            state.auth_error = null;
        },
        loginSuccess(state, payload) {
            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.token});
            if(payload.is_merchant) {
                state.dashboard = 'merchant'; 
            } 
            else if (payload.is_customer) {
                state.dashboard = ''; 
            }
            else {
                state.dashboard = 'admin'; 
            }

            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        loginFailed(state, payload) {
            state.loading = false;
            state.auth_error = payload.error;
        },
        logout(state) {
            localStorage.removeItem("user");
            state.isLoggedIn = false;
            state.currentUser = null;
        },
    },
    actions: {
        login(context) {
            context.commit("login");
        },
        loginSuccess(context) {
            // context.commit("loginSuccess");
             // this.$router.push(this.$router.query.redirect || '/')
            // if (state.isLoggedIn == true) {
            //     this.$router.push(this.$route.query.redirect || '/')
            // }
        }
    }
};