require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import {routes} from './routes';
import StoreData from './store';
import Index from './components/Index'
import datePicker from 'vue-bootstrap-datetimepicker';
import {initialize} from './helpers/general';
import 'vue-select/dist/vue-select.css';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
import vSelect from 'vue-select'
import VueCookies from 'vue-cookies';
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: window.location.origin + '/img/product-img-loader.svg',
    loading: window.location.origin + '/img/product-img-loader.svg',
    attempt: 3,
})

Vue.component('v-select', vSelect)

Vue.use(VueCookies);
Vue.use(datePicker);
Vue.use(VueRouter);
Vue.use(Vuex);

Vue.component('social', require('./components/Social').default);

export const bus = new Vue();
const store = new Vuex.Store(StoreData);

const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
    	return { x: 0, y: 0 }
	}
});

initialize(store, router);

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        Index,
    }
});
