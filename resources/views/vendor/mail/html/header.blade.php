<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Adobomall')
<img src="https://v2.adobomall.com/img/logo-adobomall.png" class="logo" alt="Adobomall Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
