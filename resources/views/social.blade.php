<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'ADM Admin Portal') }}</title>
    </head>
    <body class="hold-transition sidebar-mini">
        <div id="app">
            <social :data="{{ $token }}">
        </div>
        <!-- App script -->
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
