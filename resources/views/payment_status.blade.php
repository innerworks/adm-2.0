<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Adobomall</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <!-- <div class="content"> -->
                <div class="container m-5" style="width: 100%; margin: auto; font-size: 18px;">
                    <div class="row">
                        <div class="row col-12 mb-3">
                            <h4>
                                @if ($status['cancel'] == false)                       
                                    @if ($status['paymentStatus'] == 'success')
                                        <span>Your Order is Confirmed!</span>
                                    @elseif ($status['paymentStatus'] == 'pending')
                                        <span>Your Order is Pending.</span>
                                    @else
                                        <span>Your Order is not Confirmed</span>
                                    @endif
                                @else
                                    <span>Your Order has been Cancelled</span>
                                @endif
                            </h4>
                        </div>
                        @if ($status['cancel'] == false)
                            @if ($status['paymentStatus'] == 'success')
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        Order Confirmation for #<strong>{{ $order }}</strong> 
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    @if (!empty($user['first_name']))
                                    <span class="sub-text">
                                            Dear {{ $user['first_name'] }},
                                    </span>
                                    @endif
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        Thank you!
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        Your Order #{{ $order }} has been placed on {{ date("F j, Y, g:i a") }} via {{ $status['paymentType']['payment_type'] }}
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        Your order will be delivered to (except for vouchers):
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        {{ $user['first_name'] }} {{ $user['last_name'] }}
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        {{ $address['detailed_address'] }}
                                    </span>
                                </div>
                            @elseif($status['paymentStatus'] == 'pending')
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        Order Pending for #<strong>{{ $order }}</strong> 
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    @if (!empty($user['first_name']))
                                    <span class="sub-text">
                                        Dear {{ $user['first_name'] }},
                                    </span>
                                    @endif
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        Please go to the nearest {{ $status['paymentType']['payment_type'] }} to complete your purchase. Your Order ID is: <strong>{{ order }}</strong>.
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        Your order will be delivered to (except for vouchers):
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        {{ $user['first_name'] }} {{ $user['last_name'] }}
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        {{ $address['detailed_address'] }}
                                    </span>
                                </div>
                            @else
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        Order #<strong>{{ $order }}</strong> 
                                    </span>
                                </div>
                                <div class="row col-12 mb-3">
                                    @if (!empty($user['first_name']))
                                    <span class="sub-text">
                                        Dear {{ $user['first_name'] }} {{ $user['last_name'] }},
                                    </span>
                                    @endif
                                </div>
                                <div class="row col-12 mb-3">
                                    <span class="sub-text">
                                        We have encountered an error with Order #{{ $order }} placed on {{ date("F j, Y, g:i a") }}
                                    </span>
                                </div>
                            @endif
                        @else
                            <div class="row col-12 mb-3">
                                <span class="sub-text">
                                    Order #<strong>{{ $order }}</strong> 
                                </span>
                            </div>
                            <div class="row col-12 mb-3">
                                @if (empty($user['first_name']))
                                <span class="sub-text">
                                    Dear {{ $user['first_name'] }} {{ $user['last_name'] }},
                                </span>
                                @endif
                            </div>
                            <div class="row col-12 mb-3">
                                <span class="sub-text">
                                    Your Order #{{ $order }}, placed on {{ date("F j, Y, g:i a") }} was cancelled via Paynamics.
                                </span>
                            </div>
                        @endif                        
                    </div>
                    <div class="row col-12 mb-3">
                        <h4> 
                            Below is your order details:
                        </h4>
                    </div>
                    <div class="row col-12 mb-3">
                        <div class="table-responsive">
                            <table class="table" style="width: 90%; font-size: 14px;">
                                <thead>
                                    <tr>
                                        <td>ITEM</td>
                                        <td>ITEM NAME</td>
                                        <td>PRICE</td>
                                        <td>QTY</td>
                                        <td>TOTAL AMOUNT</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orderDetails as $order)
                                    <tr>
                                        <td><img src="{{ $order->product_image }}" style="width: 50px;"></td>
                                        <td>{{ $order->product_name }}</td>
                                        <td>@if ($order->selling_price > 0 ) {{ $order->selling_price }} @else {{ $order->base_price }} @endif</td>
                                        <td>{{ $order->qty }}</td>
                                        <td>{{ $order->total_price }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row col-12 mb-1" style="position: relative; height: 20px; margin-bottom: 5px;">
                        <span style="position: absolute; right: 0; font-weight: bold;">
                            Discount Amount: P{{ $totals['discount_price'] }}
                        </span>
                    </div>
                    <div class="row col-12 mb-1" style="position: relative; height: 20px; margin-bottom: 5px;">
                        <span style="position: absolute; right: 0; font-weight: bold;">
                            Delivery Fee: P{{ $totals['delivery_fee'] }}
                        </span>
                    </div>
                    <div class="row col-12 mb-1" style="position: relative; height: 20px; margin-bottom: 5px;">
                        <span style="position: absolute; right: 0; font-weight: bold;">
                            Total Amount: P{{ $totals['total_price'] }}
                        </span>
                    </div>
                    <div class="row col-12 mb-4" style="position: relative; height: 20px; margin-bottom: 5px;">
                        <span style="position: absolute; right: 0; font-weight: bold;">
                            Promo Code: {{ $totals['promo_code'] }}
                        </span>
                    </div>
                    <div class="row col-12">
                        @if ($status['paymentStatus'] == 'success' || $status['paymentStatus'] == 'pending')
                            <!-- <p>For <strong>Cash On Delivery</strong> purchases, please prepare exact amount for our courier. </p> -->
                        @else
                        <p style="color: red; font-weight: bold;">You may contact your card issuer to ensure that you were not charged</p>                        
                        @endif
                        <p>
                            <strong>SECURITY REMINDER:</strong> <br/><br/>
                            Adobomall will never ask you to send cash or deposit
                            money to any personal bank account or remittance
                            centers. Do not give your bank account, credit card
                            details, or any personal information to anyone who claims
                            to be an Adobomall representative not using our official
                            channels of communication. We will only get in touch with
                            you when needed through the following channels:
                        </p>
                        <p>Facebook Messenger: m.me/adobomall<br/>
                        E-mail: <a href="mailto:inquiry@adobomall.com">inquiry@adobomall.com</a>
                        </p>
                        <br/>
                        <p>Please feel free to contact our Customer Support at
                        <a href="mailto:inquiry@adobomall.com">inquiry@adobomall.com</a> if you have any questions
                        regarding your order.</p>
                    </div>
                    <div class="row col-12"> 
                        <a href="{{url()->to('/')}}" type="button" class="btn btn-sm btn-danger custom-secondary" style="font-size: 15px; margin: auto; margin-top: 25px;background:#d1242c;">Continue Shopping</a>
                        <a href="{{url()->to('/orders')}}" type="button" class="btn btn-sm btn-primary custom-primary" style="font-size: 15px; margin: auto; margin-top: 25px;background:#0d75b2;">Check Order Status</a>
                    </div>

                </div>

            </div>
        </div>
    </body>
</html>
