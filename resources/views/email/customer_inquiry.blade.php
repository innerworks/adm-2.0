@component('mail::message')


Hello {{ config('app.name') }},

{{ $message }}

You can reach me through:

** Email: ** {{ $email }} <br>
** Phone: ** {{ $phone }}

Thanks, <br>
** {{ $name }} **

@endcomponent
