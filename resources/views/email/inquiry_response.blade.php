@component('mail::message')
# Hi {{ $name }},

We have received your message and we will get back to you as soon as possible. Thank you.

Regards, <br>
Adobomall Team

@endcomponent
