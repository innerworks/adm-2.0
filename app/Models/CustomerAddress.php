<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerAddress extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 
        'first_name',
        'middle_name',
        'last_name',
        'contact_name', 
        'contact_number', 
        'country_id', 
        'region_id', 
        'province_id', 
        'city_id', 
        'barangay_id', 
        'zip_code', 
        'detailed_address', 
        'default',
        'type',
        'default_shipping',
        'default_billing'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'customer_addresses';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'name',
        'email',
        'mobile_number',
        'country_name',
        'region_name',
        'province_name',
        'city_name',
        'barangay_name',
    ];

    public function getCustomerDetails()
    {
        return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
    }

    public function getCountry()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function getRegion()
    {
        return $this->hasOne('App\Models\Region', 'region_id', 'region_id');
    }

    public function getProvince()
    {
        return $this->hasOne('App\Models\Province', 'province_id', 'province_id');
    }

    public function getCity()
    {
        return $this->hasOne('App\Models\City', 'city_id', 'city_id');
    }   

    public function getBarangay()
    {
        return $this->hasOne('App\Models\Barangay', 'id', 'barangay_id');
    } 

    public function getCountryNameAttribute() 
    {
        return $this->getCountry()->pluck('country_name')->first();
    }

    public function getRegionNameAttribute() 
    {
        return $this->getRegion()->pluck('name')->first();
    }    

    public function getProvinceNameAttribute() 
    {
        return $this->getProvince()->pluck('province_name')->first();
    } 

    public function getCityNameAttribute() 
    {
        return $this->getCity()->pluck('city_municipality_name')->first();
    }     

    public function getBarangayNameAttribute() 
    {
        return $this->getBarangay()->pluck('barangay_name')->first();
    }     

    public function getNameAttribute() 
    {
        return $this->contact_name;
    }

    public function getMobileNumberAttribute() 
    {
        return $this->contact_number;
    }        

    public function getEmailAttribute() 
    {
        return $this->getCustomerDetails()->pluck('email')->first();
    }        

}
