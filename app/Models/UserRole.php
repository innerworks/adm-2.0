<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
        'role_id'
    ];

    /**
     * set timestamps to false
     *
     * @var boolean
    */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'user_roles';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    public static function assignUserRoles($roles)
    {
        $user_id = $roles["user_id"];

        if(is_array($roles["roles"]))

            static::where('user_id',$roles["user_id"])->delete();
            foreach ($roles["roles"] as $role) {

                $roleId = ($role['role_id']) ? $role['role_id'] : $role;
                $userRoles = array('user_id' => $user_id, 'role_id' => $roleId);
                $rules = static::updateOrCreate($userRoles);

            }

            return true;

        return false;
    }
}
