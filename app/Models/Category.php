<?php

namespace App\Models;

use Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\MerchantMeta;

class Category extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 
        'name', 
        'slug_name', 
        'sort_order', 
        'image', 
        'filename', 
        'merchant_id',
        'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'categories';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'parent_name',
        'store_name',
        'product_counts',        
    ];  

    public function getParentCategory(){
        return $this->belongsTo(Category::class, 'parent_id');
    }    

    public function getMerchantName(){
        return MerchantMeta::where('merchant_id', '=', $this->merchant_id)
                ->where('meta_key', '=', 'store_name');
    }    

    public function upload($file){
        $ext = $file->getClientOriginalExtension();
        $filename = uniqid().'.'.$ext;
        $file->storeAs('public/category/', $filename);
        Storage::delete('public/category/{$this->filename}');
        $this->image = asset('storage/category/'. $filename);
        $this->filename = $filename;
        $this->save();
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getParentNameAttribute() 
    {
        return $this->getParentCategory()->pluck('name')->first();
    }

    public function getStoreNameAttribute() 
    {
        return $this->getMerchantName()->pluck('meta_value')->first();
    }

    public function getProductCountsAttribute() 
    {
        return Product::where('category_id', $this->id)->get()->count();
    }
}
