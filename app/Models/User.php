<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\Permission;
use App\Models\UserRole;
use App\Models\UserMeta;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'salt', 
        'active', 
        'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token', 
        'activation_token', 
        'salt'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'users';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Set Date format.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'user_details',
        'roles',
        'permissions'
    ];

    public static function getSalt($email)
    {
        $user = static::where('email', '=', $email)->first();
        return $user->salt;
    }

    public function getUserDetails()
    {
        return $this->hasMany('App\Models\UserMeta');
    }

    public function getUserRoles()
    {   
        return $this->hasMany('App\Models\UserRole', 'user_id');
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getUserDetailsAttribute() 
    {
        return $this->getUserDetails()->pluck('meta_value','meta_key')->toArray();
    }

    public function getRolesAttribute() 
    {
        $roles = $this->getUserRoles()->pluck('role_id')->toArray();
        return Role::whereIn('id',$roles)->get();
    }

    public function getPermissionsAttribute() 
    {
        $roles = $this->getUserRoles()->pluck('role_id')->toArray();
        return RolePermission::whereIn('role_id',$roles)->get();
    }

    public function assignRole($roleId)
    {
        $roles = UserRole::find($this->id);
        if($roles)
            $roles->delete();

        $userRoles = array('user_id' => $this->id, 'role_id' => $roleId);
        if(UserRole::updateOrCreate($userRoles))
            return true;
        return false;
    }

    public function addMeta($metaData)
    {
        foreach ($metaData as $key => $detail) {
            $where = [
                ['user_id', $this->id],
                ['meta_key', $key]        
            ];            
            $user_meta = UserMeta::where($where)->first();

            if(!$user_meta)
                $user_meta = new UserMeta;
                $user_meta->user_id = $this->id;
                $user_meta->meta_key = $key;
                $user_meta->meta_value = $detail;
                $user_meta->save();

            $user_meta->meta_key = $key;
            $user_meta->meta_value = $detail;
            $user_meta->save();
        }
    }
}
