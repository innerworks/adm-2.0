<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\Merchant;
use App\Models\Question;

class CustomerRequest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 
        'order_id', 
        'order_detail_id', 
        'product_id', 
        'merchant_id', 
        'question_id', 
        'type_id', 
        'address_id', 
        'details', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'customer_request';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'product_details',
        'merchant_details',
        'question',
    ];

    public function getProductDetailsAttribute()
    {
        $product = Product::find($this->product_id, ['name','description','primary_photo']);
        $product->makeHidden(['main_category_id', 'main_category_name', 'details', 'sizes', 'variants', 'addons', 'ratings', 'delivery_charge', 'wishlist']);

        return $product;
    }    

    public function getMerchantDetailsAttribute()
    {
        $merchant =  Merchant::find($this->merchant_id);
        $merchant->makeHidden(['details','settings','login_attempt','is_blocked','category_id','status','created_at']);
        return $merchant;
    }

    public function getQuestionAttribute()
    {
        return Question::find($this->question_id);
    }   

}
