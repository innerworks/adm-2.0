<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCountrySize extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'product_country_sizes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['country_size'];

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}
