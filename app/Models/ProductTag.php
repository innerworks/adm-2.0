<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 
        'tag_id'
    ];

    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'product_tags';
}
