<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Services\Helpers\MrSpeedyHelper;
// use App\Models\ProductVariant;
// use App\Models\ProductAddOn;
// use App\Models\Inventory;
// use App\Models\ProductTag;
// use App\Models\ProductRating;
// use App\Models\ProductMeta;
// use App\Models\CustomerAddress;
use App\Models\Brand;
use App\Models\Category;
use App\Models\TagSearch;
use App\Models\Merchant;
use App\Models\MerchantMeta;

class ElasticProduct extends Model
{
    use SoftDeletes;

     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku', 
        'name', 
        'status', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'sku',
        'description',
        'permalink', 
        'merchant_id', 
        'category_id', 
        'brand_id', 
        'banner_photo',
        'base_price', 
        'selling_price', 
        'sale_date_start', 
        'sale_date_end', 
        'sizes', 
        'weight', 
        'length', 
        'width', 
        'height', 
        'other_information', 
        'track_inventories', 
        'top_picks', 
        'primary_photo',
        'created_at',
        'updated_at',
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'products';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additional info to the return data
     *
     * @var string
     */
    public $appends = [
        'brand',
        'category',
        // 'category_slug',
        'subcategory',
        'merchant',
        'tags'
    ];

    public function getBrandAttribute()
    {
        return Brand::where('id', $this->brand_id)->value('name');
    }

    public function getCategoryAttribute()
    {
        return Category::where('id', $this->category_id)->value('name');
    }

    public function getSubCategoryAttribute()
    {
        return Category::where('parent_id', $this->category_id)->value('name');
    }

    public function getMerchantAttribute()
    {
        return Merchant::where('merchants.id', $this->merchant_id)
               ->where('merchant_meta.meta_key', 'store_name')
               ->join('merchant_meta', 'merchants.id', '=', 'merchant_meta.merchant_id')
               ->value('merchant_meta.meta_value');
    }

    public function getTagsAttribute()
    {
        return TagSearch::where('product_id', $this->id)->value('search_tag');
    }

}