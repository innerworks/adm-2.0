<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 
        'module_id', 
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 
        'updated_at', 
        'deleted_at'
    ];    

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'role_permissions';

    public static function assignRolePermission($role_id, $module_id, $status)
    {
        $asignRole = array("role_id" => $role_id, "module_id" => $module_id, "status" => $status);
        return static::updateOrCreate($asignRole);
    }
}
