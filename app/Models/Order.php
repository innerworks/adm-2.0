<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\OrderDetail;
use App\Models\Customer;
use App\Models\PaymentResponse;

class Order extends Model
{
    use SoftDeletes;

     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_number', 
        'customer_id', 
        'payment_type_id', 
        'voucher_discount_price', 
        'points_generated', 
        'points_consumed', 
        'checkout', 
        'status_id', 
        'order_date', 
        'paid_date', 
        'pending_payment_date', 
        'failed_date',
        'is_process'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'orders';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'order_details',
        'customer_detail',
        'payment_type_detail',
        'order_status_detail',
        'payment_response',
        'customer_name',
        'total_amount'
    ];   

    private static $initial_order_number = '000000000000001';

    public static function generateOrderNumber(){        
        $latest = self::orderBy('id', 'desc')->first();
        return $latest ? str_pad($latest->order_number + 1, 15, 0, STR_PAD_LEFT) : self::$initial_order_number;
    }

    public function user(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function orderDetail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function getOrderDetailsAttribute()
    {
        return $this->orderDetail()->get();
    }

    public function getCustomerDetailAttribute()
    {
        return Customer::find($this->customer_id);
    }

    public function getPaymentTypeDetailAttribute()
    {
        return PaymentType::find($this->payment_type_id);
    }

    public function getOrderStatusDetailAttribute()
    {
        return OrderStatus::find($this->status_id);
    }

    public function getPaymentResponseAttribute()
    {
        return PaymentResponse::where('order_id',$this->id)->first();
    }

    public function getCustomerNameAttribute()
    {
        $customer = Customer::find($this->customer_id);
        return $customer->first_name .' '.$customer->last_name;
    }

    public function getTotalAmountAttribute()
    {
        return OrderDetail::where('order_id', $this->id)->sum('total_price');
    }

}
