<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerSearch extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 
        'search', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'created_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'customer_search';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function addSearch($search='', $userId=0)
    {
    	$searchResults = static::where('customer_id', $userId)->get();
    	if (count($searchResults) >= 4) 
    		static::where('customer_id', $userId)->orderBy('id', 'asc')->limit(1)->delete();

        $userSearch = array('customer_id' => $userId, 'search' => $search);
    	if($search && $userId)
            return static::updateOrCreate($userSearch);
    }
}
