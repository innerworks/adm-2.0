<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageType extends Model
{
    //

    protected $table = "package_types";

    protected $fillable = [
        'courier_id',
        'package',
        'min_weight',
        'max_weight',
        'length',
        'width',
        'height',
    ];

    // public function getPackageRate($maxData, $shipAreaId){
    //     return PackageType::join('courier_rates','package_types.id', '=', 'courier_rates.package_id')
    //         ->where('courier_area_id', $shipAreaId)
    //         ->where('max_weight', '>=', $maxData)
    //         ->where('min_weight', '<=', $maxData)
    //         ->first();
    // }

    public function getPackageRate($maxData, $shipAreaId, $courier_id){
        return PackageType::join('courier_rates','package_types.id', '=', 'courier_rates.package_id')
            ->where('courier_area_id', $shipAreaId)
            ->where('max_weight', '>=', $maxData)
            ->where('min_weight', '<=', $maxData)
            ->where('courier_rates.courier_id', $courier_id)
            ->first();
    }

    public function getMaxWeightByAreaId($shipAreaId){
        return PackageType::join('courier_rates','package_types.id', '=', 'courier_rates.package_id')
            ->where('courier_area_id', $shipAreaId)
            ->orderBy('max_weight', 'desc')
            ->first();
    }

    public function courierRate(){
        return $this->hasMany(CourierRate::class);
    }

    public function courierExcessRate(){
        return $this->hasMany(CourierExcessRate::class);
    }

   public function scopePackageCourierId($query, $type){
        return $query->where('courier_id', $type);
    }

}
