<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CustomerAddress;
use App\Models\Order;
use App\Models\Province;
use DB;

use Storage;

class Customer extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    protected $guard = 'customer';
    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'salt', 
        'active', 
        'activation_token', 
        'verified_at', 
        'first_name', 
        'middle_name', 
        'last_name', 
        'suffix', 
        'birth_date', 
        'gender',
        'phone_number', 
        'mobile_number', 
        'provider', 
        'provider_id', 
        'image_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        // 'activation_token', 
        'salt'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'customers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    // public $appends = [
    //     'addresses',
    // ];

    public static function getSalt($email)
    {
        $user = static::where('email', '=', $email)->first();
        return $user->salt;
    }

    public function profilePic($file)
    {
        if($file) {
            $ext = $file->getClientOriginalExtension();
            $filename = uniqid().'.'.$ext;
            $file->storeAs('public/customers/', $filename);
            Storage::delete('public/customers/{$this->image_url}');
            return asset('storage/customers/'. $filename);
        }

        return null;
    }

    public function saveAddress($request)
    {  

        $customerAddress = CustomerAddress::find($request->id);
        $province = Province::where('province_id', $request->province_id)->first();

        $address_count = count(CustomerAddress::where([
            'contact_name'      => $request->contact_name,
            'contact_number'    => $request->contact_number,
            'province_id'       => $request->province_id,
            'city_id'           => $request->city_id,
            'barangay_id'       => $request->barangay_id,
            'zip_code'          => $request->zip_code,
            'detailed_address'  => $request->detailed_address
        ])->get());

        if (!$customerAddress) {
            $customerAddress = new CustomerAddress;
            $customerAddress->customer_id = $this->id;
            $customerAddress->first_name = $request->first_name;
            $customerAddress->middle_name = $request->middle_name == null ? "" :  $request->middle_name;
            $customerAddress->last_name = $request->last_name;
            $customerAddress->contact_name = $request->contact_name;
            $customerAddress->contact_number = $request->contact_number;
            $customerAddress->country_id = $request->country_id;
            $customerAddress->region_id = $province->region_id;
            $customerAddress->province_id = $request->province_id;
            $customerAddress->city_id = $request->city_id;
            $customerAddress->barangay_id = $request->barangay_id;
            $customerAddress->zip_code = $request->zip_code;
            $customerAddress->detailed_address = $request->detailed_address;
            $customerAddress->default = 0;
            $customerAddress->type = 0;
            $customerAddress->default_shipping = 0;
            $customerAddress->default_billing = 0;
            $customerAddress->save();
        } else {
            $customerAddress->customer_id = $this->id;
            $customerAddress->first_name = $request->first_name;
            $customerAddress->middle_name = $request->middle_name == null ? "" :  $request->middle_name;
            $customerAddress->last_name = $request->last_name;
            $customerAddress->contact_name = $request->contact_name;
            $customerAddress->contact_number = $request->contact_number;
            $customerAddress->country_id = $request->country_id;
            $customerAddress->region_id = $province->region_id;
            $customerAddress->province_id = $request->province_id;
            $customerAddress->city_id = $request->city_id;
            $customerAddress->barangay_id = $request->barangay_id;
            $customerAddress->zip_code = $request->zip_code;
            $customerAddress->detailed_address = $request->detailed_address;
            $customerAddress->default = 0;
            $customerAddress->type = 0;

            if ($request->type == 1) {
                $customerAddress->default_shipping = 1;

                $removed_default_shipping = customerAddress::where('id', '!=', $request->id)
                                            ->where('customer_id', auth()->user()->id)
                                            ->update([
                                                'default_shipping' => 0
                                            ]);
            }

            if ($request->type == 2) {
                $customerAddress->default_billing = 1;

                $removed_default_billing = customerAddress::where('id', '!=', $request->id)
                                            ->where('customer_id', auth()->user()->id)
                                            ->update([
                                                'default_billing' => 0
                                            ]);
            }

            $customerAddress->save();
        }

        return $customerAddress;

    }

    public function carts()
    {
        return $this->hasMany('App\Models\Cart');
    }

    public function getAddresses()
    {
        // $addresses =  $this->hasMany('App\Models\CustomerAddress')->distinct()->get();
        $addresses =  $this->hasMany('App\Models\CustomerAddress')->get();
        return $addresses;
    }

    public function getDefaultShipping()
    {
        $addresses =  $this->hasMany('App\Models\CustomerAddress')
        ->selectRaw('*')
        ->where('default_shipping', 1)
        ->orderByDesc('default_shipping')
        ->first();

        return $addresses;
    }

    public function getDefaultBilling()
    {
        $addresses =  $this->hasMany('App\Models\CustomerAddress')
        ->selectRaw('*')
        ->where('default_billing', 1)
        ->orderByDesc('default_billing')
        ->first();

        return $addresses;
    }

    public function getAddressById($request)
    {   
        $address = CustomerAddress::where('id','=',$request)
                    ->orderByDesc('default')
                    ->first();
        return $address;
    }

    public function order()
    {
        return $this->hasMany('App\Models\Order', 'customer_id');
    }

    // public function getAddressesAttribute()
    // {
    //     return $this->hasMany('App\Models\CustomerAddress')->get();
    // }

}
