<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Services\Helpers\MrSpeedyHelper;

class Product extends Model
{
    use SoftDeletes;

     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku', 
        'name', 
        'description', 
        'permalink', 
        'status', 
        'merchant_id', 
        'category_id', 
        'brand_id', 
        'base_price', 
        'selling_price', 
        'sale_date_start', 
        'sale_date_end', 
        'sizes', 
        'weight', 
        'length', 
        'width', 
        'height', 
        'other_information', 
        'track_inventories', 
        'top_picks', 
        'primary_photo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'products';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'main_category_id',
        'main_category_name',
        'details',
        'sizes',
        'variants',
        'addons',
        'ratings',
        'delivery_charge',
        'wishlist',
        'average_rating',
        'count_rating',
        'store_name',
        'sizes_csv',
        'tags',
        'store_slug_name',
        'inventories'
    ];    

    public $customerId;
    public $category;

    public function setCustomer($customerId)
    {
        $this->customerId = $customerId;
    }

    public function addVariant($variants)
    {
        foreach ($variants as $key => $value) {
            if($value['name'] != null || $value['name'] != '') 
            {
                $where = [
                    ['product_id', $this->id],
                    ['group_name', $value['group']],
                    ['variant', $value['name']],                        
                ];            
                $productVariant = ProductVariant::where($where)->first();

                if(!$productVariant)
                    $productVariant = new ProductVariant;
                    $productVariant->product_id = $this->id;
                    $productVariant->group_name = $value['group'];
                    $productVariant->variant = $value['name'];
                    $productVariant->price = $value['price'];
                    if($value['gallery'])
                        $productVariant->image_gallery = $value['gallery'];
                    $productVariant->save();

                $productVariant->product_id = $this->id;
                $productVariant->group_name = $value['group'];
                $productVariant->variant = $value['name'];
                $productVariant->price = $value['price'];
                if($value['gallery'])
                    $productVariant->image_gallery = $value['gallery'];
                $productVariant->save();
            }
        }
    }

    public function addAddOns($addOns)
    {
        foreach ($addOns as $key => $value) {
            if($value['name'] != null || $value['name'] != '') {
                $where = [
                    ['product_id', $this->id],
                    ['group_name', $value['group']],
                    ['addon', $value['name']],                        
                ];            
                $productAddOn = ProductAddOn::where($where)->first();

                if(!$productAddOn)
                    $productAddOn = new ProductAddOn;
                    $productAddOn->product_id = $this->id;
                    $productAddOn->group_name = $value['group'];
                    $productAddOn->addon = $value['name'];
                    $productAddOn->price = $value['price'];
                    $productAddOn->save();

                $productAddOn->product_id = $this->id;
                $productAddOn->group_name = $value['group'];
                $productAddOn->addon = $value['name'];
                $productAddOn->price = $value['price'];
                $productAddOn->save();
            } // end if
        }// end foreach
    }

    public function addTag($tags)
    {
        foreach ($tags as $key => $value) {
            if($value['id'] != null) {
                $where = [
                    ['product_id', $this->id],
                    ['tag_id', $value['id']]                       
                ];            
                $productTag = ProductTag::where($where)->first();

                if(!$productTag)
                    $productTag = new ProductTag;
                    $productTag->product_id = $this->id;
                    $productTag->tag_id = $value['id'];
                    $productTag->save();

                $productTag->product_id = $this->id;
                $productTag->tag_id = $value['id'];
                $productTag->save();
            }
        }
    }

    public function addMeta($metaData)
    {
        foreach ($metaData as $key => $detail) {
            $where = [
                ['product_id', $this->id],
                ['meta_key', $key]        
            ];            
            $productMeta = ProductMeta::where($where)->first();

            if(!$productMeta)
                $productMeta = new ProductMeta;
                $productMeta->product_id = $this->id;
                $productMeta->meta_key = $key;
                $productMeta->meta_value = $detail;
                $productMeta->save();

            $productMeta->meta_key = $key;
            $productMeta->meta_value = $detail;
            $productMeta->save();
        }
    }

    public function addInvetories($inventory)
    {
        $variants = ProductVariant::where('product_id', '=', $this->id)->get();
        foreach ($inventory as $key => $value) {
            if($value['variant'] != null) {
                $variant = $variants->where('variant', '=', trim($value['variant']))
                                    ->where('product_id', $this->id)
                                    ->first();

                $where = [
                    ['product_id', $this->id],
                    ['variant_id', $variant->id],
                    ['size_id', $value['product_size_id']]                       
                ];            

                $inventory = Inventory::where($where)->first();

                if(!$inventory)
                    $inventory = new Inventory;
                    $inventory->product_id = $this->id;
                    $inventory->variant_id = $variant->id;
                    $inventory->size_id = $value['product_size_id'];
                    $inventory->stock = $value['qty'];
                    $inventory->save();

                $inventory->product_id = $this->id;
                $inventory->variant_id = $variant->id;
                $inventory->size_id = $value['product_size_id'];
                $inventory->stock = $value['qty'];
                $inventory->save();

            }
            else {
                $where = [
                    ['product_id', $this->id],
                    ['size_id', $value['product_size_id']]                       
                ];

                $inventory = Inventory::where($where)->first();

                if(!$inventory)
                    $inventory = new Inventory;
                    $inventory->product_id = $this->id;
                    $inventory->size_id = $value['product_size_id'];
                    $inventory->stock = $value['qty'];
                    $inventory->save();

                $inventory->product_id = $this->id;
                $inventory->size_id = $value['product_size_id'];
                $inventory->stock = $value['qty'];
                $inventory->save();      

            }
        }
    }

    public function getDetails()
    {
        return $this->hasMany('App\Models\ProductMeta');
    }

    public function getDetailsApi()
    {
        return $this->hasMany('App\Models\ProductMetaApi');
    }

    public function getRatings()
    {
        return $this->hasMany('App\Models\ProductRating');
    }

    public function getVariants()
    {
        return $this->hasMany('App\Models\ProductVariant');
    }

    public function getAddOns()
    {
        return $this->hasMany('App\Models\ProductAddOn');
    }

    public function getMerchant()
    {
        return $this->hasMany('App\Models\MerchantMeta', 'merchant_id' , 'merchant_id');
    }

    public function getCategory()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    public function getInventories()
    {
        return $this->hasMany('App\Models\Inventory', 'product_id', 'id');
    }
    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getSizesAttribute() 
    {
        return $this->getDetailsApi()->where('meta_key', '=', 'sizes')
                                     ->pluck('meta_value')
                                     ->first();
    }

    public function getDetailsAttribute() 
    {
        return $this->getDetails()->pluck('meta_value','meta_key')->toArray();
    }

    public function getRatingsAttribute() 
    {
        return $this->getRatings()
        ->select('name', 'image_url', 'rating', 'review', 'product_rating.created_at as rating_date')
        ->join('customers', 'product_rating.customer_id', '=', 'customers.id')
        ->get();
    }

    public function getVariantsAttribute() 
    {
        $variants = $this->getVariants()->get();
        return $this->customGroup($variants);
    }

    public function getAddonsAttribute() 
    {
        $addons = $this->getAddOns()->get();
        return $this->customGroup($addons);
    }

    public function customGroup($params)
    {
        $newArray = array();

        foreach ($params as $value) {
            if(!array_key_exists($value->group_name, $newArray)) {
                 $newObject = new \stdClass;

                 $newObject->group_name = $value->group_name;
                 $newObject->items = array();
                 
                 $newArray[$value->group_name] = $newObject;
            }
            $itemObject = new \stdClass;

            $itemObject->id = $value->id;
            $itemObject->name = $value->variant ? $value->variant : $value->addon;
            $itemObject->price = $value->price;
            if($value->variant)
                $itemObject->image_gallery = $value->image_gallery;
            if($value->variant)
                $itemObject->inventory = $value->inventory;            

            $newArray[$value->group_name]->items[] = $itemObject;
        }

        return array_values($newArray);
    }

    public function getDeliveryChargeAttribute() 
    {
        $customerAddress = CustomerAddress::where('customer_id', '=', $this->customerId)
                                            ->where('default_shipping', 1)
                                            ->first();

        $merchantAddress = $this->getMerchant()->where('meta_key', '=', 'store_address')->pluck('meta_value')->first();

        if($customerAddress && $merchantAddress) {
            $mrSpeedy = new MrSpeedyHelper();
            $data = [
                'matter' => $this->name,
                'payment_method' => 'non_cash',
                'vehicle_type_id' => 8,
                'points' => [            
                    [ 
                        'address' => $merchantAddress, 
                        'taking_amount' => 0,
                        'buyout_amount' => 0, 
                    ], 
                    [ 
                        'address' => $customerAddress->detailed_address, 
                        'taking_amount' => 0, 
                        'buyout_amount' => 0, 
                    ], 
                ]
            ];

            $responce = $mrSpeedy->calculateOrder($data);       
            return $responce['order']['delivery_fee_amount'];
        }
        return null;
    } 

    public function getMainCategoryIdAttribute()
    {
        $category = $this->getCategory()->first();
        if($category)
            return $category->id;
        return null;
    }

    public function getMainCategoryNameAttribute()
    {
        if($this->category)           
            return $this->category->name;
        return null;
    }

    public function getWishlistAttribute()
    {
        $data = Wishlist::where('customer_id', $this->customerId)
                        ->where('product_id', $this->id)->first();
        if($data)
            return [
                'islike' => true,
                'id' => $data->id
            ];
        return [
            'islike' => false,
            'id' => 0
        ];
    }

    public function getAverageRatingAttribute()
    {
        return $this->getRatings()->average('rating');
    }

    public function getCountRatingAttribute()
    {
        return $this->getRatings()->count('rating');
    }

    public function getStoreNameAttribute()
    {
        $merchant_meta = MerchantMeta::where('merchant_id', $this->merchant_id)
        ->where('meta_key', 'store_name')->pluck('meta_value')->first();
        if($merchant_meta) {
            return $merchant_meta;
        }

        return null;
    }

    public function getStoreSlugNameAttribute()
    {
        $merchant_meta = MerchantMeta::where('merchant_id', $this->merchant_id)
        ->where('meta_key', 'store_name')->pluck('meta_value')->first();
        if($merchant_meta) {
            return str_replace(" ","-", strtolower($merchant_meta));
        }

        return null;
    }

    public function getSizesCsvAttribute()
    {
        $product_sizes = ProductMeta::where('product_id', $this->id)
        ->where('meta_key', 'sizes')->pluck('meta_value');
        if(count($product_sizes) > 0) {
            $sizes = json_decode($product_sizes[0], true);
            $size_list = [];
            foreach($sizes as $size) {
                $size_list[] = $size['size'];
            }
            return implode (", ", $size_list);
        }
        return null;
    }

    public function getTagsAttribute()
    {
        $product_tags = ProductTag::where('product_id', $this->id)->get()->toArray();
        if(count($product_tags) > 0) {
            $tags = Tag::whereIn('id', $product_tags)->pluck('tag_name')->toArray();
            return implode (", ", $tags);
        }

        return null;
    }

    public function getInventoriesAttribute()
    {
        return $this->getInventories()->get();
    }

}
