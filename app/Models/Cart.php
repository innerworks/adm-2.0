<?php

namespace App\Models;

use App\Repositories\ShippingComputation\AccessControl\ComputationVariable;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 
        'product_id', 
        'category_id', 
        'brand_id', 
        'size_id', 
        'variant_ids', 
        'addons_ids', 
        'qty', 
        'total_price', 
        'shipping_fee', 
        'checked',
        'special_instructions',
        'courier_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'carts';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'variant_ids' => 'array',
        'addons_ids' => 'array',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'product_name',
        'permalink',
        'product_image',
        'product_base_price',
        'product_selling_price',
        'merchant_name',
        'category',
        'brand',
        'size',
        'variants',
        'addOns',
        'product_details',
    ];

    public static function customGroup($carts)
    {
        $newCart = array();
        $totalShippingFee = 0;
        $totalPrice = 0;
        $totalQty = 0;
        $totalItems = 0;

        foreach ($carts as $cart) {
            if(!array_key_exists($cart->merchant_id, $newCart)) {
                 $newObject = new \stdClass;

                 $newObject->merchant_id = $cart->merchant_id;
                 $newObject->merchant_name = $cart->merchant_name;

                 $allcarts = $carts->where('merchant_id', $cart->merchant_id);
                 $checkedCart = $allcarts->where('checked', true);

                 $checked = false;
                 if($allcarts == $checkedCart)
                     $checked = true;

                 $newObject->merchant_check = $checked;
                 $newObject->items = array();
                 
                 $params = [
                    'merchant_id' => $cart->merchant_id,
                    'customer_id' => $cart->customer_id
                 ];

                 $newObject->meta = self::getTotals($carts, $params);
                 $newCart[$cart->merchant_id] = $newObject;
            }

            $subTotal = Cart::where('checked', 1)->sum('total_price');
            $subTotal = number_format($subTotal, 2, ".", ",");

            $dataObject = new \stdClass;

            $dataObject->id = $cart->id;
            $dataObject->customer_id = $cart->customer_id;
            $dataObject->qty = $cart->qty;
            $dataObject->total_price = $cart->total_price;
            $dataObject->shipping_fee = $cart->shipping_fee;
            $dataObject->product_id = $cart->product_id;
            $dataObject->product_name = $cart->product_name;
            $dataObject->permalink = $cart->permalink;
            $dataObject->product_image = $cart->product_image;
            $dataObject->product_base_price = $cart->product_base_price;
            $dataObject->product_selling_price = $cart->product_selling_price;
            $dataObject->merchant_id = $cart->merchant_id;
            $dataObject->category_id = $cart->category_id;
            $dataObject->category = $cart->category;
            $dataObject->brand_id = $cart->brand_id;
            $dataObject->brand = $cart->brand;
            $dataObject->checked = $cart->checked;
            $dataObject->size_id = $cart->size_id;
            $dataObject->size = $cart->size;
            $dataObject->variant_ids = $cart->variant_ids;
            $dataObject->variants = $cart->variants;
            $dataObject->addons_ids = $cart->addons_ids;
            $dataObject->addOns = $cart->addOns;
            $dataObject->special_instructions = $cart->special_instructions;
            $dataObject->subTotal = $subTotal;
            $dataObject->courier_id = $cart->courier_id;

            $newCart[$cart->merchant_id]->items[] = $dataObject;
        }
        return array_values($newCart);
    }

    public static function getTotals($carts, $params = array())
    {
        $totalShippingFee = 0;
        $totalPrice = 0;
        $totalQty = 0;
        $totalItems = 0;

        if(count($params) > 0) {
            $carts = $carts->where('merchant_id', '=', $params['merchant_id'])->where('checked', true);
            $totalShippingFee = self::getDeliveryCharge($carts);

            $totalItems = count($carts);

            foreach ($carts as $cart) {
                if($cart->checked) {
                    $totalPrice += $cart->total_price;
                    $totalQty += $cart->qty;

                    foreach ($cart->variants as $variant) {
                        $totalPrice += $variant->price;
                    }

                    foreach ($cart->addOns as $addOn) {
                        $totalPrice += $addOn->price;
                    }
                }
            }
        }
        else {
            foreach ($carts as $cart) {
                $totalShippingFee += str_replace(",","",$cart->meta['total_shipping_fee']);
                $totalPrice += str_replace(",","",$cart->meta['total_price']);
                $totalQty += $cart->meta['total_qty'];
                $totalItems += $cart->meta['total_items'];
            }
        }

        return $data = [
            'grand_total' => number_format(($totalPrice+$totalShippingFee), 2, '.', ''),
            'total_shipping_fee' => number_format($totalShippingFee, 2, '.', ''),
            'total_price' => number_format($totalPrice, 2, '.', ''),
            'total_qty' => $totalQty,
            'total_items' => $totalItems,
        ];
    }

    public static function getTotalPerProduct($carts, $params = array())
    {
        $totalShippingFee = 0;
        $totalPrice = 0;
        $totalQty = 0;
        $totalItems = 0;

        if(count($params) > 0) {

            $carts = $carts->where('merchant_id', '=', $params['merchant_id'])->where('checked', true);
            $totalShippingFee = self::getDeliveryCharge($carts);
            $totalItems = count($carts);

            foreach ($carts as $cart) {
                if($cart->checked) {
                    $totalPrice += $cart->total_price;
                    $totalQty += $cart->qty;

                    foreach ($cart->variants as $variant) {
                        $totalPrice += $variant->price;
                    }

                    foreach ($cart->addOns as $addOn) {
                        $totalPrice += $addOn->price;
                    }
                }
            }
        }
        else {
            $totalShippingFee += str_replace(",","",$carts[0]->meta['total_shipping_fee']);
            $totalPrice += str_replace(",","",$carts[0]->meta['total_price']);
            $totalQty += $carts[0]->meta['total_qty'];
            $totalItems += $carts[0]->meta['total_items'];
        }

        return $data = [
            'grand_total' => number_format(($totalPrice+$totalShippingFee), 2, '.', ''),
            'total_shipping_fee' => number_format($totalShippingFee, 2, '.', ''),
            'total_price' => number_format($totalPrice, 2, '.', ''),
            'total_qty' => $totalQty,
            'total_items' => $totalItems,
        ];
    }

    // public static function findKeyId($courier, $base){

    //     $arrayArea          =   CourierArea::where('courier_id', $courier->id)->get();

    //     $pregBaseStr        =   preg_replace("/[^a-zA-Z]/", " ", $base);
    //     $explode            =   explode(" ", $pregBaseStr);
    //     $filterArr          =   array_values(array_filter($explode));

    //     $checkArr = [];
    //     foreach ($arrayArea as $val){
    //         $checkArr[$val->id] = $val['state'];
    //     }

    //     $keys = array_keys(array_intersect($checkArr,$filterArr));
    //     $dataCourierAreaId = "";
    //     foreach ($keys as $key) {
    //         $dataCourierAreaId = intval($key);
    //     }

    //     return $dataCourierAreaId == "" ? "empty" : $dataCourierAreaId;
    // }

    public static function findKeyId($courier, $base){

        $arrayArea          =   Area::get();
        
        $pregBaseStr        =   preg_replace("/[^a-zA-Z]/", " ", $base);
        $explode            =   explode(" ", $pregBaseStr);
        $filterArr          =   array_values(array_filter($explode));

        $checkArr = [];
        foreach ($arrayArea as $val){
            $checkArr[$val->id] = $val['area'];
        }

        $keys = array_keys(array_intersect($checkArr,$filterArr));
        $dataCourierAreaId = "";
        foreach ($keys as $key) {
            $dataCourierAreaId = intval($key);
        }

        return $dataCourierAreaId == "" ? "empty" : $dataCourierAreaId;
    }

    public static function findShippingArea($courier)
    {
        $user               = auth()->user();
        $user_address       = CustomerAddress::where('customer_id', $user->id)->where('default_shipping', 1)->first();
        $regionBase         = Region::find($user_address->region_id);
        $areaBase           = Area::find($regionBase->area_id);

        $isKey = self::findKeyId($courier, $regionBase->name);

        if($isKey != "empty"){//find in region
            $dataCourierAreaId = $isKey;

        }else{//find area id
            $isKey = self::findKeyId($courier, $areaBase->area);

            $dataCourierAreaId = $isKey;
        }

        return $dataCourierAreaId;

    }

    public static function normalRate($item, $getPackageType){

        $valFeeCharge = ($item['price_result']) * $item['valuation_fee'];
        if ($item['weight'] > 3) {
            $excess = CourierExcessRate::where('courier_area_id', $getPackageType->courier_area_id)->first();
            $excess_item_weight = $item['weight'] - 3;
            $excess_charge = $excess->computation_rate * (abs($excess_item_weight));
            $finalShipFee = $getPackageType->delivery_rate + $excess_charge + $valFeeCharge;
        } else {
            $finalShipFee = $getPackageType->delivery_rate + $valFeeCharge;
        }

        Cart::where('customer_id', auth()->user()->id)
        ->where('merchant_id',$item['merchant_id'])
        ->update([
            'package_id'    => $getPackageType->package_id,
            'shipping_fee'  => $finalShipFee,
        ]);
    }

    public static function ownPackage($item, $getExcessRate, $packageW, $getPackageType){

        $valFeeCharge = $item['price_result'] * $item['quantity'] * $item['valuation_fee'];

        $computation = new ComputationVariable;

        $computation->setOwnPackaging(
            $packageW,
            $getExcessRate->weight,
            $getExcessRate->computation_rate,
            $getExcessRate->amount
        );

        $excessPackRate = $computation->getOwnPackaging();
        $finalShipFee = $excessPackRate + $valFeeCharge;

        Cart::where('customer_id', auth()->user()->id)
        ->where('merchant_id',$item['merchant_id'])
        ->update([
            'package_id'    => $getPackageType->package_id,
            'shipping_fee'  => number_format($finalShipFee, 2, ".", ","),
        ]);
    }

    public static function getDeliveryCharge($carts)
    {
        $shipping_fee = 0;
        $valuation_fee = 0;
        $merchant_id = null;
        $courier_id = null;
        $merchant_shipping_fee = 0;
        
        foreach ($carts as $cart) {
            $merchant_id = $cart->merchant_id;     
            $courier_id = $cart->courier_id;
        }

        // GET COURIER MERCHANT DEFAULT
        $courier = Courier::where('is_active', 1)
            ->where('is_default', 1)
            ->where('owner_id', $merchant_id)
            ->join('merchant_courier', 'couriers.id', '=', 'merchant_courier.courier_id')
            ->first();

        // GET COURIER CUSTOMER SELECTED
        if($courier_id) {
            $courier = Courier::find($courier_id);
        }

        // GET COURIER DEFAULT
        if(!$courier) {
            $courier = Courier::where('is_default', 1)->first();
        }

        $shipping_area_id = self::findShippingArea($courier);

        // CLEAR SHIPPING FEE
        if($shipping_area_id == NULL || $shipping_area_id == "empty"){ 
            foreach ($carts as $data){
                $cart = Cart::find($data->id);
                $cart->shipping_fee = NULL;
                $cart->save();
            }//end foreach
        }
        else {
            $bound_rate = DeliveryBoundary::find(1);
            $courier_area = CourierArea::find($shipping_area_id);
            $courier_excess_rate = CourierExcessRate::where('courier_id', $courier_area->courier_id)
                                                    ->where('courier_area_id', $courier_area->id)
                                                    ->first();

            $valuation_fee = $courier->valuation_fee;

            /* DOUBLE CHECK DIMENSIONS */
            $compressData = [];
            $sumL = 0;
            $sumW = 0;
            $sumH = 0;
            $sumQ = 0;
            $sumWeight = 0;
            $sumPrice = 0;
            foreach($carts as $value) {
                $compressData[$value->merchant_id] = [
                    'length'    => $sumL+=$value->product_details['length'],
                    'width'     => $sumW+=$value->product_details['width'],
                    'height'    => $sumH+=$value->product_details['height'],
                    'weight'    => $sumWeight+=$value->product_details['weight'] * $value->qty,
                    'quantity'  => $sumQ+=$value->qty,
                    'price_result' => $sumPrice+=$value->total_price * $value->qty,
                    'valuation_fee' => $valuation_fee,
                    'merchant_id'  => $value->merchant_id,
                ];
            }

            $computation = new ComputationVariable;
            $package_type = new PackageType;

            foreach ($compressData as $key => $item) {
                $computation->setCbm(
                    $item['length'],
                    $item['width'],
                    $item['height'],
                    $item['quantity']
                );

                $compare = [
                    'weight'    => number_format($item['weight'],2,".", ""),
                ];

                $rate = $package_type->getMaxWeightByAreaId($courier_area->id);
                $get_package_type = $package_type->getPackageRate(max($compare), $courier_area->id, $courier->id);

                if($rate->max_weight > max($compare) ){
                    self::normalRate($item, $get_package_type);
                } else{
                    self::ownPackage($item, $courier_excess_rate, max($compare), $get_package_type);
                }

                $comp = Cart::where('merchant_id', $item['merchant_id'])
                    ->where('customer_id', auth()->user()->id)
                    ->where('checked', 1)
                    ->groupBy('merchant_id')
                    ->first();

                $merchant_shipping_fee = $comp ? $comp->shipping_fee : null;
            }
            return $merchant_shipping_fee;

        }
        
    }

    // public static function getDeliveryCharge2($carts) 
    // {
    //     $merchant_shipping_fee = 0;
    //     $courier = Courier::where('is_default', 1)->first();
    //     $getFindShipId = self::findShippingArea($courier);

    //     if($getFindShipId == NULL || $getFindShipId == "empty"){ 
    //         foreach ($carts as $data){
    //             $cart = Cart::find($data->id);
    //             $cart->shipping_fee = NULL;
    //             $cart->save();
    //         }//end foreach
    //     }else {
    //         $boundRate          =   DeliveryBoundary::find(1);
    //         $getArea            =   CourierArea::find($getFindShipId);
    //         $getExcessRate      =   CourierExcessRate::where('courier_id', $courier->id)->first();

    //         // Valuation Fee Computation - E-Nortel
    //         if ($getArea->state == 'NCR') {
    //             $valuation_fee = "0.01";
    //         } else {
    //             $valuation_fee = "0.02";
    //         }

    //         /* DOUBLE CHECK DIMENSIONS */
    //         $compressData = [];
    //         $sumL = 0;
    //         $sumW = 0;
    //         $sumH = 0;
    //         $sumQ = 0;
    //         $sumWeight = 0;
    //         $sumPrice = 0;
    //         foreach($carts as $value) {
    //             $compressData[$value->merchant_id] = [
    //                 'length'    => $sumL+=$value->product_details['length'],
    //                 'width'     => $sumW+=$value->product_details['width'],
    //                 'height'    => $sumH+=$value->product_details['height'],
    //                 'weight'    => $sumWeight+=$value->product_details['weight'] * $value->qty,
    //                 'quantity'  => $sumQ+=$value->qty,
    //                 'price_result' => $sumPrice+=$value->total_price * $value->qty,
    //                 'valuation_fee' => $valuation_fee,
    //                 'merchant_id'  => $value->merchant_id,
    //             ];
    //         }

    //         $computation = new ComputationVariable;
    //         $package_type = new PackageType;

    //         foreach ($compressData as $key => $item) {
    //             $computation->setCbm(
    //                 $item['length'],
    //                 $item['width'],
    //                 $item['height'],
    //                 $item['quantity']
    //             );

    //             $compare = [
    //                 'weight'    => number_format($item['weight'],2,".", ""),
    //             ];

    //             $rate = $package_type->getMaxWeightByAreaId($getArea->id);
    //             $getPackageType = $package_type->getPackageRate(max($compare), $getArea->id);

    //             if($rate->max_weight > max($compare) ){
    //                 self::normalRate($item,$getPackageType);
    //             } else{
    //                 self::ownPackage($item,$getExcessRate, max($compare), $getPackageType);
    //             }

    //             $comp = Cart::where('merchant_id', $item['merchant_id'])
    //                 ->where('customer_id', auth()->user()->id)
    //                 ->where('checked', 1)
    //                 ->groupBy('merchant_id')
    //                 ->first();

    //             $merchant_shipping_fee = $comp ? $comp->shipping_fee : null;
    //         }

    //         return $merchant_shipping_fee;
    //     }

    // }

    // public static function getDeliveryCharge($merchantId, $customerId)
    // {
    //     $customerAddress = CustomerAddress::where('customer_id', '=', $customerId)
    //     ->where('default', true)
    //     ->first();

    //     $merchantAddress = MerchantMeta::where('merchant_id', '=', $merchantId)
    //     ->where('meta_key', '=', 'store_address')
    //     ->pluck('meta_value')
    //     ->first();

    //     if($customerAddress && $merchantAddress) {
    //         $mrSpeedy = new MrSpeedyHelper();
    //         $data = [
    //             'matter' => 'Sample Products',
    //             'payment_method' => 'non_cash',
    //             'vehicle_type_id' => 8,
    //             'points' => [            
    //                 [ 
    //                     'address' => $merchantAddress, 
    //                     'taking_amount' => 0,
    //                     'buyout_amount' => 0, 
    //                 ], 
    //                 [ 
    //                     'address' => $customerAddress->detailed_address, 
    //                     'taking_amount' => 0, 
    //                     'buyout_amount' => 0, 
    //                 ], 
    //             ]
    //         ];

    //         $responce = $mrSpeedy->calculateOrder($data);
    //         return $responce['order']['delivery_fee_amount'] + ($responce['order']['delivery_fee_amount']*0.10);
    //     }
    //     return null;
    // }

    public function getProductDetail()
    {
    	return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function getCategoryDetail()
    {
    	return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    public function getBrandDetail()
    {
    	return $this->hasOne('App\Models\Brand', 'id', 'brand_id');
    }

    public function getSizeDetail()
    {
    	return $this->hasOne('App\Models\ProductSize', 'id', 'size_id');
    }

    public function getMerchantDetail()
    {
        return $this->hasOne('App\Models\MerchantMeta', 'merchant_id', 'merchant_id');
    }

    public function getProductNameAttribute()
    {
    	return $this->getProductDetail()->pluck('name')->first();
    }

    public function getPermalinkAttribute()
    {
        return $this->getProductDetail()->pluck('permalink')->first();
    }

    public function getProductBasePriceAttribute()
    {
        return $this->getProductDetail()->pluck('base_price')->first();
    }    

    public function getProductSellingPriceAttribute()
    {
        return $this->getProductDetail()->pluck('selling_price')->first();
    }    

    public function getProductImageAttribute()
    {
    	return $this->getProductDetail()->pluck('primary_photo')->first();
    }

    public function getCategoryAttribute()
    {
    	return $this->getCategoryDetail()->pluck('name')->first();
    }

    public function getBrandAttribute()
    {
    	return $this->getBrandDetail()->pluck('name')->first();
    }

    public function getSizeAttribute()
    {
    	return $this->getSizeDetail()->pluck('size')->first();
    }

    public function getVariantsAttribute()
    {
        $product_variants = null;
    	if(is_array($this->variant_ids)) {
            $product_variants = ProductVariant::whereIn('id', $this->variant_ids)->get();            
        }
        else {
            $product_variants = ProductVariant::where('id', $this->variant_ids)->get();      
        }

        if($product_variants)
            return $product_variants;

        return null;
    }

    public function getAddOnsAttribute()
    {
        $product_addons = null;
    	if(is_array($this->addons_ids)) {
	    	$product_addons = ProductAddOn::whereIn('id', $this->addons_ids)->get();
        }
        else {
            $product_addons = ProductAddOn::where('id', $this->addons_ids)->get();
        }

        if($product_addons)
            return $product_addons;
        return null;
    }

    public function getMerchantNameAttribute()
    {
        return $this->getMerchantDetail()
                    ->where('meta_key', 'store_name')
                    ->pluck('meta_value')
                    ->first();
    }

    public function getProductDetailsAttribute()
    {
        return Product::find($this->product_id);
    }


}
