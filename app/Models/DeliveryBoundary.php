<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryBoundary extends Model
{
    //

    protected $table = "delivery_boundaries";
}
