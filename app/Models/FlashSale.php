<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\FlashSaleProduct;
use App\Models\Product;

class FlashSale extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 
        'short_description', 
        'start_date', 
        'end_date', 
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'flash_sales';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'products_ids'
    ];

    public function storeProducts($productIds)
    {
        $flashSale = FlashSaleProduct::where('flash_sale_id', $this->id);
        $flashSale->delete();

        foreach ($productIds as $productId) {
            $where = [
                ['flash_sale_id', $this->id],
                ['product_id', $productId]        
            ];            
            $saleProduct = FlashSaleProduct::where($where)->first();

            if(!$saleProduct)
                $saleProduct = new FlashSaleProduct;
                $saleProduct->flash_sale_id = $this->id;
                $saleProduct->product_id = $productId;
                $saleProduct->save();

            $saleProduct->flash_sale_id = $this->id;
            $saleProduct->product_id = $productId;
            $saleProduct->save();
        }   
    }

    public function getProductsIdsAttribute()
    {
        return FlashSaleProduct::where('flash_sale_id', $this->id)->get()->pluck('product_id');
    }

    public function getItemsAttribute()
    {
        $productIds = FlashSaleProduct::where('flash_sale_id', $this->id)->get()->pluck('product_id');
        $products = Product::whereIn('id', $productIds)->get();
        $products->makeHidden(['details']);
        return $products;
    }
}
