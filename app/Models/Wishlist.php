<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Wishlist;

class Wishlist extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'wishlist';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        // 'created_at'
    ]; 

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'product_details'
    ];  

    public function getProductDetailsAttribute()
    {
        $product = Product::find($this->product_id);
        return $product->makeHidden(['details']);
    }

}
