<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentResponse extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'request_id', 
        'response_id',
        'merchant_id',
        'process_date', 
        'response_code',
        'response_message',
        'response_advise', 
        'processor_response_id', 
        'processor_response_authcode', 
        'processor_response_code',
        'processor_response_mess', 
        'response_sign', 
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */    
    protected $table = 'payment_responses';
}
