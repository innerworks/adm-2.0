<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\RolePermission;

class Permission extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'module_id', 
        'create', 
        'read', 
        'update', 
        'delete'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'permissions';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';    

    public static function createUpdatePermission($permissions)
    {
        $role_id = $permissions["role_id"];
    
        if(is_array($permissions["permissions"]))
            RolePermission::where('role_id',$permissions["role_id"])->delete();
            
            foreach ($permissions["permissions"] as $permission) {
                $permission = static::updateOrCreate($permission);
                RolePermission::assignRolePermission($role_id,$permission->id);                  
            }
            return true;
        return false;
    }
}
