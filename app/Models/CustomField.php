<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomField extends Model
{
     use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cf_author', 
        'cf_parent', 
        'cf_title', 
        'cf_field_name', 
        'cf_params', 
        'cf_status', 
        'cf_order', 
        'cf_name', 
        'cf_type'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'custom_fields';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];
    
    protected $primaryKey = 'id';    
}
