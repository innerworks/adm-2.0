<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourierSpecialRate extends Model
{
	public $timestamps = false;

    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'special_name',
        'rate_discount',
        'owner_id',
        'owner_type'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = "courier_special";

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'store_name',
    ];

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getStoreNameAttribute()
    {
        $store_name = MerchantMeta::where('merchant_id', $this->owner_id)
        						  ->where('meta_key', 'store_name')
        						  ->first();
        if($store_name)
        	return $store_name->meta_value;

        return 'Admin';
    }
}
