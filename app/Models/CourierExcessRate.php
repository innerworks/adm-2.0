<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourierExcessRate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'courier_area_id',
        'package_id',
        'courier_id',
        'weight',
        'computation_rate',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = "courier_excess_rates";

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    public function courierArea(){
        return $this->belongsTo(CourierArea::class);
    }

    public function courier(){
        return $this->belongsTo(Courier::class);
    }

    public function packageType(){
        return $this->belongsTo(PackageType::class, 'package_id');
    }

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'area_name',
        'package_name',
    ];

    public function getAreaNameAttribute()
    {
        $area_name = Area::find($this->courier_area_id);
        if($area_name) {
            return $area_name->area;
        }

        return null;
    }

    public function getPackageNameAttribute()
    {
        $package_name = PackageType::find($this->package_id);
        if($package_name) {
            return $package_name->package;
        }

        return null;
    }
}
