<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagSearch extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'tag_search';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'product_id';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 
        'search_tag', 
    ];
}
