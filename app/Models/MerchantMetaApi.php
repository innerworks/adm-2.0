<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantMetaApi extends Model
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'meta_value' => 'array',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'merchant_meta';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}
