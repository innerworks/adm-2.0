<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'is_active',
        'is_default',
        'is_basis',
        'is_consumer',
        'valuation_fee',
        'cod_fee'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'couriers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

     /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'package_types',
        'rates',
        'excess_rates',
    ];    


    public function getPackageTypes()
    {
        return $this->hasMany('App\Models\PackageType', 'courier_id', 'id');
    }

    public function getRates()
    {
        return $this->hasMany('App\Models\CourierRate', 'courier_id', 'id');
    }

    public function getExcessRates()
    {
        return $this->hasMany('App\Models\CourierExcessRate', 'courier_id', 'id');
    }


    public function getPackageTypesAttribute()
    {
        $package_types = $this->getPackageTypes()->get();
        if($package_types) {
            return $package_types;
        }

        return null;
    }

    public function getRatesAttribute()
    {
        $rates = $this->getRates()->get();
        if($rates) {
            return $rates;
        }

        return null;
    }

    public function getExcessRatesAttribute()
    {
        $excess_rates = $this->getExcessRates()->get();
        if($excess_rates) {
            return $excess_rates;
        }

        return null;
    }

}
