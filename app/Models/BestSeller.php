<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BestSeller extends Model
{
    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 
        'category_id', 
        'base_price', 
        'selling_price', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'best_sellers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'product_details',
        'category_details',
    ];

    public function getProductDetailsAttribute()
    {
        return Product::find($this->product_id);
    }

    public function getCategoryDetailsAttribute()
    {
        return Category::find($this->category_id);
    }
}
