<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourierRate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'courier_area_id',
        'package_id',
        'courier_id',
        'delivery_rate'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = "courier_rates";

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];
    
    public function getCourierRate($maxData)
    {
        return CourierRate::where('max_weight', '>=', $maxData)
            ->where('min_weight', '<=', $maxData)
            ->first();
    }

    public function getWeightByAreaId($id)
    {
       return CourierRate::where('courier_area_id',$id)->orderBy('max_weight', 'desc')->first();
    }


    public function courierArea(){
        return $this->belongsTo(CourierArea::class);
    }

    public function packageType(){
        return $this->belongsTo(PackageType::class, 'package_id');
    }

    public function courier(){
        return $this->belongsTo(Courier::class, 'courier_id');
    }

    public function scopeCourierAreaId($query, $type){
        return $query->where('courier_area_id', $type);
    }

    public function scopeRateCourierId($query, $type){
        return $query->where('courier_id', $type);
    }

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'area_name',
        'package_name',
    ];

    public function getAreaNameAttribute()
    {
        $area_name = Area::find($this->courier_area_id);
        if($area_name) {
            return $area_name->area;
        }

        return null;
    }

    public function getPackageNameAttribute()
    {
        $package_name = PackageType::find($this->package_id);
        if($package_name) {
            return $package_name->package;
        }

        return null;
    }
}
