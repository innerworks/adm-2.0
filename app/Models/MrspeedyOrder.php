<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MrspeedyOrder extends Model
{
    use SoftDeletes;

     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 
        'order_name', 
        'vehicle_type_id', 
        'status', 
        'status_description', 
        'matter', 
        'points', 
        'payment_amount', 
        'delivery_fee_amount', 
        'weight_fee_amount', 
        'insurance_amount', 
        'insurance_fee_amount', 
        'loading_fee_amount', 
        'money_transfer_fee_amount', 
        'suburban_delivery_fee_amount', 
        'overnight_fee_amount', 
        'discount_amount', 
        'backpayment_amount', 
        'cod_fee_amount', 
        'payment_method'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'mrspeedy_order';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'points' => 'array',
    ];
}
