<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 
        'variant_id', 
        'size_id', 
        'stock'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at', 
        'id', 
        'product_id', 
        'variant_id', 
        'created_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'inventories';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'product_name',
        'product_sku',
        'size_name',
        'variant_name',
    ];

    public function getProduct()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function getVariant()
    {
        return $this->hasMany('App\Models\ProductVariant',  'id', 'variant_id');
    }

    public function getSize()
    {
        return $this->hasMany('App\Models\ProductSize',  'id', 'size_id');
    }

    public function getProductNameAttribute() 
    {
        $product = $this->getProduct()->first();
        return $product->name;
    }

    public function getProductSkuAttribute() 
    {
        $product = $this->getProduct()->first();
        return $product->sku;
    }

    public function getSizeNameAttribute() 
    {
        return $this->getSize()->pluck('size')->first();
    }

    public function getVariantNameAttribute() 
    {
        return $this->getVariant()->pluck('variant')->first();
    }
}
