<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTrackingStatus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'order_detail_id',
        'order_status_id',
        'merchant_id',
        'courier_id',
        'tracking_number',
        'remark'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'deleted_at'
    ];

    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'order_tracking_status';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];
}
