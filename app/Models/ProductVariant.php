<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVariant extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 
        'group', 
        'name', 
        'size', 
        'price', 
        'image_gallery'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at', 
        'created_at', 
        'product_id'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'product_variants';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'image_gallery' => 'array',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'inventory',
    ];    

    public function getInventories()
    {
        return $this->hasMany('App\Models\Inventory', 'variant_id');
    }

    public function getInventoryAttribute() 
    {
        return $this->getInventories()->get();
    }

}
