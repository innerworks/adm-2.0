<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notification_type', 
        'content', 
        'url', 
        'customer_id', 
        'merchant_id', 
        'user_id', 
        'is_seen'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'notifications';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public static function addMerchant($merchant, $user_id)
    {
    	$notification['notification_type'] = 'merchant_request';
    	$notification['content'] = 'Merchant Application for '.$merchant->details["first_name"].' '.$merchant->details["last_name"];
    	$notification['url'] = '';
    	$notification['merchant_id'] = $merchant->id;
    	$notification['user_id'] = $user_id;

    	static::updateOrCreate($notification);
    }
}
