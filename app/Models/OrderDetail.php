<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\ProductSize;
use App\Models\ProductVariant;
use App\Models\ProductAddOn;
use App\Models\Inventory;
use App\Models\MerchantMeta;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderTrackingStatus;
use App\Models\Merchant;
use App\Models\CustomerAddress;
use App\Models\Brand;
use App\Models\PaymentType;
use App\Models\OrderStatus;
use App\Models\Question;
use App\Models\MrspeedyOrder;

class OrderDetail extends Model
{
    use SoftDeletes;

     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 
        'merchant_id', 
        'category_id', 
        'brand_id', 
        'shipping_address_id', 
        'product_id', 
        'product_name', 
        'product_image', 
        'size_id', 
        'variant_ids', 
        'add_ons_ids', 
        'base_price', 
        'selling_price', 
        'variants_price',
        'add_ons_price',
        'qty',
        'total_price', 
        'shipping_fee', 
        'commision_rate', 
        'mrspeedy_order_id', 
        'delivery_status_id', 
        'shipped_date', 
        'received_date',
        'order_status_id',
        'special_instructions',        
        'courier_id'        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 
        'deleted_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'order_details';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'variant_ids' => 'array',
        'add_ons_ids' => 'array',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'size_name',
        'variants',
        'addOns',
        'merchant',
        'customer_address',
        'brand',
        'grand_total',
        'order_status',
        'order_status_message',
        'reasons',
        'delivery',
        'order_tracking_status'
    ];

    public function decrementInventory($qty)
    {
        // return Inventory::where('product_id', $this->product_id)
        //         ->where('size_id', $this->size_id)
        //         ->when($this->variant_ids, function($query){
        //             return $query->where('variant_id.name', $this->variant_ids);
        //         })
        //         ->decrement('stock', $qty);          
    }

    public function customGroup($params)
    {
        $newArray = array();

        foreach ($params as $value) {
            if(!array_key_exists($value->group_name, $newArray)) {
                 $newObject = new \stdClass;

                 $newObject->group_name = $value->group_name;
                 $newObject->items = array();
                 
                 $newArray[$value->group_name] = $newObject;
            }
            $itemObject = new \stdClass;

            $itemObject->id = $value->id;
            $itemObject->name = $value->variant ? $value->variant : $value->addon;
            $itemObject->price = $value->price;
            if($value->variant)
                $itemObject->image_gallery = $value->image_gallery;
            if($value->variant)
                $itemObject->inventory = $value->inventory;            

            $newArray[$value->group_name]->items[] = $itemObject;
        }

        return array_values($newArray);
    }

    public function getSizeNameAttribute()
    {
        $size = ProductSize::find($this->size_id);
        return $size['size'];
    }

    public function getVariantsAttribute()
    {
        $variants = array();
        if(is_array($this->variant_ids)) {
            $variants = ProductVariant::whereIn('id', $this->variant_ids)->get();
        }
        else {
            if($this->variant_ids) {
                $variantIds = array();
                foreach (json_decode($this->variant_ids) as $variantId) {
                    $variantIds[] = $variantId->id;
                }
                $variants = ProductVariant::whereIn('id', $variantIds)->get();                
            }
            else {
                $variants = ProductVariant::where('id', $this->variant_ids)->get();                
            }
        }

        return $this->customGroup($variants);
    }

    public function getAddOnsAttribute()
    {
        $addOns = array();
        if(is_array($this->add_ons_ids)) {
            $addOns = ProductAddOn::whereIn('id', $this->add_ons_ids)->get();
        }
        else {
            if($this->add_ons_ids) {
                $addOnsIds = array();
                foreach (json_decode($this->add_ons_ids) as $addOntId) {
                    $addOnsIds[] = $addOntId->id;
                }
                $addOns = ProductAddOn::whereIn('id', $addOnsIds)->get();                
            }
            else {
                $addOns = ProductAddOn::where('id', $this->add_ons_ids)->get();                
            }
        }

        return $this->customGroup($addOns);
    }

    public function getMerchantAttribute()
    {
        $merchant =  Merchant::find($this->merchant_id);
        //$merchant->makeHidden(['details','settings']);
        return $merchant;
    }

    public function getCustomerAddressAttribute()
    {
        return CustomerAddress::find($this->shipping_address_id);
    }

    public function getBrandAttribute()
    {
        return Brand::find($this->brand_id);
    }

    public function getGrandTotalAttribute()
    {
        return number_format($this->total_price + $this->variants_price + $this->add_ons_price, 2);
    }

    public function getOrderStatusAttribute()
    {
        $orderStatus = OrderStatus::find($this->order_status_id);
        if($orderStatus)
            return $orderStatus->status_title;
        return null;
    }

    public function getOrderStatusMessageAttribute()
    {
        $orderStatus = OrderStatus::find($this->order_status_id);
        if($orderStatus)
            return $orderStatus->status_message;
        return null;
    }

    public function getOrderTrackingStatusAttribute()
    {
        $trackingStatus = OrderTrackingStatus::find($this->order_id);
        if($trackingStatus)
            return $trackingStatus->tracking_number;
        return null;
    }

    public function getReasonsAttribute()
    {
        $cancelations = Question::where('type', 1)->select('id','question', 'type')->get();
        $returns = Question::where('type', 2)->select('id','question', 'type')->get();
        $warranty = Question::where('type', 3)->select('id','question', 'type')->get();

        return $data = [
            'cancellation' => $cancelations,
            'return' => $returns,
            'warranty' => $warranty,
        ];
    }

    public function getDeliveryAttribute()
    {
        return MrspeedyOrder::where('order_id', $this->mrspeedy_order_id)->first();
    }
}
