<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantCourier extends Model
{
    public $timestamps = false;

    public $incrementing = false;

    protected $primaryKey = 'courier_id';

    protected $table = 'merchant_courier';

    protected $fillable = [
        'owner_id',
        'courier_id',
        'owner_type'
    ];
}
