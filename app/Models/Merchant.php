<?php

namespace App\Models;

use Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\MerchantMeta;
use App\Models\MerchantMetaApi;

class Merchant extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    protected $guard = 'merchants';
     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'email', 
        'username', 
        'password', 
        'salt', 
        'login_attempt', 
        'is_blocked', 
        'category_id', 
        'status', 
        'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'activation_token', 
        'salt', 
        'updated_at', 
        'deleted_at'
    ];    

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'merchants';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'details',
        'store_name',
        'store_address',
        'settings',
    ];    

    public static function getSalt($email)
    {
        $user = static::where('username', '=', $email)
            ->orWhere('email', '=', $email)
            ->first();
            
        return $user->salt;
    }

    public function getDetails()
    {
        return $this->hasMany('App\Models\MerchantMeta');
    }

    public function getApiDetails()
    {
        return $this->hasMany('App\Models\MerchantMetaApi');
    }

    public function addMeta($metaData)
    {
        foreach ($metaData as $key => $detail) {
            $where = [
                ['merchant_id', $this->id],
                ['meta_key', $key]        
            ];            
            $user_meta = MerchantMeta::where($where)->first();

            if(!$user_meta)
                $user_meta = new MerchantMeta;
                $user_meta->merchant_id = $this->id;
                $user_meta->meta_key = $key;
                $user_meta->meta_value = $detail;
                $user_meta->save();

            $user_meta->meta_key = $key;
            $user_meta->meta_value = $detail;
            $user_meta->save();
        }
    }

    public function uploadLogo($file, $logoFileName = null)
    {
        $ext = $file->getClientOriginalExtension();
        $filename = uniqid().'.'.$ext;
        $file->storeAs('public/merchant/logo/', $filename);
        Storage::delete('public/merchant/logo/'. $logoFileName);
        return asset('storage/merchant/logo/'. $filename);
    }

    public function uploadBanner($file, $bannerFileName = null)
    {
        $ext = $file->getClientOriginalExtension();
        $filename = uniqid().'.'.$ext;
        $file->storeAs('public/merchant/banner/', $filename);
        Storage::delete('public/merchant/banner/'. $bannerFileName);
        return asset('storage/merchant/banner/'. $filename);
    }

    /****************************************
    *           ATTRIBUTES PARTS            *
    ****************************************/
    public function getDetailsAttribute() 
    {
        return $this->getDetails()->pluck('meta_value','meta_key')->toArray();
    }

    public function getSettingsAttribute()
    {
        return $this->getApiDetails()
                    ->where('meta_key', '=', 'settings')
                    ->pluck('meta_value')
                    ->first();
    }

    public function getStoreNameAttribute()
    {
        return $this->getDetails()
                    ->where('meta_key', '=', 'store_name')
                    ->pluck('meta_value')
                    ->first();
    }

    public function getStoreAddressAttribute()
    {
        return $this->getDetails()
                    ->where('meta_key', '=', 'store_address')
                    ->pluck('meta_value')
                    ->first();
    }


}
