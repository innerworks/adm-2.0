<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPromoVoucher extends Model
{
    protected $table = "order_promo_vouchers";
}
