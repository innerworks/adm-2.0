<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourierArea extends Model
{
    //
    protected $table = "courier_areas";


    public function courierRate(){
        return $this->hasMany(CourierRate::class);
    }

    public function courier(){
        return $this->belongsTo(Courier::class);
    }

    public function courierExcessRate(){
        return $this->hasMany(CourierExcessRate::class, 'courier_area_id');
    }


}
