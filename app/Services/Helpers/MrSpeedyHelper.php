<?php

namespace App\Services\Helpers;

class MrSpeedyHelper 
{
    private $environment;

    private $AuthToken;
    private $callbackAuthToken;
    private $ApiUrl;
    private $apiCallbackUrl;
    private $errorsMessages = array();
    private $errorsCodes = array(
        'required' => 'Required parameter was not provided',
        'unknown' => 'Unknown parameter was encountered',
        'invalid_list' => 'Invalid JSON list',
        'invalid_object' => 'Invalid JSON object',
        'invalid_boolean' => 'Invalid boolean',
        'invalid_date' => 'Invalid date or time',
        'invalid_date_format' => 'Invalid date and time format',
        'invalid_float' => 'Invalid floating point number',
        'invalid_integer' => 'Invalid integer number',
        'invalid_string' => 'Invalid string',
        'invalid_order' => 'Order ID was not found',
        'invalid_point' => 'Point ID was not found',
        'invalid_order_status' => 'Invalid order status',
        'invalid_vehicle_type' => 'Invalid vehicle type',
        'invalid_courier' => 'Invalid courier ID',
        'invalid_phone' => 'Invalid phone number',
        'invalid_region' => 'Address is out of the delivery area for the region',
        'invalid_order_package' => 'Package was not found',
        'invalid_delivery_id' => 'Delivery ID was not found',
        'invalid_delivery_package' => 'Package ID for delivery was not found',
        'invalid_delivery_status' => 'Invalid delivery status',
        'invalid_bank_card' => 'Bank card ID was not found',
        'invalid_url' => 'Invalid url',
        'invalid_enum_value' => 'Invalid enum value',
        'different_regions' => 'Addresses from different regions are not allowed',
        'address_not_found' => 'Address geocoding failed. Check your address with Google Maps service',
        'min_length' => 'String value is too short',
        'max_length' => 'String value is too long',
        'min_date' => 'Date and time is older than possible',
        'max_date' => 'Date and time is later than possible',
        'min_size' => 'List size is too small',
        'max_size' => 'List size is too large',
        'min_value' => 'Value is too small',
        'max_value' => 'Value is too large',
        'cannot_be_past' => 'Date and time cannot be in the past',
        'start_after_end' => 'Incorrect time interval. Start time should be earlier than the end',
        'earlier_than_previous_point' => 'Incorrect time interval. Time cannot be earlier than previous point time',
        'coordinates_out_of_bounds' => 'Point coordinates are outside acceptable delivery areas',
        'not_nullable' => 'Value can not be null',
        'not_allowed' => 'Parameter not allowed',
        'order_payment_only_one_point' => 'Order payment can be specified only for one point',
        'cod_agreement_required' => 'COD agreement required',
    );

    public function __construct() {
        $this->environment = config('mrspeedy.environment');

        if ($this->environment == "prod") {            
            $this->AuthToken = config('mrspeedy.prod_secret_auth_token');
            $this->callbackAuthToken = config('mrspeedy.prod_callback_auth_token');
            $this->ApiUrl = config('mrspeedy.api_prod_url');
            $this->apiCallbackUrl = config('mrspeedy.prod_callback_url');
        }
        else {
            $this->AuthToken = config('mrspeedy.test_sercret_auth_token');
            $this->callbackAuthToken = config('mrspeedy.test_callback_auth_token');
            $this->ApiUrl = config('mrspeedy.api_test_url');
            $this->apiCallbackUrl = config('mrspeedy.test_callback_url');   
        }
    }

    public function testConnect()
    {
        try {
            $curl = curl_init(); 
            curl_setopt($curl, CURLOPT_URL, $this->ApiUrl); 
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['X-DV-Auth-Token: '.$this->AuthToken]); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
             
            $result = curl_exec($curl); 
            if ($result === false) { 
                throw new \Exception(curl_error($curl), curl_errno($curl)); 
            } 
             
            return $result; 
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => 404,
            ], 404);
        }
    }

    public function calculateOrder($params)
    {
        $data = [
            'request' => 'calculate-order',
            'data' => $params
        ];

        $response = $this->sendPostRequest($data);
        return $response = json_decode($response, true);
    }

    public function createOrder($params)
    {
        $data = [
            'request' => 'create-order',
            'data' => $params
        ];

        $response = $this->sendPostRequest($data);
        return $response = json_decode($response, true);
    }

    public function editOrder($params)
    {
        $data = [
            'request' => 'edit-order',
            'data' => $params
        ];

        $response = $this->sendPostRequest($data);
        return $response = json_decode($response, true);
    }

    public function cancelOrder($params)
    {
        $data = [
            'request' => 'cancel-order',
            'data' => $params
        ];

        $response = $this->sendPostRequest($data);
        return $response = json_decode($response, true);
    }

    public function getOrders($params)
    {
        $status = '';
        if($params['status'])
            $status = '?status='.$params['status'];

        $data = [
            'request' => 'orders'.$status,
        ];

        $response = $this->getRequest($data);
        return $response = json_decode($response, true);
    }

    public function getCourier($params)
    {
        $status = '';
        if($params['order_id'])
            $status = '?order_id='.$params['order_id'];

        $data = [
            'request' => 'courier'.$status,
        ];

        $response = $this->getRequest($data);
        return $response = json_decode($response, true);
    }

    public function getClient()
    {
        $data = [
            'request' => 'client'
        ];

        $response = $this->getRequest($data);
        return $response = json_decode($response, true);
    }

    public function createDeliveries($params)
    {
        $data = [
            'request' => 'create-deliveries',
            'data' => $params
        ];

        $response = $this->sendPostRequest($data);
        return $response = json_decode($response, true);
    }

    public function sendPostRequest($params)
    {
        try {
            $curl = curl_init(); 
            curl_setopt($curl, CURLOPT_URL, $this->ApiUrl.'/'.$params['request']); 
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST'); 
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['X-DV-Auth-Token: '.$this->AuthToken]); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 

            $json = json_encode($params['data'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json); 
             
            $result = curl_exec($curl); 
            if ($result === false) { 
                throw new \Exception(curl_error($curl), curl_errno($curl)); 
            } 
             
            return $result; 
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => 404,
            ], 404);
        }
    }

    public function getRequest($params)
    {
        try {
            $curl = curl_init(); 
            curl_setopt($curl, CURLOPT_URL, $this->ApiUrl.'/'.$params['request']); 
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['X-DV-Auth-Token: '.$this->AuthToken]); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
             
            $result = curl_exec($curl); 
            if ($result === false) { 
                throw new \Exception(curl_error($curl), curl_errno($curl)); 
            } 
             
            return $result; 
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => 404,
            ], 404);
        }
    }

    public function errors($params)
    {
        foreach ($params as $key => $value) {
            if(is_array($value))
                $this->errors($value);
            else 
                if($value != null)
                    $this->errorsMessage['errors'][] = $this->errorsCodes[$value];
        };

        return $this->errorsMessage;
    }


}