<?php
namespace App\Services\Helpers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

use App\Models\PaymentResponse; 
use App\Models\PaymentType; 
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderTrackingStatus;
use App\Models\Cart;

use Illuminate\Support\Carbon;
use SoapClient;

class PaymentGatewayHelper 
{
    const STATUS = array(
        'GR001' => 'success',
        'GR002' => 'success',
        'GR033' => 'pending'
    );

	private $_mid;
	private $_cert;
	private $_ipaddress;
	private $_sec3d;
	public $_currency;
	public $_country;
	public $_paymentMethod;
	public $_totalAmountFormat;
	public $_userDetails;
	public $_orderBillingAddress;
	public $_orderDetails;
	public $_totalShippingFee;
	public $_totalDsicount;
	public $_order;
	public $_shippingFee;

    private $requestId;
    private $responseId;
    public $order;
    public $cancel;

	public function makeRequestPaynamics() {
        if(app()->environment() == 'local' || app()->environment() == 'development'){
            $cons = Config::get('constant.PAYNAMICS');
        }else{
            $cons = Config::get('constant.PAYNAMICS_PROD');
        }

        $_mid = $cons['MERCHANT_ID'];//<-- your merchant id
        $cert = $cons['MERCHANT_KEY']; //<-- your merchant key
        $_requestid = substr(uniqid(), 0, 13);
        $_ipaddress = $cons['IP_ADDRESS'];
        $_noturl = URL::to('/paymentNotification'); // url where response is posted
        $_resurl = URL::to('/payment-status/?ordernumber=').$this->_order->order_number; //url of merchant landing page
        $_cancelurl = URL::to('/payment-status/?ordernumber=').$this->_order->order_number.'&cancel=1'; //url of merchant landing page
        $_currency = $this->_currency;//PHP or USD
        $_fname = $this->_userDetails->first_name;
        $_mname = $this->_userDetails->middle_name;
        $_lname = $this->_userDetails->last_name;
        $_addr1 = $this->_orderBillingAddress->detailed_address;
        $_addr2 = "";
        $_city = $this->_orderBillingAddress->city_id;
        $_state = $this->_orderBillingAddress->province_id;
        $_country = $this->_country;
        $_zip = $this->_orderBillingAddress->zip_code;
        $_sec3d = $cons['SEC3D'];
        $_email = $this->_orderBillingAddress->email;
        $_phone = "";
        $_mobile = $this->_orderBillingAddress->mobile_number;
        $_clientip = $_SERVER['REMOTE_ADDR'];
        $_mtacurl = URL::to('terms-and-conditions');

        if($this->_totalDsicount){
            $total = $this->_totalAmountFormat - $this->_totalDsicount - $this->_order->points_consumed;
        }else{
            $total = $this->_totalAmountFormat - $this->_order->points_consumed;
        }
        $_amount = number_format($total,2, '.', '');

        $forSign = $_mid . $_requestid . $_ipaddress . $_noturl . $_resurl .  $_fname . $_lname . $_mname . $_addr1 . $_addr2 . $_city . $_state . $_country . $_zip . $_email . $_phone . $_clientip . $_amount . $_currency . $_sec3d;
        $_sign = hash("sha512", $forSign.$cert);

        $checkIfExistResponse = PaymentResponse::where('order_id',$this->_order->id)->first();
        $getPaymentId = PaymentType::where('payment_acronym',$this->_paymentMethod)->first();

        if($checkIfExistResponse == null){
            $paymentResponse = new PaymentResponse();
            $paymentResponse->customer_id       = $this->_userDetails->id;
            $paymentResponse->request_id        = $_requestid;
            $paymentResponse->order_id          = $this->_order->id;
            $paymentResponse->payment_type_id   = $getPaymentId->id;
            $paymentResponse->save();
        }else{
            $updateResponse = PaymentResponse::find($checkIfExistResponse->id);
            $updateResponse->request_id     = $_requestid;
            $updateResponse->payment_type_id   = $getPaymentId->id;
            $updateResponse->save();
        }

        /*****soap items****/
        $strxml = "";
        $strxml = $strxml . "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
        $strxml = $strxml . "<Request>";
        $strxml = $strxml . "<orders>";
        $strxml = $strxml . "<items>";

        foreach ($this->_orderDetails as $data){

            $isSize = "Size:".$data->size_name;

           if($data->selling_price == "0"){
                $price = $data->base_price + $data->variants_price + $data->variants_price;
            }else{
                $price = $data->selling_price + $data->variants_price + $data->variants_price;
            }
			
			$variants = "";
            if($data->variants && count($data->variants) > 0 ){
            	foreach ($data->variants as $variant) {
                    foreach ($variant->items as $value) {
                        if($value)
                            $variants .= ' | '.$value->name;
                        $variants .= $value->name;
                    }
            	}
                $variants = "Variants:".$variants;
            }else{
                $variants = "";
            }

			$addOns = "";

            if($data->addOns && count($data->addOns) > 0 ){

            	foreach ($data->addOns as $addOn) {
                    foreach ($addOn->items as $value) {
                        if($addOns)
                            $addOns .= ' | '.$value->name;
                        $addOns .= $value->name;
                    }
            	}
	            $addOns = "Add Ons:".$addOns;
            }else{
                $addOns = "";
            }

            $strxml = $strxml . "<Items>";
            $strxml = $strxml . "<itemname>Product Name:$data->product_name $isSize $variants $addOns</itemname>
                                <quantity>$data->qty</quantity>
                                <amount>$price</amount>";
            $strxml = $strxml . "</Items>";
        }

        foreach ($this->_shippingFee as $fee) {
	        $strxml = $strxml . "<Items>";
	        $strxml = $strxml . "<itemname>Shipping Fee:</itemname><quantity>1</quantity><amount>".$fee."</amount>";
	        $strxml = $strxml . "</Items>";
        }

        $strxml = $strxml . "</items>";
        $strxml = $strxml . "</orders>";
        $strxml = $strxml . "<mid>" . $_mid . "</mid>";
        $strxml = $strxml . "<request_id>" . $_requestid . "</request_id>";
        $strxml = $strxml . "<ip_address>" . $_ipaddress . "</ip_address>";
        $strxml = $strxml . "<notification_url>" . $_noturl . "</notification_url>";
        $strxml = $strxml . "<response_url>" . $_resurl . "</response_url>";
        $strxml = $strxml . "<cancel_url>" . $_cancelurl . "</cancel_url>";
        $strxml = $strxml . "<mtac_url>" . $_mtacurl . "</mtac_url>"; // pls set this to the url where your terms and conditions are hosted
        $strxml = $strxml . "<descriptor_note>ADOBOMALL INC.-EC</descriptor_note>"; // pls set this to the descriptor of the merchant ""
        $strxml = $strxml . "<fname>" . $_fname . "</fname>";
        $strxml = $strxml . "<lname>" . $_lname . "</lname>";
        $strxml = $strxml . "<mname>" . $_mname . "</mname>";
        $strxml = $strxml . "<address1>" . $_addr1 . "</address1>";
        $strxml = $strxml . "<address2>" . $_addr2 . "</address2>";
        $strxml = $strxml . "<city>" . $_city . "</city>";
        $strxml = $strxml . "<state>" . $_state . "</state>";
        $strxml = $strxml . "<country>" . $_country . "</country>";
        $strxml = $strxml . "<zip>" . $_zip . "</zip>";
        $strxml = $strxml . "<secure3d>" . $_sec3d . "</secure3d>";
        $strxml = $strxml . "<trxtype>sale</trxtype>";
        $strxml = $strxml . "<email>" . $_email . "</email>";
        $strxml = $strxml . "<phone>" . $_phone . "</phone>";
        $strxml = $strxml . "<mobile>" . $_mobile . "</mobile>";
        $strxml = $strxml . "<client_ip>" . $_clientip . "</client_ip>";
        $strxml = $strxml . "<amount>" . $_amount . "</amount>";
        $strxml = $strxml . "<currency>" . $_currency . "</currency>";
        $strxml = $strxml . "<mlogo_url></mlogo_url>";// pls set this to the url where your logo is hosted
        $strxml = $strxml . "<pmethod>". $this->_paymentMethod ."</pmethod>";
        $strxml = $strxml . "<signature>" . $_sign . "</signature>";
        $strxml = $strxml . "</Request>";

        return $b64string =  base64_encode($strxml);
	}

    public function setValidation($requestId = null, $responseId = null, $order, $cancel=false) {
        $this->requestId    = $requestId;
        $this->responseId   = $responseId;
        $this->order        = $order;
        $this->cancel       = $cancel;
    }

    public function pay(){

        ini_set('soap.wsdl_cache_enabled', '0');
        $response =  $this->buildRequestQuery();
        $paymentStatus = $this->getPaymentStatus($response);

        $this->setOrderStatus($paymentStatus);
        $getPaymentType = PaymentType::find($response->payment_type_id);

        return [
            'paymentStatus' => $paymentStatus,
            'paymentType'   => $getPaymentType,
            'cancel'        => $this->cancel,
        ];
    }

    public function buildRequestQuery() {
        if(app()->environment() == 'local' || app()->environment() == 'development'){
            $soap = new SoapClient('https://testpti.payserv.net/Paygate/ccservice.asmx?WSDL');
            $cons = Config::get('constant.PAYNAMICS');
        }else{
            $soap = new SoapClient('https://ptiservice.paynamics.net/pnxquery/queryservice.asmx?WSDL');
            $cons = Config::get('constant.PAYNAMICS_PROD');
        }

        $subId = substr(uniqid(), 0, 13);
        $forSign = $cons['MERCHANT_ID'] . $subId . $this->getResponseId() . "" . $cons['MERCHANT_KEY'];
        $hash =  hash("sha512", $forSign);

        $data = array(
            'merchantid'    => $cons['MERCHANT_ID'],
            'request_id'    => $subId,
            'org_trxid'     => $this->getResponseId(),
            'org_trxid2'    => "",
            'signature'     => $hash
        );

        $result = $soap->query($data);
        return $this->updatePaymentResponse($result);
    }    

    protected function getPaymentStatus($response) {
        return array_key_exists($response->response_code, self::STATUS) ? self::STATUS[$response->response_code] : 'failed';
    }    

    protected function getResponseId() {
        return addslashes(base64_decode($this->responseId));
    }

    protected function getRequestId() {
        return addslashes(base64_decode($this->requestId));
    }

    protected function updatePaymentResponse($result) {

        try{
            $responseStatus = $result->queryResult->txns->ServiceResponse->responseStatus;
            $application = $result->queryResult->txns->ServiceResponse->application;

        }catch (\Exception $ex){
            $responseStatus = $result->queryResult->responseStatus;
            $application = $result->queryResult->application;

        }

        PaymentResponse::where('customer_id', $this->order->user->id)
            ->where('order_id', $this->order->id)
            ->update([
                'request_id'    => $this->getRequestId(),
                'response_id'   => $this->getResponseId(),
                'process_date'  => $application->timestamp,
                'response_code' => $responseStatus->response_code,
            ]);

        Order::where('id', $this->order->id)
        ->update([
            'status_id' =>  1 ,
        ]);

        return PaymentResponse::where('order_id', $this->order->id)
            ->where('customer_id', $this->order->user->id)
            ->first();
    }

    protected function setOrderStatus($status) {
        $order = $this->order;

        if($status == 'success'){
            if($order->paid_date == NULL){

                $this->updateOrderDate($order, $status);

                foreach ($order->order_details as $detail){

                    if($detail->qty == 0){
                        $orderStatusId = 8;
                        $this->updateOrderStatusId($detail, $orderStatusId);
                    }else{
                        $orderStatusId = 1;
                        $this->updateOrderStatusId($detail, $orderStatusId);
                        //successfully deducted in qty
                        $detail->decrementInventory($detail->qty);
                    }

                    return $this->createOrderTrackingStatus($detail, $orderStatusId);
                }
            }//end
        } 
        else if($status == 'pending') {
            if ($order->pending_payment_date == NULL) {

                $this->updateOrderDate($order, $status);

                foreach ($order->order_details as $detail){
                    $orderStatusId = 7;
                    $this->updateOrderStatusId($detail, $orderStatusId);
                    //remove tracking because payment status 7 was pending
                }
            }
        }
        else {
            if ($order->failed_date == NULL) {

                $this->updateOrderDate($order, $status);

                foreach ($order->order_details as $detail){
                    $orderStatusId = 9;
                    $this->updateOrderStatusId($detail, $orderStatusId);
                    $this->createOrderTrackingStatus($detail, $orderStatusId);
                }

            }
        }
        //delete cart items
        $this->updateUserCart($status);
    }

    public function updateOrderDate($order, $status) {
        return $order->update([
            'paid_date'             => $status == 'success' ? Carbon::now() : null,
            'pending_payment_date'  => $status == 'pending' ? Carbon::now() : null,
            'failed_date'           => $status == 'failed'  ? Carbon::now() : null,
            'payment_type_id'       => $this->getOrderPaymentResponse($order)->payment_type_id,
            'is_process'            => 1, //indicate that the order was passed in payment gateway
        ]);
    }

    public function getOrderPaymentResponse($order) {
        return PaymentResponse::where('order_id', $order->id)
            ->where('customer_id', $this->order->user->id)
            ->first();
    }

    public function updateOrderStatusId($detail, $orderStatusId) {
        $orderDetail = OrderDetail::find($detail->id);
        $orderDetail->order_status_id = $orderStatusId;
        $orderDetail->save();
    }

    public function createOrderTrackingStatus($detail, $orderStatusId){
        OrderTrackingStatus::create([
            'order_id' => $detail->order_id,
            'order_detail_id' => $detail->id,
            'order_status_id' => $orderStatusId,
            'merchant_id' => $detail->merchant_id,
            'courier_id' => 1,
            'tracking_number' => $detail->mrspeedy_order_id,        
        ]);
    }

   public function updateUserCart($status){
        if ( $status != 'failed' ) {
            Cart::where('customer_id', $this->order->user->id)->where('checked', 1)->delete();
        }
    }
}