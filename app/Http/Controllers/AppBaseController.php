<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @SWG\Swagger(
 *     basePath="/api/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="ADM 2.0 Swagger API",
 *         description="ADM 2.0 Swagger API description",
 *         @SWG\Contact(
 *             email="bautistael23@gmail.com"
 *         ),
 *     )
 * )
 */

/**
 * @SWG\SecurityScheme(
 *   securityDefinition="default",
 *   type="apiKey",
 *   in="header",
 *   name="Authorization"
 * )
 */
class AppBaseController extends Controller
{
    // Success status
    public $successStatus = 200;    
    // Unauthorized status
    public $unauthorizedStatus = 401;
    // Not Found status
    public $notFoundStatus = 404;
    // Unprocessable Entity
    public $unprocessableEntity = 422;
    // Request time out
    public $requestTimeOut = 408;
    // Duplicate request
    public $duplicateEntry = 409;

    public function response($data = [], $message = '', $code = 200)
    {
        $response = array(
            'data' => $data,
            'message' => $message,
            'status_code' => $code,
            'status' => true,
            'resource' => request()->getBasePath() . '/' . request()->path() . ((count(request()->all()) > 0) ? '?' . http_build_query(request()->all()) : '')
        );

        return response($response, $code);
    }

    public function responsePaginate($data = [], $message = '', $code = 200)
    {
        $response = [
            'meta' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'first_page_url' => $data->onFirstPage(),
                'last_page_url' => $data->previousPageUrl(),
                'next_page_url' => $data->nextPageUrl(),
                'prev_page_url' => $data->previousPageUrl(),
                'path' => $data->url(request('page')),
                'from' => $data->currentPage(),
                'to' => $data->count(),
            ],
            'data' => $data->items(),         
            'message' => $message,
            'status_code' => $code,
            'status' => true,
            'resource' => request()->getBasePath() . '/' . request()->path() . ((count(request()->all()) > 0) ? '?' . http_build_query(request()->all()) : '')
        ];

        return response($response, $code);
    }

    public function responseSale($data = [], $saleInfo = null, $message = '', $code = 200)
    {
        $response = [
            'meta' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'first_page_url' => $data->onFirstPage(),
                'last_page_url' => $data->previousPageUrl(),
                'next_page_url' => $data->nextPageUrl(),
                'prev_page_url' => $data->previousPageUrl(),
                'path' => $data->url(request('page')),
                'from' => $data->currentPage(),
                'to' => $data->count(),
            ],
            'sale_info' => $saleInfo,
            'data' => $data->items(),         
            'message' => $message,
            'status_code' => $code,
            'status' => true,
            'resource' => request()->getBasePath() . '/' . request()->path() . ((count(request()->all()) > 0) ? '?' . http_build_query(request()->all()) : '')
        ];

        return response($response, $code);
    }
}
