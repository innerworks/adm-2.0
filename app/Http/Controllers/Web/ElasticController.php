<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Traits\PaginationTrait;

use Config;
use App\Models\Customer;
use App\Models\FlashSale;
use App\Models\Brand;
use App\Models\Merchant; 
use App\Models\Category; 
use App\Models\Product;
use App\Models\ProductRating; 
use App\Models\CustomerAddress;
use App\Models\MerchantMeta;
use App\Models\CustomerSearch;
use App\Models\Inventory;
use App\Models\ElasticProduct;
use Carbon\Carbon;
use Elasticsearch\ClientBuilder;

class ElasticController extends ApiBaseController
{
    // private $elastic_server = "http://localhost:9200/adm_products/_search";

    public function client()
    {
        // if (env('APP_ENV') == "local") {
        //     return ClientBuilder::create()->setHosts(
        //         config('constant.ELASTICSEARCH_LOCAL')
        //     )->build();
        // } else if(env('APP_ENV') == "development" || env('APP_ENV' == "production")) {
        //     return ClientBuilder::create()->setHosts(
        //         config('constant.ELASTICSEARCH_LIVE')
        //     )->build();
        // }

        return ClientBuilder::create()->setHosts(
                config('constant.ELASTICSEARCH_LIVE')
            )->build();
    }

    public function createIndex(Request $request)
    {
        $params = [
            'index' => $request->index_name,
            'body'  => [
                'settings' => [
                    'number_of_shards'   => 3,
                    'number_of_replicas' => 0
                ]
            ]
        ];

        $response = $this->client()->indices()->create($params);

        echo "<pre>";
        print_r($response);
    }

    public function createBatchProducts(Request $request)
    {
        $products = ElasticProduct::get();

        $params = ['body' => []];

        foreach ($products as $index => $product) {            
            $params['body'][] = [
                'index' => [
                    '_index' => $request->index_name,
                    '_id'    => $product->id
                ]
            ];

            $params['body'][] = [
                'id'           => $product->id,
                'name'         => $product->name,
                'status'       => $product->status,
                'brand'        => $product->brand,
                'category'     => $product->category,
                'subcategory'  => $product->subcategory,
                'merchant'     => $product->merchant,
                'tags'         => $product->tags,
            ];

            // Every 1000 documents stop and send the bulk request
            if ($index % 1000 == 0) {
                $responses = $this->client()->bulk($params);

                // erase the old bulk request
                $params = ['body' => []];

                // unset the bulk response when you are done to save memory
                unset($responses);
            }
        }

        // Send the last batch if it exists
        if (!empty($params['body'])) {
            $responses = $this->client()->bulk($params);
        }

        echo "<pre>";
        print_r($responses);
    }

    public function getIndex(Request $request)
    {
        $params = [
            'index' => $request->index_name,
        ];

        $response = $this->client()->cat()->indices($params);

        echo "<pre>";
        print_r($response);
    }

    public function getIndices()
    {
        $params = []; // if empty, return all indexes

        $response = $this->client()->cat()->indices($params);

        echo "<pre>";
        print_r($response);
    }

    public function deleteIndex(Request $request)
    {
        $params = [
            'index' => $request->index_name
        ];

        $response = $this->client()->indices()->delete($params);

        echo "<pre>";
        print_r($response);
    }

    public function getSearchFields(Request $request)
    {
        $params = [
            'index' => $request->index_name
        ];

        $response = $this->client()->indices()->get($params);

        echo "<pre>";
        print_r($response);
    }

    public function getSearch(Request $request)
    {
        // $time_start = microtime(true);
        $search   = $request->search;
        $category = $request->category;

        $curl   = curl_init();
        $header = [
            'Content-Type:application/json',
            // 'Authorization: Basic '. base64_encode("elastic:3l@Stic123")
        ];

        // "fuzziness": "AUTO",
        // "fields": ["brand^4", "tags^5", "name^3", "category^2", "subcategory^2", "merchant^1"],

        if ($category == "all") {
            $body = '{
                "size": 1000,
                "query": {
                    "bool": {  
                        "must": {  
                            "multi_match": {
                              "query": "'.$search.'",
                              "fields": ["brand^4","name^3", "category^2", "subcategory^2", "merchant^1"],
                              "type": "most_fields",
                              "operator": "and",
                              "tie_breaker": 0.0,
                              "analyzer": "standard",
                              "boost": 1,
                              "fuzziness": 1,
                              "fuzzy_transpositions": true,
                              "lenient": false,
                              "prefix_length": 0,
                              "max_expansions": 50,
                              "auto_generate_synonyms_phrase_query": true,
                              "cutoff_frequency": 0.01,
                              "zero_terms_query": "none"
                            }
                        },
                        "filter": {
                            "bool": {
                                "must": [
                                    { "match": { "status": 1 }}
                                ]
                            }
                        }
                    }
                }
            }';

        } else {
            $body = '{
                "size": 1000,
                "query": {
                    "bool": {  
                        "must": {  
                            "multi_match": {
                              "query": "'.$search.'",
                              "fields": ["brand^4", "tags^5", "name^3", "category^2", "subcategory^2", "merchant^1"],
                              "type": "most_fields",
                              "operator": "and",
                              "tie_breaker": 0.0,
                              "analyzer": "standard",
                              "boost": 1,
                              "fuzziness": 1,
                              "fuzzy_transpositions": true,
                              "lenient": false,
                              "prefix_length": 0,
                              "max_expansions": 50,
                              "auto_generate_synonyms_phrase_query": true,
                              "cutoff_frequency": 0.01,
                              "zero_terms_query": "none"
                            }
                        },
                        "filter": {
                            "bool": {
                                "must": [
                                    { "term": { "status": 1 }},
                                    { "match": { "category": "'.ucfirst(str_replace('_',' ', $category)).'" }}
                                ]
                            }
                        }
                    }
                }
            }';
        }

        //  "term": {"status": 1}},
        $path = "/adm_products/_search";

        // if (env('APP_ENV') == 'local') {
        //     curl_setopt($curl, CURLOPT_URL, config('constant.ELASTICSEARCH_LOCAL.host').$path);
        // } else if (env('APP_ENV')  == 'development' || env('APP_ENV') == 'production') {
        //     curl_setopt($curl, CURLOPT_URL, config('constant.ELASTICSEARCH_LIVE.host').$path);
        // }

        curl_setopt($curl, CURLOPT_URL, config('constant.ELASTICSEARCH_LIVE.host').$path);

        curl_setopt($curl,CURLOPT_HTTPHEADER, $header);
        // curl_setopt($curl, CURLOPT_GET, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        $result = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($result);
        $search_result = $result->hits->hits;
        $new_products = [];
        $temp_products = [];
        $product_ids = [];

        foreach ($search_result as $key => $val) {
            array_push($product_ids, $val->_source->id);         
        }

        $products = Product::whereIn('products.id', $product_ids)
            ->where('products.status', 1)
            ->leftJoin('view_trackings', function($join) {
                $join->on('products.id', '=', 'view_trackings.type_id')
                     ->where('view_trackings.type', 'product');
            })
            ->select('*', 'products.id as product_id')
            ->get();
            // ->when(request('sort'), function($query) {
            //     if(request('sort') == 'price-high') {
            //         return $query->orderBy('base_price', 'DESC');
            //     } else if(request('sort') == 'price-low') {
            //         return $query->orderBy('base_price', 'ASC');
            //     } else if(request('sort') == 'popular') {
            //         return $query->orderBy('view_trackings.daily', 'DESC');
            //     }
            // })
            

        foreach ($products as $index => $product) {
            $temp_products = [
                'addon'              => $product->addon,
                'average_rating'     => $product->average_rating,
                'category_id'        => $product->category_id,
                'count_rating'       => $product->count_rating,
                'delivery_charge'    => $product->delivery_charge,
                'description'        => $product->description,
                'details'            => $product->details,
                'main_category_id'   => $product->main_category_id,
                'main_category_name' => $product->main_category_name,
                'permalink'          => $product->permalink,
                'primary_photo'      => $product->primary_photo,
                'ratings'            => $product->ratings,
                'sizes'              => $product->sizes,
                'sizes_csv'          => $product->sizes_csv,
                'store_name'         => $product->store_name,
                'store_slug_name'    => $product->store_slug_name,
                'tags'               => $product->tags,
                'variants'           => $product->variants,
                'wishlist'           => $product->wishlist,
                'id'                 => $product->product_id,
                'sku'                => $product->sku,
                'name'               => $product->name,
                'description'        => $product->description,
                'permalink'          => $product->permalink,
                'merchant_id'        => $product->merchant_id,
                'category_id'        => $product->category_id,
                'base_price'         => $product->base_price,
                'selling_price'      => $product->selling_price,
                'primary_photo'      => $product->primary_photo,
                'created_at'         => $product->created_at,
            ];

            array_push($new_products, $temp_products);
        }

        $new_products = $this->paginate($new_products);

        // $time_end = microtime(true);
        // $time = $time_end - $time_start;

        // echo $time;
        // dd($new_products);

        return $this->responsePaginate($new_products, 'Successfully Retrieved!', $this->successStatus);
    }

    public function paginate($items, $perPage = 500, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
