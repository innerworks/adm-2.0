<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Services\Helpers\PaymentGatewayHelper;
use App\Services\Helpers\MrSpeedyHelper;

use App\Models\Order;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\PaymentType;

use Carbon\Carbon;
use App\Mail\PlaceOrder;

class PaymentStatusController extends Controller
{
    public function index(Request $request)
    {
        $status = null;
        $order = null;
        $user = null;

        if ( $request->has('requestid') && $request->has('responseid') ){
        	$order = Order::where('order_number', $request->ordernumber)->first();

            $paynamics = new PaymentGatewayHelper();
            $paynamics->setValidation($request->requestid, $request->responseid, $order, $request->has('cancel'));

            $status = $paynamics->pay();
            if($status['paymentStatus'] == 'failed') {
            	$this->cancelMrSpeedy($order);
            }
        }

		$user = Customer::find($order->user->id);
		$orderDetails = $order->order_details;
		$address = $user->getAddresses()->where('default_shipping', 1)->first();

		$totals = [
			'discount_price' => '0.00',
			'delivery_fee' => number_format($order->order_details->sum('shipping_fee'), 2),
			'total_price' => number_format($order->order_details->sum('total_price') + $order->order_details->sum('variants_price') + $order->order_details->sum('add_ons_price') + $order->order_details->sum('shipping_fee'), 2), 
			'promo_code' => '',
		];

        $order = $request->ordernumber;

        if($status['paymentStatus'] == 'success'){
            $this->sendEmail($order);
        }

        return view('payment_status', compact('status', 'order', 'orderDetails', 'user', 'address', 'totals'));
    }

    public function cancelMrSpeedy($order)
    {
    	$mrspeedy_order_id = array();
    	foreach ($order->order_details as $order) {
    		$mrspeedy_order_id[$order->mrspeedy_order_id] = $order->mrspeedy_order_id;
    	}

    	$mrSpeedy = new MrSpeedyHelper();
    	foreach ($mrspeedy_order_id as $key => $value) {
    		$data = [ 
			    'order_id' => $value, 
			]; 
    		$mrSpeedy->cancelOrder($data);
    	}
    }

	public function sendEmail($number){

        $order = Order::where('order_number', $number)->first();
        $date = Carbon::now();
        
		$cc = null;
		$orders = array();

		$customer = Customer::find($order->customer_id);
		$address = $customer->getAddresses()->where('default_shipping', 1)->first();

		$details = $order->order_details;
		$paymentType = PaymentType::find($order->payment_type_id);

		foreach($details as $detail){
			$tempArray = [
				'quantity'=> $detail->qty, 
				'product_name'=> $detail->product_name, 
				'amount' => number_format($detail->total_price, 2),
				''
			];
			array_push($orders, $tempArray);
		}

		// Fetch emails to cc
		$env = config('app.env');
		if($env == 'production'){
			$cc = config('constant.ADMIN.EMAIL');
		}else {
			$cc = config('constant.ADMIN.TEST');
		}

		$details = [
			'subject' => "Order Being Processed " . $order->order_number,
			'order_number' => $order->order_number,
			'name' => $address->name,
			'address' => $address->detailed_address,
			'phone' => $address->mobile_number,
			'email' => $customer->email,
            'orders' => $orders,
            'payment_type' => $paymentType->payment_type,
			'delivery_charge' => number_format($order->order_details->sum('shipping_fee'), 2),
			'total' => number_format($order->order_details->sum('total_price') + $order->order_details->sum('shipping_fee'), 2),
			'date_placed' => $date->format('l jS \\of F Y h:i:s A'),
			'status' => 'New Order'
		];

		return Mail::to($customer->email)
			->cc($cc)
			->send(new PlaceOrder($details));
	}    
}
