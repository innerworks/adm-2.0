<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Requests\Api\Product\AddRequest;
use App\Http\Requests\Api\Product\UpdateRequest;
use App\Http\Requests\Api\Customer\UpdateProfile;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Requests\Api\Product\RatingRequest;
use App\Services\Helpers\MrSpeedyHelper;
use App\Services\Helpers\PaymentGatewayHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Traits\PaginationTrait;

use Config;
use App\Models\Customer;
use App\Models\FlashSale;
use App\Models\Brand;
use App\Models\Merchant; 
use App\Models\Category; 
use App\Models\Product;
use App\Models\ProductRating; 
use App\Models\Cart;
use App\Models\Wishlist;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\OrderTrackingStatus;
use App\Models\PaymentType;
use App\Models\CustomerAddress;
use App\Models\MerchantMeta;
use App\Models\MrspeedyOrder;
use App\Models\OrderPromoVoucher;
use App\Models\CommissionRate;
use App\Models\CustomerSearch;
use App\Models\BestSeller;
use App\Models\Inventory;
// use App\Models\ItemHistory;
use App\Models\ElasticProduct;
use Carbon\Carbon;

class WebController extends ApiBaseController
{
    public function search(Request $request)
    {
        try
        {

            $products = Product::when(request('search'), function($query){
                if (request('category') == "all") {
                    return $query->where('products.name', 'LIKE', '%' . request('search') . '%')
                                  ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%')
                                  ->orWhere('tag_search.search_tag', 'LIKE', '%' . request('search') . '%');
                } else {
                    return $query->where('products.name', 'LIKE', '%' . request('search') . '%')
                                 ->where('categories.slug_name', 'LIKE', '%' . request('category') . '%')
                                 ->orWhere('tag_search.search_tag', 'LIKE', '%' . request('search') . '%');
                }
            })
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->leftJoin('tag_search', 'products.id', '=', 'tag_search.product_id')
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->where('products.status', 1)
            ->orderByDesc('products.created_at')
            ->latest()
            ->paginate(request('perPage'));

            return $this->responsePaginate($products, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function searchAll(Request $request)
    {
        try
        {
            $products = Product::where('status', 1)
            // ->join('merchant_meta', function( $join )
            // {
            //     $join->on('products.merchant_id', '=', 'merchant_meta.merchant_id')
            //          ->where('merchant_meta.meta_key', '=', 'store_name');
            // })
            // ->join('product_tags', 'products.id', '=', 'product_tags.product_id')
            // ->join('tags', 'product_tags.tag_id', '=', 'tags.id')
            // ->join('categories', 'products.category_id', '=', 'categories.id')
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->orderByDesc('products.created_at')
            ->where('products.status', 1)
            ->latest()
            ->paginate(request('perPage'));

            return $this->responsePaginate($products, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getAllCategories()
    {
        try
        {
            $category = DB::select("SELECT c.*, pc.cnt 
                                          FROM categories AS c
                                          INNER JOIN (
                                          SELECT category_id, COUNT(*) AS cnt 
                                          FROM products 
                                          WHERE STATUS = 1 AND deleted_at IS NULL GROUP BY category_id ORDER BY COUNT(*) DESC
                                          )AS pc ON c.id = pc.category_id
                                          WHERE c.active = 1
                                          ORDER BY c.name ASC");

            $new_cat_list = [];

            foreach ($category as $a => $b) {
                if ($b->parent_id == null) {
                    array_push($new_cat_list, $b);

                    foreach ($category as $c => $d) {
                        if ($d->parent_id == $b->id) {
                            array_push($new_cat_list, $d);
                        }
                    }
                }
            }

            return $this->response($new_cat_list, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getParentCategories()
    {
        try
        {
            $category_parent_id = Product::select('categories.parent_id')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->distinct()
            ->pluck('parent_id');

            $categories = Category::whereIn('id', $category_parent_id)->orderBy('name')->get()->toArray();

            $new_cat_list = [];
            foreach ($categories as $a => $b) {
                array_push($new_cat_list, $b);
            }

            return $this->response($new_cat_list, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }


    public function getBrands(Request $request)
    {
        try
        {
            $top_brand = DB::select("SELECT brand_id FROM products WHERE STATUS = 1 AND brand_id != '' AND deleted_at IS NULL GROUP BY brand_id ORDER BY COUNT(*) DESC");

            $top_brand_ids = array();
            foreach($top_brand as $brand) {
                $top_brand_ids[] = $brand->brand_id;
            }

            $brands = Brand::whereIn('id', $top_brand_ids)
            ->where('active', 1)
            ->paginate(request('perPage'));

            return $this->responsePaginate($brands, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getBrand($id)
    {
        try
        {
            $brand = Brand::find($id);
            return $this->response($brand, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getFlashSale(Request $request)
    {
        try
        {
            $current_date = Carbon::now()->timestamp;
            $productIds = FlashSale::where('status', true)
            ->whereRaw('TIMESTAMP(start_date) >= '.$current_date.' OR TIMESTAMP(end_date) <= '.$current_date.'')
            ->first();

            $products = Product::whereIn('products.id', $productIds->products_ids)
            ->where('products.status', 1)
            ->orWhere('products.selling_price', '>', '0')
            ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->latest()
            ->paginate(request('perPage'));

            return $this->responsePaginate($products,'Successfully Retrieved!', $this->successStatus);

             // $products = Product::where('products.selling_price', '!=', '0')
            // ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
            // ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            // ->where('products.status', 1)
            // ->latest()
            // ->paginate(request('perPage'));

            // $products->makeHidden(['details','sizes','variants','addons']);
            // $productIds->makeHidden(['products_ids']);
            // return $this->responsePaginate($products,'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    
    }

    public function getBestSellers(Request $request)
    {
        try
        {  
            $products = Product::select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->join('best_sellers', 'products.id', '=', 'best_sellers.product_id')
            // ->leftJoin('view_trackings', function($join) {
            //     $join->on('products.id', '=', 'view_trackings.type_id')
            //          ->where('view_trackings.type', 'product');
            // })
            ->where('products.status', 1)
            // ->when(request('sort'), function($query) {
            //     if(request('sort') == 'price-high') {
            //         return $query->orderBy('products.base_price', 'DESC');
            //     } else if(request('sort') == 'price-low') {
            //         return $query->orderBy('products.base_price', 'ASC');
            //     } else if(request('sort') == 'popular') {
            //         return $query->orderBy('view_trackings.daily', 'DESC');
            //     }
            // }) 
            ->inRandomOrder()
            ->paginate(request('perPage'));

            $cnt = 0;
            foreach ($products as $product) {
                if($cnt <= 11) {
                    $this->best_products[] = $product->id;
                }
                $cnt++;
            }

            return $this->responsePaginate($products, 'Successfully Retrieved!', $this->successStatus);            
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getNewProducts(Request $request)
    {
        try
        {
            // $date = \Carbon\Carbon::today()->subDays(300); // Last 7 Days

            // $products = Product::when(request('categoryId'), function($query){
            //     $subCategories = Category::where('parent_id', request('categoryId'))->get()->pluck('id');
            //     if(count($subCategories) > 0) {
            //         return $query->whereIn('category_id', $subCategories);
            //     }
            //     else {
            //         return $query->where('category_id', request('categoryId'));
            //     }
            // })
            // ->when(request('merchantId'), function($query){
            //     return $query->where('merchant_id', '=', request('merchantId'));
            // })
            // ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            // ->where('products.status', 1)
            // ->orderByDesc('created_at')
            // ->paginate(request('perPage'));

            $products = Product::select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->join('best_sellers', 'products.id', '=', 'best_sellers.product_id')
            // ->leftJoin('view_trackings', function($join) {
            //     $join->on('products.id', '=', 'view_trackings.type_id')
            //          ->where('view_trackings.type', 'product');
            // })
            ->where('products.status', 1)
            // ->when(request('sort'), function($query) {
            //     if(request('sort') == 'price-high') {
            //         return $query->orderBy('base_price', 'DESC');
            //     } else if(request('sort') == 'price-low') {
            //         return $query->orderBy('base_price', 'ASC');
            //     } else if(request('sort') == 'popular') {
            //         return $query->orderBy('view_trackings.daily', 'DESC');
            //     }
            // }) 
            ->inRandomOrder()
            ->orderByDesc('products.created_at')
            ->paginate(request('perPage'));

            return $this->responsePaginate($products, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getProductsByBrand(Request $request)
    {      
        try
        {
            $brand = Brand::where('slug_name', $request->slug)->first();

            $products = Product::where('products.brand_id', $brand->id)
            ->where('products.status', 1)
            ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
            ->leftJoin('view_trackings', function($join) {
                $join->on('products.id', '=', 'view_trackings.type_id')
                     ->where('view_trackings.type', 'product');
            })
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            // ->latest()
            ->where('products.status', 1)
            ->when(request('sort'), function($query) {
                if(request('sort') == 'price-high') {
                    return $query->orderBy('products.base_price', 'DESC');
                } else if(request('sort') == 'price-low') {
                    return $query->orderBy('products.base_price', 'ASC');
                } else if(request('sort') == 'popular') {
                    return $query->orderBy('view_trackings.daily', 'DESC');
                } else if(request('sort') == 'recent') {
                    return $query->orderBy('products.created_at', 'DESC');
                }
            }) 
            ->paginate(request('perPage'));

            return $this->responsePaginate($products,'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => '',//$e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    
    }

    public function getProductsByCategory(Request $request)
    {      
        try
        {
            if ($request->slug == "all") {
                $products = Product::where('products.id','!=',0)
                ->where('products.status', 1)
                ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
                ->leftJoin('view_trackings', function($join) {
                    $join->on('products.id', '=', 'view_trackings.type_id')
                         ->where('view_trackings.type', 'product');
                })
                ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
                ->where('products.status', true)
                ->when(request('sort'), function($query) {
                    if(request('sort') == 'price-high') {
                        return $query->orderBy('base_price', 'DESC');
                    } else if(request('sort') == 'price-low') {
                        return $query->orderBy('base_price', 'ASC');
                    } else if(request('sort') == 'popular') {
                        return $query->orderBy('view_trackings.daily', 'DESC');
                    }  else if(request('sort') == 'recent') {
                        return $query->orderBy('products.created_at', 'DESC');
                    }
                }) 
                ->paginate(request('perPage'));

            } else {
                $category = Category::where('slug_name', $request->slug)->first();

                $products = Product::where('products.category_id', $category->id)
                ->where('products.status', 1)
                ->orWhere('categories.parent_id', $category->id)
                ->join('merchants', 'products.merchant_id', '=', 'merchants.id')          
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->leftJoin('view_trackings', function($join) {
                    $join->on('products.id', '=', 'view_trackings.type_id')
                         ->where('view_trackings.type', 'product');
                })
                ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at', 'products.primary_photo', 'categories.parent_id')
                ->where('products.status', 1)
                ->when(request('sort'), function($query) {
                    if(request('sort') == 'price-high') {
                        return $query->orderBy('products.base_price', 'DESC');
                    } else if(request('sort') == 'price-low') {
                        return $query->orderBy('products.base_price', 'ASC');
                    } else if(request('sort') == 'popular') {
                        return $query->orderBy('view_trackings.daily', 'DESC');
                    }  else if(request('sort') == 'recent') {
                        return $query->orderBy('products.created_at', 'DESC');
                    }
                }) 
                ->paginate(request('perPage'));
            }
            return $this->responsePaginate($products,'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    
    }

    public function getWishlists($id)
    {
        try
        {
            $wishlist = Wishlist::where('customer_id', $id)->latest()->paginate(request('perPage'));
            return $this->responsePaginate($wishlist, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function addWishList(Request $request) {
        try
        {
            // if (empty(auth()->user())) {
            //     return $this->response(false, 'Please login first to continue.', $this->successStatus);
            // }

            $where = [
                ['customer_id', auth()->user()->id],     
                ['product_id', $request->id]
            ];

            $wishlist = Wishlist::where($where)->first();
            if(!$wishlist || $wishlist == null)
                $wishlist = new Wishlist;

            $wishlist->product_id = $request->id;
            $wishlist->customer_id = $request->user_id;
            $wishlist->save();

            return $this->response($wishlist, 'Added to Wishlist!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getWishItem($id)
    {
        try
        {   
            $wishlist = Wishlist::where('id', $id)->latest()->paginate(request('perPage'));
            return $this->responsePaginate($wishlist, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function deleteWishlist($id)
    {
        try
        {
            $wishlist = Wishlist::find($id);
            $wishlist->delete();
            return $this->response($wishlist, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }

    }

    public function getProduct($id)
    {
        try
        {
            $product = Product::find($id);
            return $this->response($product, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getRelatedProducts(Request $request)
    {
        try
        {
            $products = Product::where('category_id', $request->category_id)
                               ->where('permalink', '!=', $request->permalink)
                               ->where('status', 1)
                               ->inRandomOrder()->paginate(5);

            return $this->response($products, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getOnSaleProducts(Request $request)
    {
        try
        {
            // $productIds = FlashSale::where('status', true)->latest()->first();

            // $products = Product::whereIn('products.id', $productIds->products_ids)
            // ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
            // ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            // ->where('products.status', 1)
            // ->latest()
            // ->paginate(20);

            // // $products->makeHidden(['details','sizes','variants','addons']);
            // $productIds->makeHidden(['products_ids']);


            $products = Product::where('products.selling_price', '!=', '0')
            // ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->leftJoin('view_trackings', function($join) {
                $join->on('products.id', '=', 'view_trackings.type_id')
                     ->where('view_trackings.type', 'product');
            })
            ->where('products.status', 1)
            ->when(request('sort'), function($query) {
                if(request('sort') == 'price-high') {
                    return $query->orderBy('products.selling_price', 'DESC');
                } else if(request('sort') == 'price-low') {
                    return $query->orderBy('products.selling_price', 'ASC');
                } else if(request('sort') == 'popular') {
                    return $query->orderBy('view_trackings.daily', 'DESC');
                }  else if(request('sort') == 'recent') {
                    return $query->orderBy('products.created_at', 'DESC');
                }
            })
            ->latest()
            ->paginate(request('perPage'));

            // $products->makeHidden(['details','sizes','variants','addons']);
            // $productIds->makeHidden(['products_ids']);

            // return $this->responseSale($products, $productIds,'Successfully Retreived!', $this->successStatus);
            return $this->responsePaginate($products,'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    
    }

    public function getProductByLink($permalink)
    {
        try
        {
            $product = Product::where('permalink', $permalink)
                ->where('status', 1)
                ->get();

            return $this->response($product, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getMerchantByLink($permalink)
    {
        try
        {
            $product = Product::where('permalink', $permalink)
                ->where('status', 1)
                ->first();


            $merchant = Merchant::where('id', $product->merchant_id)->first();

            return $this->response($merchant, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getProductsByMerchant(Request $request) {
        try
        {   
            $merchant = Merchant::where('id', $request->id)->first();

            $products = Product::where('products.merchant_id', $merchant->id)
            ->where('products.status', 1)
            ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
            ->leftJoin('view_trackings', function($join) {
                $join->on('products.id', '=', 'view_trackings.type_id')
                     ->where('view_trackings.type', 'product');
            })
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->where('products.status', 1)
            ->when(request('sort'), function($query) {
                if(request('sort') == 'price-high') {
                    return $query->orderBy('products.base_price', 'DESC');
                } else if(request('sort') == 'price-low') {
                    return $query->orderBy('products.base_price', 'ASC');
                } else if(request('sort') == 'popular') {
                    return $query->orderBy('view_trackings.daily', 'DESC');
                }  else if(request('sort') == 'recent') {
                    return $query->orderBy('products.created_at', 'DESC');
                }
            }) 
            ->paginate(request('perPage'));

            return $this->responsePaginate($products,'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => '',//$e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    
    }

    public function getMerchantBanner(Request $request) {
        $merchant_meta = MerchantMeta::where('merchant_id', $request->id)
        ->where('meta_key', 'settings')
        ->first();
        return $this->response($merchant_meta, 'Successfully Retrieved!', $this->successStatus);
    }

    public function countCart()
    {
        try
        {
            if(Auth::check()) {
                $user = auth()->user();            
                $count = Cart::where('customer_id', $user->id)->distinct('product_id')->count();
            }
            return $this->response($count, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => 'Unathenticated. Please login.', //$e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getCarts()
    {
        // try
        // {
            $user = auth()->user();

            $carts = Cart::where('customer_id', $user->id)
                         ->orderByDesc('id')
                         ->orderByDesc('brand_id')
                         ->get();

            $carts = Cart::customGroup($carts);
            $totals = Cart::getTotals($carts);

            $data = [
                'carts' => $carts,
                'totals' => $totals
            ];

            return $this->response($data, 'Successfully Retrieved!', $this->successStatus);
        // }
        // catch (\Exception $e)
        // {
        //     return response([
        //         'message' => $e->getMessage(),
        //         'status' => false,
        //         'status_code' => $this->requestTimeOut,
        //     ], $this->requestTimeOut);
        // }
    }

    public function deleteCart($cart_id)
    {
        try
        {   
            $cart = Cart::where('id', $cart_id);
            $cart->delete();

            $user = auth()->user();

            if ($cart) {
                $cart = Cart::where('customer_id', $user->id)->distinct('product_id')->count();
            }

            return $this->response($cart, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function updateCartQty(Request $request){
        try
        {
            $user = auth()->user();
            $cart_item = Cart::where('customer_id', $user->id)->where('id', $request->cartId)->first();
            $base_price = $cart_item->total_price / $request->currentQty;

            if ($request->type == "add") {
                $request->currentQty = $request->currentQty + 1;
            } else {
                 $request->currentQty = $request->currentQty - 1;
            }

            $updateCart = Cart::where('id', $request->cartId)
            ->update([
                'qty' => $request->currentQty,
                'total_price' => $base_price * $request->currentQty
            ]);

            return $this->response($updateCart, 'Updated cart!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function selectThisItems(Request $request){
        try
        {
            if($request->isChecked == 1){
                $bulk_cart = Cart::where('merchant_id', $request->merchantId)->update(['checked' => 1]);
            }else{
                $bulk_cart = Cart::where('merchant_id', $request->merchantId)->update(['checked' => 0]);
            }

            $subTotal = Cart::where('merchant_id', $request->merchantId)->where('checked', 1)->sum('total_price');
            if ($subTotal) {
                $subTotal = number_format($subTotal, 2, ".", ",");
            } else {
                $subTotal = "0.00";
            }

            return $this->response($subTotal, 'Updated selected cart!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function selectThisItem(Request $request){
        try
        {
            if($request->isChecked == 1){
                $cart = Cart::find($request->cartId);
                $cart->checked = 1;
                $cart->save();

            }else{
                $cart = Cart::find($request->cartId);
                $cart->checked = 0;
                $cart->save();
            }

            $subTotal = Cart::where('id', $request->cartId)->where('checked', 1)->sum('total_price');
            if ($subTotal) {
                $subTotal = number_format($subTotal, 2, ".", ",");
            } else {
                $subTotal = "0.00";
            }

            return $this->response($subTotal, 'Updated selected cart!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function storeToCart(Request $request)
    {
        try
        {
            if (empty($request->variant_ids)) { $request->variant_ids = "[]"; }

            if (empty($request->addons_ids)) { $request->addon_ids = "[]"; }

            $price = 0;
            if ($request->sale_price == null) {
                $price = $request->base_price;
            } else {
                $price = $request->sale_price;
            }

            $cart_checker = Cart::where('product_id', $request->product_id)
                                ->where('customer_id', auth()->user()->id)
                                ->where('variant_ids', json_encode($request->variant_ids))
                                ->where('addons_ids', json_encode($request->addons_ids))
                                ->where('size_id', $request->size_id)
                                ->get();

            if (count($cart_checker) > 0) {
                $cart = Cart::where('product_id', $request->product_id)
                            ->where('customer_id', auth()->user()->id)
                            ->where('variant_ids', json_encode($request->variant_ids))
                            ->where('addons_ids', json_encode($request->addons_ids))
                            ->where('size_id', $request->size_id)
                            ->update([
                                'qty' => ($request->qty + $cart_checker[0]->qty),
                                'total_price' => ($cart_checker[0]->total_price + ($request->qty * $price)),
                                'shipping_fee' => $request->shipping_fee,
                            ]);
            } else {
                $cart = new Cart;
                $cart->customer_id          = auth()->user()->id;
                $cart->product_id           = $request->product_id;
                $cart->merchant_id          = $request->merchant_id;
                $cart->category_id          = $request->category_id;
                $cart->brand_id             = $request->brand_id;
                $cart->size_id              = $request->size_id;
                $cart->variant_ids          = $request->variant_ids;
                $cart->addons_ids           = $request->addons_ids;
                $cart->qty                  = $request->qty;
                $cart->total_price          = $price * $request->qty;
                $cart->shipping_fee         = $request->shipping_fee;
                $cart->checked              = $request->checked;
                $cart->special_instructions = $request->special_instructions;
                $cart->save();
            }

            // $cart = new Cart;
            // if($request->cart_id != 0) 
            //     $cart = Cart::find($request->cart_id);

            // $cart->customer_id = $request->customer_id;
            // $cart->product_id = $request->product_id;
            // $cart->merchant_id = $request->merchant_id;
            // $cart->category_id = $request->category_id;
            // $cart->brand_id = $request->brand_id;
            // $cart->size_id = $request->size_id;
            // $cart->variant_ids = $request->variant_ids;
            // $cart->addons_ids = $request->addons_ids;
            // $cart->qty = $request->qty;
            // $cart->total_price = ($price*$request->qty);
            // $cart->shipping_fee = $request->shipping_fee;
            // $cart->checked = $request->checked;
            // $cart->special_instructions = $request->special_instructions;
            // $cart->save();
            
            // $cart->makeHidden(['product_id','category_id','brand_id','size_id','variant_ids','addons_ids','created_at']);

            if ($cart) {
                $cart = Cart::where('customer_id', auth()->user()->id)
                        ->distinct('product_id')
                        ->count();
            }

            return $this->response($cart, 'Added to cart!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }

    }

    public function getDefaultShipping()
    {
        try
        {
            $user = auth()->user();
            $addresses = $user->getDefaultShipping();
            return $this->response($addresses, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDefaultBilling()
    {
        try
        {
            $user = auth()->user();
            $addresses = $user->getDefaultBilling();
            return $this->response($addresses, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function updateCourier(Request $request) 
    {
        try
        {
            $user =  auth()->user();
            $cart = null;
            if($request->merchant_id && $request->courier_id) {
                $cart = Cart::where('customer_id', $user->id)
                            ->where('merchant_id', $request->merchant_id)
                            ->update(['courier_id' => $request->courier_id]);
            }
            else {
                $cart = Cart::where('customer_id', $user->id)
                            ->update(['courier_id' => null]);
            }


            return $this->response($cart, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }

    }

    public function getCheckout()
    {
        // try
        // {
            $user =  auth()->user();

            $addresses = $user->getAddresses();

            if(count($addresses) == 0) 
                return $this->response($carts, 'Please add an address to continue.', $this->successStatus);

            $defaultAddress = $addresses->where('default_shipping', 1)->first();

            if ($defaultAddress == null)
                return $this->response($carts, 'Select default shipping and billing address to continue.', $this->successStatus);

            $carts = $user->carts()->where('checked', true)->get();

            if(count($carts) == 0) 
                return $this->response(null, 'Cart is Empty', $this->successStatus);            

            $carts = Cart::customGroup($carts);
            $totals = Cart::getTotals($carts);

            $data = [
                'carts' => $carts,
                'totals' => $totals,             
            ];

            return $this->response($data, 'Successfully Retrieved!', $this->successStatus);
        // }
        // catch (\Exception $e)
        // {
        //     return response([
        //         'message' => $e->getMessage(),
        //         'status' => false,
        //         'status_code' => $this->requestTimeOut,
        //     ], $this->requestTimeOut);
        // }
    }

    public function getCheckoutByProduct($product_id)
    {
        try
        {
            $user =  auth()->user();
            $carts = $user->carts()->where('checked', true)
                                   ->where('product_id', $product_id)
                                   ->limit(1)
                                   ->get();

            if(count($carts) == 0) 
                return $this->response(null, 'Cart is Empty', $this->successStatus);            

            $carts = Cart::customGroup($carts);
            $total = Cart::getTotals($carts);
            // $subTotal = Cart::where('checked', 1)
            //                 ->where('product_id', $product_id)
            //                 ->pluck('total_price')->first();
            // $subTotal = number_format($subTotal, 2, ".", ",");
            // $carts['subTotal'] = $subTotal;

            $addresses = $user->getAddresses();

            if(count($addresses) == 0) 
                return $this->response($carts, 'Please add an address to continue.', $this->successStatus); 

            $defaultAddress = $addresses->where('default_shipping', 1)->first();

            if ($defaultAddress == null)
                return $this->response($carts, 'Select default shipping and billing address to continue.', $this->successStatus);                                                                
            $paymentType = PaymentType::get();
            $data = [
                'user' => [
                    'full_name' => $user->first_name. ' '. $user->last_name,
                    'email' => $user->email,
                ],
                'billing_address' => [
                    'id' => $defaultAddress->id,
                    'name' => $defaultAddress->name,
                    'email' => $user->email,
                    'address' => $defaultAddress->detailed_address,
                    'mobile_number' => $defaultAddress->mobile_number,
                    'country_id' => $defaultAddress->country_id,
                    'country_name' => $defaultAddress->country_name,
                    'region_id' => $defaultAddress->region_id,
                    'region_name' => $defaultAddress->region_name,
                    'province_id' => $defaultAddress->province_id,
                    'province_name' => $defaultAddress->province_name,
                    'city_id' => $defaultAddress->city_id,
                    'city_name' => $defaultAddress->city_name,
                    'barangay_id' => $defaultAddress->barangay_id,
                    'barangay_name' => $defaultAddress->barangay_name,
                ],
                'shipping_address' => [
                    'id' => $defaultAddress->id,
                    'name' => $defaultAddress->name,
                    'email' => $user->email,
                    'address' => $defaultAddress->detailed_address,
                    'mobile_number' => $defaultAddress->mobile_number,
                    'country_id' => $defaultAddress->country_id,
                    'country_name' => $defaultAddress->country_name,
                    'region_id' => $defaultAddress->region_id,
                    'region_name' => $defaultAddress->region_name,
                    'province_id' => $defaultAddress->province_id,
                    'province_name' => $defaultAddress->province_name,
                    'city_id' => $defaultAddress->city_id,
                    'city_name' => $defaultAddress->city_name,
                    'barangay_id' => $defaultAddress->barangay_id,
                    'barangay_name' => $defaultAddress->barangay_name,
                ],
                'payment_type' => $paymentType,
                'carts' => $carts,
                'meta' => $total,
            ];

            return $this->response($carts, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function getPaymentType()
    {
        try
        {
            $payment_type = PaymentType::get();
            return $this->response($payment_type, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }

    }

    public function placeOrder(Request $request)
    {
        try
        {
            $user = auth()->user();
            $orderNum = Order::generateOrderNumber();
            $carts = $user->carts()->where('checked', true)->get();
            $carts = Cart::customGroup($carts);
            $payment_type = PaymentType::where('payment_acronym', $request->payment_acronym)->first();

            if(is_array($carts) && count($carts) > 0) {
                $voucher = null;

                $order = $user->order()->create([
                    'order_number' => $orderNum,
                    'voucher_discount_price' => $voucher,
                    'order_date' => Carbon::now(),
                    'payment_type_id' => $payment_type['id'],
                    'status_id' => 7,
                ]);

                foreach ($carts as $cart) {
                   foreach ($cart->items as $key => $item) {

                        $shipping_fee = null;
                        if($key == 0) 
                            $shipping_fee = $cart->meta['total_shipping_fee'];

                        $orderDetail = $this->createOrderDetail($order, $item, $shipping_fee);
                        OrderTrackingStatus::create([
                            'order_id' => $order->id,
                            'order_detail_id' => $orderDetail->id,
                            'order_status_id' => 7,
                            'merchant_id' => $cart->merchant_id,
                            'courier_id' => 1,
                        ]);

                        // get product details
                        $product = Product::find($orderDetail->product_id);
                        if($product->track_inventories == 1) {
                            $inventory = Inventory::where('product_id', $orderDetail->product_id)
                                                  ->where('size_id', $orderDetail->size_id)
                                                  ->first();                                       
                            if($inventory) {
                                $inventory->stock = $inventory->stock-$orderDetail->qty;
                                $inventory->save();
                            }
                        }
                    }
                }

                $message = 'Success Placed Order.';
                if($request->payment_acronym != 'cod') {
                    $data = $this->setPayment($order->order_number, $request->payment_acronym);
                }
                else {
                    $data = [
                        'order' => Order::where('order_number', $orderNum)->first()
                    ];             
                }

                // $carts = $user->carts()->get();
                // $cartIds = $carts->map(function($item){
                //     return [$item['id']];
                // });

                // Cart::whereIn('id', $cartIds)->delete();

                Cart::where('customer_id', auth()->user()->id)
                    ->where('checked', 1)
                    ->delete();
            }
            else {
                $message = 'Cart is Empty.';
                $data = [];                
            }

            $data = (object) $data;

            return $this->response($data, $message, $this->successStatus);      
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function placeOrderSingleProduct(Request $request)
    {
        try
        {
            $user = auth()->user();
            $product = Product::where('permalink', $request->permalink)->first();
            
            $orderNum = Order::generateOrderNumber();
            $carts = $user->carts()->where('checked', true)
                                   ->where('product_id', $product->id)
                                   ->where('qty', 1)
                                   ->limit(1)
                                   ->get();

            $carts = Cart::customGroup($carts);

            // $errors = $this->validatePlaceOrder($carts, $request);
            // if($errors) {
            //     return $this->mrSpeedy->errors($errors['parameter_warnings']);
            // }

            $payment_type = PaymentType::where('payment_acronym', $request->payment_acronym)->first();

            if(is_array($carts) && count($carts) > 0) {

                $voucher = null;

                $order = $user->order()->create([
                    'order_number' => $orderNum,
                    'voucher_discount_price' => $voucher,
                    'order_date' => Carbon::now(),
                    'payment_type_id' => $payment_type['id'],
                    'status_id' => 7,
                ]);

                foreach ($carts as $cart) {
                    // $mrSpeedyOrder = $this->mrSpeedyPlaceOrder($cart, $request);
                    // $this->createMrSpeedyOrder($mrSpeedyOrder);

                    // $mrSpeedyOrderId = $mrSpeedyOrder['order']['order_id'];                  
                    foreach ($cart->items as $key => $item) {

                        $shipping_fee = null;
                        if($key == 0) 
                            $shipping_fee = $cart->meta['total_shipping_fee'];

                        $orderDetail = $this->createOrderDetail($order, $item, $shipping_fee);

                        OrderTrackingStatus::create([
                            'order_id' => $order->id,
                            'order_detail_id' => $orderDetail->id,
                            'order_status_id' => 7,
                            'merchant_id' => $cart->merchant_id,
                            'courier_id' => 1,
                            'tracking_number' => null //$mrSpeedyOrderId,
                        ]);

                        // get product details
                        $product = Product::find($orderDetail->product_id);
                        if($product->track_inventories == 1) {
                            $inventory = Inventory::where('product_id', $orderDetail->product_id)
                                                  ->where('size_id', $orderDetail->size_id)
                                                  ->first();                                        
                            if($inventory) {
                                $inventory->stock = $inventory->stock-$orderDetail->qty;
                                $inventory->save();
                            }
                        }
                    }
                }

                $message = 'Success Placed Order.';
                if($request->payment_acronym != 'cod') {
                    $data = $this->setPayment($order->order_number, $request->payment_acronym);
                }
                else {
                    $data = [
                        'order' => Order::where('order_number', $orderNum)->first()
                    ];             
                }
                // $carts = $user->carts()->get();
                // $cartIds = $carts->map(function($item){
                //     return [$item['id']];
                // });
                // Cart::whereIn('id', $cartIds)->delete();

                Cart::where('customer_id', auth()->user()->id)
                    ->where('checked', 1)
                    ->delete();
            }
            else {
                $message = 'Cart is Empty.';
                $data = [];                
            }

            $data = (object) $data;

            return $this->response($data, $message, $this->successStatus);      
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function createOrderDetail($order, $value, $shipping_fee=null)
    {
        $user = auth()->user();
        $address = $user->getAddresses()->where('default_shipping', 1)->first();

        $variants_price = 0;
        $variants_name = '';
        $variants_image = '';       

        if(is_array($value->variants) && count($value->variants)) {
            foreach ($value->variants as $variant) {
                $variants_price += $variant->price;

                if(!$variants_name)
                    $variants_name .= $variant->variant;
                else
                    $variants_name .= '|'.$variant->variant;                    
    
                if($variant->image_gallery)
                    $variants_image = $variant->image_gallery[0]['image'];
            }
        }

        $add_ons_price = 0;
        if(is_array($value->addOns) && count($value->addOns)) {
            foreach ($value->addOns as $addOn) {
                $add_ons_price += $addOn->price;
            }
        }

        $variant_ids = "[]";

        if ($value->variant_ids == "[]") {
            $variant_ids = "[]";
        } else {
            $variant_ids = json_encode($value->variant_ids);
        }

        $addons_ids = "[]";
        
        if ($value->addons_ids == "[]") {
            $addons_ids = "[]";
        } else {
            $addons_ids = json_encode($value->addons_ids);
        }

        return $order->orderDetail()->create([
            'merchant_id'           => $value->merchant_id,
            'category_id'           => $value->category_id,
            'brand_id'              => $value->brand_id,
            'shipping_address_id'   => $address->id,
            'product_id'            => $value->product_id,
            'product_name'          => $value->product_name,
            'product_image'         => $variants_image ? $variants_image : $value->product_image,
            'size_id'               => $value->size_id == null ? 1 : $value->size_id,
            'variant_ids'           => $variant_ids,
            'add_ons_ids'           => $addons_ids,
            'base_price'            => $value->product_base_price,
            'selling_price'         => $value->product_selling_price,
            'variants_price'        => $variants_price,
            'add_ons_price'         => $add_ons_price,
            'qty'                   => $value->qty,
            'total_price'           => $value->total_price,
            'shipping_fee'          => $shipping_fee,
            'commision_rate'        => 0, //$this->getCommissionRate($value),
            'delivery_status_id'    => null,
            'shipped_date'          => null,
            'received_date'         => null,
            'order_status_id'       => 7,
            'special_instructions'  => $value->special_instructions,
            'courier_id'            => $value->courier_id,
        ]);
    }

    public function getCommissionRate($item)
    {
        $commissionRate = 0;

        $merchantRate = CommissionRate::where('merchant_id', $item->merchant_id)
            ->first();

        $productRate = CommissionRate::where('product_id', $item->product_id)
            ->first();
        
        $categoryRate = CommissionRate::where('category_id', $item->category_id)
            ->where('merchant_id', $item->merchant_id)
            ->first();

        if($productRate){
            $commissionRate = $productRate['product_rate'];
        } else if ($categoryRate){
            $commissionRate = $productRate['category_rate'];
        } else {
            $commissionRate = $merchantRate['merchant_rate'];
        }
        
        return $commissionRate;
    }

    public function validatePlaceOrder($carts, $request)
    {
        $errors = array();
        foreach ($carts as $key => $value) {
            $mrSpeedyOrder = $this->mrSpeedyPlaceOrder($value, $request, true);
            if($mrSpeedyOrder['warnings']) {
                $errors['warnings'] = $mrSpeedyOrder['warnings'];
                $errors['parameter_warnings'] = $mrSpeedyOrder['parameter_warnings'];
            }
        }
        return $errors;
    }

    public function mrSpeedyPlaceOrder($params, $request, $validate = false)
    {
        $merchant = Merchant::find($params->merchant_id);

        $user = auth()->user();
        $customerAddress = $user->getAddresses()->where('default_shipping', 1)->first();

        $packages = array();
        $productNames = '';
        foreach ($params->items as $item) {
            $packageObject = new \stdClass;
            $packageObject->description = $item->product_name.' ( Size '.$item->size. ' )';
            $packageObject->items_count = $item->qty;
            $packages[] = $packageObject;

            if($productNames)
                $productNames .= " | ".$item->product_name.' ( Size '.$item->size. ' )';
            $productNames .= $item->product_name.' ( Size '.$item->size. ' )';
        }

        // CHECK IF PAYMENT TYPE IS COD OR CC
        $buyout_amount = 0;
        $taking_amount = 0;

        if(request('payment_acronym') == 'cod')
            $taking_amount = $params->meta['grand_total'];

        if($merchant->category_id == 1 && request('payment_acronym') == 'cod')
            $buyout_amount = $params->meta['total_price'];

        // check if there's a selection of vehicle
        // 7 = Car
        // 8 = Motorbike
        $vehicle_type_id = 8;

        // MR.SPEEDY API CLASS
        $data = [
                'matter' => $productNames,
                'payment_method' => 'non_cash',
                'vehicle_type_id' => $vehicle_type_id,
                'points' => [            
                    [ 
                        'address' => $merchant->details['store_address'], 
                        'contact_person' => [ 
                            'name' => $merchant->details['first_name']. ' '.$merchant->details['last_name'], 
                            'phone' =>$merchant->details['mobile'], 
                        ],                        
                        'taking_amount' => 0,
                        'buyout_amount' => $buyout_amount,
                        'note' => $merchant->details['store_address'],
                        // 'packages' => $packages,
                    ], 
                    [ 
                        'address' => $customerAddress->detailed_address, 
                        'contact_person' => [ 
                            'name' => $customerAddress->name, 
                            'phone' => $customerAddress->mobile_number, 
                        ],                           
                        'taking_amount' => $taking_amount, 
                        'buyout_amount' => 0, 
                        'note' => $customerAddress->detailed_address,
                        // 'packages' => $packages,
                    ],
                ]
            ];

        if($validate) {
            return $this->mrSpeedy->calculateOrder($data);
        }
        else {
            return $responce = $this->mrSpeedy->createOrder($data);
        }
    }

    public function createMrSpeedyOrder($mrSpeedyOrder)
    {
        MrspeedyOrder::create([
            'order_id'                          => $mrSpeedyOrder['order']['order_id'], 
            'order_name'                        => $mrSpeedyOrder['order']['order_name'], 
            'vehicle_type_id'                   => $mrSpeedyOrder['order']['vehicle_type_id'], 
            'status'                            => $mrSpeedyOrder['order']['status'], 
            'status_description'                => $mrSpeedyOrder['order']['status_description'], 
            'matter'                            => $mrSpeedyOrder['order']['matter'], 
            'points'                            => json_encode($mrSpeedyOrder['order']['points']), 
            'payment_amount'                    => $mrSpeedyOrder['order']['payment_amount'], 
            'delivery_fee_amount'               => $mrSpeedyOrder['order']['delivery_fee_amount'], 
            'intercity_delivery_fee_amount'     => $mrSpeedyOrder['order']['intercity_delivery_fee_amount'],
            'weight_fee_amount'                 => $mrSpeedyOrder['order']['weight_fee_amount'], 
            'insurance_amount'                  => $mrSpeedyOrder['order']['insurance_amount'],
            'insurance_fee_amount'              => $mrSpeedyOrder['order']['insurance_fee_amount'],
            'loading_fee_amount'                => $mrSpeedyOrder['order']['loading_fee_amount'],
            'money_transfer_fee_amount'         => $mrSpeedyOrder['order']['money_transfer_fee_amount'],
            'suburban_delivery_fee_amount'      => $mrSpeedyOrder['order']['suburban_delivery_fee_amount'],
            'overnight_fee_amount'              => $mrSpeedyOrder['order']['overnight_fee_amount'],
            'discount_amount'                   => $mrSpeedyOrder['order']['discount_amount'],
            'backpayment_amount'                => $mrSpeedyOrder['order']['backpayment_amount'],
            'cod_fee_amount'                    => $mrSpeedyOrder['order']['cod_fee_amount'],
            'payment_method'                    => $mrSpeedyOrder['order']['payment_method']
        ]);
    }

    public function setPayment($order_number, $payment_type)
    {
        if(app()->environment() == 'local' || app()->environment() == 'development'){
            $paynamicsUrl = Config::get('constant.PAYNAMICS.FORM_ACTION_URL');
        }else{
            $paynamicsUrl = Config::get('constant.PAYNAMICS_PROD.FORM_ACTION_URL');
        }

        $getSum = [];
        $shippingFee = [];
        $response = 'In Progress';

        $user = auth()->user();
        $addresses = $user->getAddresses();

        $order = Order::where('order_number', $order_number)
                      ->where('customer_id', $user->id)
                      ->first();        

        $billingAddress = $addresses->where('default_shipping', 1)->first();

        $orderPromoVouchers = OrderPromoVoucher::where('order_id', $order->id)
                                                ->where('voucher_id', '!=', NULL)
                                                ->first();
        if($orderPromoVouchers){
            $isVoucher = $orderPromoVouchers->discount_price;
        }else{
            $isVoucher = NULL;
        }

        foreach ($order->order_details as $total){
            $getSum[] = $total->total_price;
            if($total->shipping_fee)
                $shippingFee[] = $total->shipping_fee;
        }
        $totalDiscount = number_format($isVoucher,2, '.', '');

        $sumShipFee = number_format(array_sum($shippingFee),2, '.', '');
        
        $totalAmtFormat = number_format(array_sum($getSum) + array_sum($shippingFee),2, '.', '');

        if($payment_type != 'cod') {
            $paynamics = new PaymentGatewayHelper();
            // SET DATA
            $paynamics->_currency = 'PHP';
            $paynamics->_country = 'PH';
            $paynamics->_paymentMethod = $payment_type;
            $paynamics->_totalAmountFormat = $totalAmtFormat;
            $paynamics->_userDetails = $user;
            $paynamics->_orderBillingAddress = $billingAddress;
            $paynamics->_orderDetails = $order->order_details;
            $paynamics->_totalShippingFee = $sumShipFee;
            $paynamics->_totalDsicount = $totalDiscount;
            $paynamics->_order = $order;
            $paynamics->_shippingFee = $shippingFee;

            $b64string = $paynamics->makeRequestPaynamics();   
        }

        return [
            'paynamicsUrl' => $paynamicsUrl,
            'responseCode' => $b64string
        ];
    }

    public function paymentStatus(Request $request)
    {
        $message = null;
        $deduct = null;
        
        if ( $request->has('requestid') && $request->has('responseid') ){
            $paynamics = new PaynamicsGateway($request->requestid, $request->responseid);
            $status = $paynamics->pay();
            if($status['paymentStatus'] == 'success'){

                // ProcessOrderConfirmation::dispatch($request->ordernumber);
                // ProcessRewardPoints::dispatch($request->ordernumber);

            }
        } else {
            $paynamics = new PaynamicsGateway();
            $status = $paynamics->expressPay();
            if($status['paymentStatus'] == 'success'){
                // ProcessOrderConfirmation::dispatch($request->ordernumber);
                // ProcessRewardPoints::dispatch($request->ordernumber);
            }
        }

        $user = Auth::user();
        $order = $request->ordernumber;
        return view('payment_status', compact('status', 'order', 'user', 'message'));
    }


    public function getProfile(Request $request)
    {
        try
        {
            $customer = auth()->user();

            if(!$customer)
                return response([
                    'message' => 'Invalid User Credentials',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            return $this->response($customer, 'Successfully Returned!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function editProfile(UpdateProfile $request)
    {
        try
        {
            $customer = auth()->user();

            if(!$customer)
                return response([
                    'message' => 'Invalid User Credentials',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            if ($request->first_name) {
                $customer->first_name = $request->first_name;
            }

            if ($request->middle_name){
                $customer->middle_name = $request->middle_name;
            } else {
                $customer->middle_name = "";
            }

            if ($request->last_name) {
                $customer->last_name = $request->last_name;
                $customer->name = "{$request->first_name} {$request->middle_name} {$request->last_name}";
            }

            if ($request->suffix) {
               $customer->suffix = $request->suffix;
            } else {
                $customer->suffix = "";
            }

            if ($request->birth_date != "null") {
                $customer->birth_date = $request->birth_date;
            }

            if ($request->gender != "null") {
                $customer->gender = $request->gender;
            }

            if ($request->phone_number) {
                $customer->phone_number = $request->phone_number;
            } else {
                $customer->phone_number = "";
            }

            if ($request->mobile_number) {
                $customer->mobile_number = $request->mobile_number;
            }

            if ($request->image_url != "undefined") {
               $customer->image_url = $customer->profilePic($request->image_url);
            }
            
            if($request->password) {
                if ($request->password != "undefined") {
                    $this->validate($request, [
                        'password' => 'confirmed|min:8',
                    ]);
                    
                    $customer->password = PasswordHelper::generate($customer->salt, $request->password);

                }
            }

            $customer->save();            

            return $this->response($customer, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getOrders(Request $request)
    {
        try
        {
            $user = auth()->user();

            $orders = OrderDetail::where('orders.customer_id', $user->id)
            ->when(request('status_id'), function($query) {
                return $query->where('order_status_id',request('status_id'));
            })             
            ->join('orders', 'order_details.order_id', '=', 'orders.id')            
            ->join('order_status', 'order_details.order_status_id', '=', 'order_status.id')            
            ->select('orders.order_number', 'orders.customer_id', 'orders.voucher_discount_price', 'orders.points_generated', 'orders.points_consumed', 'order_details.*', 'order_status.status', 'orders.created_at' )
            ->orderByDesc('orders.created_at')
            ->paginate(request('perPage'));

            return $this->responsePaginate($orders, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getOrderByStatus(Request $request)
    {
        try
        {
            $user = auth()->user();

            $orders = OrderDetail::where('orders.customer_id', $user->id)
            ->when(request('status_id'), function($query) {
                return $query->where('order_status_id',request('status_id'));
            })             
            ->join('orders', 'order_details.order_id', '=', 'orders.id')            
            ->join('order_status', 'order_details.order_status_id', '=', 'order_status.id')            
            ->select('orders.order_number', 'orders.customer_id', 'orders.voucher_discount_price', 'orders.points_generated', 'orders.points_consumed', 'order_details.*', 'order_status.status' )
            ->orderByDesc('orders.created_at')
            ->paginate(request('perPage'));

            return $this->responsePaginate($orders, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getOrderDetail(Request $request)
    {
        try
        {   
            $order = Order::where('order_number', $request->order_id)->first();
            $order_items = OrderDetail::where('order_id', $order->id)->get();

            return $this->response($order_items, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getOrderTrackingStatus(Request $request)
    {
        try {
            $order = Order::where('order_number', $request->order_id)->first();
            $tracking_status = OrderTrackingStatus::where('order_id', $order->id)->get();

            $order_messages = [];

            foreach($tracking_status as $key => $val) {
                $order_status = OrderStatus::where('id', $val->order_status_id)->first();
                $order_messages[] = ['date' => date_format($val->created_at, 'Y-m-d H:i:s'), 'message' => $order_status->status_message];
                // array_push($order_messages, $order_status->status_message);

            }

            return $this->response($order_messages, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function addRating(RatingRequest $request)
    {
        try
        {
            $rating_checker = ProductRating::where('order_id', $request->order_id)
                                           ->where('product_id', $request->product_id)
                                           ->where('customer_id', $request->customer_id)
                                           ->count();

            if ($rating_checker == 0) {
                $rating = new ProductRating;
                $rating->order_id = $request->order_id;
                $rating->product_id = $request->product_id;
                $rating->customer_id = $request->customer_id;
                $rating->rating = $request->rating;
                $rating->review = $request->review;
                $rating->save();
                return $this->response($rating, 'Rating submitted successfully!', $this->successStatus);
            } else {
                return $this->response([], 'Rating already added!', $this->successStatus);
            }
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function ratingChecker(Request $request)
    {
        $rating_checker = ProductRating::where('order_id', $request->order_id)
                                       ->where('product_id', $request->product_id)
                                       ->where('customer_id', $request->customer_id)
                                       ->count();
        if ($rating_checker == 1) {
            return $this->response($rating_checker, 'Rating already submitted!', $this->successStatus);
        }
    }

    public function getBestSellersRandom()
    {
        $date = \Carbon\Carbon::today()->subDays(300); // Last 7 Days

            $products = Product::when(request('categoryId'), function($query){
                $subCategories = Category::where('parent_id', request('categoryId'))->get()->pluck('id');
                if(count($subCategories) > 0) {
                    return $query->whereIn('category_id', $subCategories);
                }
                else {
                    return $query->where('category_id', request('categoryId'));
                }
            })
            ->when(request('merchantId'), function($query){
                return $query->where('merchant_id', '=', request('merchantId'));
            })
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            // ->where('created_at','>=',$date)
            ->where('products.status', 1)
            ->paginate(request('perPage'));

            // $products->makeHidden(['details']);

            return $this->responsePaginate($products, 'Successfully Retrieved!', $this->successStatus);
    }

    public function setupDataElastic()
    {
        $products = ElasticProduct::get();
        $new_products    = [];
        $temp_products   = [];
        $create_products = [];
        $merge_arr       = [];
        foreach ($products as $index => $product) {
            $temp_products   = [
                "create" => [
                    "_index" => "testsearch1",
                    "_type"  => "products",
                    "_id"    => $product->id
                ],
                    'id'           => $product->id,
                    'name'         => $product->name,
                    'status'       => $product->status,
                    'brand'        => $product->brand,
                    'category'     => $product->category,
                    'subcategory'  => $product->subcategory,
                    'merchant'     => $product->merchant,
                    'tags'         => $product->tags,
            ];

            array_push($new_products, $temp_products);
        }

        $new_products = json_encode($new_products);
        $new_products = str_replace('[', '', $new_products);
        $new_products = str_replace(']', '', $new_products);
        $new_products = str_replace('},"', "}}\n{", $new_products);
        $new_products = str_replace('"}', "\"}\n", $new_products);
        $new_products = str_replace(',{', "{", $new_products);
        $new_products = str_replace('{id', "{\"id", $new_products);
        $new_products = str_replace('null}', "}\n", $new_products);

        return $new_products;
    }

    public function getElasticSearch(Request $request)
    {
        // $time_start = microtime(true);

        $search   = $request->search;
        $category = $request->category;

        $curl   = curl_init();
        $header = [
            'Content-Type:application/json',
            'Authorization: Basic '. base64_encode("admin:openSearch123@")
        ];

        if ($category == "all") {
            $body = '{
                "size": 1000,
                "query": {
                    "bool": {  
                        "must": {  
                            "multi_match": {
                              "query": "'.$search.'",
                              "fields": ["brand^4", "tags^5", "name^3", "category^2", "subcategory^2", "merchant^1"],
                              "type": "most_fields",
                              "operator": "and",
                              "tie_breaker": 0.0,
                              "analyzer": "standard",
                              "boost": 1,
                              "fuzziness": "AUTO",
                              "fuzzy_transpositions": true,
                              "lenient": false,
                              "prefix_length": 0,
                              "max_expansions": 50,
                              "auto_generate_synonyms_phrase_query": true,
                              "cutoff_frequency": 0.01,
                              "zero_terms_query": "none"
                            }
                        },
                        "filter": {
                            "bool": {
                                "must": [
                                    { "match": { "status": 1 }}
                                ]
                            }
                        }
                    }
                }
            }';

        } else {
            $body = '{
                "size": 1000,
                "query": {
                    "bool": {  
                        "must": {  
                            "multi_match": {
                              "query": "'.$search.'",
                              "fields": ["brand^4", "tags^5", "name^3", "category^2", "subcategory^2", "merchant^1"],
                              "type": "most_fields",
                              "operator": "and",
                              "tie_breaker": 0.0,
                              "analyzer": "standard",
                              "boost": 1,
                              "fuzziness": "AUTO",
                              "fuzzy_transpositions": true,
                              "lenient": false,
                              "prefix_length": 0,
                              "max_expansions": 50,
                              "auto_generate_synonyms_phrase_query": true,
                              "cutoff_frequency": 0.01,
                              "zero_terms_query": "none"
                            }
                        },
                        "filter": {
                            "bool": {
                                "must": [
                                    { "term": { "status": 1 }},
                                    { "match": { "category": "'.ucfirst(str_replace('_',' ', $category)).'" }}
                                ]
                            }
                        }
                    }
                }
            }';
        }

        //  "term": {"status": 1}},
        curl_setopt($curl, CURLOPT_URL, "https://search-opensearch-wyffscjbv2bc3ywldtadggchzi.ap-southeast-1.es.amazonaws.com/testsearch1/products/_search");
        curl_setopt($curl,CURLOPT_HTTPHEADER, $header);
        // curl_setopt($curl, CURLOPT_GET, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        $result = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($result);
        $search_result = $result->hits->hits;
        $new_products = [];
        $temp_products = [];
        $product_ids = [];

        foreach ($search_result as $key => $val) {
            array_push($product_ids, $val->_source->id);         
        }

        $products = Product::whereIn('products.id', $product_ids)
            ->where('products.status', 1)
            ->leftJoin('view_trackings', function($join) {
                $join->on('products.id', '=', 'view_trackings.type_id')
                     ->where('view_trackings.type', 'product');
            })
            ->when(request('sort'), function($query) {
                if(request('sort') == 'price-high') {
                    return $query->orderBy('base_price', 'DESC');
                } else if(request('sort') == 'price-low') {
                    return $query->orderBy('base_price', 'ASC');
                } else if(request('sort') == 'popular') {
                    return $query->orderBy('view_trackings.daily', 'DESC');
                }
            }) 
            ->get();

        foreach ($products as $index => $product) {
            $temp_products = [
                'addon'              => $product->addon,
                'average_rating'     => $product->average_rating,
                'category_id'        => $product->category_id,
                'count_rating'       => $product->count_rating,
                'delivery_charge'    => $product->delivery_charge,
                'description'        => $product->description,
                'details'            => $product->details,
                'main_category_id'   => $product->main_category_id,
                'main_category_name' => $product->main_category_name,
                'permalink'          => $product->permalink,
                'primary_photo'      => $product->primary_photo,
                'ratings'            => $product->ratings,
                'sizes'              => $product->sizes,
                'sizes_csv'          => $product->sizes_csv,
                'store_name'         => $product->store_name,
                'store_slug_name'    => $product->store_slug_name,
                'tags'               => $product->tags,
                'variants'           => $product->variants,
                'wishlist'           => $product->wishlist,
                'id'                 => $product->id,
                'sku'                => $product->sku,
                'name'               => $product->name,
                'description'        => $product->description,
                'permalink'          => $product->permalink,
                'merchant_id'        => $product->merchant_id,
                'category_id'        => $product->category_id,
                'base_price'         => $product->base_price,
                'selling_price'      => $product->selling_price,
                'primary_photo'      => $product->primary_photo,
                'created_at'         => $product->created_at,
            ];

            array_push($new_products, $temp_products);
        }

        $new_products = $this->paginate($new_products);

        // $time_end = microtime(true);
        // $time = $time_end - $time_start;

        // echo $time;

        return $this->responsePaginate($new_products, 'Successfully Retrieved!', $this->successStatus);
    }

    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    // public function addItemHistory(Request $request) {
    //     try
    //     {
    //         $where = [
    //             ['customer_id', auth()->user()->id],     
    //             // ['product_ids', $request->product_id]
    //         ];

    //         $item_history = ItemHistory::where($where)->first();
    //         if(!$item_history || $item_history == null)
    //             $item_history = new ItemHistory;

    //         $item_history->product_ids = $item_history->product_ids.",".$request->product_id;

    //         // if ($item_history->product_ids != $request->product_id) {
    //         //     $item_history->product_ids = $item_history->product_ids.",".$request->product_id;
    //         // } else {
    //         //     $item_history->product_ids = $request->product_id.",";
    //         // }

    //         $item_history->customer_id = auth()->user()->id;
    //         $item_history->save();

    //         return $this->response($item_history, 'Added to Item History!', $this->successStatus);
    //     }
    //     catch (\Exception $e)
    //     {
    //         return response([
    //             'message' => $e->getMessage(),
    //             'status' => false,
    //             'status_code' => $this->requestTimeOut,
    //         ], $this->requestTimeOut);
    //     }
    // }

}
