<?php

namespace App\Http\Controllers\Socialite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use Socialite;
use App\Services\Helpers\PasswordHelper;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use Laravel\Passport\HasApiTokens;
use App\Models\Customer;

// use Laravel\Passport\Client as OClient; 

header("Access-Control-Allow-Methods: GET");

class LoginController extends ApiBaseController
{
    use HasApiTokens;

    public function redirectToProviderFacebook(){
    	return Socialite::driver('facebook')->redirect();
    	// return Socialite::driver('facebook')->stateless()->user(); // API based
	}

	public function handleProviderCallbackFacebook(){
	    $user =  Socialite::driver('facebook')->stateless()->user();
        $check_user = Customer::where(['email' => $user->getEmail()])->first();

        if($check_user){
            $random = Str::random(40);
            $check_user->activation_token = $random;
            $check_user->save();

            return redirect('/social/'.$random);

            // Auth::login($check_user);
            // $request = $check_user->createToken(env('APP_NAME'));
            // $data = [
            //     'token' => $request->accessToken,
            //     'user'  => $user,
            //     'is_social' => true,
            //     'is_customer' => true,
            // ];

            // return response()->json($user);
            return view('social', ['token' => $request->accessToken]);
        } else {
            $salt = bcrypt(PasswordHelper::generateSalt());
            $random = Str::random(40);

            $user = Customer::create([
                'salt'          => $salt,
                'provider'   	=> 'facebook',
                'provider_id'	=> $user->getId(),
            	'name'          => $user->getName(),
                'email'         => $user->getEmail(),
                'image_url'     => $user->getAvatar(),
                'active'		=> 1,
                'activation_token' => $random,
            ]);

            return redirect('/social/'.$random);

            // $request = $user->createToken(env('APP_NAME'));
            // $data = [
            //     'token' => $request->accessToken,
            //     'user'  => $user,
            //     'is_social' => true,
            //     'is_customer' => true,
            // ];


            // return redirect('/');
            // return response()->json($user);
            // return redirect('/')->with('user', $user);
        }
	}

	public function redirectToProviderGoogle(){
    	return Socialite::driver('google')->redirect();
    	// return Socialite::driver('google')->stateless()->user(); // API based
	}

	public function handleProviderCallbackGoogle(){
	   	$user =  Socialite::driver('google')->stateless()->user();
        $check_user = Customer::where(['email' => $user->getEmail()])->first();

        if($check_user){

            $random = Str::random(40);
            $check_user->activation_token = $random;
            $check_user->save();

            return redirect('/social/'.$random);

            //Auth::login($check_user);
            // $request = $check_user->createToken(env('APP_NAME'));
            // $data = [
            //     'token' => $request->accessToken,
            //     'user'  => $user,
            //     'is_social' => true,
            //     'is_customer' => true,
            // ];
            // return redirect('/')->with('data', $data);

        } else {
            $salt = bcrypt(PasswordHelper::generateSalt());
            $random = Str::random(40);

            $user = Customer::create([
                'salt'          => $salt,
                'provider'   	=> 'google',
                'provider_id'   => $user->getId(),
            	'name'          => $user->getName(),
                'email'         => $user->getEmail(),
                'image_url'     => $user->getAvatar(),
                'active'		=> 1,
                'activation_token' => $random,
            ]);

            return redirect('/social/'.$random);

            // $request = $user->createToken(env('APP_NAME'));
            // $data = [
            //     'token' => $request->accessToken,
            //     'user'  => $user,
            //     'is_social' => true,
            //     'is_customer' => true,
            // ];

            //return redirect('/')->with('data', $data);
        }
	}

}

