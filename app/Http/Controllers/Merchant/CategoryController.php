<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Requests\Api\Category\CategoryAddRequest;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Merchant\Interfaces\CategoryControllerInterface;

use App\Imports\CategoryImport;
use App\Exports\CategoryExport;
use App\Models\Category; 
use App\Models\Merchant; 

use Storage;

class CategoryController extends ApiBaseController implements CategoryControllerInterface
{
    /********************************
    * 			CATEGORIES 			*
    *********************************/
    public function getCategory($id)
	{
		try
        {
        	$category = Category::find($id);
            return $this->response($category, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(CategoryAddRequest $request)
    {
        try
        {
            $merchant = auth()->user();
            $merchant_id = $merchant->id;

            $category = new Category;
            $category->name = $request->name;
            $category->slug_name = $request->slug_name;
            $category->image_url = $request->image_url;
            $category->merchant_id = $merchant_id;
            $category->parent_id = $request->parent_id;
            $category->save();

            return $this->response($category, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(CategoryAddRequest $request)
    {
        try
        {
            $merchant = auth()->user();
            $merchant_id = $merchant->id;

            $category = Category::find($request->id);
            $category->name = $request->name;
            $category->slug_name = $request->slug_name;
            $category->image_url = $request->image_url;
            $category->merchant_id = $merchant_id;
            $category->parent_id = $request->parent_id;
            $category->save();

            return $this->response($category, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $category = Category::find($id);
            $category->delete();
            return $this->response($category, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $merchant = auth()->user();
            $merchant_id = $merchant->id;

            $category = Category::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('slug_name', 'LIKE', '%' . request('search') . '%');
            })
            ->when($merchant_id, function($query, $merchant_id) {
                return $query->where('merchant_id', '=', $merchant_id);
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($category, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDropdown(Request $request)
    {
        try
        {
            // $merchant = auth()->user();
            // $merchant_id = $merchant->id;
            // $category = Category::where('merchant_id', $merchant_id)->orderBy('sort_order')->get();

            $category = Category::orderBy('name')->get();

            return $this->response($category, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getAllCategories()
    {
        try
        {
            $category = Category::orderBy('id')->get();
            return $this->response($category, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }    

    public function batchUpload(Request $request)
    {
        try
        {
            Excel::import(new CategoryImport, $request->file('file'));
            return $this->response(true, 'Successfully Retreived!', $this->successStatus);  
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function downloadCsv()
    {
        try
        {
            $directory = 'public/export/category/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = "Categories-".date("Y-m-d").".csv";
            // Store on default disk
            Excel::store(new CategoryExport, $directory.$filename);

            $data = [
                'filepath' => '/Storage/export/category/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus); 
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

}
