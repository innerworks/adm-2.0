<?php

namespace App\Http\Controllers\Merchant\Interfaces;

use Illuminate\Http\Request;

interface OrderControllerInterface
{
    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @param string $search
	 * @return Response
	 * @SWG\Get(
	 *      path="/merchant-orders/list",
	 *      summary="Merchant Order List",
	 *      tags={"Merchant Dashboard - Orders"},
	 *      description="Merchant Order List",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="search",
	 *          description="Search String",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function list(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/merchant-orders/{id}",
	 *      summary="Get Order Detail",
	 *      tags={"Merchant Dashboard - Orders"},
	 *      description="Get Order Detail",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Order Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getDetails($id);

    /**
	 * @param integer $id
	 * @param integer $status_id
	 * @return Response
	 * @SWG\Post(
	 *      path="/merchant-orders/update",
	 *      summary="Get Order Detail",
	 *      tags={"Merchant Dashboard - Orders"},
	 *      description="Get Order Detail",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Order Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="status_id",
	 *          description="Status ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function updateOrder(Request $request);

    /**
	 * @param array $id
	 * @param integer $status_id
	 * @return Response
	 * @SWG\Post(
	 *      path="/merchant/orders/update-statuses",
	 *      summary="Get Order Detail",
	 *      tags={"Merchant Dashboard - Orders"},
	 *      description="Get Order Detail",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Order Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="status_id",
	 *          description="Status ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function updateBatchOrder(Request $request);

    /**
	 * @param string $dateFrom
	 * @param string $dateTo
	 * @return Response
	 * @SWG\Post(
	 *      path="/merchant/orders/downloadCsv",
	 *      summary="Download Orders",
	 *      tags={"Merchant Dashboard - Orders"},
	 *      description="Download Orders",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
	 *          name="dateFrom",
	 *          description="Date From",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="dateTo",
	 *          description="Date To",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */ 
    public function downloadCsv(Request $request);
}
