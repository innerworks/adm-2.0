<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Merchant\Interfaces\OrderControllerInterface;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\OrderTrackingStatus;
use App\Exports\MerchantOrdersExport;
use Carbon\Carbon;
use Storage;

class OrderController extends ApiBaseController implements OrderControllerInterface
{
    public function list(Request $request)
    {
    	try
        {
	        $merchant = auth()->user();

			$orderItems = OrderDetail::when(request('search'), function($query){
                return $query->where('order_details.product_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('orders.order_number', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('customers.first_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('customers.last_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%');
            })
	        ->where('order_details.merchant_id', $merchant->id)        
            ->join('orders', 'order_details.order_id', '=', 'orders.id')
            ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
            ->leftJoin('categories', 'order_details.category_id', '=', 'categories.id')
            ->leftJoin('order_status', 'order_details.order_status_id', '=', 'order_status.id')            
            ->leftJoin('payment_types', 'orders.payment_type_id', '=', 'payment_types.id')            
            ->selectRaw('order_details.*, orders.order_number, orders.order_date , CONCAT(customers.first_name, " " ,customers.last_name) AS customer_name, customers.email, order_status.status, payment_types.payment_type')
            ->latest()
            ->paginate(request('perPage'));	        

            return $this->responsePaginate($orderItems, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDetails($id)
    {
    	try
        {
	        $order = OrderDetail::find($id);
            return $this->response($order, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    	
    }

    public function updateOrder(Request $request)
    {
    	try
        {
	        $order = OrderDetail::find($request->id);
	        $order->order_status_id = $request->status;
	        $order->save();
            return $this->response($order, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    	
    }

    public function updateBatchOrder(Request $request)
    {
        try
        {
            $order = OrderDetail::whereIn('id', $request->ids)
                                ->update(['order_status_id' => $request->status]);

            foreach ($request->ids as $id) {
                $order_detail = OrderDetail::where('id', $id)->first();

                $order_tracking_status  = OrderTrackingStatus::create([
                    'order_id'        => $order_detail->order_id,
                    'order_detail_id' => $order_detail->id,
                    'order_status_id' => $request->status,
                    'merchant_id'     => $order_detail->merchant_id,
                    'courier_id'      => 1,
                    'created_at'      => date('Y-m-d H:i:s')
                ]);
            }

            return $this->response($order, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }       
    }

    public function downloadCsv(Request $request)
    {
        try
        {
            $merchant = auth()->user();

            $directory = 'public/export/orders/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = "Orders-".date("Y-m-d").".csv";
            // Store on default disk
            Excel::store(new MerchantOrdersExport($request->dateFrom, $request->dateTo, $merchant->id), $directory.$filename);

            $data = [
                'filepath' => '/Storage/export/orders/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus); 
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDropdownStatus()
    {
        $collection = array(1,2,3,4,14);
        $statuses = OrderStatus::whereIn('id',$collection)->get();

        return $this->response($statuses, 'Successfully Retreived test!', $this->successStatus); 
    }
}
