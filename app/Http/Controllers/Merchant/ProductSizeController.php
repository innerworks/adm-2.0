<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Merchant\Interfaces\ProductSizeControllerInterface;

use App\Imports\ProductSizeImport;
use App\Models\ProductSize;

class ProductSizeController extends ApiBaseController implements ProductSizeControllerInterface
{
    /****************************************
    * 			PRODUCT SIZE 			    *
    *****************************************/
    public function getSize($id)
	{
		try
        {
        	$ProductSize = ProductSize::find($id);
            return $this->response($ProductSize, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $ProductSize = new ProductSize;
            $ProductSize->size = $request->name;
            $ProductSize->save();

            return $this->response($ProductSize, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $ProductSize = ProductSize::find($request->id);
            $ProductSize->size = $request->name;
            $ProductSize->save();

            return $this->response($ProductSize, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $ProductSize = ProductSize::find($id);
            $ProductSize->delete();
            return $this->response($ProductSize, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $ProductSize = ProductSize::when(request('search'), function($query){
                return $query->where('size', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($ProductSize, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDropdown()
    {
        try
        {
            $ProductSize = ProductSize::get();
            return $this->response($ProductSize, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function batchUpload(Request $request)
    {
        try
        {
        	Excel::import(new ProductSizeImport, $request->file('file'));
        	return $this->response(true, 'Successfully Retreived!', $this->successStatus);	
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
