<?php

namespace App\Http\Controllers\Merchant;

use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Product\AddRequest;
use App\Http\Requests\Api\Product\UpdateRequest;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Merchant\Interfaces\ProductControllerInterface;

use App\Imports\ProductImport;
use App\Exports\ProductExport;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\Brand;
use App\Models\Tag;

use Storage;

class ProductController extends ApiBaseController implements ProductControllerInterface
{
    /************************************
    * 			PRODUCTS 			    *
    ************************************/
    public function getProduct($id)
	{
		try
        {
        	$product = Product::find($id);
            return $this->response($product, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(AddRequest $request)
    {
        try
        {
            $merchant = auth()->user();
            $merchant_id = $merchant->id;

            $product = new Product;
            $product->sku = $request->sku;
            $product->name = $request->name;
            $product->description = $request->description;
            $product->permalink = $this->generateLinkToken($request->name);
            $product->status = ($request->status == 'true') ? true : false;
            $product->merchant_id = $merchant_id;
            $product->category_id = $request->category_id;
            $product->brand_id = $request->brand_id;
            $product->base_price = $request->base_price;
            $product->selling_price = $request->selling_price;
            $product->weight = $request->weight;
            $product->length = $request->length;
            $product->width = $request->width;
            $product->height = $request->height;
            $product->other_information = $request->other_information;
            $product->track_inventories = ($request->trackInventory == 'true') ? true : false;
            $product->primary_photo = $request->primary_image;
            $product->save();

            if($request->variants)
                $product->addVariant($request->variants);

            if($request->addons)
                $product->addAddOns($request->addons);

            if($request->tags)
                $product->addTag($request->tags);

            if($request->inventories)
                $product->addInvetories($request->inventories);            

            $details = [
                'addons' => json_encode($request->addons),
                'inventories' => json_encode($request->inventories),
                'sizes' => json_encode($request->sizes),
                'tags' => json_encode($request->tags),
                'variants' => json_encode($request->variants),
                'gallery' => json_encode($request->gallery)                
            ];
            $product->addMeta($details);

            return $this->response($product, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(UpdateRequest $request)
    {
        try
        {
            $merchant = auth()->user();
            $merchant_id = $merchant->id;

            $product = Product::find($request->id);
            $product->sku = $request->sku;
            $product->name = $request->name;
            $product->description = $request->description;
            $product->permalink = $this->generateLinkToken($request->name);
            $product->status = ($request->status == 'true' || $request->status == 1) ? true : false;
            $product->merchant_id = $merchant_id;
            $product->category_id = $request->category_id;
            $product->brand_id = $request->brand_id;
            $product->base_price = $request->base_price;
            $product->selling_price = $request->selling_price;
            $product->weight = $request->weight;
            $product->length = $request->length;
            $product->width = $request->width;
            $product->height = $request->height;
            $product->other_information = $request->other_information;
            $product->track_inventories = ($request->trackInventory == 'true' || $request->trackInventory == 1) ? true : false;
            $product->primary_photo = $request->primary_image;
            $product->save();

            if($request->variants)
                $product->addVariant($request->variants);

            if($request->addons)
                $product->addAddOns($request->addons);

            if($request->tags)
                $product->addTag($request->tags);

            if($request->inventories)
                $product->addInvetories($request->inventories);            

            $details = [
                'addons' => json_encode($request->addons),
                'inventories' => json_encode($request->inventories),
                'sizes' => json_encode($request->sizes),
                'tags' => json_encode($request->tags),
                'variants' => json_encode($request->variants),
                'gallery' => json_encode($request->gallery)                
            ];
            $product->addMeta($details);

            return $this->response($product, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $product = Product::find($id);
            $product->delete();
            
            // return $this->response($product, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $merchant = auth()->user();
            $merchant_id = $merchant->id;


            // $product = Product::when(request('search'), function($query){
            //     return $query->where('products.name', 'LIKE', '%' . request('search') . '%')
            //                  ->orWhere('products.sku', 'LIKE', '%' . request('search') . '%')
            //                  ->orWhere('products.description', 'LIKE', '%' . request('search') . '%')
            //                  ->orWhere('brands.name', 'LIKE', '%' . request('search') . '%')
            //                  ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%')
            //                  ->orWhere('merchant_meta.meta_value', 'LIKE', '%' . request('search') . '%');
            // })
            $product = Product::where('products.merchant_id', '=', $merchant_id)
            ->where('merchant_meta.meta_key', '=', 'store_name')
            ->where('products.name', 'LIKE', '%'.$request->search.'%')
            ->join('merchant_meta', 'products.merchant_id', '=', 'merchant_meta.merchant_id')
            ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
            ->join('categories', 'products.category_id', '=', 'categories.id')            
            ->select('products.*','brands.name as brand_name', 'merchant_meta.meta_value as store_name', 'categories.name as category_name')
            ->orderByDesc('products.created_at')
            ->paginate(request('perPage'));

            return $this->response($product, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function batchUpload(Request $request)
    {
        // try
        // {
        	Excel::import(new ProductImport, $request->file('file'));
        	return $this->response(true, 'Successfully Retreived!', $this->successStatus);	
        // }
        // catch (\Exception $e)
        // {
        //     return response([
        //         'message' => $e->getMessage(),
        //         'status' => false,
        //         'status_code' => $this->unauthorizedStatus,
        //     ], $this->unauthorizedStatus);
        // }
    }

    public function downloadCsv()
    {
	    try
        {
            $merchant = auth()->user();
        	$products = Product::select('products.*', 'brands.name as brand_name', 'categories.name as category_name')
            ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
            ->join('categories', 'products.category_id', '=', 'categories.id')            
            ->where('products.merchant_id', $merchant->id)
            ->orderByDesc('products.created_at')
            ->get();

            $directory = 'public/export/product/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = "Products-".date("Y-m-d").".csv";
            // Store on default disk
            Excel::store(new ProductExport($products), $directory.$filename);

            $data = [
                'filepath' => '/storage/export/product/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getProductSizes()
    {
    	try
        {
            $productSize = ProductSize::get();
            return $this->response($productSize, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function generateLinkToken($name) {
        $token = str_replace(' ', '-', strtolower($name)) . '-' . str_random(10);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $token);
    }

    public function getBrands()
    {
        try
        {
            $brand = Brand::get();
            return $this->response($brand, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getTags()
    {
        try
        {
            $tags = Tag::orderBy('tag_name')->get();
            return $this->response($tags, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
