<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Merchant\Interfaces\MerchantDashboardControllerInterface;
use Illuminate\Support\Facades\Auth; 
use App\Services\Helpers\PasswordHelper;
use App\Notifications\RegisterByEmail;
use App\Http\Requests\Api\Merchant\UpdateRequest;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Exports\MerchantOrdersExport;
use App\Models\Merchant;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\ProductRating;
use App\Models\ViewTracking;
use App\Models\PaymentType;
use Hash;
use DB;

class MerchantDashboardController extends ApiBaseController implements MerchantDashboardControllerInterface
{
    public function login(Request $request)
    {
    	try
    	{
            $merchant = Merchant::whereEmail(request('email'))->first();
            if(!$merchant) {
        		$merchant = Merchant::where('username', request('email'))->first();
            }


    		if (!Hash::check(Merchant::getSalt(request('email')).env("PEPPER_HASH").request('password'), $merchant->password)) 
    			return response([
                    'message' => 'Unauthorized.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

    		Auth::login($merchant);
		    // get new token
		    $tokenResult = $merchant->createToken(env('APP_NAME'));

	        $data = [
                'token' => $tokenResult->accessToken,
                'user'  => $merchant,
                'is_merchant' => true,
             ];

            return $this->response($data, 'Successfully Logged!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		 return response([
                'message' => 'Unauthenticated. Please login.', //$e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
    }

    public function logout(Request $request)
    {
    	try
        {
            $request->user()->token()->revoke();
            $request->user()->tokens->each(function ($token,$key) {
                $token->delete();
            });

            return $this->response(null, 'Successfully logged out!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getMerchantDetails()
    {
        try
        {
            $merchant = auth()->user();
            //$merchant = Merchant::find($id);
            return $this->response($merchant, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function update(UpdateRequest $request)
    {
        try
        {
            $user = auth()->user();
            $merchant = Merchant::find($request->id);
            $merchant->email = $request->email;
            $merchant->username = $request->username;
            $merchant->status = $request->status;
            $merchant->category_id = $request->category;      

            if($request->password)
                $merchant->password = PasswordHelper::generate($merchant->salt, $request->password);
            
            $merchant->save();

            $businessInfo = [
                'business_name' => $request->businessName,
                'permit_number' => $request->permitNumber,
                'business_permit_date' => $request->busPermitDate,
                'trade_style' => $request->tradeStyle,
                'tin' => $request->tin,
                'date_started_operation' => $request->dateStartedOperation,
                'sec_registration_number' => $request->secRegistrationNumber,
                'sec_registration_date' => $request->secRegistrationDate,
                'dti_registation_number' => $request->dtiRegistationNumber,
                'dti_registation_date' => $request->dtiRegistationDate,
                'business_type' => $request->businessType
            ];

            $settings = [
                'scheduled_order' => $request->scheduledOrder,
                'sunday' => $request->sunday,
                'sunday_time' => $request->sundayTime,
                'monday' => $request->monday,
                'monday_time' => $request->mondayTime,
                'tuesday' => $request->tuesday,
                'tuesday_time' => $request->tuesdayTime,
                'wednesday' => $request->wednesday,
                'wednesday_time' => $request->wednesdayTime,
                'thursday' => $request->thursday,
                'thursday_time' => $request->thursdayTime,
                'friday' => $request->friday,
                'friday_time' => $request->fridayTime,
                'saturday' => $request->saturday,
                'saturday_time' => $request->saturdayTime,
                'other_info' => $request->otherInfo,
                'logo' => $request->logo,
                'banner' => $request->banner,
            ];

            $details = [
                'store_name' => $request->storeName,
                'store_address' => $request->storeAddress,
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
                'first_name' => $request->firstName,
                'middle_name' => $request->middleName,
                'last_name' => $request->lastName,
                'mobile' => $request->mobile,
                'phone' => $request->phone,
                'brand' => $request->brand,
                'business_info' => json_encode($businessInfo),
                'settings' => json_encode($settings),
            ];
            $merchant->addMeta($details);

            return $this->response($merchant, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function fetchOrdersCount()
    {

        // pending
        $pending = Order::whereHas('orderDetail', function($query){
            $query->where('order_status_id', 1)
            ->where('merchant_id', auth()->user()->id);
        })
        ->count();

        // processing
        $processing = Order::whereHas('orderDetail', function($query){
            $query->where('order_status_id', 2)
            ->where('merchant_id', auth()->user()->id);
        })
        ->count();

        // shipped
        $shipped = Order::whereHas('orderDetail', function($query){
            $query->where('order_status_id', 3)
            ->where('merchant_id', auth()->user()->id);
        })
        ->count();

        // delivered
        $delivered = Order::whereHas('orderDetail', function($query){
            $query->where('order_status_id', 4)
            ->where('merchant_id', auth()->user()->id);
        })
        ->count();

        // cancelled
        $cancelled = Order::whereHas('orderDetail', function($query){
            $query->where('order_status_id', 14)
            ->where('merchant_id', auth()->user()->id);
        })
        ->count();

        return response()->json([
            'data' => [
                'pending'    => $pending,
                'processing' => $processing,
                'shipped'    => $shipped,
                'delivered'  => $delivered,
                'cancelled'  => $cancelled
            ]
        ], 200);

    }

    public function getMonthlySalesReport($filter){
        $months       = 12;
        $delivered    = 4;
        $sales_amount = [];
        $orders_count = [];
        $merchant_id  = auth()->user()->id;

        // age report
        $age_report_result = OrderDetail::select(DB::raw('TIMESTAMPDIFF(YEAR, customers.birth_date, CURDATE()) as age'), DB::raw('COUNT(*) as count'))
            ->join('orders', 'order_details.order_id', '=', 'orders.id')
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->where('order_details.merchant_id', $merchant_id)
            ->groupBy('age')
            ->get();

        $age_report = [];

        $age_a = 0; // 17-24
        $age_b = 0; // 25-34
        $age_c = 0; // 35-44
        $age_d = 0; // 45-54
        $age_e = 0; // 55-64
        $age_f = 0; // 65+

        foreach ($age_report_result as $val) {

            if ($val->age > 17 && $val->age <= 24) {
                $age_a = $val->count;
            }

            if ($val->age > 24 && $val->age <= 34) {
                $age_b = $val->count;
            }

            if ($val->age > 34 && $val->age <= 44) {
                $age_c = $val->count;
            }

            if ($val->age > 44 && $val->age <= 54) {
                $age_d = $val->count;
            }

            if ($val->age > 54 && $val->age <= 64) {
                $age_e = $val->count;
            }

            if ($val->age > 64) {
                $age_f = $val->count;
            }
        }

        array_push($age_report, $age_a,$age_b,$age_c,$age_d,$age_e,$age_f);

        // convert to percentage
        $total = array_sum($age_report);
        
        if ($total != 0) {
            $age_report = array_map(function($hits) use ($total) {
               return round($hits / $total * 100, 1);
            }, $age_report);
        } else {
            $age_report = [];
        }

        // age report end

        // gender report
        $gender_male = OrderDetail::where('customers.gender', 'Male')
                        ->join('orders', 'order_details.order_id', '=', 'orders.id')
                        ->join('customers', 'orders.customer_id', '=', 'customers.id')
                        ->where('order_details.merchant_id', $merchant_id)
                        ->count();

        $gender_female = OrderDetail::where('customers.gender', 'Female')
                        ->join('orders', 'order_details.order_id', '=', 'orders.id')
                        ->join('customers', 'orders.customer_id', '=', 'customers.id')
                        ->where('order_details.merchant_id', $merchant_id)
                        ->count();

        $gender_report = [];
        array_push($gender_report, $gender_male, $gender_female);
        // gender report end

        // top rated product report
        $product_rating = ProductRating::select('product_rating.rating', 'products.sku')
                        ->where('products.merchant_id', $merchant_id)
                        ->join('products', 'product_rating.product_id', '=', 'products.id')
                        ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
                        ->groupBy('product_rating.rating')
                        ->limit(5)
                        ->orderBy('product_rating.rating', 'desc')
                        ->get();

       $average_rating = ProductRating::select('product_rating.rating', 'products.sku')
                        ->where('products.merchant_id', $merchant_id)
                        ->join('products', 'product_rating.product_id', '=', 'products.id')
                        ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
                        ->avg('rating');

        $top_rated_products = [];
        $top_rated_product_ratings = [];

        foreach ($product_rating as $val) {
            array_push($top_rated_products, $val->sku);
            array_push($top_rated_product_ratings, $val->rating);
        }
        // top rated product report end

        // top selling product report
        $top_selling = Product::select('products.sku', DB::raw('count(products.id) as sold'))
                            ->join('order_details', 'products.id', '=', 'order_details.product_id')
                            ->where('order_details.merchant_id', $merchant_id)
                            ->where('order_details.order_status_id', 4)
                            ->groupBy('products.id')
                            ->limit(5)
                            ->get();

        $top_selling_products = [];
        $top_product_sold = [];

        foreach ($top_selling as $val) {
            array_push($top_selling_products, $val->sku);
            array_push($top_product_sold, $val->sold);
        }
        // top selling product report end

        // most viewed product (top 5)
        $filter = strtolower($filter);

        $most_viewed_result = ViewTracking::select('products.id','products.sku', DB::raw('count(*) as count'), DB::raw("sum(view_trackings.{$filter}) as total"))
            ->join('products', 'view_trackings.type_id', '=', 'products.id')
            ->where('type', 'product')
            ->where('merchant_id', $merchant_id)
            ->orderByRaw("SUM(view_trackings.{$filter}) DESC")
            ->groupBy('products.id')
            ->limit(5)
            ->get();

        $most_viewed_products = [];
        $most_viewed_product_count = [];

        foreach ($most_viewed_result as $val) {
            array_push($most_viewed_products, $val->sku);
            array_push($most_viewed_product_count, $val->total);
        }
        // most viewed product (top 5) end

        // recent transactions (top 5)
        $orders = Order::select('orders.*', 'payment_types.payment_type', 'order_status.status')->where('order_details.merchant_id', $merchant_id)
        ->join('customers', 'orders.customer_id', '=', 'customers.id')
        ->join('order_details', 'orders.id', '=', 'order_details.order_id')
        ->join('payment_types', 'orders.payment_type_id', '=', 'payment_types.id')
        ->join('order_status', 'orders.status_id', '=', 'order_status.id')
        ->where('order_details.order_status_id', '!=', 7)
        ->orderBy('orders.order_date', 'DESC')
        ->limit(5)
        ->groupBy('orders.order_number')
        ->get();
        
        // recent transaction (top 5) end

        // total sales report
        for ($month = 1; $month <= $months; $month++) {
            $data = OrderDetail::select('qty', 'commision_rate', 'total_price')
                    ->whereIn('order_status_id', [1, 2, 3, 4, 17])
                    ->whereMonth('created_at', $month)
                    ->where('merchant_id', $merchant_id)
                    ->get();

            $total_price = $data->sum('total_price');
            array_push($sales_amount, $total_price);

            // $count = OrderDetail::distinct('order_id')
            //     ->whereIn('order_status_id', [1, 2, 3, 4, 17])
            //     ->whereMonth('created_at', $month)
            //     ->count();
            
            // array_push($orders_count, $count);
        }

        // total sales report end

        // most used payment modes report
        $payment_types = PaymentType::select('payment_type')->get();

        $payment_type_arr = [];        
        foreach ($payment_types as $val) {
            array_push($payment_type_arr, $val->payment_type);
        }

        $order_ids = OrderDetail::where('merchant_id', $merchant_id)
                                 ->groupBy('order_id')
                                 ->pluck('order_id');

        $type_count = Order::select('orders.payment_type_id', DB::raw('count(id) as count'))
                      ->whereIn('id', $order_ids)
                      ->groupBy('payment_type_id')
                      ->get();

        $payment_type_count = [
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '6' => 0,
        ];

        foreach ($type_count as $key => $val) {
            // $this->comparePaymentTypeId($payment_type_count, $val);
            foreach ($payment_type_count as $key2 => $val2) {
                if ($key2 == $val->payment_type_id) {
                    $payment_type_count[$key2] = $val->count;
                }
            
            }
        }
        // most used payment modes report end

        // total weekly purchase report

        $total_weekly_purchased = [];
        $total_weekly_purchased_values = [];

        $daily_purchase    = $this->getDailyPurchaseReport($merchant_id);

        // total weekly purchase report end

        // average visitors per week report
        $carbon = new Carbon();
        $week_start = $carbon->now()->startOfWeek()->format('Y-m-d H:i');
        $week_end   = $carbon->now()->endOfWeek()->format('Y-m-d H:i');

        $visitors_per_week = ViewTracking::select('products.id','products.name')
                            ->join('products', 'view_trackings.type_id', '=', 'products.id')
                            ->where('type', 'product')
                            ->where('merchant_id', $merchant_id)
                            ->whereBetween('view_trackings.created_at', [$week_start, $week_end])
                            ->count();
        // average visitors per week report end

        return response()->json([
            'age_report'                        => array_values($age_report),
            'gender_report'                     => $gender_report,
            'top_rated_products'                => $top_rated_products,
            'top_rated_product_ratings'         => $top_rated_product_ratings,
            'average_rating'                    => round($average_rating, 1),
            'top_selling_products'              => $top_selling_products,
            'top_product_sold'                  => $top_product_sold,
            'most_viewed_products'              => $most_viewed_products,
            'most_viewed_product_count'         => $most_viewed_product_count,
            'orders'                            => $orders,
            'total_sales'                       => $sales_amount,
            'payment_types'                     => $payment_type_arr,
            'payment_type_count'                => array_values($payment_type_count),
            //'total_weekly_purchased'        => $daily_purchase['total_weekly_purchased'],
            'total_weekly_purchased_values'     => $daily_purchase['total_weekly_purchased_values'],
            'visitors_per_week'                 => $visitors_per_week,
        ], 200);
    }

    public function comparePaymentTypeId($payment_type_count, $val)
    {
        foreach ($payment_type_count as $key2 => $val2) {
            if ($key2 == $val->payment_type_id) {
                return $payment_type_count[$key2] = $val->count;
            }
            
        }
    }

    public function getDailyPurchaseReport($merchant_id) {
        $carbon = new Carbon();
        $weekly_purchased = [];

        for ($counter = 1; $counter <= 7; $counter++) {
            $new_day = Product::select('products.sku')
                     ->join('order_details', 'products.id', '=', 'order_details.product_id')
                     ->whereIn('order_details.order_status_id', [1, 2, 3, 4, 17])
                     ->where('order_details.merchant_id', $merchant_id)
                     ->whereRaw("date_format(order_details.created_at, '%Y-%m-%d') = '".$carbon->weekday($counter)->format('Y-m-d')."'")
                     ->get()
                     ->count();

            //$weekly_purchased['total_weekly_purchased'][$counter-1][] = $val->name;
            $weekly_purchased['total_weekly_purchased_values'][$counter-1][] = $new_day;

        }

        if(count($weekly_purchased) > 0)
            return $weekly_purchased;

        return null;
    }

}
