<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\BestSellerControllerInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Category;
use App\Models\Product;
use App\Models\BestSeller;

class BestSellerController extends ApiBaseController implements BestSellerControllerInterface
{
    public function generate()
    {
        $top_category = DB::select("SELECT c.*, pc.cnt 
                                    FROM categories AS c
                                    INNER JOIN (
                                    SELECT category_id, COUNT(*) AS cnt 
                                    FROM products 
                                    WHERE STATUS = 1 GROUP BY category_id ORDER BY COUNT(*) DESC
                                    )AS pc ON c.id = pc.category_id
                                    WHERE c.active = 1
                                    ORDER BY pc.cnt DESC LIMIT 12");

        $arrayProducts = [];
        foreach ($top_category as $category) {
            $arrayProducts[] = $this->getRandomProducts($category->id);
        }

        return BestSeller::get();        
    }

    public function delete()
    {
        BestSeller::truncate();
    }

    public function list(Request $request)
    {
        try
        {
            $filters = json_decode($request->filters, true);

            $product = Product::when($filters['category_id'], function($query) use ($filters){
                return $query->where('products.category_id', $filters['category_id']);
            })
            ->when($filters['brand_id'], function($query)  use ($filters){
                return $query->where('products.brand_id', $filters['brand_id']);
            })
            ->when($filters['merchant_id'], function($query)  use ($filters){
                return $query->where('products.merchant_id', $filters['merchant_id']);
            })
            ->when($filters['status'], function($query)  use ($filters){
                return $query->where('products.status', $filters['status']);
            })
            ->when(request('search'), function($query){
                return $query->where('products.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('products.sku', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('products.description', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('brands.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%');
                             // ->orWhere('merchant_meta.meta_value', 'LIKE', '%' . request('search') . '%');

            })
            ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->join('best_sellers', 'products.id', '=', 'best_sellers.product_id')
            ->select('products.*', 'brands.name as brand_name', 'categories.name as category_name','products.created_at' )
            ->orderByDesc('products.created_at')
            ->paginate(request('perPage'));
            return $this->response($product, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getRandomProducts($category_id)
    {
        // DELETE ALL BESTSELLER 
        
        $products = DB::select("SELECT id, category_id, name, base_price, selling_price FROM products WHERE category_id = {$category_id} ORDER BY rand() LIMIT 10");
        
        foreach ($products as $product) {
            $data = [
                'product_id' => $product->id,
                'category_id' => $product->category_id,
                'base_price' => $product->base_price,
                'selling_price' => $product->selling_price,
            ];
            BestSeller::create($data);
        }
    }
}
