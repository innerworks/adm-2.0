<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Merchant\RegisterRequest;
use App\Http\Requests\Api\Merchant\AddRequest;
use App\Http\Requests\Api\Merchant\UpdateRequest;
use App\Http\Requests\Api\Auth\LoginRequest;

interface MerchantControllerInterface
{
	/**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/merchant/{id}",
	 *      summary="Get Merchant Details by ID",
	 *      tags={"Merchant"},
	 *      description="Get Merchant Details by ID",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Merchant Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getMerchantDetails($id);

    /**
	 * @param string $storeName
	 * @param string $storeAddress
	 * @param string $firstName
	 * @param string $middleName
	 * @param string $lastName
	 * @param string $email
	 * @param string $mobile
	 * @param string $phone
	 * @param integer $category
	 * @return Response
	 * @SWG\Post(
	 *      path="/merchant/signup",
	 *      summary="Sign up new merchant",
	 *      tags={"Merchant"},
	 *      description="Sign up new merchant",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="storeName",
	 *          description="store Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="storeAddress",
	 *          description="Store Address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="firstName",
	 *          description="First Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="middleName",
	 *          description="Middle Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="lastName",
	 *          description="Last Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="mobile",
	 *          description="Mobile",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="phone",
	 *          description="Phone",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="category",
	 *          description="Category",
	 *          type="integer",
	 *			enum={"1", "2", "3", "4", "5", "6", "7", "8"},
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function register(RegisterRequest $request);

    /**
	 * @param string $storeName
	 * @param string $storeAddress
	 * @param string $firstName
	 * @param string $middleName
	 * @param string $lastName
	 * @param string $email
	 * @param string $mobile
	 * @param string $phone
	 * @param string $password
	 * @param string $brand
	 * @param integer $category
	 * @param integer $status
	 * @param string $businessName
	 * @param string $permitNumber
	 * @param string $busPermitDate
	 * @param string $tradeStyle
	 * @param string $tin
	 * @param string $dateStartedOperation
	 * @param string $secRegistrationNumber
	 * @param string $secRegistrationDate
	 * @param string $dtiRegistationNumber
	 * @param string $dtiRegistationDate
	 * @param string $businessType
	 * @param boolean $scheduledOrder
	 * @param boolean $sunday
	 * @param string $sundayTime
	 * @param boolean $monday
	 * @param string $mondayTime
	 * @param boolean $tuesday
	 * @param string $tuesdayTime
	 * @param boolean $wednesday
	 * @param string $wednesdayTime
	 * @param boolean $thursday
	 * @param string $thursdayTime
	 * @param boolean $friday
	 * @param string $fridayTime
	 * @param boolean $saturday
	 * @param string $saturdayTime
	 * @param string $otherInfo
	 * @param file $logofile
	 * @param file $bannerfile
	 * @return Response
	 * @SWG\Post(
	 *      path="/merchant/store",
	 *      summary="Add new merchant",
	 *      tags={"Merchant"},
	 *      description="Add new merchant",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },	 
	 *      @SWG\Parameter(
	 *          name="storeName",
	 *          description="store Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="storeAddress",
	 *          description="Store Address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="firstName",
	 *          description="First Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="middleName",
	 *          description="Middle Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="lastName",
	 *          description="Last Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="mobile",
	 *          description="Mobile",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="phone",
	 *          description="Phone",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="category",
	 *          description="Category",
	 *          type="integer",
	 *			enum={"1", "2", "3", "4", "5", "6", "7", "8"},
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="status",
	 *          description="Status",
	 *          type="integer",
	 *			enum={"1", "2", "3", "4", "5"},
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password",
	 *          description="password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="businessName",
	 *          description="Business Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="permitNumber",
	 *          description="Permit Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="busPermitDate",
	 *          description="Bus Permit Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="tradeStyle",
	 *          description="Trade Style",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="tin",
	 *          description="TIN",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="dateStartedOperation",
	 *          description="Date Started Operation",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="secRegistrationNumber",
	 *          description="Sec Registration Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="secRegistrationDate",
	 *          description="Sec Registration Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="dtiRegistationNumber",
	 *          description="DTI Registation Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="dtiRegistationDate",
	 *          description="DTI Registation Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="businessType",
	 *          description="Business Type",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="scheduledOrder",
	 *          description="Scheduled Order",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="sunday",
	 *          description="Sunday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="sundayTime",
	 *          description="Sunday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="monday",
	 *          description="Monday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="mondayTime",
	 *          description="Monday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="tuesday",
	 *          description="Tuesday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="tuesdayTime",
	 *          description="Tuesday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="wednesday",
	 *          description="Wednesday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="wednesdayTime",
	 *          description="Wednesday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="thursday",
	 *          description="Thursday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="thursdayTime",
	 *          description="Thursday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="friday",
	 *          description="Friday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="fridayTime",
	 *          description="Friday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="saturday",
	 *          description="Saturday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="saturdayTime",
	 *          description="Saturday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="otherInfo",
	 *          description="Other Info",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="logofile",
	 *          description="Logo File",
	 *          type="file",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="bannerfile",
	 *          description="Banner File",
	 *          type="file",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function store(AddRequest $request);

    /**
	 * @param string $storeName
	 * @param string $storeAddress
	 * @param string $firstName
	 * @param string $middleName
	 * @param string $lastName
	 * @param string $email
	 * @param string $mobile
	 * @param string $phone
	 * @param string $password
	 * @param string $brand
	 * @param integer $category
	 * @param integer $status
	 * @param string $businessName
	 * @param string $permitNumber
	 * @param string $busPermitDate
	 * @param string $tradeStyle
	 * @param string $tin
	 * @param string $dateStartedOperation
	 * @param string $secRegistrationNumber
	 * @param string $secRegistrationDate
	 * @param string $dtiRegistationNumber
	 * @param string $dtiRegistationDate
	 * @param string $businessType
	 * @param boolean $scheduledOrder
	 * @param boolean $sunday
	 * @param string $sundayTime
	 * @param boolean $monday
	 * @param string $mondayTime
	 * @param boolean $tuesday
	 * @param string $tuesdayTime
	 * @param boolean $wednesday
	 * @param string $wednesdayTime
	 * @param boolean $thursday
	 * @param string $thursdayTime
	 * @param boolean $friday
	 * @param string $fridayTime
	 * @param boolean $saturday
	 * @param string $saturdayTime
	 * @param string $otherInfo
	 * @param file $logofile
	 * @param file $bannerfile	 
	 * @return Response
	 * @SWG\Post(
	 *      path="/merchant/update",
	 *      summary="Update merchant",
	 *      tags={"Merchant"},
	 *      description="Update merchant",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },	 
	 *      @SWG\Parameter(
	 *          name="id",
	 *          description="Merchant ID",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Parameter(
	 *          name="storeName",
	 *          description="store Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="storeAddress",
	 *          description="Store Address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="firstName",
	 *          description="First Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="middleName",
	 *          description="Middle Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="lastName",
	 *          description="Last Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="mobile",
	 *          description="Mobile",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="phone",
	 *          description="Phone",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="category",
	 *          description="Category",
	 *          type="integer",
	 *			enum={"1", "2", "3", "4", "5", "6", "7", "8"},
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="status",
	 *          description="Status",
	 *          type="integer",
	 *			enum={"1", "2", "3", "4", "5"},
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password",
	 *          description="password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="businessName",
	 *          description="Business Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="permitNumber",
	 *          description="Permit Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="busPermitDate",
	 *          description="Bus Permit Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="tradeStyle",
	 *          description="Trade Style",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="tin",
	 *          description="TIN",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="dateStartedOperation",
	 *          description="Date Started Operation",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="secRegistrationNumber",
	 *          description="Sec Registration Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="secRegistrationDate",
	 *          description="Sec Registration Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="dtiRegistationNumber",
	 *          description="DTI Registation Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="dtiRegistationDate",
	 *          description="DTI Registation Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="businessType",
	 *          description="Business Type",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="scheduledOrder",
	 *          description="Scheduled Order",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="sunday",
	 *          description="Sunday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="sundayTime",
	 *          description="Sunday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="monday",
	 *          description="Monday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="mondayTime",
	 *          description="Monday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="tuesday",
	 *          description="Tuesday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="tuesdayTime",
	 *          description="Tuesday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="wednesday",
	 *          description="Wednesday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="wednesdayTime",
	 *          description="Wednesday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="thursday",
	 *          description="Thursday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="thursdayTime",
	 *          description="Thursday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="friday",
	 *          description="Friday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="fridayTime",
	 *          description="Friday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="saturday",
	 *          description="Saturday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="saturdayTime",
	 *          description="Saturday Time",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="otherInfo",
	 *          description="Other Info",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="logofile",
	 *          description="Logo File",
	 *          type="file",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="bannerfile",
	 *          description="Banner File",
	 *          type="file",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function update(UpdateRequest $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/merchant/list",
	 *      summary="List of Merchants",
	 *      tags={"Merchant"},
	 *      description="List of Merchants",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function list(Request $request);

    /**
	 * @param string $moduleId
	 * @return Response
	 * @SWG\Get(
	 *      path="/merchant/delete/{id}",
	 *      summary="Delete Merchant",
	 *      tags={"Merchant"},
	 *      description="Delete Merchant",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Merchant Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function delete($id);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/merchant/getStatus",
	 *      summary="List of merchant status",
	 *      tags={"Merchant"},
	 *      description="List of merchant status",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getStatus();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/merchant/getDropdown",
	 *      summary="Get All Merchant",
	 *      tags={"Merchant"},
	 *      description="Get All Merchant",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getDropdown();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/merchant/local-migration",
	 *      summary="Local merchant migration",
	 *      tags={"Merchant"},
	 *      description="Local merchant migration",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function localMigration();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/merchant/requests",
	 *      summary="Get Merchant Request",
	 *      tags={"Merchant"},
	 *      description="Get Merchant Request",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    //public function requests();
}
