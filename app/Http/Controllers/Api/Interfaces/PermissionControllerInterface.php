<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Permission\PermissionRequest;

interface PermissionControllerInterface
{
    /**
     * @param PermissionRequest $request
	 * @return Response
	 * @SWG\Post(
	 *      path="/permission/grant_permission",
	 *      summary="Grant permission to role",
	 *      tags={"Permission"},
	 *      description="Grant permission to role",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Set Permissions",
     *          required=false,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="role_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="permissions",
     *                  type="object",
     *                  @SWG\Property(property="module_id",type="integer"),
     *                  @SWG\Property(property="permission",type="boolean"),
     *              )
     *          )
     *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function grantPermission(PermissionRequest $request);
}
