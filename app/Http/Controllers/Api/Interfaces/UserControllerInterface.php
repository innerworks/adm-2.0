<?php

namespace App\Http\Controllers\Api\Interfaces;
use Illuminate\Http\Request;
use App\Http\Requests\Api\User\UserUpdateRequest;
use App\Http\Requests\Api\User\UserAddRequest;

interface UserControllerInterface
{
    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user/{id}",
	 *      summary="Get User",
	 *      tags={"User"},
	 *      description="Get User",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="User Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getUserDetails($id);

	/**
	 * @param string $userName
	 * @param string $email
	 * @param string $firstName
	 * @param string $lastName
	 * @param string $password
	 * @param boolean $emailNotification
	 * @param integer $role
	 * @return Response
	 * @SWG\Post(
	 *      path="/user/store",
	 *      summary="Store new User",
	 *      tags={"User"},
	 *      description="Store new User",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="userName",
	 *          description="User Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="firstName",
	 *          description="First Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="lastName",
	 *          description="Last Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password",
	 *          description="Password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *		 @SWG\Parameter(
	 *          name="emailNotification",
	 *          description="Email Notification input must 1 or 0",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *		 @SWG\Parameter(
	 *          name="role",
	 *          description="Role",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */  
    public function store(UserAddRequest $request);

    /**
     * @param UserRoles $request
	 * @return Response
	 * @SWG\Post(
	 *      path="/user/update",
	 *      summary="Update user Details",
	 *      tags={"User"},
	 *      description="Update user Details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Update user details",
     *          required=false,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="user_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="name",
     *                  type="string"
     *              ),     
     *              @SWG\Property(
     *                  property="email",
     *                  type="string"
     *              ),              
     *              @SWG\Property(
     *                  property="password",
     *                  type="string"
     *              ),              
     *              @SWG\Property(
     *                  property="confirm_password",
     *                  type="string"
     *              ),              
     *              @SWG\Property(
     *                  property="other_details",
     *                  type="object",
     *                  @SWG\Property(property="first_name",type="string"),
     *                  @SWG\Property(property="last_name",type="string"),
     *                  @SWG\Property(property="middle_name",type="string"),
     *                  @SWG\Property(property="phone_number",type="string"),
     *                  @SWG\Property(property="mobile_number",type="string"),
     *                  @SWG\Property(property="permanent_address",type="string"),
     *                  @SWG\Property(property="current_address",type="string")
     *              )
     *          )
     *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function update(UserUpdateRequest $request);

    /**
	 * @param string $moduleId
	 * @return Response
	 * @SWG\Get(
	 *      path="/user/delete/{id}",
	 *      summary="Delete User",
	 *      tags={"User"},
	 *      description="Delete User",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="User Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function delete($id);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/user/list",
	 *      summary="List of Users",
	 *      tags={"User"},
	 *      description="List of Users",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function list(Request $request);
}
