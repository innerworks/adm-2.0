<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;

interface LookUpControllerInterface
{
	/**
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/getCountries",
	 *      summary="Get all Countries",
	 *      tags={"Look Up"},
	 *      description="Get all Countries",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getCountries();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/getRegions",
	 *      summary="Get all Regions",
	 *      tags={"Look Up"},
	 *      description="Get all Regions",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getRegions();

    /**
	 * @param integer $regionId
	 * @return Response
	 * @SWG\Post(
	 *      path="/lookup/getProvinces",
	 *      summary="Get all Provinces",
	 *      tags={"Look Up"},
	 *      description="Get all Provinces",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="regionId",
	 *          description="Region ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getProvinces(Request $request);

	/**
	 * @param integer $regionId
	 * @param integer $province_id
	 * @return Response
	 * @SWG\Post(
	 *      path="/lookup/getCities",
	 *      summary="Get all Cities",
	 *      tags={"Look Up"},
	 *      description="Get all Cities",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="regionId",
	 *          description="Region ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="province_id",
	 *          description="Province ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getCities(Request $request);

    /**
	 * @param integer $regionId
	 * @param integer $province_id
	 * @param integer $city_id
	 * @return Response
	 * @SWG\Post(
	 *      path="/lookup/getBarangays",
	 *      summary="Get all Barangays",
	 *      tags={"Look Up"},
	 *      description="Get all Barangays",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="regionId",
	 *          description="Region ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="province_id",
	 *          description="Province ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="city_id",
	 *          description="City ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getBarangays(Request $request);

    /**
	 * @param string $zip_code
	 * @return Response
	 * @SWG\Post(
	 *      path="/lookup/checkZipCode",
	 *      summary="Check if zip code is valid",
	 *      tags={"Look Up"},
	 *      description="Check if zip code is valid",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="zip_code",
	 *          description="Zip Code",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function checkZipCode(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/getMerchants",
	 *      summary="Get Merchant list",
	 *      tags={"Look Up"},
	 *      description="Get Merchant list",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getMerchants();

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/lookup/getCouriers",
	 *      summary="Get Couriers list",
	 *      tags={"Look Up"},
	 *      description="Get Couriers list",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getCouriers();

}
