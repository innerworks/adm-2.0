<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;

interface CustomerRequestControllerInterface
{
    /**
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/storeRequest",
	 *      summary="Add / Update Customer Request",
	 *      tags={"Customer"},
	 *      description="Request Type 1 = Cancel, 2 = Return, 3 = Warranty",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Set Permissions",
     *          required=false,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="request_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="customer_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="order_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="order_detail_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="product_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="merchant_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="question_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="type_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="address_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="details",
     *                  type="string"
     *              ),             
     *          )
     *      ),
     *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function storeRequest(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/customer/request-list",
	 *      summary="Get All Customer Request List",
	 *      tags={"Customer"},
	 *      description="Get All Customer Request List",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function list();
}
