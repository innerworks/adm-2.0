<?php

namespace App\Http\Controllers\Api\Interfaces;

use App\Http\Requests\Api\PasswordReset\CreateRequest;
use App\Http\Requests\Api\PasswordReset\ResetRequest;

interface PasswordResetControllerInterface
{
    /**
	 * @param string $email
	 * @return Response
	 * @SWG\Post(
	 *      path="/password/create",
	 *      summary="Create reset password",
	 *      tags={"Password Reset"},
	 *      description="Reset password",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */ 
    public function create(CreateRequest $request);

    /**
	 * @param string $token
	 * @return Response
	 * @SWG\Get(
	 *      path="/password/find/{token}",
	 *      summary="Find user from token",
	 *      tags={"Password Reset"},
	 *      description="Find user from token",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="token",
	 *          description="Activation Token",
	 *          type="string",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function find($token);

    /**
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
	 * @return Response
	 * @SWG\Post(
	 *      path="/password/reset",
	 *      summary="Reset user password",
	 *      tags={"Password Reset"},
	 *      description="Reset user password",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password",
	 *          description="Password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password_confirmation",
	 *          description="Confirm Password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	
	 *       @SWG\Parameter(
	 *          name="token",
	 *          description="Token",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	  	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */ 
    public function reset(ResetRequest $request);

    /**
	 * @param string $email
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/password/create",
	 *      summary="Create reset password",
	 *      tags={"Password Reset"},
	 *      description="Reset password",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */ 
    public function createPasswordToken(CreateRequest $request);

    /**
	 * @param string $token
	 * @return Response
	 * @SWG\Get(
	 *      path="/customer/password/find/{token}",
	 *      summary="Find customer from token",
	 *      tags={"Password Reset"},
	 *      description="Find customer from token",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="token",
	 *          description="Activation Token",
	 *          type="string",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function findPasswordToken($token);

    /**
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/password/reset",
	 *      summary="Reset customer password",
	 *      tags={"Password Reset"},
	 *      description="Reset customer password",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password",
	 *          description="Password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password_confirmation",
	 *          description="Confirm Password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	
	 *       @SWG\Parameter(
	 *          name="token",
	 *          description="Token",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),	  	 
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */ 
    public function resetPasswordToken(ResetRequest $request);
}
