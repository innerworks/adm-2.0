<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;

interface CheckoutControllerInterface
{
	/**
	 * @return Response
	 * @SWG\Get(
	 *      path="/checkout",
	 *      summary="Checkout",
	 *      tags={"Check Out"},
	 *      description="Checkout",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function index();

    /**
	 * @param string $payment_acronym
	 * @return Response
	 * @SWG\Get(
	 *      path="/checkout/placeOrder",
	 *      summary="Place Order",
	 *      tags={"Check Out"},
	 *      description="Place Order",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="payment_acronym",
	 *          description="Payment Acronym",
	 *          type="string",
	 *          required=true,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function placeOrder(Request $request);
}
