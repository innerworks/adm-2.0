<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;

interface FlashSaleControllerInterface
{
    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/flash-sale/{id}",
	 *      summary="Get Flash Sale Detail",
	 *      tags={"Flash Sale"},
	 *      description="Get Flash Sale Detail",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Flash Sale Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getFlashSaleDetails($id);

	/**
	 * @param string $title
	 * @param string $short_description
	 * @param string $start_date
	 * @param string $end_date
	 * @param boolean $status
	 * @return Response
	 * @SWG\Post(
	 *      path="/flash-sale/store",
	 *      summary="Store New Flash Sale",
	 *      tags={"Flash Sale"},
	 *      description="Store New Flash Sale",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="title",
	 *          description="Ads Title",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="short_description",
	 *          description="Ads Short Description",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="start_date",
	 *          description="Start Date",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="end_date",
	 *          description="End Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="status",
	 *          description="Status",
	 *          type="boolean",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */  
    public function store(Request $request);

    /**
	 * @param string $id
	 * @param string $title
	 * @param string $short_description
	 * @param string $start_date
	 * @param string $end_date
	 * @param boolean $status
	 * @return Response
	 * @SWG\Put(
	 *      path="/flash-sale/update",
	 *      summary="Update Flash Sale",
	 *      tags={"Flash Sale"},
	 *      description="Update Flash Sale",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="title",
	 *          description="Ads Title",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="short_description",
	 *          description="Ads Short Description",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="start_date",
	 *          description="Start Date",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="end_date",
	 *          description="End Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="status",
	 *          description="Status",
	 *          type="boolean",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function update(Request $request);

    /**
	 * @param string $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/flash-sale/delete/{id}",
	 *      summary="Delete Flash Sale",
	 *      tags={"Flash Sale"},
	 *      description="Delete Flash Sale",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Flash Sale ID",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function delete($id);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/flash-sale/list",
	 *      summary="List of Flash Sale",
	 *      tags={"Flash Sale"},
	 *      description="List of Flash Sale",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function list(Request $request);
}
