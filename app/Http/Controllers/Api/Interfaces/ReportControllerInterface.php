<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;

interface ReportControllerInterface
{
    /**
     * @param integer $id
     * @return Response
     * @SWG\Get(
     *      path="/report/sales",
     *      summary="Get Sales Report",
     *      tags={"Orders"},
     *      description="Get Sales Report",
     *      produces={"application/json"},
     *      security={
     *         {
     *             "default": {}
     *         }
     *      },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="status_code",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="resource",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function sales(Request $request);
}
