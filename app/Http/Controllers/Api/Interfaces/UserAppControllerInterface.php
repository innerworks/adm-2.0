<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Product\RatingRequest;

interface UserAppControllerInterface
{
	/**
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getAds",
	 *      summary="Get Random Ads",
	 *      tags={"User App"},
	 *      description="Get Random Ads",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getAds();

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @param string $search
	 * @param integer $user_id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/smart-search",
	 *      summary="Smart Search",
	 *      tags={"User App"},
	 *      description="Smart Search",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="user_id",
	 *          description="User ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function smartSearch(Request $request);

	/**
	 * @param integer $perPage
	 * @param integer $page
	 * @param string $search
	 * @param integer $user_id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/search",
	 *      summary="Search Products",
	 *      tags={"User App"},
	 *      description="Search Products",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="search",
	 *          description="Search String",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="user_id",
	 *          description="User ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function search(Request $request);

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getBanners",
	 *      summary="Get all Hero Banners",
	 *      tags={"User App"},
	 *      description="Get all Hero Banners",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getBanners(Request $request);

     /**
	 * @param integer $perPage
	 * @param integer $page
	 * @return Response
	 * @SWG\Get(
	 *      path="/getBrands",
	 *      summary="Get all Brands",
	 *      tags={"User App"},
	 *      description="Get all Brands",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */  
    public function getBrands(Request $request);

    /**
	 * @param integer $parentId
	 * @param integer $merchantId
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getCategories",
	 *      summary="Get all Main Categories",
	 *      tags={"User App"},
	 *      description="Get all Main Categories",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="parentId",
	 *          description="Parent ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="merchantId",
	 *          description="Merchant ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getCategories(Request $request);

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getTopPicks",
	 *      summary="Get all Top Picks Products",
	 *      tags={"User App"},
	 *      description="Get all Top Picks Products",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getTopPicks(Request $request);

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @param integer $categoryId
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getFlashSale",
	 *      summary="Get all Product On Sale",
	 *      tags={"User App"},
	 *      description="Get all Product On Sale",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="categoryId",
	 *          description="Category ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getFlashSale(Request $request);

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @param integer $categoryId
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getStores",
	 *      summary="Get all Random Stores",
	 *      tags={"User App"},
	 *      description="Get all Random Stores",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="categoryId",
	 *          description="Category ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getStores(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getStoreDetails",
	 *      summary="Get all Random Stores",
	 *      tags={"User App"},
	 *      description="Get all Random Stores",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Merchant ID",
	 *          type="integer",
	 *          required=true,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getStoreDetails(Request $request);

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @param number $user_lat
	 * @param number $user_long
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/storesNearYou",
	 *      summary="Get all Stores neer your location",
	 *      tags={"User App"},
	 *      description="Get all Random Stores",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="user_lat",
	 *          description="User Location Latitude",
	 *          type="number",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="user_long",
	 *          description="User Location Longitude",
	 *          type="number",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function storesNearYou(Request $request);

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @param integer $categoryId
	 * @param integer $merchantId
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getRandomProducts",
	 *      summary="Get all Random Products",
	 *      tags={"User App"},
	 *      description="Get all Random Stores",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="categoryId",
	 *          description="Category ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="merchantId",
	 *          description="Merchant ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getRandomProducts(Request $request);

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @param integer $categoryId
	 * @param integer $merchantId
	 * @param integer $customer_id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getProducts",
	 *      summary="Get all Products",
	 *      tags={"User App"},
	 *      description="Get all Random Stores",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="categoryId",
	 *          description="Category ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="merchantId",
	 *          description="Merchant ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="customer_id",
	 *          description="Customer ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getProducts(Request $request);

    /**
	 * @param integer $id
	 * @param integer $customer_id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getProductDetails",
	 *      summary="Get Product Details",
	 *      tags={"User App"},
	 *      description="Get Product Details",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="Product ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="customer_id",
	 *          description="Customer ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getProductDetails(Request $request);

    /**
	 * @param integer $product_id
	 * @param integer $customer_id
	 * @param number $rating
	 * @param string $review
	 * @return Response
	 * @SWG\Post(
	 *      path="/user-app/addRating",
	 *      summary="Add Rating and Review",
	 *      tags={"User App"},
	 *      description="Add Rating and Review",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="product_id",
	 *          description="Product ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="customer_id",
	 *          description="Customer ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="rating",
	 *          description="Rating",
	 *          type="number",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="review",
	 *          description="Review",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function addRating(RatingRequest $request);

    /**
	 * @return Response
	 * @SWG\Post(
	 *      path="/user-app/storeToCart",
	 *      summary="Add / Update Cart Item",
	 *      tags={"User App"},
	 *      description="Add / Update Cart Item",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Set Permissions",
     *          required=false,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="cart_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="customer_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="product_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="merchant_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="category_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="brand_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="size_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="qty",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="total_price",
     *                  type="number"
     *              ),     
     *              @SWG\Property(
     *                  property="checked",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="variant_ids",
	 *				     @SWG\Items(
     *                   	@SWG\Property(property="id", type="integer")
	 *				     ),
     *              ),
     *              @SWG\Property(
     *                  property="addons_ids",
	 *				     @SWG\Items(
     *                   	@SWG\Property(property="id", type="integer")
	 *				     )
     *              ),
     *              @SWG\Property(
     *                  property="special_instructions",
     *                  type="string"
     *              ),          
     *          )
     *      ),
     *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function storeToCart(Request $request);

    /**
	 * @param integer $merchant_id
	 * @param boolean $checked
	 * @return Response
	 * @SWG\Post(
	 *      path="/user-app/checkUncheckCart",
	 *      summary="Check / Uncheck Cart Item",
	 *      tags={"User App"},
	 *      description="Check / Uncheck Cart Item",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="merchant_id",
	 *          description="Merchant ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="checked",
	 *          description="Checked",
	 *          type="boolean",
	 *          required=true,
	 *          in="query"
	 *      ),
     *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function checkUncheckCart(Request $request);

    /**
	 * @param integer $customer_id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/clearCart",
	 *      summary="Clear Customer Carts",
	 *      tags={"User App"},
	 *      description="Clear Customer Carts",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *       },
	 *       @SWG\Parameter(
	 *          name="customer_id",
	 *          description="Customer ID",
	 *          type="integer",
	 *          required=true,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function clearCart(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/deleteCart",
	 *      summary="Delete single cart",
	 *      tags={"User App"},
	 *      description="Delete single cart",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *       },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=true,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function deleteCart(Request $request);

    /**
	 * @param integer $customer_id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/getCarts",
	 *      summary="Get Customer Carts",
	 *      tags={"User App"},
	 *      description="Get Customer Carts",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *       },
	 *       @SWG\Parameter(
	 *          name="customer_id",
	 *          description="Customer ID",
	 *          type="integer",
	 *          required=true,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getCarts(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/user-app/voucher",
	 *      summary="Get Voucher",
	 *      tags={"User App"},
	 *      description="Get Voucher",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *       },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function voucher(Request $request);

    /**
	 * @param integer $voucher_id
	 * @return Response
	 * @SWG\Post(
	 *      path="/user-app/addVoucher",
	 *      summary="Get Voucher",
	 *      tags={"User App"},
	 *      description="Get Voucher",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *       },
	 *       @SWG\Parameter(
	 *          name="voucher_id",
	 *          description="Voucher ID",
	 *          type="integer",
	 *          required=true,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function addVoucher(Request $request);
}
