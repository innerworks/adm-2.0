<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;

interface BestSellerControllerInterface
{
    /**
     * @return Response
     * @SWG\Get(
     *      path="/best-saler/generate",
     *      summary="Generate Best Seller",
     *      tags={"Best Seller"},
     *      description="Generate Best Seller",
     *      produces={"application/json"},
     *      security={
     *         {
     *             "default": {}
     *         }
     *      },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="status_code",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="resource",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function generate();
}
