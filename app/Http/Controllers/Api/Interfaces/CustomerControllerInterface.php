<?php

namespace App\Http\Controllers\Api\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Customer\AddByEmail;
use App\Http\Requests\Api\Customer\AddByMobile;
use App\Http\Requests\Api\Customer\SetAccount;
use App\Http\Requests\Api\Customer\Login;
use App\Http\Requests\Api\Customer\Social;
use App\Http\Requests\Api\Customer\UpdateProfile;
use App\Http\Requests\Api\Customer\AddAddress;


interface CustomerControllerInterface
{
    /**
	 * @param string $mobile_number
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/registerByMobile",
	 *      summary="Sign up via mobile",
	 *      tags={"Customer"},
	 *      description="Sign up via mobile",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="mobile_number",
	 *          description="Mobile Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function registerByMobile(AddByMobile $request);

    /**
	 * @param string $mobile_number
	 * @param string $code
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/validateByMobile",
	 *      summary="Validate using code",
	 *      tags={"Customer"},
	 *      description="Validate using code",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="mobile_number",
	 *          description="Mobile Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="code",
	 *          description="Validation Code",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function validateByMobile(Request $request);

    /**
	 * @param string $email
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/registerByEmail",
	 *      summary="Sign up via email",
	 *      tags={"Customer"},
	 *      description="Sign up via email",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function registerByEmail(AddByEmail $request);

    /**
	 * @param string $email
	 * @param string $code
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/validateByEmail",
	 *      summary="Validate using code",
	 *      tags={"Customer"},
	 *      description="Validate using code",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="code",
	 *          description="Validation Code",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function validateByEmail(Request $request);

    /**
	 * @param string $mobile_number
	 * @param string $email
	 * @param string $full_name
	 * @param string $password
	 * @return Response
	 * @SWG\Put(
	 *      path="/customer/setAccount",
	 *      summary="Set Account",
	 *      tags={"Customer"},
	 *      description="Set Account, set full name and password",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="mobile_number",
	 *          description="Mobile Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="full_name",
	 *          description="Full Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password",
	 *          description="Password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function setAccount(SetAccount $request);

    /**
	 * @param string $username
	 * @param string $password
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/login",
	 *      summary="Customer Login",
	 *      tags={"Customer"},
	 *      description="Customer Login",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="username",
	 *          description="User Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password",
	 *          description="Password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function login(Login $request);

    /**
	 * @param string $full_name
	 * @param string $email
	 * @param string $provider
	 * @param string $provider_id
	 * @param string $image_url
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/social",
	 *      summary="Customer Login via Social Media",
	 *      tags={"Customer"},
	 *      description="Customer Login via Social Media",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="full_name",
	 *          description="Full Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="provider",
	 *          description="Provider",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="provider_id",
	 *          description="Provider ID",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="image_url",
	 *          description="Image Url",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function social(Social $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/getProfile",
	 *      summary="Get Customer Profile",
	 *      tags={"Customer"},
	 *      description="Get Customer Profile",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="id",
	 *          type="integer",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getProfile(Request $request);

    /**
	 * @param string $email
	 * @param string $password
	 * @param string $first_name
	 * @param string $middle_name
	 * @param string $last_name
	 * @param string $suffix
	 * @param string $birth_date
	 * @param string $phone_number
	 * @param string $mobile_number
	 * @param string $image_url
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/editProfile",
	 *      summary="Customer Edit Profile",
	 *      tags={"Customer"},
	 *      description="Customer Edit Profile",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password",
	 *          description="Password",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="first_name",
	 *          description="First Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="middle_name",
	 *          description="Middle Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="last_name",
	 *          description="Last Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="suffix",
	 *          description="Suffix",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="birth_date",
	 *          description="Birth Date",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="phone_number",
	 *          description="Phone Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="mobile_number",
	 *          description="Mobile Number",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="image_url",
	 *          description="Image Url",
	 *          type="file",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function editProfile(UpdateProfile $request);

    /**
	 * @param string $email
	 * @param integer $id
	 * @param string $contact_name
	 * @param string $contact_number
	 * @param string $country_id
	 * @param string $province_id
	 * @param string $city_id
	 * @param string $barangay_id
	 * @param string $zip_code
	 * @param string $detailed_address
	 * @param boolean $default
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/saveAddress",
	 *      summary="Customer Add/Update Address",
	 *      tags={"Customer"},
	 *      description="Customer Add/Update Address",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="first_name",
	 *          description="First Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="middle_name",
	 *          description="Middle Name",
	 *          type="string",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="last_name",
	 *          description="Last Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="contact_name",
	 *          description="Contact Name",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="contact_number",
	 *          description="Contact Number",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="country_id",
	 *          description="Country ID",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="province_id",
	 *          description="Province ID",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="city_id",
	 *          description="City ID",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="barangay_id",
	 *          description="Barangay ID",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="zip_code",
	 *          description="ZIP Code",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="detailed_address",
	 *          description="Detailed Address",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="default",
	 *          description="Default",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function saveAddress(AddAddress $request);

    /**
	 * @param integer $customer_id
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/getAddresses",
	 *      summary="Get Customer Addresses",
	 *      tags={"Customer"},
	 *      description="Get Customer Addresses",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="customer_id",
	 *          description="customer_id",
	 *          type="integer",
	 *          required=true,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getAddresses(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Post(
	 *      path="/customer/getAddressById",
	 *      summary="Customer Get Address By Id",
	 *      tags={"Customer"},
	 *      description="Customer Get Address By Id",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="ID",
	 *          type="integer",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getAddressById(Request $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/customer/deleteAddress",
	 *      summary="Delete User Address",
	 *      tags={"Customer"},
	 *      description="Delete User Address",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="id",
	 *          type="integer",
	 *          required=true,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function deleteAddress(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/customer/getDetails",
	 *      summary="Get User Details",
	 *      tags={"Customer"},
	 *      description="Get User Details",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getDetails();

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @param integer $status_id
	 * @return Response
	 * @SWG\Get(
	 *      path="/customer/getOrders",
	 *      summary="Get All User Orders",
	 *      tags={"Customer"},
	 *      description="Get All User Orders",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="status_id",
	 *          description="Status ID",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getOrders();

    /**
	 * @param integer $perPage
	 * @param integer $page
	 * @return Response
	 * @SWG\Get(
	 *      path="/customer/getLikes",
	 *      summary="Get All User Likes",
	 *      tags={"Customer"},
	 *      description="Get All User Likes",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="perPage",
	 *          description="Number of record per page",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="page",
	 *          description="Page Number",
	 *          type="integer",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getLikes();
}
