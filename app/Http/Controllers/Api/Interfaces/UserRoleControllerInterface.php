<?php

namespace App\Http\Controllers\Api\Interfaces;

use App\Http\Requests\Api\UserRole\UserRoleAddRequest;

interface UserRoleControllerInterface
{
    /**
     * @param UserRoles $request
	 * @return Response
	 * @SWG\Post(
	 *      path="/user/assign_roles",
	 *      summary="Grant user to roles",
	 *      tags={"User Roles"},
	 *      description="Grant user to roles",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Set User Roles",
     *          required=false,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="user_id",
     *                  type="integer"
     *              ),     
     *              @SWG\Property(
     *                  property="roles",
     *                  type="object",
     *                  @SWG\Property(property="role_id",type="integer")
     *              )
     *          )
     *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function setUserRoles(UserRoleAddRequest $request);

    /**
	 * @param integer $id
	 * @return Response
	 * @SWG\Get(
	 *      path="/user/get_roles/{id}",
	 *      summary="Get User Roles",
	 *      tags={"User Roles"},
	 *      description="Get User Roles",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="id",
	 *          description="User Id",
	 *          type="integer",
	 *          required=true,
	 *          in="path"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */
    public function getUserRoles($id);
}
