<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\CommissionRateControllerInterface;

use App\Models\CommissionRate;

class CommissionRateController extends ApiBaseController implements CommissionRateControllerInterface
{
    /********************************************
    * 			COMMISSION RATE 			    *
    ********************************************/
    public function getCommissionRateDetails($id)
	{
		try
        {
        	$commissionRate = CommissionRate::find($id);
            return $this->response($commissionRate, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $commissionRate = new CommissionRate;
            $commissionRate->merchant_id = $request->merchant_id;
            $commissionRate->merchant_rate = $request->merchant_rate;
            $commissionRate->category_id = $request->category_id;
            $commissionRate->category_rate = $request->category_rate;
            $commissionRate->product_id = $request->product_id;
            $commissionRate->product_rate = $request->product_rate;
            $commissionRate->save();

            return $this->response($commissionRate, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $commissionRate = CommissionRate::find($request->id);
            $commissionRate->merchant_id = $request->merchant_id;
            $commissionRate->merchant_rate = $request->merchant_rate;
            $commissionRate->category_id = $request->category_id;
            $commissionRate->category_rate = $request->category_rate;
            $commissionRate->product_id = $request->product_id;
            $commissionRate->product_rate = $request->product_rate;
            $commissionRate->save();

            return $this->response($commissionRate, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $commissionRate = CommissionRate::find($id);
            $commissionRate->delete();
            return $this->response($commissionRate, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $commissionRates = CommissionRate::when(request('search'), function($query) {
            	return $query->where('products.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('merchant_meta.meta_value', 'LIKE', '%' . request('search') . '%');
            })
            ->leftJoin('merchant_meta', function( $join )
            {
                $join->on('commission_rate.merchant_id', '=', 'merchant_meta.merchant_id')
                     ->where('merchant_meta.meta_key', '=', 'store_name');
            })
            ->leftJoin('categories', 'commission_rate.category_id', '=', 'categories.id')            
            ->leftJoin('products', 'commission_rate.product_id', '=', 'products.id')
            ->select('commission_rate.*', 'merchant_meta.meta_value as merchant_name', 'categories.name as category_name', 'products.name as product_name' )        
            ->orderByDesc('commission_rate.updated_at')
            ->paginate(request('perPage'));
            return $this->response($commissionRates, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
