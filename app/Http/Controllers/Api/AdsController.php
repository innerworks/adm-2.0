<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\AdsControllerInterface;

use App\Models\Ads;

class AdsController extends ApiBaseController implements AdsControllerInterface
{
    /********************************
    * 			BANNER 			    *
    *********************************/
    public function getAdsDetails($id)
	{
		try
        {
        	$ads = Ads::find($id);
            return $this->response($ads, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $ads = new Ads;
            $ads->title = $request->title;
            $ads->short_description = $request->short_description;
            $ads->image_url = $request->image_url;
            $ads->link = $request->link;
            $ads->active = 1;
            $ads->save();

            return $this->response($ads, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $ads = Ads::find($request->id);
            $ads->title = $request->title;
            $ads->short_description = $request->short_description;
            $ads->image_url = $request->image_url;
            $ads->link = $request->link;
            $ads->active = $request->active;
            $ads->save();

            return $this->response($ads, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $ads = Ads::find($id);
            $ads->delete();
            return $this->response($ads, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $ads = Ads::when(request('search'), function($query){
                return $query->where('title', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('short_description', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($ads, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
