<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\VoucherControllerInterface;
use App\Models\Voucher;

class VoucherController extends ApiBaseController implements VoucherControllerInterface
{
    /************************************
    * 			VOUCHER 			    *
    *************************************/
    public function getVoucherDetails($id)
	{
		try
        {
        	$voucher = Voucher::find($id);
            return $this->response($voucher, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $voucher = new Voucher;
            $voucher->title = $request->title;
            $voucher->code = $request->code;
            $voucher->description = $request->description;
            $voucher->discount_percent = $request->discount_percent;
            $voucher->discount_price = $request->discount_price;
            $voucher->start_date = $request->start_date;
            $voucher->end_date = $request->end_date;
            $voucher->merchant_id = $request->merchant_id;
            $voucher->voucher_stock = $request->voucher_stock;
            $voucher->is_unlimited = $request->is_unlimited;
            $voucher->active = 1;
            $voucher->save();

            return $this->response($voucher, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $voucher = Voucher::find($request->id);
            $voucher->title = $request->title;
            $voucher->code = $request->code;
            $voucher->description = $request->description;
            $voucher->discount_percent = $request->discount_percent;
            $voucher->discount_price = $request->discount_price;
            $voucher->start_date = $request->start_date;
            $voucher->end_date = $request->end_date;
            $voucher->merchant_id = $request->merchant_id;
            $voucher->voucher_stock = $request->voucher_stock;
            $voucher->is_unlimited = $request->is_unlimited;
            $voucher->active = $request->active;
            $voucher->save();

            return $this->response($voucher, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $voucher = Voucher::find($id);
            $voucher->delete();
            return $this->response($voucher, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $vouchers = Voucher::when(request('search'), function($query){
                return $query->where('title', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('short_description', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($vouchers, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
