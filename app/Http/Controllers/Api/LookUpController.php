<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\LookUpControllerInterface;

use App\Models\Country;
use App\Models\Region;
use App\Models\Province;
use App\Models\City;
use App\Models\Barangay;
use App\Models\ZipCode;
use App\Models\Merchant;
use App\Models\Courier;

class LookUpController extends ApiBaseController implements LookUpControllerInterface
{
    
    public function getCountries()
    {
        try
        {
            $country = Country::get();

            return $this->response($country, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getRegions()
    {
        try
        {
            $region = Region::get();

            return $this->response($region, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getProvinces(Request $request)
    {
        try
        {
            $province = Province::when(request('regionId'), function($query){
                return $query->where('region_id', '=', request('regionId'));
            })
            ->orderBy('province_name', 'asc')
            ->get();

            return $this->response($province, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getCities(Request $request)
    {
    	try
        {
        	$city = City::when(request('regionId'), function($query){
                return $query->where('region_id', request('regionId'));
            })
            ->when(request('province_id'), function($query){

	            return $query->where('province_id', request('province_id'));
	        })
            ->orderBy('city_municipality_name', 'asc')
            ->get();

            return $this->response($city, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getBarangays(Request $request)
    {
        try
        {
            $barangay = Barangay::when(request('regionId'), function($query){
                return $query->where('region_id', request('regionId'));
            })
            ->when(request('province_id'), function($query){
                return $query->where('province_id', request('province_id'));
            })
            ->when(request('city_id'), function($query){
                return $query->where('city_id', request('city_id'));
            })
            ->get();

            return $this->response($barangay, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function checkZipCode(Request $request)
    {
        try
        {
            $zipCode = ZipCode::where('zip_code', request('zip_code'))->first();
            return $this->response($zipCode, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getMerchants()
    {
        try
        {
            $merchant = Merchant::where('status', 3)->orderBy('username')->get();
            return $this->response($merchant, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getCouriers()
    {
        try
        {
            $courier = Courier::where('is_active', 1)
                               ->where('is_consumer', 1)
                               ->get();
            return $this->response($courier, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
