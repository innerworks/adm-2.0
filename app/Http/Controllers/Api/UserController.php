<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\UserControllerInterface;
use App\Http\Requests\Api\User\UserUpdateRequest;
use App\Http\Requests\Api\User\UserAddRequest;
use App\Notifications\AddNewUser;
use App\Services\Helpers\PasswordHelper;
use App\Models\User; 
use App\Models\UserMeta; 
use Carbon\Carbon;

class UserController extends ApiBaseController implements UserControllerInterface
{
    /****************************
    * 			USER 			*
    ****************************/
    
    public function getUserDetails($id)
	{
		try
        {
        	$user = User::find($id);
            return $this->response($user, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(UserAddRequest $request)
    {
        try
        {
            $salt = bcrypt(PasswordHelper::generateSalt());

            $user = new User;
            $user->name = $request->userName;
            $user->email = $request->email;
            $user->salt = $salt;
            $user->active = 1;
            $user->password = PasswordHelper::generate($salt, $request->password);
            $user->save();

            $user->assignRole($request->role);

            $other_details = ["first_name" => $request->firstName, "last_name" => $request->lastName];
            $user->addMeta($other_details);

            if($request->emailNotification)
                $user->notify(new AddNewUser($request->password));

            return $this->response($user, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(UserUpdateRequest $request)
    {
        try
        {
            $user = User::find($request->id);
            $user->name = $request->userName;
            $user->email = $request->email;
            $user->active = $request->isActive;

            if($request->password)
                $user->password = PasswordHelper::generate($user->salt, $request->password);
            
            $user->save();

            $user->assignRole($request->role);

            $other_details = ["first_name" => $request->firstName, "last_name" => $request->lastName];
            $user->addMeta($other_details);

            if($request->emailNotification)
                $user->notify(new AddNewUser($request->password));

            return $this->response($user, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $user = User::find($id);
            $user->delete();
            return $this->response($user, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $user = User::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('email', 'LIKE', '%' . request('search') . '%');
            })
            ->where('name', '<>', 'Administrator')
            ->select('id' ,'name', 'email', 'active', 'created_at')
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($user, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
