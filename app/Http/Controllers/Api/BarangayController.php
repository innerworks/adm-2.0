<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\BarangayControllerInterface;

use App\Models\Barangay;

class BarangayController extends ApiBaseController implements BarangayControllerInterface
{
    //
}
