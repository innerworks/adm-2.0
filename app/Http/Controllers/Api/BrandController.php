<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Requests\Api\Brand\BrandAddRequest;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\BrandControllerInterface;
use App\Imports\BrandsImport;
use App\Exports\BrandsExport;
use App\Models\Brand;

use Storage;

class BrandController extends ApiBaseController implements BrandControllerInterface
{
    /********************************
    * 			BRANDS 			    *
    *********************************/
    public function getBrand($id)
	{
		try
        {
        	$brand = Brand::find($id);
            return $this->response($brand, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(BrandAddRequest $request)
    {
        try
        {
            $brand = new Brand;
            $brand->name = $request->name;
            $brand->slug_name = $request->slug_name;
            $brand->distribution_type = $request->distribution_type;
            $brand->image_url = $request->image_url;
            $brand->active = 1;
            $brand->save();

            return $this->response($brand, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(BrandAddRequest $request)
    {
        try
        {
            $active = 1;
            if($request->isActive == 'false' || $request->isActive == false) {
	            $active = 0;
            }

            $brand = Brand::find($request->id);
            $brand->name = $request->name;
            $brand->slug_name = $request->slug_name;
            $brand->distribution_type = $request->distribution_type;
            $brand->image_url = $request->image_url;
            $brand->active = $request->isActive;

            $brand->save();

            return $this->response($brand, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $brand = Brand::find($id);
            $brand->delete();
            return $this->response($brand, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $brand = Brand::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('slug_name', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($brand, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDropdown()
    {
        try
        {
            $brand = Brand::orderBy('name')->get();
            return $this->response($brand, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function batchUpload(Request $request)
    {
        try
        {
        	Excel::import(new BrandsImport, $request->file('file'));
        	return $this->response(true, 'Successfully Retreived!', $this->successStatus);	
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function downloadCsv()
    {
	    try
        {
        	$directory = 'public/export/brand/';
        	$files = Storage::files($directory);
        	foreach ($files as $file) {
        		Storage::delete($file);
        	}

	    	$filename = "Brands-".date("Y-m-d").".csv";
	    	// Store on default disk
		    Excel::store(new BrandsExport, $directory.$filename);

		    $data = [
		    	'filepath' => '/Storage/export/brand/'.$filename,
		    	'filename' => $filename
		    ];
		    
		    if(Storage::exists($directory.$filename))
		    	return $this->response($data, 'Successfully Retreived!', $this->successStatus);	

	    	return $this->response(false, 'Successfully Retreived!', $this->successStatus);	
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
