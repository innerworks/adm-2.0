<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\UserRoleControllerInterface;
use App\Http\Requests\Api\UserRole\UserRoleAddRequest;
use App\Models\UserRole;

class UserRoleController extends ApiBaseController implements UserRoleControllerInterface
{
	/********************************
    * 			USER ROLES 			*
    ********************************/
    public function setUserRoles(UserRoleAddRequest $request)
    {
    	try
        {
            $success = UserRole::assignUserRoles($request->all());
            return $this->response($success, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getUserRoles($id)
    {
        try
        {
            $roles = UserRole::where('user_id', $id)->get();
            return $this->response($roles, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
