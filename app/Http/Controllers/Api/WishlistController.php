<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\WishlistControllerInterface;
use App\Models\Wishlist;

class WishlistController extends ApiBaseController implements WishlistControllerInterface
{
    public function likeProduct(Request $request)
    {
        try
        {
	        $user = auth()->user();

            $where = [
                ['product_id', $request->product_id],
                ['customer_id', $user->id]        
            ];            
            $wishlist = Wishlist::where($where)->first();
            if(!$wishlist)
                $wishlist = new Wishlist;
            
            $wishlist->product_id = $request->product_id;
            $wishlist->customer_id = $user->id;
            $wishlist->save();

            return $this->response($wishlist, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function unlikeProduct($id)
    {
        try
        {
            $wishlist = Wishlist::find($id);
            $wishlist->delete();
            return $this->response($wishlist, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }

    }
}
