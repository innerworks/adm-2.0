<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\Api\BannerAd\AddRequest;
use App\Http\Requests\Api\BannerAd\UpdateRequest;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\BannerAdControllerInterface;

use App\Models\BannerAds;

class BannerAdController extends ApiBaseController implements BannerAdControllerInterface
{
    /********************************
    * 			BANNER 			    *
    *********************************/
    public function getBannerDetails($id)
	{
		try
        {
        	$bannerAds = BannerAds::find($id);
            return $this->response($bannerAds, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(AddRequest $request)
    {
        try
        {
            $bannerAds = new BannerAds;
            $bannerAds->title = $request->title;
            $bannerAds->short_description = $request->short_description;
            $bannerAds->image_url = $request->image_url;
            $bannerAds->link = $request->link;
            $bannerAds->active = 1;
            $bannerAds->save();

            return $this->response($bannerAds, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(UpdateRequest $request)
    {
        try
        {
            $bannerAds = BannerAds::find($request->id);
            $bannerAds->title = $request->title;
            $bannerAds->short_description = $request->short_description;
            $bannerAds->image_url = $request->image_url;
            $bannerAds->link = $request->link;
            $bannerAds->active = ($request->active == 'true') ? true : false;
            $bannerAds->save();

            return $this->response($bannerAds, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $bannerAds = BannerAds::find($id);
            $bannerAds->delete();
            return $this->response($bannerAds, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $bannerAds = BannerAds::when(request('search'), function($query){
                return $query->where('title', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('short_description', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($bannerAds, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
