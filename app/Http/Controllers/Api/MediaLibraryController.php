<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\MediaLibraryControllerInterface;
use App\Models\MediaLibrary;

use Storage;
use Image;

class MediaLibraryController extends ApiBaseController implements MediaLibraryControllerInterface
{
    /************************************
    * 			MEDIA LIBRARY 			*
    ************************************/
    public function getMediaDetails($id)
	{
		try
        {
        	$media = MediaLibrary::find($id);
            return $this->response($media, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
        	if (count($request->images)) {
	            foreach ($request->images as $image) {

	            	$data = getimagesize($image);
		            $media = new MediaLibrary;
		            $media->file_type = $image->getClientMimeType();
		            $media->file_size = $image->getSize();
		            $media->dimensions = $data[0].' x '.$data[1];
                    $media->merchant_id = $request->merchant_id;                    
		            $media->save();

		            $media->upload($image);
	            }
	        }
        	return $this->response($media, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function delete($id)
    {
        try
        {
            $media = MediaLibrary::find($id);
            $media->delete();
            return $this->response($media, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $media = MediaLibrary::when(request('search'), function($query){
                return $query->where('file_name', 'LIKE', '%' . request('search') . '%');
            })
            ->when(request('merchant_id'), function($query) {
                return $query->where('merchant_id', request('merchant_id'));
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($media, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
