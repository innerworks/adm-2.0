<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\ReportControllerInterface;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\OrderTrackingStatus;
use App\ViewModels\OrderDetailsViewModel;

use App\Exports\SalesReportExport;
use Carbon\Carbon;
use Storage;

class ReportController extends ApiBaseController implements ReportControllerInterface
{
    public function sales(Request $request) 
    {
        try
        {

            $filters = json_decode($request->filters, true);
            $date = explode(" - ",$filters['date_range']);

            $sales = OrderDetailsViewModel::when($filters['category_id'], function($query) use ($filters){
                return $query->where('category_id', $filters['category_id']);
            })
            ->when($filters['brand_id'], function($query)  use ($filters){
                return $query->where('brand_id', $filters['brand_id']);
            })
            ->when($filters['merchant_id'], function($query)  use ($filters){
                return $query->where('merchant_id', $filters['merchant_id']);
            })
            ->when($filters['status'], function($query)  use ($filters){
                return $query->where('order_status_id', $filters['status']);
            })
            ->when($filters['date_range'], function($query)  use ($date){
                return $query->where('updated_at','>=', Carbon::create($date[0])->format('Y-m-d')." 00:00:00")
                             ->where('updated_at','<=', Carbon::create($date[1])->format('Y-m-d')." 23:59:59");
            })
            ->orderByDesc('created_at')
            ->paginate(request('perPage'));

            return $this->response($sales, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function downloadCsv(Request $request)
    {
        try
        {

            $filters = json_decode($request->filters, true);
            $date = explode(" - ",$filters['date_range']);

            $sales = OrderDetailsViewModel::when($filters['category_id'], function($query) use ($filters){
                return $query->where('category_id', $filters['category_id']);
            })
            ->when($filters['brand_id'], function($query)  use ($filters){
                return $query->where('brand_id', $filters['brand_id']);
            })
            ->when($filters['merchant_id'], function($query)  use ($filters){
                return $query->where('merchant_id', $filters['merchant_id']);
            })
            ->when($filters['status'], function($query)  use ($filters){
                return $query->where('order_status_id', $filters['status']);
            })
            ->when($filters['date_range'], function($query)  use ($date){
                return $query->where('updated_at','>=', Carbon::create($date[0])->format('Y-m-d')." 00:00:00")
                             ->where('updated_at','<=', Carbon::create($date[1])->format('Y-m-d')." 23:59:59");
            })
            ->orderByDesc('created_at')
            ->get();

            $directory = 'public/export/sales/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = "Sales-".date("Y-m-d").".csv";
            // Store on default disk
            Excel::store(new SalesReportExport($sales), $directory.$filename);

            $data = [
                'filepath' => '/Storage/export/sales/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getSales(Request $request)
    {
        try
        {

            $filters = json_decode($request->filters, true);
            $date = explode(" - ",$filters['date_range']);

            $sales = OrderDetailsViewModel::when($filters['category_id'], function($query) use ($filters){
                return $query->where('category_id', $filters['category_id']);
            })
            ->when($filters['brand_id'], function($query)  use ($filters){
                return $query->where('brand_id', $filters['brand_id']);
            })
            ->when($filters['merchant_id'], function($query)  use ($filters){
                return $query->where('merchant_id', $filters['merchant_id']);
            })
            ->when($filters['status'], function($query)  use ($filters){
                return $query->where('order_status_id', $filters['status']);
            })
            ->when($filters['date_range'], function($query)  use ($date){
                return $query->where('updated_at','>=', Carbon::create($date[0])->format('Y-m-d')." 00:00:00")
                             ->where('updated_at','<=', Carbon::create($date[1])->format('Y-m-d')." 23:59:59");
            })
            ->orderByDesc('created_at')
            ->get();

            return $this->response($sales, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
