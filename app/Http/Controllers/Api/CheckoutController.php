<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\CheckoutControllerInterface;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Product\RatingRequest;
use App\Services\Helpers\MrSpeedyHelper;
use App\Services\Helpers\PaymentGatewayHelper;

use Config;
use App\Models\Cart;
use App\Models\Order;
use App\Models\PaymentType;
use App\Models\CustomerAddress;
use App\Models\OrderDetail;
use App\Models\OrderTrackingStatus;
use App\Models\Merchant;
use App\Models\MerchantMeta;
use App\Models\MrspeedyOrder;
use App\Models\OrderPromoVoucher;
use App\Models\CommissionRate;

use Carbon\Carbon;

class CheckoutController extends ApiBaseController implements CheckoutControllerInterface
{
    private $mrSpeedy;

    public function __construct() {
        $this->mrSpeedy = new MrSpeedyHelper();    
    }

    public function index()
    {
        try
        {
            $user = auth()->user()->get();

            $carts = $user->carts()->where('checked', true)->get();

            if(count($carts) == 0) 
                return $this->response(null, 'Cart is Empty', $this->successStatus);            

            $carts = Cart::customGroup($carts);
            $total = Cart::getTotals($carts);

            $addresses = $user->getAddresses();
            if(count($addresses) == 0) 
                return $this->response(null, 'Address is Empty', $this->successStatus);                            

            $defaultAddress = $addresses->where('default_shipping', 1)->first();
            $paymentType = PaymentType::get();
            $data = [
                'user' => [
                    'full_name' => $user->first_name. ' '. $user->last_name,
                    'email' => $user->email,
                ],
                'billing_address' => [
                    'id' => $defaultAddress->id,
                    'name' => $defaultAddress->name,
                    'email' => $user->email,
                    'address' => $defaultAddress->detailed_address,
                    'mobile_number' => $defaultAddress->mobile_number,
                    'country_id' => $defaultAddress->country_id,
                    'country_name' => $defaultAddress->country_name,
                    'region_id' => $defaultAddress->region_id,
                    'region_name' => $defaultAddress->region_name,
                    'province_id' => $defaultAddress->province_id,
                    'province_name' => $defaultAddress->province_name,
                    'city_id' => $defaultAddress->city_id,
                    'city_name' => $defaultAddress->city_name,
                    'barangay_id' => $defaultAddress->barangay_id,
                    'barangay_name' => $defaultAddress->barangay_name,
                ],
                'shipping_address' => [
                    'id' => $defaultAddress->id,
                    'name' => $defaultAddress->name,
                    'email' => $user->email,
                    'address' => $defaultAddress->detailed_address,
                    'mobile_number' => $defaultAddress->mobile_number,
                    'country_id' => $defaultAddress->country_id,
                    'country_name' => $defaultAddress->country_name,
                    'region_id' => $defaultAddress->region_id,
                    'region_name' => $defaultAddress->region_name,
                    'province_id' => $defaultAddress->province_id,
                    'province_name' => $defaultAddress->province_name,
                    'city_id' => $defaultAddress->city_id,
                    'city_name' => $defaultAddress->city_name,
                    'barangay_id' => $defaultAddress->barangay_id,
                    'barangay_name' => $defaultAddress->barangay_name,
                ],
                'payment_type' => $paymentType,
                'carts' => $carts,
                'meta' => $total,
            ];

            return $this->response($data, 'Successfully Deleted!', $this->successStatus);            
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function placeOrder(Request $request)
    {
    	try
        {
	    	$user = auth()->user();
            $orderNum = Order::generateOrderNumber();
            $carts = $user->carts()->where('checked', true)->get();
            $carts = Cart::customGroup($carts);

            $errors = $this->validatePlaceOrder($carts, $request);
            if($errors) {
                return $this->mrSpeedy->errors($errors['parameter_warnings']);
            }

            if(is_array($carts) && count($carts) > 0) {

                $voucher = null;

                $order = $user->order()->create([
                    'order_number' => $orderNum,
                    'voucher_discount_price' => $voucher,
                    'order_date' => Carbon::now(),
                    'payment_type_id' => (request('payment_acronym') == 'cod') ? 5 : null,
                    'status_id' => (request('payment_acronym') == 'cod') ? 1 : null,
                ]);

                foreach ($carts as $cart) {
                    $mrSpeedyOrder = $this->mrSpeedyPlaceOrder($cart, $request);
                    $this->createMrSpeedyOrder($mrSpeedyOrder);

                    $mrSpeedyOrderId = $mrSpeedyOrder['order']['order_id'];                  
                    foreach ($cart->items as $key => $item) {

                        $shipping_fee = null;
                        if($key == 0) 
                            $shipping_fee = $cart->meta['total_shipping_fee'];

                        $orderDetail = $this->createOrderDetail($order, $item, $shipping_fee, $mrSpeedyOrderId);

                        OrderTrackingStatus::create([
                            'order_id' => $order->id,
                            'order_detail_id' => $orderDetail->id,
                            'order_status_id' => 7,
                            'merchant_id' => $cart->merchant_id,
                            'courier_id' => 1,
                            'tracking_number' => $mrSpeedyOrderId,
                        ]);
                    }
                }

                $message = 'Success Placed Order.';
                if(request('payment_acronym') != 'cod') {
                    $data = $this->setPayment($order->order_number, request('payment_acronym'));
                }
                else {
                    $data = [
                        'order' => Order::where('order_number', $orderNum)->first()
                    ];             
                }
                $carts = $user->carts()->get();
                $cartIds = $carts->map(function($item){
                    return [$item['id']];
                });
                Cart::whereIn('id', $cartIds)->delete();                    
            }
            else {
                $message = 'Cart is Empty.';
                $data = [];                
            }

            return $this->response($data, $message, $this->successStatus);            
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function createOrderDetail($order, $value, $shipping_fee=null, $mrSpeedyOrderId)
    {
        $user = auth()->user();
        $address = $user->getAddresses()->where('default_shipping', 1)->first();

        $variants_price = 0;
        $variants_name = '';
        $variants_image = '';       
        if(is_array($value->variants) && count($value->variants)) {
            foreach ($value->variants as $variant) {
                $variants_price += $variant->price;

                if(!$variants_name)
                    $variants_name .= $variant->variant;
                else
                    $variants_name .= '|'.$variant->variant;                    
    
                if($variant->image_gallery)
                    $variants_image = $variant->image_gallery[0]['image'];
            }
        } 

        $add_ons_price = 0;
        if(is_array($value->addOns) && count($value->addOns)) {
            foreach ($value->addOns as $addOn) {
                $add_ons_price += $addOn->price;
            }
        }

        return $order->orderDetail()->create([
            'merchant_id'           => $value->merchant_id,
            'category_id'           => $value->category_id,
            'brand_id'              => $value->brand_id,
            'shipping_address_id'   => $address->id,
            'product_id'            => $value->product_id,
            'product_name'          => $value->product_name,
            'product_image'         => $variants_image ? $variants_image : $value->product_image,
            'size_id'               => $value->size_id,
            'variant_ids'           => $value->variant_ids ? json_encode($value->variant_ids) : null,
            'add_ons_ids'           => $value->addons_ids ? json_encode($value->addons_ids) : null,
            'base_price'            => $value->product_base_price,
            'selling_price'         => $value->product_selling_price,
            'variants_price'        => $variants_price,
            'add_ons_price'         => $add_ons_price,
            'qty'                   => $value->qty,
            'total_price'           => $value->total_price,
            'shipping_fee'          => $shipping_fee,
            'commision_rate'        => $this->getCommissionRate($value),
            'mrspeedy_order_id'     => $mrSpeedyOrderId,
            'delivery_status_id'    => null,
            'shipped_date'          => null,
            'received_date'         => null,
            'order_status_id'       => 0,
            'special_instructions'  => $value->special_instructions,
        ]);
    }

    public function getCommissionRate($item)
    {
        $commissionRate = 0;

        $merchantRate = CommissionRate::where('merchant_id', $item->merchant_id)
            ->first();

        $productRate = CommissionRate::where('product_id', $item->product_id)
            ->first();
        
        $categoryRate = CommissionRate::where('category_id', $item->category_id)
            ->where('merchant_id', $item->merchant_id)
            ->first();

        if($productRate){
            $commissionRate = $productRate['product_rate'];
        } else if ($categoryRate){
            $commissionRate = $productRate['category_rate'];
        } else {
            $commissionRate = $merchantRate['merchant_rate'];
        }
        
        return $commissionRate;
    }

    public function validatePlaceOrder($carts, $request)
    {
        $errors = array();
        foreach ($carts as $key => $value) {
            $mrSpeedyOrder = $this->mrSpeedyPlaceOrder($value, $request, true);
            if($mrSpeedyOrder['warnings']) {
                $errors['warnings'] = $mrSpeedyOrder['warnings'];
                $errors['parameter_warnings'] = $mrSpeedyOrder['parameter_warnings'];
            }
        }
        return $errors;
    }

    public function mrSpeedyPlaceOrder($params, $request, $validate = false)
    {
        $merchant = Merchant::find($params->merchant_id);

        $user = auth()->user();
        $customerAddress = $user->getAddresses()->where('default_shipping', 1)->first();

        $packages = array();
        $productNames = '';
        foreach ($params->items as $item) {
            $packageObject = new \stdClass;
            $packageObject->description = $item->product_name.' ( Size '.$item->size. ' )';
            $packageObject->items_count = $item->qty;
            $packages[] = $packageObject;

            if($productNames)
                $productNames .= " | ".$item->product_name.' ( Size '.$item->size. ' )';
            $productNames .= $item->product_name.' ( Size '.$item->size. ' )';
        }

        // CHECK IF PAYMENT TYPE IS COD OR CC
        $buyout_amount = 0;
        $taking_amount = 0;

        if(request('payment_acronym') == 'cod')
            $taking_amount = $params->meta['grand_total'];

        if($merchant->category_id == 1 && request('payment_acronym') == 'cod')
            $buyout_amount = $params->meta['total_price'];

        // check if there's a selection of vehicle
        // 7 = Car
        // 8 = Motorbike
        $vehicle_type_id = 8;

        // MR.SPEEDY API CLASS
        $data = [
                'matter' => $productNames,
                'payment_method' => 'non_cash',
                'vehicle_type_id' => $vehicle_type_id,
                'points' => [            
                    [ 
                        'address' => $merchant->details['store_address'], 
                        'contact_person' => [ 
                            'name' => $merchant->details['first_name']. ' '.$merchant->details['last_name'], 
                            'phone' =>$merchant->details['mobile'], 
                        ],                        
                        'taking_amount' => 0,
                        'buyout_amount' => $buyout_amount,
                        'note' => $merchant->details['store_address'],
                        // 'packages' => $packages,
                    ], 
                    [ 
                        'address' => $customerAddress->detailed_address, 
                        'contact_person' => [ 
                            'name' => $customerAddress->name, 
                            'phone' => $customerAddress->mobile_number, 
                        ],                           
                        'taking_amount' => $taking_amount, 
                        'buyout_amount' => 0, 
                        'note' => $customerAddress->detailed_address,
                        // 'packages' => $packages,
                    ],
                ]
            ];

        if($validate) {
            return $this->mrSpeedy->calculateOrder($data);
        }
        else {
            return $responce = $this->mrSpeedy->createOrder($data);
        }
    }

    public function createMrSpeedyOrder($mrSpeedyOrder)
    {
        MrspeedyOrder::create([
            'order_id'                          => $mrSpeedyOrder['order']['order_id'], 
            'order_name'                        => $mrSpeedyOrder['order']['order_name'], 
            'vehicle_type_id'                   => $mrSpeedyOrder['order']['vehicle_type_id'], 
            'status'                            => $mrSpeedyOrder['order']['status'], 
            'status_description'                => $mrSpeedyOrder['order']['status_description'], 
            'matter'                            => $mrSpeedyOrder['order']['matter'], 
            'points'                            => json_encode($mrSpeedyOrder['order']['points']), 
            'payment_amount'                    => $mrSpeedyOrder['order']['payment_amount'], 
            'delivery_fee_amount'               => $mrSpeedyOrder['order']['delivery_fee_amount'], 
            'intercity_delivery_fee_amount'     => $mrSpeedyOrder['order']['intercity_delivery_fee_amount'],
            'weight_fee_amount'                 => $mrSpeedyOrder['order']['weight_fee_amount'], 
            'insurance_amount'                  => $mrSpeedyOrder['order']['insurance_amount'],
            'insurance_fee_amount'              => $mrSpeedyOrder['order']['insurance_fee_amount'],
            'loading_fee_amount'                => $mrSpeedyOrder['order']['loading_fee_amount'],
            'money_transfer_fee_amount'         => $mrSpeedyOrder['order']['money_transfer_fee_amount'],
            'suburban_delivery_fee_amount'      => $mrSpeedyOrder['order']['suburban_delivery_fee_amount'],
            'overnight_fee_amount'              => $mrSpeedyOrder['order']['overnight_fee_amount'],
            'discount_amount'                   => $mrSpeedyOrder['order']['discount_amount'],
            'backpayment_amount'                => $mrSpeedyOrder['order']['backpayment_amount'],
            'cod_fee_amount'                    => $mrSpeedyOrder['order']['cod_fee_amount'],
            'payment_method'                    => $mrSpeedyOrder['order']['payment_method']
        ]);
    }

    public function setPayment($order_number, $payment_type)
    {
        if(app()->environment() == 'local' || app()->environment() == 'development'){
            $paynamicsUrl = Config::get('constant.PAYNAMICS.FORM_ACTION_URL');
        }else{
            $paynamicsUrl = Config::get('constant.PAYNAMICS_PROD.FORM_ACTION_URL');
        }

        $getSum = [];
        $shippingFee = [];
        $response = 'In Progress';

        $user = auth()->user();
        $addresses = $user->getAddresses();

        $order = Order::where('order_number', $order_number)
                      ->where('customer_id', $user->id)
                      ->first();        

        $billingAddress = $addresses->where('default_shipping', 1)->first();

        $orderPromoVouchers = OrderPromoVoucher::where('order_id', $order->id)
                                                ->where('voucher_id', '!=', NULL)
                                                ->first();
        if($orderPromoVouchers){
            $isVoucher = $orderPromoVouchers->discount_price;
        }else{
            $isVoucher = NULL;
        }

        foreach ($order->order_details as $total){
            $getSum[] = $total->total_price;
            if($total->shipping_fee)
                $shippingFee[] = $total->shipping_fee;
        }
        $totalDiscount = number_format($isVoucher,2, '.', '');

        $sumShipFee = number_format(array_sum($shippingFee),2, '.', '');
        
        $totalAmtFormat = number_format(array_sum($getSum) + array_sum($shippingFee),2, '.', '');

        if($payment_type != 'cod') {
            $paynamics = new PaymentGatewayHelper();
            // SET DATA
            $paynamics->_currency = 'PHP';
            $paynamics->_country = 'PH';
            $paynamics->_paymentMethod = $payment_type;
            $paynamics->_totalAmountFormat = $totalAmtFormat;
            $paynamics->_userDetails = $user;
            $paynamics->_orderBillingAddress = $billingAddress;
            $paynamics->_orderDetails = $order->order_details;
            $paynamics->_totalShippingFee = $sumShipFee;
            $paynamics->_totalDsicount = $totalDiscount;
            $paynamics->_order = $order;
            $paynamics->_shippingFee = $shippingFee;

            $b64string = $paynamics->makeRequestPaynamics();   
        }

        return [
            'paynamicsUrl' => $paynamicsUrl,
            'responseCode' => $b64string
        ];
    }

}
