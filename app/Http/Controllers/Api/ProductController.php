<?php

namespace App\Http\Controllers\Api;

use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Product\AddRequest;
use App\Http\Requests\Api\Product\UpdateRequest;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\ProductControllerInterface;
use App\Imports\ProductImport;
use App\Exports\ProductExport;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\TagSearch;
use App\Models\ElasticProduct;

use Illuminate\Support\Facades\DB;
use Storage;

class ProductController extends ApiBaseController implements ProductControllerInterface
{
    /************************************
    *           PRODUCTS                *
    ************************************/
    public function getProduct($id)
    {
        try
        {
            $product = Product::find($id);
            return $this->response($product, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function store(AddRequest $request)
    {
        try
        {
            $product = new Product;
            $product->sku = $request->sku;
            $product->name = $request->name;
            $product->description = $request->description;
            $product->permalink = $this->generateLinkToken($request->name);
            $product->status = ($request->status == 'true') ? true : false;
            $product->merchant_id = $request->merchant_id;
            $product->category_id = $request->category_id;
            $product->brand_id = $request->brand_id;
            $product->base_price = $request->base_price;
            $product->selling_price = $request->selling_price;
            $product->weight = $request->weight;
            $product->length = $request->length;
            $product->width = $request->width;
            $product->height = $request->height;
            $product->other_information = $request->other_information;
            $product->track_inventories = ($request->trackInventory == 'true') ? true : false;
            $product->primary_photo = $request->primary_image;
            $product->save();

            // tag search mapping
            $search_tag = "";
            foreach ($request->tags as $tag) {
                $search_tag .= $tag['tag_name'].',';
            }

            $tag_search = new TagSearch;
            $tag_search->product_id = $product->id;
            $tag_search->search_tag = $search_tag;
            $tag_search->save();
            // end tag searh mapping

            if($request->variants)
                $product->addVariant($request->variants);

            if($request->addons)
                $product->addAddOns($request->addons);

            if($request->tags)
                $product->addTag($request->tags);

            if($request->inventories)
                $product->addInvetories($request->inventories);            

            $details = [
                'addons' => json_encode($request->addons),
                'inventories' => json_encode($request->inventories),
                'sizes' => json_encode($request->sizes),
                'tags' => json_encode($request->tags),
                'variants' => json_encode($request->variants),
                'gallery' => json_encode($request->gallery)                
            ];
            $product->addMeta($details);

            return $this->response($product, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(UpdateRequest $request)
    {
        try
        {
            $product = Product::find($request->id);
            $product->sku = $request->sku;
            $product->name = $request->name;
            $product->description = $request->description;
            $product->permalink = $this->generateLinkToken($request->name);
            $product->status = ($request->status == 'true' || $request->status == 1) ? true : false;
            $product->merchant_id = $request->merchant_id;
            $product->category_id = $request->category_id;
            $product->brand_id = $request->brand_id;
            $product->base_price = $request->base_price;
            $product->selling_price = $request->selling_price;
            $product->weight = $request->weight;
            $product->length = $request->length;
            $product->width = $request->width;
            $product->height = $request->height;
            $product->other_information = $request->other_information;
            $product->track_inventories = ($request->trackInventory == 'true' || $request->trackInventory == 1) ? true : false;
            $product->primary_photo = $request->primary_image;
            $product->save();

            // tag search mapping
            $search_tag = "";
            foreach ($request->tags as $tag) {
                $search_tag .= $tag['tag_name'].',';
            }

            $tag_search = TagSearch::where('product_id', $product->id)->update([
                'product_id' => $product->id,
                'search_tag' => $search_tag
            ]);
            // end tag searh mapping

            if($request->variants)
                $product->addVariant($request->variants);

            if($request->addons)
                $product->addAddOns($request->addons);

            if($request->tags)
                $product->addTag($request->tags);

            if($request->inventories)
                $product->addInvetories($request->inventories);            

            $details = [
                'addons' => json_encode($request->addons),
                'inventories' => json_encode($request->inventories),
                'sizes' => json_encode($request->sizes),
                'tags' => json_encode($request->tags),
                'variants' => json_encode($request->variants),
                'gallery' => json_encode($request->gallery)                
            ];
            $product->addMeta($details);

            $this->updateElasticProduct($request->id);

            return $this->response($product, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function updateElasticProduct($id)
    {
        $product = ElasticProduct::where('id', $id)->first();

        $curl   = curl_init();
        $header = [
            'Content-Type:application/json',
            'Authorization: Basic '. base64_encode(env('ELASTICSEARCH_USERNAME').":".env('ELASTICSEARCH_PASSWORD'))
        ];

        $body = '
            {
              "doc": {
                "name": "'.$product->name.'",
                "status": "'.$product->status.'",
                "brand": "'.$product->brand.'",
                "category": "'.$product->category.'",
                "subcategory": "'.$product->subcategory.'",
                "merchant": "'.$product->merchant.'",
                "tags": "'.$product->tags.'"
              },
              "doc_as_upsert": true
            }

        ';

        curl_setopt($curl, CURLOPT_URL, "https://search-opensearch-wyffscjbv2bc3ywldtadggchzi.ap-southeast-1.es.amazonaws.com/testsearch1/products/".$id."/_update");
        curl_setopt($curl,CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        $result = curl_exec($curl);
        curl_close($curl);
    }

    public function delete($id)
    {
        try
        {
            $product = Product::find($id);
            $product->delete();
            return $this->response($product, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $filters = json_decode($request->filters, true);

            $product = Product::when($filters['category_id'], function($query) use ($filters){
                return $query->where('products.category_id', $filters['category_id']);
            })
            ->when($filters['brand_id'], function($query)  use ($filters){
                return $query->where('products.brand_id', $filters['brand_id']);
            })
            ->when($filters['merchant_id'], function($query)  use ($filters){
                return $query->where('products.merchant_id', $filters['merchant_id']);
            })
            ->when($filters['status'], function($query)  use ($filters){
                return $query->where('products.status', $filters['status']);
            })
            ->when(request('search'), function($query){
                return $query->where('products.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('products.sku', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('products.description', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('brands.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%');
                             // ->orWhere('merchant_meta.meta_value', 'LIKE', '%' . request('search') . '%');

            })
            ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
            ->join('categories', 'products.category_id', '=', 'categories.id')            
            ->select('products.*', 'brands.name as brand_name', 'categories.name as category_name','products.created_at' )
            ->orderByDesc('products.created_at')
            ->paginate(request('perPage'));
            return $this->response($product, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function batchUpload(Request $request)
    {
        // try
        // {
            Excel::import(new ProductImport, $request->file('file'));
            return $this->response(true, 'Successfully uploaded!', $this->successStatus);   
        // }
        // catch (\Exception $e)
        // {
        //     return response([
        //         'message' => $e->getMessage(),
        //         'status' => false,
        //         'status_code' => $this->unauthorizedStatus,
        //     ], $this->unauthorizedStatus);
        // }
    }

    public function downloadCsv(Request $request)
    {
        try
        {  
            $filters = json_decode($request->filters, true);

            $products = Product::when($filters['category_id'], function($query) use ($filters){
                return $query->where('products.category_id', $filters['category_id']);
            })
            ->when($filters['brand_id'], function($query)  use ($filters){
                return $query->where('products.brand_id', $filters['brand_id']);
            })
            ->when($filters['merchant_id'], function($query)  use ($filters){
                return $query->where('products.merchant_id', $filters['merchant_id']);
            })
            ->when($filters['status'], function($query)  use ($filters){
                return $query->where('products.status', $filters['status']);
            })
            ->when(request('search'), function($query){
                return $query->where('products.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('products.sku', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('products.description', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('brands.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%');
                             // ->orWhere('merchant_meta.meta_value', 'LIKE', '%' . request('search') . '%');

            })
            ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
            ->join('categories', 'products.category_id', '=', 'categories.id')            
            ->select('products.*', 'brands.name as brand_name', 'categories.name as category_name')
            ->orderByDesc('products.created_at')
            ->get();

            $directory = 'public/export/product/';
            $files = Storage::files($directory);
            foreach ($files as $file) {
                Storage::delete($file);
            }

            $filename = "Products-".date("Y-m-d").".csv";
            // Store on default disk
            Excel::store(new ProductExport($products), $directory.$filename);

            $data = [
                'filepath' => '/storage/export/product/'.$filename,
                'filename' => $filename
            ];
            
            if(Storage::exists($directory.$filename))
                return $this->response($data, 'Successfully Retreived!', $this->successStatus); 

            return $this->response(false, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getProductSizes()
    {
        try
        {
            $productSize = ProductSize::get();
            return $this->response($productSize, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function generateLinkToken($name) {
        $token = str_replace(' ', '-', strtolower($name)) . '-' . str_random(10);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $token);
    }

    public function getAll(Request $request)
    {
        try
        {
            $products = Product::where('merchant_id', request('merchant_id'))->get();
            return $this->response($products, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function productMetaMigration()
    {
        try
        {

            $products = DB::select("SELECT product_id, (CASE WHEN size = '' THEN 'FREE SIZE/NO SIZE' ELSE size END) AS size  FROM adobomall_v1.products
                WHERE product_id > (SELECT (product_id-1) FROM adobomall_v2.product_meta ORDER BY id DESC LIMIT 1)");

            foreach($products as $request) {
                $product = Product::find($request->product_id);
                
                $product_size = ProductSize::where('size', 'like', '%'.$request->size.'%')->first()->toArray();

                // $addons = array("group" => null,"name" => null,"price" => null);
                // $inventories =  array(
                //     "product_name" => $product->name,
                //     "product_sku" => $product->sku,
                //     "group_name" => null,
                //     "variant" => null,
                //     "product_size_id" => null,
                //     "size" => null,
                //     "qty" => 0
                // );

                // $tags = array(
                //     "id" => null,
                //     "tag_name" => null
                // );

                // $variants = array(
                //     "group" => null,
                //     "name" => null,
                //     "price" => null,
                //     "gallery" => array()
                // );

                $details = [
                    // 'addons' => json_encode($addons),
                    // 'inventories' => json_encode($inventories),
                    'sizes' => json_encode($product_size),
                    // 'tags' => json_encode($tags),
                    // 'variants' => json_encode($variants),
                    'gallery' => json_encode(array())                
                ];
                $product->addMeta($details);
            }
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function batchUpdate(Request $request)
    {
        try
        {
            if(count($request->productIds) > 0){
                $products = Product::whereIn('id', $request->productIds)->update(['status' => $request->status]);
                return $this->response(true, 'Successfully Updated!', $this->successStatus);   
            }
            return $this->response(true, 'No selected product!', $this->successStatus);   
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
