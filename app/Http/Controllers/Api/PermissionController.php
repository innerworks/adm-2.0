<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\PermissionControllerInterface;
use App\Http\Requests\Api\Permission\PermissionRequest;
use App\Models\RolePermission;

class PermissionController extends ApiBaseController implements PermissionControllerInterface
{
    /********************************
    * 			PERMISSION 			*
    ********************************/
    public function grantPermission(PermissionRequest $request)
	{
		try
        {
            $success = false;
            $role_id = $request["role_id"];

            if(is_array($request["permissions"]))
                RolePermission::where('role_id',$request["role_id"])->delete();
                
                foreach ($request["permissions"] as $permission) {
                    RolePermission::assignRolePermission($role_id, $permission['module_id'], $permission['permission']);                  
                }
                $success = true;

            return $this->response($success, 'Successfully Granted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}
}
