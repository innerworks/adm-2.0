<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\MerchantControllerInterface;
use App\Services\Helpers\PasswordHelper;

use App\Http\Requests\Api\Merchant\RegisterRequest;
use App\Http\Requests\Api\Merchant\AddRequest;
use App\Http\Requests\Api\Merchant\UpdateRequest;
use App\Http\Requests\Api\Auth\LoginRequest;

use Illuminate\Support\Facades\DB;

use App\Models\Merchant;
use App\Models\MerchantStatus;
use App\Models\Notification;
use App\Models\CommissionRate;

use Carbon\Carbon;

class MerchantController extends ApiBaseController implements MerchantControllerInterface
{
	/********************************
    * 			MERCHANT 			*
    ********************************/
    public function getMerchantDetails($id)
    {
        try
        {
            $merchant = Merchant::find($id);
            return $this->response($merchant, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function register(RegisterRequest $request)
    {    
        try
        {
            $merchant = new Merchant;
            $merchant->email = $request->email;
            $merchant->category_id = $request->category;      
            $merchant->status = 1;
            $merchant->save();

            $details = [
                'store_name' => $request->storeName,
                'store_address' => $request->storeAddress,
                'first_name' => $request->firstName,
                'middle_name' => $request->middleName,
                'last_name' => $request->lastName,
                'mobile' => $request->mobile,
                'phone' => $request->phone,
            ];
            $merchant->addMeta($details);

            Notification::addMerchant($merchant, 1);
            // email notification for merchant registrant

            // email notification for admin

            return $this->response($merchant, 'Thank you for registering. your application is subject for approval one of our staff will call you for assessment.', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function list(Request $request)
    {
        try
        {
            $merchants = Merchant::when(request('search'), function($query){
                return $query->where('merchants.email', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('merchant_meta.meta_value', 'LIKE', '%' . request('search') . '%');
            })
            ->join('merchant_meta', function( $join )
            {
                $join->on('merchants.id', '=', 'merchant_meta.merchant_id')
                     ->where('merchant_meta.meta_key', '=', 'store_name');
            })
            ->leftJoin('merchant_status', 'merchants.status', '=', 'merchant_status.id')
            ->leftJoin('categories', 'merchants.category_id', '=', 'categories.id')
            ->select('merchants.id' ,'merchants.email', 'merchants.status', 'merchants.created_at', 'merchant_meta.meta_value', 'categories.name as category_name')
            ->latest()
            ->paginate(request('perPage'));
            return $this->responsePaginate($merchants, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }    

    public function store(AddRequest $request)
    {
        try
        {
            $salt = bcrypt(PasswordHelper::generateSalt());

            $merchant = new Merchant;
            $merchant->email = $request->email;
            $merchant->username = $request->username;
            $merchant->status = $request->status;
            $merchant->category_id = $request->category;      
            $merchant->salt = $salt;
            $merchant->password = PasswordHelper::generate($salt, $request->password);
            $merchant->save();

            $businessInfo = [
                'business_name' => $request->businessName,
                'permit_number' => $request->permitNumber,
                'business_permit_date' => $request->busPermitDate,
                'trade_style' => $request->tradeStyle,
                'tin' => $request->tin,
                'date_started_operation' => $request->dateStartedOperation,
                'sec_registration_number' => $request->secRegistrationNumber,
                'sec_registration_date' => $request->secRegistrationDate,
                'dti_registation_number' => $request->dtiRegistationNumber,
                'dti_registation_date' => $request->dtiRegistationDate,
                'business_type' => $request->businessType
            ];

            $settings = [
                'scheduled_order' => $request->scheduledOrder,
                'sunday' => $request->sunday,
                'sunday_time' => $request->sundayTime,
                'monday' => $request->monday,
                'monday_time' => $request->mondayTime,
                'tuesday' => $request->tuesday,
                'tuesday_time' => $request->tuesdayTime,
                'wednesday' => $request->wednesday,
                'wednesday_time' => $request->wednesdayTime,
                'thursday' => $request->thursday,
                'thursday_time' => $request->thursdayTime,
                'friday' => $request->friday,
                'friday_time' => $request->fridayTime,
                'saturday' => $request->saturday,
                'saturday_time' => $request->saturdayTime,
                'other_info' => $request->otherInfo,
                'logo' => $request->logo,
                'banner' => $request->banner,
            ];

            $details = [
                'store_name' => $request->storeName,
                'store_address' => $request->storeAddress,
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
                'first_name' => $request->firstName,
                'middle_name' => $request->middleName,
                'last_name' => $request->lastName,
                'mobile' => $request->mobile,
                'phone' => $request->phone,
                'brand' => $request->brand,
                'business_info' => json_encode($businessInfo),
                'settings' => json_encode($settings),
            ];
            $merchant->addMeta($details);

            return $this->response($merchant, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function update(UpdateRequest $request)
    {
        try
        {
            $merchant = Merchant::find($request->id);
            $merchant->email = $request->email;
            $merchant->username = $request->username;
            $merchant->status = $request->status;
            $merchant->category_id = $request->category;      

            if($request->password)
                $merchant->password = PasswordHelper::generate($merchant->salt, $request->password);
            
            $merchant->save();

            $businessInfo = [
                'business_name' => $request->businessName,
                'permit_number' => $request->permitNumber,
                'business_permit_date' => $request->busPermitDate,
                'trade_style' => $request->tradeStyle,
                'tin' => $request->tin,
                'date_started_operation' => $request->dateStartedOperation,
                'sec_registration_number' => $request->secRegistrationNumber,
                'sec_registration_date' => $request->secRegistrationDate,
                'dti_registation_number' => $request->dtiRegistationNumber,
                'dti_registation_date' => $request->dtiRegistationDate,
                'business_type' => $request->businessType
            ];

            $settings = [
                'scheduled_order' => $request->scheduledOrder,
                'sunday' => $request->sunday,
                'sunday_time' => $request->sundayTime,
                'monday' => $request->monday,
                'monday_time' => $request->mondayTime,
                'tuesday' => $request->tuesday,
                'tuesday_time' => $request->tuesdayTime,
                'wednesday' => $request->wednesday,
                'wednesday_time' => $request->wednesdayTime,
                'thursday' => $request->thursday,
                'thursday_time' => $request->thursdayTime,
                'friday' => $request->friday,
                'friday_time' => $request->fridayTime,
                'saturday' => $request->saturday,
                'saturday_time' => $request->saturdayTime,
                'other_info' => $request->otherInfo,
                'logo' => $request->logo,
                'banner' => $request->banner,
            ];

            $details = [
                'store_name' => $request->storeName,
                'store_address' => $request->storeAddress,
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
                'first_name' => $request->firstName,
                'middle_name' => $request->middleName,
                'last_name' => $request->lastName,
                'mobile' => $request->mobile,
                'phone' => $request->phone,
                'brand' => $request->brand,
                'business_info' => json_encode($businessInfo),
                'settings' => json_encode($settings),
            ];
            $merchant->addMeta($details);

            return $this->response($merchant, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $merchant = Merchant::find($id);
            $merchant->delete();
            return $this->response($merchant, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getStatus()
    {
        try
        {
            $merchantStatus = MerchantStatus::get();
            return $this->response($merchantStatus, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDropdown()
    {
        try
        {
            $merchant = Merchant::select('merchants.*')
            ->join('merchant_meta', function( $join ){
                $join->on('merchants.id', '=', 'merchant_meta.merchant_id')
                     ->where('merchant_meta.meta_key', '=', 'store_name');
            })
            ->orderBy('merchant_meta.meta_value')
            ->get();
            return $this->response($merchant, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function localMigration()
    {
        try
        {
            $merchants = DB::select("SELECT 
m.merchant_id, store_name, slug_name, merchant_category, username, merchant_lname, merchant_fname,
merchant_mname, email_address, phone_number, mobile_number, 
CONCAT('https://ammediaserver.s3.amazonaws.com/beta/media/',primary_img) AS primary_img, 
CONCAT('https://ammediaserver.s3.amazonaws.com/beta/media/',primary_mobile_img) AS primary_mobile_img,  
CONCAT('https://ammediaserver.s3.amazonaws.com/beta/media/',profile_img_uri) AS profile_img_uri,  
business_name, business_permit_number, tin, m.status, profile_details,
commission_rate, store_contact_person, store_contact_number, store_address1, 
store_address2, store_city_code, store_prov_code, store_brgy_code, store_zip_code
FROM adobomall_v1.merchants AS m
LEFT JOIN adobomall_v1.merchant_stores AS ms ON m.merchant_id = ms.merchant_id
WHERE m.merchant_id > 781
GROUP BY m.merchant_id
ORDER BY m.merchant_id
LIMIT 30");

            foreach($merchants as $old_merchant) {

                $salt = bcrypt(PasswordHelper::generateSalt());

                $merchant = new Merchant;
                $merchant->id = $old_merchant->merchant_id;
                $merchant->email = $old_merchant->email_address;
                $merchant->username = $old_merchant->username;
                $merchant->status = $old_merchant->status;
                $merchant->category_id = $old_merchant->merchant_category;      
                $merchant->salt = $salt;
                $merchant->password = PasswordHelper::generate($salt, $old_merchant->username);
                $merchant->save();

                $businessInfo = [
                    'business_name' => $old_merchant->business_name,
                    'permit_number' => $old_merchant->business_permit_number,
                    'business_permit_date' => null,
                    'trade_style' => null,
                    'tin' => $old_merchant->tin,
                    'date_started_operation' => null,
                    'sec_registration_number' => null,
                    'sec_registration_date' => null,
                    'dti_registation_number' => null,
                    'dti_registation_date' => null,
                    'business_type' => null,
                ];

                $settings = [
                    'scheduled_order' => null,
                    'sunday' => null,
                    'sunday_time' => null,
                    'monday' => null,
                    'monday_time' => null,
                    'tuesday' => null,
                    'tuesday_time' => null,
                    'wednesday' => null,
                    'wednesday_time' => null,
                    'thursday' => null,
                    'thursday_time' => null,
                    'friday' => null,
                    'friday_time' => null,
                    'saturday' => null,
                    'saturday_time' => null,
                    'other_info' => $old_merchant->profile_details,
                    'logo' => $old_merchant->profile_img_uri,
                    'banner' => $old_merchant->primary_img,
                ];

                $details = [
                    'store_name' => $old_merchant->store_name,
                    'store_contact_person' => $old_merchant->store_contact_person,
                    'store_contact_number' => $old_merchant->store_contact_number,
                    'store_address' => $old_merchant->store_address1,
                    'store_address2' => $old_merchant->store_address2,
                    'store_city_code' => $old_merchant->store_city_code,
                    'store_prov_code' => $old_merchant->store_prov_code,
                    'store_brgy_code' => $old_merchant->store_brgy_code,
                    'store_zip_code' => $old_merchant->store_zip_code,                    
                    'longitude' => null,
                    'latitude' => null,
                    'first_name' => $old_merchant->merchant_fname,
                    'middle_name' => $old_merchant->merchant_mname,
                    'last_name' => $old_merchant->merchant_lname,
                    'mobile' => $old_merchant->mobile_number,
                    'phone' => $old_merchant->phone_number,
                    'brand' => null,
                    'business_info' => json_encode($businessInfo),
                    'settings' => json_encode($settings),
                ];
                $merchant->addMeta($details);

                $commission_rate = [
                    'merchant_id' => $old_merchant->merchant_id,
                    'merchant_rate' => $old_merchant->commission_rate
                ];

                $commissionRate = new CommissionRate;
                $commissionRate->merchant_id = $old_merchant->merchant_id;
                $commissionRate->merchant_rate = $old_merchant->commission_rate;
                $commissionRate->save();
            }

            return Merchant::get()->count();
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }



    // public function requests()
    // {
    //     try
    //     {
    //         $merchant = Merchant::when(request('search'), function($query){
    //             return $query->where('meta_value', 'LIKE', '%test%');
    //         })
    //         ->where('status', '=', '1')
    //         ->where('meta_value', 'LIKE', '%test%')
    //         ->latest()
    //         ->paginate(request('perPage'));
    //         return $this->response($merchant, 'Successfully Retreived!', $this->successStatus);
    //     }
    //     catch (\Exception $e)
    //     {
    //         return response([
    //             'message' => $e->getMessage(),
    //             'status' => false,
    //             'status_code' => $this->unauthorizedStatus,
    //         ], $this->unauthorizedStatus);
    //     }
    // }

	
}
