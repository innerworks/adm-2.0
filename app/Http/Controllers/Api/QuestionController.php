<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\QuestionControllerInterface;
use App\Models\Question;

class QuestionController extends ApiBaseController implements QuestionControllerInterface
{
    /************************************
    * 			question 			    *
    *************************************/
    public function getQuestion($id)
	{
		try
        {
        	$question = Question::find($id);
            return $this->response($question, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $question = new Question;
            $question->question = $request->question;
            $question->type = $request->type;
            $question->save();

            return $this->response($question, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $question = Question::find($request->id);
            $question->question = $request->question;
            $question->type = $request->type;
            $question->save();

            return $this->response($question, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $question = Question::find($id);
            $question->delete();
            return $this->response($question, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $question = Question::when(request('search'), function($query){
                return $query->where('question', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($question, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
