<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\PasswordResetControllerInterface;
use Carbon\Carbon;
use App\Notifications\CustomerPasswordResetRequest;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Services\Helpers\PasswordHelper;
use App\Http\Requests\Api\PasswordReset\CreateRequest;
use App\Http\Requests\Api\PasswordReset\ResetRequest;
use App\Models\User;
use App\Models\Customer;
use App\Models\PasswordReset;

class PasswordResetController extends ApiBaseController implements PasswordResetControllerInterface
{
    /************************************
    * 			PASSWORD RESET 			*
    ************************************/

    public function create(CreateRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user)
	        return $this->response(null, "We can't find a user with that e-mail address.", 404);
        
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            ['email' => $user->email, 'token' => str_random(60)]
        );

        if ($user && $passwordReset)
            $user->notify(new PasswordResetRequest($passwordReset->token));

        return $this->response(['token'=>$passwordReset->token], 'We have e-mailed your password reset link!', 200);
    }

    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset)
	        return $this->response(null, 'This password reset token is invalid.', 404);

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) 
        {
            $passwordReset->delete();
	        return $this->response(null, 'This password reset token is invalid.', 404);
        }

        return $this->response($passwordReset, '', 200);
    }

    public function reset(ResetRequest $request)
    {
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if (!$passwordReset)
	        return $this->response(null, 'This password reset token is invalid.', 404);

        $user = User::where('email', $passwordReset->email)->first();

        if (!$user)
	        return $this->response(null, "We can't find a user with that e-mail address.", 404);

        $user->password = PasswordHelper::generate($user->salt, $request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        
        return $this->response($user, 'Password successfully reset!', 200);
    }

    // customer reset password
    public function createPasswordToken(CreateRequest $request)
    {
        $user = Customer::where('email', $request->email)->first();

        if (!$user)
            return $this->response(null, "We can't find a user with that e-mail address.", 404);
        
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            ['email' => $user->email, 'token' => str_random(60)]
        );

        if ($user && $passwordReset)
            $user->notify(new CustomerPasswordResetRequest($passwordReset->token));

        return $this->response(['token'=>$passwordReset->token], 'We have e-mailed your password reset link!', 200);
    }

    public function findPasswordToken($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset)
            return $this->response(null, 'This password reset token is invalid.', 404);

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) 
        {
            $passwordReset->delete();
            return $this->response(null, 'This password reset token is invalid.', 404);
        }

        return $this->response($passwordReset, '', 200);
    }

    public function resetPasswordToken(ResetRequest $request)
    {
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if (!$passwordReset)
            return $this->response(null, 'This password reset token is invalid.', 404);

        $user = Customer::where('email', $passwordReset->email)->first();

        if (!$user)
            return $this->response(null, "We can't find a user with that e-mail address.", 404);

        $user->password = PasswordHelper::generate($user->salt, $request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        
        return $this->response($user, 'Password successfully reset!', 200);
    }
}
