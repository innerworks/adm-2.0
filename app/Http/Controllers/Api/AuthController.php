<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\LoginRequest;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\AuthControllerInterface;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use App\Services\Helpers\PasswordHelper;
use App\Notifications\SignupActivate;
use Laravel\Passport\Client as OClient; 

use Illuminate\Support\Str;
use App\Models\User;
use Carbon\Carbon;

use Validator;
use Hash;

class AuthController extends ApiBaseController implements AuthControllerInterface
{
    /****************************
    * 			AUTH 			*
    ****************************/
    public function register(RegisterRequest $request)
    {    
        try
        {
            $salt = bcrypt(PasswordHelper::generateSalt());

            $input = $request->all();
            
            $input['salt'] = $salt;
            $input['password'] = PasswordHelper::generate($salt, $input['password']);
            $input['activation_token'] = Str::random(60);

            $user = User::create($input); 

            $user->notify(new SignupActivate($user));

            $success['token'] = $user->createToken(env('APP_NAME'))->accessToken;

            return $this->response($success, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}


	public function signupActivate($token)
	{
        try
        {
            $user = User::where('activation_token', $token)->first();
            if (!$user) 
                return response([
                    'message' => 'This activation token is invalid.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

            $user->active = true;
            $user->activation_token = '';
            $user->save();

            return $this->response($user, 'Successfully Acctivated!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }		
	}

	public function login(LoginRequest $request)
	{ 
		try
    	{

            $user = User::whereEmail(request('email'))->first();
            if(!$user)
                return response([
                    'message' => 'Wrong email or password',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            $client = OClient::where('password_client', 1)->first();

            $data = [
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'providers' => 'users',
                'username' => request('email'),
                'password' => User::getSalt(request('email')).env("PEPPER_HASH").request('password'),
            ];

            $request = Request::create('/oauth/token', 'POST', $data);
            
            $response = app()->handle($request);

            // Check if the request was successful
            if ($response->getStatusCode() != 200)
                return response([
                    'message' => 'Wrong email or password',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            $token = json_decode($response->getContent());

            $data = [
                'token' => $token->access_token,
                'refresh_token' => $token->refresh_token,
                'user'  => $user,
                'is_merchant' => false,
                'is_customer' => false,
                'is_social'   => false,
             ];

            return $this->response($data, 'Successfully Logged!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		 return response([
                'message' => 'Unauthenticated. Please login.', //$e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
	}

    public function refresh(Request $request)
    {
        try
        {  
            $client = DB::table('oauth_clients')
                        ->where('password_client', true)
                        ->first();

            $data = [
                        'grant_type' => 'refresh_token',
                        'refresh_token' => $request->refresh_token,
                        'client_id' => $client->id,
                        'client_secret' => $client->secret,
                        'scope' => ''
                    ];

            $request = Request::create('/oauth/token', 'POST', $data);
            $response = app()->handle($request);

            $token = json_decode($response->getContent());

            $data = [
                'token' => $token->access_token,
                'refresh_token' => $token->refresh_token,
             ];

            return $this->response($data, 'Successfully Loged!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }


    public function logout(Request $request)
    {
        try
        {
            $request->user()->token()->revoke();
            $request->user()->tokens->each(function ($token,$key) {
                $token->delete();
            });

            setcookie("data", "", time() - 3600);

            return $this->response(null, 'Successfully logged out!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function forgotPassword(Request $request)
    {
        // code...
    }
}
