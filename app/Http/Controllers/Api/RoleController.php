<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\RoleControllerInterface;
use App\Http\Requests\Api\Role\RoleRequest;
use App\Http\Requests\Api\Role\RoleUpdateRequest;
use App\Models\Role; 

class RoleController extends ApiBaseController implements RoleControllerInterface
{
    /****************************
    * 			ROLE 			*
    ****************************/
    public function create(RoleRequest $request)
	{
		try
        {
            $user = auth()->user();
            
            $input = $request->all();
            $input['user_id'] = $user->id;
            $input['active'] = 1;
            
            $role = Role::create($input); 
            return $this->response($role, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

	
    public function show($roleId)
	{
		try
        {
        	$role = Role::find($roleId);
            return $this->response($role, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

	
    public function update(RoleUpdateRequest $request)
	{
		try
        {
            $input = $request->all();

        	$role = Role::find($input['id']);
        	$role->name = $input['name'];
        	$role->description = $input['description'];
            $role->active = $input['isActive'];
            
        	$role->save();
            return $this->response($role, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

	
    public function delete($roleId)
	{
		try
        {
            $role = Role::find($roleId);
            $role->delete();
            return $this->response($role, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

	
    public function list(Request $request)
	{
		try
        {
            $roles = Role::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('description', 'LIKE', '%' . request('search') . '%');
            })
            ->where('name', '<>', 'Administrator')
            ->select('id' ,'name', 'description', 'active', 'created_at')
            ->latest()
            ->paginate(request('perPage'));

            return $this->response($roles, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}	

}
