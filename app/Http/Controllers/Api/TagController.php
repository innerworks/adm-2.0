<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\TagControllerInterface;
use App\Models\Tag;
use DB;


class TagController extends ApiBaseController implements TagControllerInterface
{
    public function list()
    {
        try
        {
            $tags = Tag::orderBy('tag_name')->get();
            return $this->response($tags, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storeTag(Request $request)
    {
        try
        {
            $tags = Tag::where('tag_name', $request->tag_name)->get();

            if (count($tags) <= 0) {
                $new_tag = new Tag;
                $new_tag->tag_name = $request->tag_name;
                $new_tag->save();

                return $this->response($new_tag, 'New tags added successfully!', $this->successStatus);
            }

        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function reconstruct(Request $request)
    {
        try
        {   

            $new_tags = [];
            foreach ($request['params'] as $tag) {
                $db_tags = Tag::where('tag_name', $tag['tag_name'])->first();
                if($db_tags) {
                    array_push($new_tags, ['id' => $db_tags->id, 'tag_name' => $db_tags->tag_name]);
                }
            }

            return $this->response($new_tags, 'Successfully Retrieved!', $this->successStatus);

        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
