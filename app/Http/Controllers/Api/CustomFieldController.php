<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\CustomFieldControllerInterface;
use App\Models\CustomField; 

class CustomFieldController extends ApiBaseController implements CustomFieldControllerInterface
{
    /************************************
    * 			CUSTOM FIELDS 			*
    ************************************/

}
