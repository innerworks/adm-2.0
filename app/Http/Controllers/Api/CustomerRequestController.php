<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\CustomerRequestControllerInterface;

use App\Models\CustomerRequest;

class CustomerRequestController extends ApiBaseController implements CustomerRequestControllerInterface
{
    public function storeRequest(Request $request)
    {
        try
        {
            $customerRequest = new CustomerRequest;
            if($request->request_id != 0) 
                $customerRequest = CustomerRequest::find($request->request_id);

            $customerRequest->customer_id = $request->customer_id;
            $customerRequest->order_id = $request->order_id;
            $customerRequest->order_detail_id = $request->order_detail_id;
            $customerRequest->product_id = $request->product_id;
            $customerRequest->merchant_id = $request->merchant_id;
            $customerRequest->question_id = $request->question_id;
            $customerRequest->type_id = $request->type_id;
            $customerRequest->address_id = $request->address_id;
            $customerRequest->details = $request->details;
            $customerRequest->save();

            return $this->response($customerRequest, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function list()
    {
        try
        {
	        $cancelations = CustomerRequest::where('type_id', 1)->get();
	        $returns = CustomerRequest::where('type_id', 2)->get();
	        $warranty = CustomerRequest::where('type_id', 3)->get();

	        $data = [
	            'cancellation' => $cancelations,
	            'return' => $returns,
	            'warranty' => $warranty,
	        ];

            return $this->response($data, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }
}
