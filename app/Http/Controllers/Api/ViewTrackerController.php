<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\ViewTrackerControllerInterface;

use App\Models\ViewTracking; 
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Merchant;

use Cookie;
use Carbon\Carbon;

class ViewTrackerController extends ApiBaseController implements ViewTrackerControllerInterface
{
    public function home(Request $request)
    {
        $data = [
            'timestamp' => Carbon::now(),
            'type' => 'home',
            'type_id' => 1,
            'uri' => '/',                
        ];

        $this->saveCookie($data, 'homepage');        
    }

    public function category(Request $request) {
        $category = Category::where('slug_name', $request->slug)->first();

        $data = [
            'timestamp' => Carbon::now(),
            'type' => 'category',
            'type_id' => $category->id,
            'uri' => '/category/'.$request->slug,                
        ];

        $this->saveCookie($data, 'category');
    }

    public function brand(Request $request) {
        $brand = Brand::where('slug_name', $request->slug)->first();

        $data = [
            'timestamp' => Carbon::now(),
            'type' => 'brand',
            'type_id' => $brand->id,
            'uri' => '/brand/'.$request->slug,                
        ];

        $this->saveCookie($data, 'brand');
    }

    public function merchant(Request $request) {
        $merchant = Merchant::find($request->id);
        $store_name = str_replace(" ","-", strtolower($merchant->store_name));

        $data = [
            'timestamp' => Carbon::now(),
            'type' => 'merchant',
            'type_id' => $merchant->id,
            'uri' => '/merchant-'.$merchant->id.'/'.$store_name,                
        ];

        $this->saveCookie($data, 'merchant');
    }   

    public function product(Request $request) {
        $product = Product::where('permalink', $request->slug)->first();

        $data = [
            'timestamp' => Carbon::now(),
            'type' => 'product',
            'type_id' => $product->id,
            'uri' => '/product/'.$request->slug,                
        ];

        $this->saveCookie($data, 'product');
    }

    public function saveCookie($data, $name) {
        $array_data = [];
        $cookie_data = [
            'timestamp' => $data['timestamp'],
            'type_id' => $data['type_id'],
        ];
        $current_time = Carbon::now();
        $current_cookie = $this->checkCookie($name);
        $array_cookie = json_decode($current_cookie, true);

        if(is_array($array_cookie) && count($array_cookie) > 0) {
            
            $is_exist = false;
            foreach($array_cookie as $index => $cookie) {

                if($cookie['type_id'] == $data['type_id']) {
                    $date = Carbon::parse($cookie['timestamp']);
                    $now = Carbon::now();

                    $diff = $date->diffInHours($now);

                    if($diff >= 24) {
                        $this->setViewTracking($data, $name);
                    }
                    $array_data[$index] = $cookie_data;
                    $is_exist = true;
                }
                else {
                    $array_data[] = $cookie_data;
                }
            }

            if(!$is_exist) {
                $array_data[] = $cookie_data;
                $this->setViewTracking($data, $name);
            }
        }
        else {
            $array_data[] = $data;
            $this->setViewTracking($data, $name);
        }

        $this->setCookie($array_data, $name); 
    }

    public function checkCookie($param)
    {
        return @$_COOKIE[$param];
    }

    public function setCookie($data, $name)
    {
        $jdata = json_encode($data);
        setcookie($name, $jdata, time()+86400);  /* expire in 24 hour */    
    }

    public function setViewTracking($data, $name)
    {
        $tracking_data = ViewTracking::where('type_id', $data['type_id'])->where('type', $data['type'])->first();
        $this->addUpdateTracking($tracking_data, $data);
    }

    public function addUpdateTracking($viewTracking, $data)
    {
        if($viewTracking) {
            $viewTracking->daily = $viewTracking->daily+1;
            $viewTracking->weekly = $viewTracking->weekly+1;
            $viewTracking->monthly = $viewTracking->monthly+1;
            $viewTracking->save();
        }
        else {
            $track_data = [
                'uri' => $data['uri'],
                'type' => $data['type'],
                'type_id' => $data['type_id'],
                'daily' => 1,
                'weekly' => 1,
                'monthly' => 1,
            ];
            ViewTracking::create($track_data);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $view_tracker = ViewTracking::when(request('search'), function($query){
                return $query->where('type', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('uri', 'LIKE', '%' . request('search') . '%');
            })
            ->orderBy('daily', 'DESC')
            ->paginate(request('perPage'));
            return $this->responsePaginate($view_tracker, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }


}
