<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\UserAppControllerInterface;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Product\RatingRequest;
use App\Services\Helpers\MrSpeedyHelper;
use Illuminate\Support\Facades\DB;

use App\Models\Brand;
use App\Models\BannerAds; 
use App\Models\Category; 
use App\Models\Product; 
use App\Models\Merchant; 
use App\Models\ProductRating; 
use App\Models\Cart; 
use App\Models\Ads; 
use App\Models\FlashSale; 
use App\Models\Voucher; 
use App\Models\OrderDetail;
use App\Models\CustomerSearch;
use App\Models\Wishlist;

use Carbon\Carbon;

class UserAppController extends ApiBaseController implements UserAppControllerInterface
{
    /********************************
    *           USER APP            *
    ********************************/
    public function getAds()
    {
        try
        {
            $ads = Ads::where('active', true)->inRandomOrder()->first();
            return $this->response($ads, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function smartSearch(Request $request)
    {
        try
        {
            $data = array();
            $userSearch = CustomerSearch::where('customer_id', request('user_id'))->get(['search']);

            if(count($userSearch) > 0) 
                foreach ($userSearch as $keyItem) {
                    $data[] = $keyItem->search;
                }

            $wishList = Wishlist::where('customer_id', request('user_id'))->get();
            if(count($wishList) > 0)
                foreach ($wishList as $keyItem) {
                    $data[] = $keyItem->product_details->name;
                }                

            $orders = OrderDetail::where('orders.customer_id', request('user_id'))
            ->join('orders', 'order_details.order_id', '=', 'orders.id')            
            ->distinct()
            ->get(['order_details.product_name']);

            if(count($orders) > 0)
                foreach ($orders as $keyItem) {
                    $data[] = $keyItem->product_name;
                }                

            return $this->response($data, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function search(Request $request)
    {
        try
        {
            $userSearch = new CustomerSearch;
            if(request('user_id'))
                $userSearch->addSearch(request('search'), request('user_id'));

            $products = Product::when(request('search'), function($query){
                return $query->where('products.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('tags.tag_name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('categories.name', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('merchant_meta.meta_value', 'LIKE', '%' . request('search') . '%');
            })
            ->join('merchant_meta', function( $join )
            {
                $join->on('products.merchant_id', '=', 'merchant_meta.merchant_id')
                     ->where('merchant_meta.meta_key', '=', 'store_name');
            })
            ->join('product_tags', 'products.id', '=', 'product_tags.product_id')
            ->join('tags', 'product_tags.tag_id', '=', 'tags.id')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->latest()
            ->paginate(request('perPage'));

            $products->makeHidden(['details']);

            return $this->responsePaginate($products, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getBrands(Request $request)
    {
        try
        {
            $brands = Brand::latest()->paginate(request('perPage'));
            return $this->responsePaginate($brands, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getBanners(Request $request)
    {
    	try
        {
            $bannerAds = BannerAds::latest()->paginate(request('perPage'));
            return $this->responsePaginate($bannerAds, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getCategories(Request $request)
    {
        try
        {
            if($request->merchantId) {
                $categories = Category::where('merchant_id', '=',$request->merchantId)->get();
            }
            else if ($request->parentId) {
                $categories = Category::where('parent_id', '=',$request->parentId)->get();
            }
            else {
                $categories = Category::where('parent_id', NULL)->get();
            }

            return $this->response($categories, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    
    }

    public function getTopPicks(Request $request)
    {
        try
        {
            $productIds = DB::select('SELECT product_id FROM order_details GROUP BY product_id ORDER BY count(*) DESC LIMIT 20');
            $productIds =json_decode(json_encode($productIds), true);

            $products = Product::whereIn('products.id', $productIds)
            ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->inRandomOrder()
            ->paginate(request('perPage'));

            $products->makeHidden(['details','sizes','variants','addons']);

            return $this->responsePaginate($products, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    
    }

    public function getFlashSale(Request $request)
    {
        try
        {
            $productIds = FlashSale::where('status', true)->latest()->first();

            $products = Product::whereIn('products.id', $productIds->products_ids)
            ->join('merchants', 'products.merchant_id', '=', 'merchants.id')
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->latest()
            ->paginate(request('perPage'));

            $products->makeHidden(['details','sizes','variants','addons']);
            $productIds->makeHidden(['products_ids']);

            return $this->responseSale($products, $productIds,'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }    
    }

    public function getStores(Request $request)
    {
    	try
        {
            $merchants = Merchant::when(request('categoryId'), function($query) {
                return $query->where('category_id', '=', request('categoryId'));
            })
            ->select('merchants.id', 'merchants.email', 'merchants.category_id', 'merchants.status', 'merchants.created_at')
            ->latest()
            ->paginate(request('perPage'));

            $merchants->makeHidden(['details']);

            return $this->responsePaginate($merchants, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getStoreDetails(Request $request)
    {
        try
        {
            $merchant = Merchant::find(request('id'));
            $merchant->makeHidden(['details']);
            
            return $this->response($merchant, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storesNearYou(Request $request)
    {
        try
        {
            $merchantIds = $this->getMerchantIds($request);
            $merchants = Merchant::whereIn('id', $merchantIds)->get();
            $merchants->makeHidden(['details']);
            
            return $this->response($merchants, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getMerchantIds($request)
    {
        $ids = array();
        $merchants = Merchant::get();
        foreach ($merchants as $merchant) {
            $lat1 = $request->user_lat;
            $long1 = $request->user_long;
            $lat2 = $merchant->details['latitude'];
            $long2 = $merchant->details['longitude'];
            $kiloMeters = $this->calculate_distance($lat1, $long1, $lat2, $long2, 'K');
            if($kiloMeters <= 30)
                $ids[] = $merchant->id;
        }
        return $ids;
    }

    public function calculate_distance($lat1, $long1, $lat2, $long2, $unit='N')
    {
        $theta = $long1 - $long2; 
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
        $dist = acos($dist); 
        $dist = rad2deg($dist); 
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344); 
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
        return NULL;
    }

    public function getRandomProducts(Request $request)
    {
        try
        {
            $products = Product::when(request('categoryId'), function($query){
                $subCategories = Category::where('parent_id', request('categoryId'))->get()->pluck('id');
                return $query->whereIn('category_id', $subCategories);
            })
            ->when(request('merchantId'), function($query){
                return $query->where('merchant_id', '=', request('merchantId'));
            })
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->inRandomOrder()
            ->paginate(request('perPage'));

            $products->makeHidden(['details']);

            return $this->responsePaginate($products, 'Successfully Retreived!', $this->successStatus);

        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getProducts(Request $request)
    {
        try
        {
            $products = Product::when(request('categoryId'), function($query){
                $subCategories = Category::where('parent_id', request('categoryId'))->get()->pluck('id');
                if(count($subCategories) > 0) {
                    return $query->whereIn('category_id', $subCategories);
                }
                else {
                    return $query->where('category_id', request('categoryId'));
                }
            })
            ->when(request('merchantId'), function($query){
                return $query->where('merchant_id', '=', request('merchantId'));
            })
            ->select('products.id', 'products.sku', 'products.name', 'products.description', 'products.permalink', 'products.merchant_id', 'products.category_id', 'products.base_price', 'products.selling_price', 'products.primary_photo', 'products.created_at')
            ->latest()
            ->paginate(request('perPage'));

            $products->makeHidden(['details']);

            return $this->responsePaginate($products, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getProductDetails(Request $request)
    {
        try
        {
            $product = Product::find(request('id'));
            $product->setCustomer(request('customer_id'));
            $product->makeHidden(['details']);  

            return $this->response($product, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function addRating(RatingRequest $request)
    {
        try
        {
            $rating = new ProductRating;
            $rating->product_id = $request->product_id;
            $rating->customer_id = $request->customer_id;
            $rating->rating = $request->rating;
            $rating->review = $request->review;
            $rating->save();

            return $this->response($rating, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function storeToCart(Request $request)
    {
        try
        {
            $cart = new Cart;
            if($request->cart_id != 0) 
                $cart = Cart::find($request->cart_id);

            $cart->customer_id = $request->customer_id;
            $cart->product_id = $request->product_id;
            $cart->merchant_id = $request->merchant_id;
            $cart->category_id = $request->category_id;
            $cart->brand_id = $request->brand_id;
            $cart->size_id = $request->size_id;
            $cart->variant_ids = $request->variant_ids;
            $cart->addons_ids = $request->addons_ids;
            $cart->qty = $request->qty;
            $cart->total_price = ($request->total_price*$request->qty);
            $cart->shipping_fee = $request->shipping_fee;
            $cart->checked = $request->checked;
            $cart->special_instructions = $request->special_instructions;
            $cart->save();

            $cart->makeHidden(['product_id','category_id','brand_id','size_id','variant_ids','addons_ids','created_at']);

            return $this->response($cart, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function checkUncheckCart(Request $request)
    {
        try
        {
            $user = auth()->user();
            $carts = $user->carts();

            $carts = $carts->when(request('merchant_id'), function($query){
                return $query->where('merchant_id', '=', request('merchant_id'));
            })->get();

            foreach ($carts as $cart) {
                $cart->checked = ($request->checked == 'true') ? true : false;
                $cart->save();
            }
            return $this->response(true, 'Successfully updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function clearCart(Request $request)
    {
        
        try
        {
            $user = auth()->user();
            $carts = $user->carts();
            $carts->delete();

            return $this->response($carts, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function deleteCart(Request $request)
    {
        try
        {
            $user = auth()->user();
            $cart = $user->carts()->where('id', $request->id);
            $cart->delete();

            return $this->response($cart, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }    

    public function getCarts(Request $request)
    {        
        try
        {
            $user = auth()->user();
            $carts = $user->carts()->get();
            $carts = Cart::customGroup($carts);
            $total = Cart::getTotals($carts);

            $response = [
                'data' => $carts,         
                'meta' => $total,
                'message' => 'Successfully Retreived!',
                'status_code' => 200,
                'status' => true,
                'resource' => request()->getBasePath() . '/' . request()->path() . ((count(request()->all()) > 0) ? '?' . http_build_query(request()->all()) : '')
            ];
            return response($response, 200);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function voucher(Request $request)
    {        
        try
        {
            $vouchers = Voucher::where('active', true)->get();
            return $this->response($vouchers, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function addVoucher(Request $request)
    {
        # code...
    }
}
