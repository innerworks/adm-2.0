<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\CourierControlllerInterface;
use App\Models\Courier; 
use App\Models\PackageType; 
use App\Models\Area; 
use App\Models\CourierExcessRate; 
use App\Models\CourierRate; 
use App\Models\CourierSpecialRate; 
use App\Models\MerchantCourier;

class CourierControlller extends ApiBaseController implements CourierControlllerInterface
{
    public function getDetails($id)
    {
    	try
        {
            $courier = Courier::find($id); 
            return $this->response($courier, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
    	try
        {
            $merchant = auth()->user();
            $merchant_coriers_ids = [];
            $is_merchant = false;
            if($merchant->store_name) {
                $merchant_coriers_ids = MerchantCourier::where('owner_id', $merchant->id)->pluck('courier_id');
                $is_merchant = true;
            }

            $couriers = Courier::when(request('search'), function($query){
                return $query->where('name', 'LIKE', '%' . request('search') . '%');
            })
            ->when($is_merchant, function($query) use ($merchant_coriers_ids) {
                return $query->whereIn('id', $merchant_coriers_ids);
            })
            ->latest()
            ->paginate(request('perPage'));
            return $this->response($couriers, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function store(Request $request)
    {
    	try
        {
            $owner_id = 1;
            if($request->owner_id) {
                $owner_id = $request->owner_id;
            }
            else {
                $merchant = auth()->user();
                $owner_id = $merchant->id;
            }

            $courier = Courier::find($request->id);
            $courier_id = 0;
            if($courier) {
                $courier->name = $request->name;
                $courier->valuation_fee = $request->valuation_fee;
                $courier->cod_fee = $request->cod_fee;
                $courier->is_active = $request->is_active;
                $courier->is_default = $request->is_default;
                $courier->is_consumer = ($request->is_consumer) ? $request->is_consumer : 0;
                $courier->save();
                $courier_id = $courier->id;
            }
            else {
                $new_courier = [
                    'name' => $request->name,
                    'is_active' => $request->is_active,
                    'is_default' => $request->is_default,
                    'is_consumer' => ($request->is_consumer) ? $request->is_consumer : 0,
                    'valuation_fee' => $request->valuation_fee,
                    'cod_fee' => $request->cod_fee,
                ];

                $courier = Courier::create($new_courier);
                $courier_id = $courier->id;
            }

            // add to courier maping table
            MerchantCourier::updateOrCreate(
                [
                    'courier_id' => $courier_id,            
                ],
                [
                    'owner_id' => $owner_id,
                    'owner_type' => ($owner_id == 1) ? 'admin' : 'merchant',
                ]
            );

            return $this->response($courier, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $courier = Courier::find($id);
            $courier->delete();

            return $this->response($courier, 'Courier successfully deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storePackage(Request $request)
    {
    	try
        {
            $package = PackageType::find($request->id);
            if($package) {
                $package->package = $request->package;
                $package->courier_id = $request->courier_id;
                $package->min_weight = $request->min_weight;
                $package->max_weight = $request->max_weight;
                $package->length = $request->length;
                $package->width = $request->width;
                $package->height = $request->height;
                $package->save();
            }
            else {
                $new_package = [
                    'package' => $request->package,
                    'courier_id' => $request->courier_id,
                    'min_weight' => $request->min_weight,
                    'max_weight' => $request->max_weight,
                    'length' => $request->length,
                    'width' => $request->width,
                    'height' => $request->height
                ];

                $package = PackageType::create($new_package);
            }

            return $this->response($package, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function deletePackage($id)
    {
    	try
        {
            $package = PackageType::find($id);
            $package->delete();

            return $this->response($package, 'Package successfully deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getAreas()
    {
        try
        {
            $areas = Area::get();
            return $this->response($areas, 'Package successfully deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storeRate(Request $request)
    {
        try
        {
            $courier_rate = CourierRate::find($request->id);
            if($courier_rate) {
                $courier_rate->courier_area_id = $request->courier_area_id;
                $courier_rate->package_id = $request->package_id;
                $courier_rate->courier_id = $request->courier_id;
                $courier_rate->delivery_rate = $request->delivery_rate;
                $courier_rate->save();
            }
            else {
                $new_courier_rate = [
                    'courier_area_id' => $request->courier_area_id,
                    'package_id' => $request->package_id,
                    'courier_id' => $request->courier_id,
                    'delivery_rate' => $request->delivery_rate
                ];

                $courier_rate = CourierRate::create($new_courier_rate);
            }

            return $this->response($courier_rate, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function deleteRate($id)
    {
        try
        {
            $courier_rate = CourierRate::find($id);
            $courier_rate->delete();

            return $this->response($courier_rate, 'Rate successfully deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storeExcessRate(Request $request)
    {
        try
        {
            //return $request->all();
            $courier_rate = CourierExcessRate::find($request->id);
            if($courier_rate) {
                $courier_rate->courier_area_id = $request->courier_area_id;
                $courier_rate->package_id = $request->package_id;
                $courier_rate->courier_id = $request->courier_id;
                $courier_rate->weight = $request->weight;
                $courier_rate->computation_rate = $request->computation_rate;
                $courier_rate->save();
            }
            else {
                $new_courier_rate = [
                    'courier_area_id' => $request->courier_area_id,
                    'package_id' => $request->package_id,
                    'courier_id' => $request->courier_id,
                    'weight' => $request->weight,
                    'computation_rate' => $request->computation_rate
                ];

                $courier_rate = CourierExcessRate::create($new_courier_rate);
            }

            return $this->response($courier_rate, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function deleteExcessRate($id)
    {
        try
        {
            $courier_rate = CourierExcessRate::find($id);
            $courier_rate->delete();

            return $this->response($courier_rate, 'Excess rate successfully deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function specialRateList(Request $request)
    {
        try
        {
            $merchant = auth()->user();
            $is_merchant = false;
            if($merchant->store_name) {
                $merchant_coriers_ids = MerchantCourier::where('owner_id', $merchant->id)->pluck('courier_id');
                $is_merchant = true;
            }

            $special_rate = CourierSpecialRate::when(request('search'), function($query){
                return $query->where('special_name', 'LIKE', '%' . request('search') . '%');
            })
            ->when($is_merchant, function($query) use ($merchant_coriers_ids) {
                return $query->whereIn('owner_id', $merchant_coriers_ids);
            })
            ->paginate(request('perPage'));
            return $this->response($special_rate, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function storeSpecialRate(Request $request)
    {
        try
        {
            $special_rate = CourierSpecialRate::find($request->id);
            if($special_rate) {
                $special_rate->special_name = $request->special_name;
                $special_rate->rate_discount = $request->rate_discount;
                $special_rate->owner_id = $request->owner_id;
                $special_rate->owner_type = $request->owner_type;
                $special_rate->save();
            }
            else {
                $new_special_rate = [
                    'special_name' => $request->special_name,
                    'rate_discount' => $request->rate_discount,
                    'owner_id' => $request->owner_id,
                    'owner_type' => $request->owner_type
                ];

                $special_rate = CourierSpecialRate::create($new_special_rate);
            }

            return $this->response($special_rate, 'Successfully Saved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

}
