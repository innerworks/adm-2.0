<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Http\Controllers\Api\Interfaces\FlashSaleControllerInterface;
use Illuminate\Http\Request;

use App\Models\FlashSale;

class FlashSaleController extends ApiBaseController implements FlashSaleControllerInterface
{
    /************************************
    * 			FLASH SALE 			    *
    *************************************/
    public function getFlashSaleDetails($id)
	{
		try
        {
        	$flashSale = FlashSale::find($id);
            return $this->response($flashSale, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
	}

    public function store(Request $request)
    {
        try
        {
            $flashSale = new FlashSale;
            $flashSale->title = $request->title;
            $flashSale->short_description = $request->short_description;
            $flashSale->start_date = $request->start_date;
            $flashSale->end_date = $request->end_date;
            $flashSale->status = 1;
            $flashSale->save();

            $flashSale->storeProducts($request->productIds);

            return $this->response($flashSale, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->requestTimeOut,
            ], $this->requestTimeOut);
        }
    }

    public function update(Request $request)
    {
        try
        {
            $flashSale = FlashSale::find($request->id);
            $flashSale->title = $request->title;
            $flashSale->short_description = $request->short_description;
            $flashSale->start_date = $request->start_date;
            $flashSale->end_date = $request->end_date;
            $flashSale->status = $request->status;
            $flashSale->save();

            $flashSale->storeProducts($request->productIds);

            return $this->response($flashSale, 'Successfully Updated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function delete($id)
    {
        try
        {
            $flashSale = FlashSale::find($id);
            $flashSale->delete();
            return $this->response($flashSale, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function list(Request $request)
    {
        try
        {
            $flashSales = FlashSale::when(request('search'), function($query){
                return $query->where('title', 'LIKE', '%' . request('search') . '%')
                             ->orWhere('short_description', 'LIKE', '%' . request('search') . '%');
            })
            ->latest()
            ->paginate(request('perPage'));
            // ->paginate(10);
            return $this->response($flashSales, 'Successfully Retreived!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }
}
