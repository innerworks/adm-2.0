<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\Api\Customer\AddByEmail;
use App\Http\Requests\Api\Customer\AddByMobile;
use App\Http\Requests\Api\Customer\SetAccount;
use App\Http\Requests\Api\Customer\Login;
use App\Http\Requests\Api\Customer\Social;
use App\Http\Requests\Api\Customer\UpdateProfile;
use App\Http\Requests\Api\Customer\AddAddress;
use App\Http\Requests\Api\Customer\EditAddress;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\Api\Interfaces\CustomerControllerInterface;

use Illuminate\Support\Facades\Auth; 
use App\Services\Helpers\PasswordHelper;
use App\Notifications\RegisterByEmail;

use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Wishlist;
use App\Models\OrderDetail;
use Hash;

use Illuminate\Support\Facades\Mail;
use App\Mail\CustomerInquiry;
use App\Mail\InquiryResponse;

class CustomerController extends ApiBaseController implements CustomerControllerInterface
{
    /********************************
    * 			CUSTOMER 			*
    *********************************/

    private $dateNow;

    public function __construct()
    {
    	$this->dateNow = Carbon::now(); 
    }

    public function registerByMobile(AddByMobile $request)
    {
        try
        {
            $input = $request->all();
            $input['email'] = $input['mobile_number'].'@user.com';
            $input['activation_token'] = strtoupper(Str::random(6));

            $customer = Customer::where('mobile_number', $input['mobile_number'])->first(); 

            if($customer)
                return response([
                    'message' => 'Mobile number already exist.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

            $customer = Customer::create($input); 
            if($customer)
                $sent = $this->sendSMS($input['mobile_number'], $customer->activation_token);

            return $this->response($customer, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function sendSMS($mobileNumber, $activationCode)
    {
        try
        {
            $ch = curl_init(); 
            $parameters = array(
                      'apikey' => '5e85186af0fd3b6054563fc0fe54f5d1', 
                      'number' => $mobileNumber,
                      'message' => 'YOUR ACTIVATION CODE: '.$activationCode, 
                      'sendername' => 'SEMAPHORE'
                      );
            curl_setopt($ch, CURLOPT_URL,'https://api.semaphore.co/api/v4/messages' );
            curl_setopt($ch, CURLOPT_POST, 1 );
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            //Send the parameters set above with the request
            curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $parameters ) );

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                $error_msg = curl_error($ch);
                return $error_msg;
            }
            curl_close($ch);
            return true;
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function validateByMobile(Request $request)
    {
    	try
        {
            $customer = Customer::where('mobile_number', request('mobile_number'))->first();

            if($customer->activation_token != $request->code)
                return response([
                    'message' => 'This activation code is invalid.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

            $customer->active = true;
            $customer->verified_at = $this->dateNow->toDateTimeString();
            $customer->save();

            return $this->response($customer, 'Successfully Validated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function registerByEmail(AddByEmail $request)
    {
    	try
        {
            $input = $request->all();
            $input['activation_token'] = strtoupper(Str::random(6));

            $email_checker = Customer::where('email', $request->email)->count();
            if ($email_checker == 1) {
                return $this->response([], 'Email already exists!', $this->unauthorizedStatus);
            } else {
                $customer = Customer::create($input); 
                $customer->notify(new RegisterByEmail($customer));

                return $this->response($customer, 'Successfully Created!', $this->successStatus);
            }

        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function validateByEmail(Request $request)
    {
        try
        {
	        $customer = Customer::whereEmail(request('email'))->first();

	        if($customer->activation_token != $request->code)
	        	return response([
                    'message' => 'This activation code is invalid.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

	        $customer->active = true;
	        $customer->verified_at = $this->dateNow->toDateTimeString();
	        $customer->save();

            return $this->response($customer, 'Successfully Validated!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => '$e->getMessage()',
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function setAccount(SetAccount $request)
    {
    	try
        {
            $salt = bcrypt(PasswordHelper::generateSalt());

            $customer = array();

            if($request->isViaEmail) {
            	$customer = Customer::whereEmail($request->email)->first();
            } elseif($request->mobile_number) {
                $customer = Customer::where('mobile_number', $request->mobile_number)->first();
            } 

            if(!$customer)
        		return response([
                    'message' => 'User not found.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

	        $customer->name = $request->full_name;
	        $customer->salt = $salt;
	        $customer->password = PasswordHelper::generate($salt, $request->password);
	        $customer->save();

	        Auth::login($customer);

	        $request = $customer->createToken(env('APP_NAME'));

            $data = [
                'token' => $request->accessToken,
                'user'  => $customer,
             ];

            return $this->response($data, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function login(Login $request)
    {
    	try
    	{
            if($request->token) {
                $customer = Customer::where('activation_token', $request->token)->first();

                Auth::login($customer);
                // get new token
                $tokenResult = $customer->createToken(env('APP_NAME'));

                $data = [
                    'token' => $tokenResult->accessToken,
                    'user'  => $customer,
                ];
                return $this->response($data, 'Successfully Logged!', $this->successStatus);
            }

            $customer = Customer::whereEmail($request->username)->first();

            if(!$customer)
                $customer = Customer::where('mobile_number', $request->username)->first();

    		if (!Hash::check(Customer::getSalt($customer->email).env("PEPPER_HASH").request('password'), $customer->password)) 
    			return response([
                    'message' => 'Unathenticated. Please login.',
                    'status' => false,
                    'status_code' => $this->notFoundStatus,
                ], $this->notFoundStatus);

    		Auth::login($customer);
		    // get new token
		    $tokenResult = $customer->createToken(env('APP_NAME'));

	        $data = [
                'token' => $tokenResult->accessToken,
                'user'  => $customer,
            ];

            return $this->response($data, 'Successfully Logged!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		 return response([
                'message' => 'Unauthenticated. Please login.', //$e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
    }

    public function social(Social $request)
    {
    	try
    	{
            $customer = Customer::whereEmail($request->email)->first();

	        if(!$customer)
	            $customer = new Customer; 
	        	$customer->name = $request->full_name;
	        	$customer->email = $request->email;
	        	$customer->provider = $request->provider;
	        	$customer->provider_id = $request->provider_id;
	        	$customer->image_url = $request->image_url;
	        	$customer->save();

	        Auth::login($customer);

	        $request = $customer->createToken(env('APP_NAME'));

	        $data = [
	            'token' => $request->accessToken,
	            'user'  => $customer,
	         ];

	        return $this->response($data, 'Successfully Loged!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		 return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
    }

    public function getProfile(Request $request)
    {
        try
        {
            $customer = auth()->user();

            if(!$customer)
                return response([
                    'message' => 'Invalid User Credentials',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            return $this->response($customer, 'Successfully Returned!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function editProfile(UpdateProfile $request)
    {
    	try
    	{
    		$customer = auth()->user();

	        if(!$customer)
	            return response([
                    'message' => 'Invalid User Credentials',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

            if ($request->first_name) {
	            $customer->first_name = $request->first_name;

            }

	        if ($request->middle_name){
                $customer->middle_name = $request->middle_name;
            } else {
                $customer->middle_name = "";
            }

	        if ($request->last_name) {
                $customer->last_name = $request->last_name;
                $customer->name = "{$request->first_name} {$request->middle_name} {$request->last_name}";
            }

            if ($request->suffix) {
	           $customer->suffix = $request->suffix;
            } else {
                $customer->suffix = "";
            }

            if ($request->birth_date != "null") {
                $customer->birth_date = $request->birth_date;
            }

            if ($request->phone_number) {
                $customer->phone_number = $request->phone_number;
            } else {
                $customer->phone_number = "";
            }

            if ($request->mobile_number) {
                $customer->mobile_number = $request->mobile_number;
            }

            if ($request->image_url != "undefined") {
	           $customer->image_url = $customer->profilePic($request->image_url);
            }
	        
            if($request->password) {
                if ($request->password != "undefined") {
                    $this->validate($request, [
                        'password' => 'confirmed|min:8',
                    ]);
                    
                    $customer->password = PasswordHelper::generate($customer->salt, $request->password);

                }
            }

			$customer->save();            

	        return $this->response($customer, 'Successfully Updated!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		 return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
    }

    public function saveAddress(AddAddress $request)
    {   
    	try
    	{
    		$customer = Customer::where('id',auth()->user()->id)->first();

            if(!$customer)
              return response([
                    'message' => 'Invalid User Credentials',
                    'status' => false,
                    'status_code' => $this->unauthorizedStatus,
                ], $this->unauthorizedStatus);

			$customer->saveAddress($request);       

	        return $this->response($customer, 'Successfully Saved!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
    }

    public function getAddressById(Request $request)
    {
        try
        {
            $user = auth()->user();
            $address = $user->getAddressById($request->id);
            return $this->response($address, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getAddresses(Request $request)
    {
        try
        {
            $user = auth()->user();
            $addresses = $user->getAddresses();
            return $this->response($addresses, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function deleteAddress(Request $request)
    {
        try
        {   
            $address = CustomerAddress::where('id',$request->id)->first();
            $address_list = CustomerAddress::all();

            foreach($address_list as $key => $val) {
                if (
                    $val->contact_name == $address->contact_name &&
                    $val->contact_number = $address->contact_number &&
                    $val->province_id == $address->province_id &&
                    $val->city_id == $address->city_id &&
                    $val->barangay_id == $address->barangay_id &&
                    $val->detailed_address == $address->detailed_address
                ) {
                    $delete_address = CustomerAddress::find($val->id);
                    $val->delete();
                }
            }
            return $this->response($address, 'Successfully Deleted!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getDetails()
    {
        try
        {
            $user = auth()->user();
            return $this->response($user, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getOrders()
    {
        try
        {
            $user = auth()->user();

            $orders = OrderDetail::where('orders.customer_id', $user->id)
            ->when(request('status_id'), function($query) {
                return $query->where('order_status_id',request('status_id'));
            })             
            ->join('orders', 'order_details.order_id', '=', 'orders.id')            
            ->join('order_status', 'order_details.order_status_id', '=', 'order_status.id')            
            ->select('orders.order_number', 'orders.customer_id', 'orders.voucher_discount_price', 'orders.points_generated', 'orders.points_consumed', 'order_details.*', 'order_status.status' )
            ->orderByDesc('orders.created_at')
            ->paginate(request('perPage'));

            return $this->responsePaginate($orders, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function getLikes()
    {
        try
        {
            $user = auth()->user();
            $wishlist = Wishlist::where('customer_id', $user->id)->paginate(request('perPage'));
            $wishlist->makeHidden(['wishlist']);

            return $this->responsePaginate($wishlist, 'Successfully Retrieved!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    public function sendInquiry(Request $request)
    {
        try
        {
            // Mail::to('support@adobomall.com')
            Mail::to('adobomall.dev@gmail.com')
                ->send(new CustomerInquiry(
                    $request->name, 
                    $request->email,
                    $request->phone, 
                    $request->subject, 
                    $request->message
                )
            );

            Mail::to($request->email)
                ->send(new InquiryResponse($request->name));

            return $this->response([], 'Successfully Sent!', $this->successStatus);
        }
        catch (\Exception $e)
        {
            return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

    // public function fetchData()
    // {
    //     $url = "https://20.222.168.92/api/v4/projects/2/issues";
    //     $token = "EM1BdWUDxv-DxbDPch99";

    //     $curl = curl_init($url);
    //     curl_setopt($curl, CURLOPT_URL, $url);
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    //     $headers = array(
    //        "Accept: application/json",
    //        "Authorization: Bearer ".$token,
    //     );
    //     curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    //     $response = curl_exec($curl);
    //     curl_close($curl);

    //     return json_decode($response);
    // }

    // public function updateData(Request $request)
    // {
    //     $url = "https://20.222.168.92/api/v4/projects/2/issues/".$request->issue_id;
    //     $token = "EM1BdWUDxv-DxbDPch99";

    //     $data = json_encode($request->all());

    //     $curl = curl_init($url);

    //     $headers = array(
    //     "Content-Type: application/json",
    //        "Accept: application/json",
    //        "Authorization: Bearer ".$token,
    //     );

    //     curl_setopt($curl, CURLOPT_URL, $url);
    //     curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    //     curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    //     curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    //     $response = curl_exec($curl);
    //     curl_close($curl);

    //     return $response;
    // }

}
