<?php

namespace App\Http\Controllers\MrSpeedy\Interfaces;

use Illuminate\Http\Request;

interface MrSpeedyControllerInterface
{
    /**
	 * @param string $matter
	 * @param string $payment_method
	 * @param string $bank_card_id	 
	 * @param string $vehicle_type_id 	 
	 * @param string $pickup_address
	 * @param string $pickup_contact_person
	 * @param string $pickup_contact_number
	 * @param string $pickup_taking_amount
	 * @param string $pickup_buying_amount
	 * @param string $pickup_note
	 * @param string $deliver_address
	 * @param string $deliver_contact_person
	 * @param string $deliver_contact_number
	 * @param string $deliver_taking_amount
	 * @param string $deliver_buying_amount
	 * @param string $deliver_note
	 * @param string $cod_fee_amount	 
	 * @return Response
	 * @SWG\Get(
	 *      path="/mr-speedy/placeOrder",
	 *      summary="Test Mr. Speedy",
	 *      tags={"Mr. Speedy API"},
	 *      description="Test Mr. Speedy",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="matter",
	 *          description="matter",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="payment_method",
	 *          description="payment_method",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="bank_card_id",
	 *          description="bank_card_id",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="vehicle_type_id",
	 *          description="vehicle_type_id",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="pickup_address",
	 *          description="pickup_address",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="pickup_contact_person",
	 *          description="pickup_contact_person",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="pickup_contact_number",
	 *          description="pickup_contact_number",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="pickup_taking_amount",
	 *          description="pickup_taking_amount",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="pickup_buying_amount",
	 *          description="pickup_buying_amount",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="pickup_note",
	 *          description="pickup_note",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="deliver_address",
	 *          description="deliver_address",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="deliver_contact_person",
	 *          description="deliver_contact_person",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="deliver_contact_number",
	 *          description="deliver_contact_number",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="deliver_taking_amount",
	 *          description="deliver_taking_amount",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),	 
	 *       @SWG\Parameter(
	 *          name="deliver_buying_amount",
	 *          description="deliver_buying_amount",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="deliver_note",
	 *          description="deliver_note",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="cod_fee_amount",
	 *          description="cod_fee_amount",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function placeOrder(Request $request);

    /**
	 * @return Response
	 * @SWG\Post(
	 *      path="/mr-speedy/callBack",
	 *      summary="Test Mr. Speedy",
	 *      tags={"Mr. Speedy API"},
	 *      description="Test Mr. Speedy",
	 *      produces={"application/json"},
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function callBack(Request $request);

    /**
	 * @param string $order_id
	 * @return Response
	 * @SWG\Get(
	 *      path="/mr-speedy/get-courier",
	 *      summary="Get Courier Location",
	 *      tags={"Mr. Speedy API"},
	 *      description="Get Courier Location",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="order_id",
	 *          description="Order ID",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getCourierLocation(Request $request);

	/**
	 * @param string $order_id
	 * @return Response
	 * @SWG\Get(
	 *      path="/mr-speedy/get-courier-sample",
	 *      summary="Get Courier Location",
	 *      tags={"Mr. Speedy API"},
	 *      description="Get Courier Location",
	 *      produces={"application/json"},
	 *      @SWG\Parameter(
	 *          name="order_id",
	 *          description="Order ID",
	 *          type="string",
	 *          required=false,
	 *          in="query"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function getCourierLocationSample(Request $request);    
}
