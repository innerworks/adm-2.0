<?php

namespace App\Http\Controllers\MrSpeedy;

use App\Http\Controllers\AppBaseController as ApiBaseController;

use App\Http\Controllers\MrSpeedy\Interfaces\MrSpeedyControllerInterface;

use Illuminate\Http\Request;
use App\Services\Helpers\MrSpeedyHelper;
use App\Models\MrSpeedyCallback;
use App\Models\MrspeedyOrder;
use App\Models\OrderDetail;

class MrSpeedyController extends ApiBaseController implements MrSpeedyControllerInterface
{
    public function placeOrder(Request $request)
    {
        $mrSpeedy = new MrSpeedyHelper();
        $data = [
            'matter' => $request->matter,
            'payment_method' => $request->payment_method,
            'bank_card_id' => $request->bank_card_id,
            'vehicle_type_id' => $request->vehicle_type_id,
            'points' => [            
                [ 
                    'address' => $request->pickup_address, 
                    'contact_person' => [ 
                        'name' => $request->pickup_contact_person, 
                        'phone' => $request->pickup_contact_number, 
                    ],
                    'taking_amount' => $request->pickup_taking_amount,
                    'buyout_amount' => $request->pickup_buying_amount, 
                    'note' => $request->pickup_note,                    
                ], 
                [ 
                    'address' => $request->deliver_address, 
                    'contact_person' => [ 
                        'name' => $request->deliver_contact_person,
                        'phone' => $request->deliver_contact_number,
                    ], 
                    'taking_amount' => $request->deliver_taking_amount, 
                    'buyout_amount' => $request->deliver_buying_amount, 
                    'note' => $request->deliver_note,                    
                ], 
            ]
        ];
        return $mrSpeedy->calculateOrder($data);
    }

    public function callBack(Request $request)
    {
        $environment = config('mrspeedy.environment');

        if ($environment == "prod") {            
            $callbackAuthToken = config('mrspeedy.prod_callback_auth_token');
        }
        else {
            $callbackAuthToken = config('mrspeedy.test_callback_auth_token');
        }

    	if (!isset($_SERVER['HTTP_X_DV_SIGNATURE'])) { 
		    echo 'Error: Signature not found'; 
		    exit; 
		} 
		 
		$data = file_get_contents('php://input'); 
		 
		$signature = hash_hmac('sha256', $data, $callbackAuthToken); 
		if ($signature != $_SERVER['HTTP_X_DV_SIGNATURE']) { 
		    echo 'Error: Signature is not found'; 
		    exit; 
		} 
		
        $callBack = array('data' => $data);
        MrSpeedyCallback::updateOrCreate($callBack);

        $orders = MrSpeedyCallback::get();
        foreach ($orders as $order) {
            $data = json_decode($order->data);
            $updatedOrder = $this->updateOrder($data);
            if($updatedOrder)
                MrSpeedyCallback::find($order->id)->delete();
        }        
    }

    public function updateOrder($data)
    {
        $order = MrspeedyOrder::where('order_id', $data->order->order_id)->first();
        if($order) {
            $order->status = $data->order->status;
            $order->status_description = $data->order->status_description;
            $order->points = $data->order->points;
            $order->save();

            $orderDetails = OrderDetail::where('mrspeedy_order_id', $data->order->order_id)->first();
            $orderDetails->order_status_id = $this->getStatus($data->order->status);
            return $orderDetails->save();            
        }
        return false;
    }

    public function getStatus($status)
    {
        switch ($status) {
            case 'new':
                return 1;
            break;

            case 'available':
                return 1;
            break;

            case 'active':
                return 2;
            break;

            case 'completed':
                return 4;
            break;

            case 'reactivated':
                return 2;
            break;

            case 'draft':
                return 1;
            break;

            case 'canceled':
                return 14;
            break;

            case 'delayed':
                return 8;
            break;
        }
    }

    public function getCourierLocation(Request $request)
    {
        $mrSpeedy = new MrSpeedyHelper();

        $data = [
            'order_id' => $request->order_id,
        ];
        return $mrSpeedy->getCourier($data);
    }

    public function getCourierLocationSample(Request $request)
    {
        $randomLoc = [
            '1' => [
                'lat' => "14.558713",
                'long' => "121.0372351"
            ],
            '2' => [
                'lat' => "14.569282",
                'long' => "121.046307"
            ],
            '3' => [
                'lat' => "14.576253",
                'long' => "121.050186"
            ],
            '4' => [
                'lat' => "14.582462",
                'long' => "121.054563"
            ],            
        ];

        $rand = rand(1,4);

        return [
            "is_successful" => true, 
            "courier" => [ 
                "courier_id" => 72384, 
                "surname" => "Mas Pogi", 
                "name" => "Pogi", 
                "middlename" =>  null, 
                "phone" => "09493593166", 
                "photo_url" => 'https://scontent.fmnl4-3.fna.fbcdn.net/v/t1.0-9/101718260_2971208376260438_8115103396290730308_o.jpg?_nc_cat=110&_nc_sid=09cbfe&_nc_eui2=AeFhpIzROwd78zzYIFJncCSFe_OK_RZAJhJ784r9FkAmEoeXI9CoBltBRgYqX_8n4_dGKZsTNvbo47chMDE8xnW2&_nc_ohc=2uCnG7ubPM4AX8bgcwC&_nc_ht=scontent.fmnl4-3.fna&oh=05bad99245d964fbc94fc9723d20992a&oe=5FA43C0D', 
                "latitude" => $randomLoc[$rand]['lat'], 
                "longitude" => $randomLoc[$rand]['long'] 
            ]
        ];
    }    
}
