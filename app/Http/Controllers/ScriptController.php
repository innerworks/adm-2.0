<?php

use App\Models\Merchants;
use App\Models\Merchant;
use App\Models\MerchantMeta;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderTrackingStatus;
use App\Models\MediaLibrary;
use App\Models\Product;
use App\Models\ProductVariant;
use Laravel\Passport\Client as OClient;


Route::get('run-script-merchants', function() {

	ini_set('max_execution_time', 1800);

    $meta_key_array  = DB::connection('mysql2')->getSchemaBuilder()->getColumnListing('merchants');
    $exception_array = ['merchant_token','status','created_by','updated_by'];

    $meta_key = [];

    $old_merchants = DB::connection('mysql2')->table('merchants')->select('*')->get();

    $list_merchants = [];
    foreach ($old_merchants as $merchant) {
    	$list_merchants[$merchant->email_address][] = $merchant;
    }

    foreach($list_merchants as $index => $merchant) {
    	$new_merchant = new Merchant;
    	$new_merchant->email = $index;
    	$new_merchant->save();
    	foreach ($merchant as $key => $value) {
    		foreach($value as $key3 => $val) {
	    		if (!in_array($key3, $exception_array)) {
	    			$new_merchant_meta = new MerchantMeta;
	    			$new_merchant_meta->merchant_id = $value->merchant_id;
	    			$new_merchant_meta->meta_key    = $key3;
	    			$new_merchant_meta->meta_value  = $val;
	    			$new_merchant_meta->created_at  = $value->created_date;
	    			$new_merchant_meta->updated_at  = $value->updated_date;
	    			$new_merchant_meta->save();
	    			if ($new_merchant_meta) {
	    				echo "sql success";
	    			} else {
	    				echo "sql failed";
	    			}
		    	}
	    	}
    	}
    }
});

Route::get('run-script-customers', function() {
	// note: email/news subscription existing on adm1
	ini_set('max_execution_time', 1800);

	$old_consumers = DB::connection('mysql2')
	   ->table('consumers')
	   ->select('consumers.consumer_id',
	   			'consumers.email',
	   			'consumers.fb_id',
	   			'consumers.google_id',
	   			'consumers.status',
	   			'consumers.created_date')
	    ->get();

	$old_consumer_profile = DB::connection('mysql2')
	   ->table('consumer_profile')
	   ->select('consumer_id',
	   			'first_name',
	   			'middle_name',
	   			'last_name',
	   			'mobile_number',
	   			'birthdate')
	    ->get();

	// step 2
	foreach ($old_consumer_profile as $profile) {
		$customer = Customer::where('id', $profile->consumer_id);
		$customer->update([
			'first_name' => $profile->first_name,
			'middle_name' => $profile->middle_name,
			'last_name' => $profile->last_name,
			'name'	=> $profile->first_name." ".$profile->last_name,
			'mobile_number' => $profile->mobile_number,
			'birth_date' => $profile->birthdate,
		]);
	}

	//step 3
	$old_consumer_social = DB::connection('mysql2')
	   ->table('consumer_social')
	   ->select('consumer_id', 'social_type', 'picture')
	   ->get();

	foreach ($old_consumer_social as $social) {
		$social_type = $social->social_type == 1 ? "Google" : "Facebook";
		$customer = Customer::where('id', $social->consumer_id);
		$customer->update([
			'provider' => $social_type,
			'image_url' => $social->picture,
		]);
	}

   $old_consumer_social2 = DB::connection('mysql2')
	   ->table('consumers')
	   ->select('fb_id', 'google_id')
	   ->get();


	foreach ($old_consumers as $consumer) {
		$old_consumer_social2 = DB::connection('mysql2')
			->table('consumer_social')
			->select('social_type')
			->get();

			foreach($old_consumer_social2 as $social) {
				$provider_id = $social->social_type == 1 ? $consumer->google_id : $consumer->fb_id;
				$customer = Customer::where('id', $consumer->consumer_id);
				$customer->update([
					'provider_id' => $provider_id,
				]);
			}
	}

	// $old_consumer_address = DB::connection('mysql2')
	// 	->table('consumer_address')
	// 	->select('consumer_id','address_type','name','address1','city_id','province_id','contact_no','zip_code')
	// 	->get();

	// foreach ($old_consumer_address as $address) {
	// 	$customer = CustomerAddress::where('id', $address->consumer_id);
	// 	$customer->update([
	// 		'costumer_id' => $address->consumer_id,
	// 		'type' 		  => $address->address_type,
	// 		'contact_name' => $address->name,
	// 		'detailed_address' => $address->address1,
	// 		'province_id' => $address->province_id,
	// 		'city_id' => $address->city_id,
	// 		'contact_number' => $address->contact_no,
	// 		'zip_code' => $address->zip_code,
	// 	]);
	// }

	$list_consumers = [];
	$i = 0;

	$new_customer = Customer::get();

	// foreach($old_consumers as $consumer) {

	// 	$list_consumers['consumers'][$i] = $consumer;	
	// 	$i++;
	// }

	// step 1
	// foreach($list_consumers['consumers'] as $key => $value) {
		// $new_customers = new Customer;
		// $new_customers->id = $value->consumer_id;
		// $new_customers->email = $value->email;
		// $new_customers->provider = null;
		// $new_customers->provider_id = null;
		// $new_customers->created_at = $value->created_date;
		// $new_customers->save();
	// }

	// 	$new_customers = new Customer;
 //    	$new_customers->id = $value->consumer_id;
 //    	$new_customers->name = $value->first_name." ".$value->last_name;
 //    	$new_customers->email = $value->email;
 //    	$new_customers->active = $value->status;
 //    	$new_customers->first_name = $value->first_name;
 //    	$new_customers->middle_name = $value->middle_name;
 //    	$new_customers->last_name = $value->last_name;
 //    	$new_customers->birth_date = $value->birthdate;
 //    	$new_customers->mobile_number = $value->mobile_number;
 //    	$new_customers->provider = null;
 //    	$new_customers->provider_id = null;
 //    	$new_customers->image_url = null;
 //    	$new_customers->created_at = $value->created_date;
 //    	$new_customers->save();

 //    	$new_address = new CustomerAddress;
 //    	$new_address->customer_id = $value->consumer_id;
 //    	$new_address->first_name = $value->first_name;
 //    	$new_address->middle_name = $value->middle_name;
 //    	$new_address->last_name = $value->last_name;
 //    	$new_address->contact_name = $value->first_name." ".$value->last_name;
 //    	$new_address->contact_number = $value->contact_no;
 //    	$new_address->country_id = 175;
 //    	$new_address->region_id = 0;
 //    	$new_address->province_id = 0;
 //    	$new_address->city_id = 0;
 //    	$new_address->barangay_id = 0;
 //    	$new_address->zip_code = $value->zip_code;
 //    	$new_address->detailed_address = $value->address1;
 //    	$new_address->default = 0;
 //    	$new_address->type = $value->address_type;
 //    	$new_customers->save();
	// }
});

Route::get('run-script-customers-address', function() {
	ini_set('max_execution_time', 1800);

	// step 4
	$old_consumers = DB::connection('mysql2')
	   ->table('consumers')
	   ->select('consumers.consumer_id',
	   			'consumers.email',
	   			'consumers.fb_id',
	   			'consumers.google_id',
	   			'consumers.status',
	   			'consumers.created_date')
	    ->get();


	$old_consumer_address = DB::connection('mysql2')
		->table('consumer_address')
		->select('consumer_id','address_type','name','address1','city_id','province_id','contact_no','zip_code')
		->get();

	foreach ($old_consumer_address as $address) {
		$customer = CustomerAddress::create([
			'customer_id' => $address->consumer_id,
			'type' 		  => $address->address_type,
			'contact_name' => $address->name,
			'detailed_address' => $address->address1,
			'province_id' => $address->province_id,
			'city_id' => $address->city_id,
			'contact_number' => $address->contact_no,
			'zip_code' => $address->zip_code,
		]);
	}
});

Route::get('run-script-order-items', function() {
	ini_set('max_execution_time', 1800);

	$old_order_items = DB::connection('mysql2')
	   ->table('order_items')
	   ->select('order_id',
	   			'product_id',
				'merchant_id',
				'product_qty',
				'product_srp',
				'product_sale_price',
				'product_amount',
				'shipping_fee',
				'comm_rate',
				'orderitem_status',
				'created_date',
				'updated_date'
				)
	    ->get();

	foreach ($old_order_items as $order_items) {
		$customer = OrderDetail::create([
			'order_id' => $order_items->order_id,
			'product_id' => $order_items->product_id,
			'merchant_id' => $order_items->merchant_id,
			'qty' => $order_items->product_qty,
			'base_price' => $order_items->product_srp,
			'selling_price	' => $order_items->product_sale_price,
			'total_price' => $order_items->product_amount,
			'shipping_fee' => $order_items->shipping_fee,
			'commission_rate' => $order_items->comm_rate,
			'created_at' => $order_items->created_date,
			'updated_at' => $order_items->updated_date,
		]);
	}
});

Route::get('run-script-product-media', function() {
	ini_set('max_execution_time', 1800);

	$old_media = DB::connection('mysql2')
	   ->table('product_media')
	   ->select('product_id',
	   			'file_name',
	   			'file_type',
	   			'file_size',
	   			'product_dimension',
	   			'server_destination',
	   			'uri',
	   			'uploaded_date',
	   			'updated_date'
				)
	    ->get();

	foreach ($old_media as $media) {
		$customer = MediaLibrary::create([
			'id' => $media->product_id,
			'file_name' => $media->file_name,
			'file_type' => $media->file_type,
			'file_size' => $media->file_size,
			'dimensions' => $media->product_dimension,
			'file_url' => $media->server_destination.$media->file_name,
			'created_at' => $media->uploaded_date,
			'updated_at' => $media->updated_date
		]);
	}
});

Route::get('run-script-product-primary-photo', function() {
	ini_set('max_execution_time', 1800);

	//product_uploads
	$primary_photo = DB::connection('mysql2')
		->table('products')
		->select('products.product_id as product_id','product_media.server_destination as server_destination','product_media.file_name as file_name')
		->leftJoin('product_media','products.product_primary_image_id','=','product_media.media_id')
		->get();

	foreach ($primary_photo as $primary) {
		$customer = Product::where('id',$primary->product_id)->update([
			'primary_photo' => $primary->server_destination.$primary->file_name
		]);
	}
});


Route::get('run-script-orders', function() {
	ini_set('max_execution_time', 1800);

	$orders = DB::connection('mysql2')
		->table('orders')
		->select('order_id', 'order_num','consumer_id','created_date')
		->get();

	foreach ($orders as $order) {
		$order_save = Order::create([
			'id' 		   => $order->order_id,
			'order_number' => $order->order_num,
			'customer_id'  => $order->consumer_id,
			'created_at' => $order->created_date
		]);
	}
});

Route::get('run-script-order-history', function() {
	ini_set('max_execution_time', 1800);

	$order_histories = DB::connection('mysql2')
		->table('order_history')
		->select('order_history.order_history_id as order_history_id','order_history.order_id','order_history.status','order_history.details','order_history.updated_date as updated_date','orders.tracking_number as tracking_number','orders.merchant_id')
		->leftJoin('orders','order_history.order_id','=','orders.order_id')
		->get();

	// echo '<pre>';
	// print_r($order_histories[0]->updated_date);die();

	foreach ($order_histories as $history) {
		$status = [
			'NEW' 		 		=> 7,
			'READY_TO_SHIP'		=> 2,
			'PICKUP_PENDING'	=> 1,
			'PICKUP_COMPLETED'	=> 3,
			'PICKUP_COMPLETED1'	=> 3,
			'DELIVER_PENDING'	=> 20,
			'IN_TRANSIT' 		=> 21,
			'COMPLETED'  		=> 4,
			'FAILED_TO_DELIVER'	=> 25,
			'CANCELLED'  		=> 23,
			'FOR_REPLACEMENT'	=> 11,
			'ON_HOLD'			=> 8,
			'REJECTED'			=> 15,
		];

		$order_status = isset($status[$history->status]) ? $status[$history->status] : "";

		$order_tracking_status  = OrderTrackingStatus::create([
			'id' 	   			=> $history->order_history_id,
			'order_id' 			=> $history->order_id,
			'order_status_id' 	=> $order_status,
			'merchant_id' 		=> $history->merchant_id,
			'tracking_number' 	=> $history->tracking_number,
			'created_at' 		=> $history->updated_date
		]);
	}
});

Route::get('run-script-order-detail-order-tracking', function() {
	ini_set('max_execution_time', 1800);

	$order_details = DB::table('order_details')
		->select('id', 'order_id')
		->get();

	foreach ($order_details as $order_detail) {
		$order_detail_id = OrderTrackingStatus::where('order_id',$order_detail->order_id)->update([
			'order_detail_id' => $order_detail->id
		]);
	}
});

Route::get('run-script-product-variants', function() {
	ini_set('max_execution_time', 1800);

	$products = DB::connection('mysql2')
		->table('products')
		->select('products.product_id', 'products.product_primary_image_id','products.srp','products.sale_price','products.color','products.size')
		->get();

	$gallery = [];
	
	foreach ($products as $product) {
		$product_media = DB::connection('mysql2')
			->table('product_media')
			->select('product_id','server_destination','file_name')
			->where('product_id',$product->product_id)
			->get();

		$product_id_check = isset($gallery[$product->product_id]) ? $product->product_id : 0;

		$temporary_arr = [];
		foreach ($product_media as $media) {
			$obj = new stdClass();
			$obj->image = $media->server_destination.$media->file_name;
			// $gallery[$product->product_id][] = $obj;
			array_push($temporary_arr, $obj);
		}

		$product_id = ProductVariant::create([
			'product_id'	=> $product->product_id,
			'variant'		=> "",
			'price'			=> $product->srp,
			'image_gallery' => json_encode($temporary_arr)
		]);

		// unset($gallery[$product->product_id]);
	}

});

Route::get('run-script-variant-s3', function() {
	ini_set('max_execution_time', 1800);

	$product_variants = DB::table('product_variants')->where('image_gallery', 'like', '%\\"%')->first();

	foreach($product_variants as $variant) {
		// $gallery = json_encode($variant->image_gallery);
		// json first modification
		// $gallery = str_replace('\\\\','', $gallery);
		// $gallery = str_replace('\/var\/www\/','https:\/\/ammediaserver.s3.amazonaws.com\/', $gallery);
		
		// $variant_update = ProductVariant::where('id',$variant->id)->update([
		// 	'image_gallery' => $gallery
		// ]);

		// json second modification
		// $gallery = str_replace('s3:\/\/ammediaserver\/','https:\/\/ammediaserver.s3.amazonaws.com\/', $variant->image_gallery);

		// $variant_update = ProductVariant::where('id',$variant->id)->update([
		// 	'image_gallery' => $gallery
		// ]);

		// json third modification

	}

	echo 'success';
});