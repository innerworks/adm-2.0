<?php

namespace App\Http\Requests\Api\Customer;

use Illuminate\Foundation\Http\FormRequest;

class AddAddress extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string',
            'id' => 'nullable', 
            'contact_name' => 'required|string', 
            'contact_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:13',
            'country_id' => 'required',
            // 'region_id' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'barangay_id' => 'required',
            'zip_code' => 'required|string',
            'detailed_address' => 'required|string', 
            'default' => 'nullable', 
            // 'type'    => 'nullable',
            'default_shipping' => 'nullable',
            'default_billing' => 'nullable',
        ];
    }
}
