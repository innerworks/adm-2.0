<?php

namespace App\Http\Requests\Api\Product;

use Illuminate\Foundation\Http\FormRequest;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' => 'required|string',
            'name' => 'required|string',
            'description' => 'required|string',
            // 'merchant_id' => 'required|integer',
            'category_id' => 'required|integer',
            'brand_id' => 'nullable',
            'status' => 'nullable',
            'base_price' => 'required|between:0,99.99',
            'selling_price' => 'required|between:0,99.99',                  
            'sizes' => 'nullable',
            'variants' => 'nullable',
            'addons' => 'nullable',
            'weight' => 'nullable|between:0,99.99',
            'length' => 'nullable|between:0,99.99',
            'width' => 'nullable|between:0,99.99',
            'height' => 'nullable|between:0,99.99',
            'other_information' => 'nullable|string',
            'inventories' => 'nullable'
        ];
    }
}
