<?php

namespace App\Http\Requests\Api\Merchant;

use Illuminate\Foundation\Http\FormRequest;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'storeName' => 'required|string',
            'storeAddress' => 'required|string',
            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
            'firstName' => 'required|string',
            'middleName' => 'nullable|string',
            'lastName' => 'required|string',
            'email' => 'required|email',
            'username' => 'required|string',
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:13',
            'phone' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:7',
            'password' => 'required|string',
            'brand' => 'nullable',
            'category' => 'required|integer',
            'status' => 'required|integer',
            'businessName' => 'nullable|string',
            'permitNumber' => 'nullable|string',
            'busPermitDate' => 'nullable|string',
            'tradeStyle' => 'nullable|string',
            'tin' => 'nullable|string',
            'dateStartedOperation' => 'nullable|string',
            'secRegistrationNumber' => 'nullable|string',
            'secRegistrationDate' => 'nullable|string',
            'dtiRegistationNumber' => 'nullable|string',
            'dtiRegistationDate' => 'nullable|string',
            'businessType' => 'nullable|string',
            'scheduledOrder' => 'nullable',
            'sunday' => 'nullable',
            'sundayTime' => 'nullable|string',
            'monday' => 'nullable',
            'mondayTime' => 'nullable|string',
            'tuesday' => 'nullable',
            'tuesdayTime' => 'nullable|string',
            'wednesday' => 'nullable',
            'wednesdayTime' => 'nullable|string',
            'thursday' => 'nullable',
            'thursdayTime' => 'nullable|string',
            'friday' => 'nullable',
            'fridayTime' => 'nullable|string',
            'saturday' => 'nullable',
            'saturdayTime' => 'nullable|string',
            'otherInfo' => 'nullable|string',
        ];
    }
}
