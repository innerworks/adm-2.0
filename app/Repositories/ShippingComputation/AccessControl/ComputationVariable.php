<?php
namespace App\Repositories\ShippingComputation\AccessControl;



class ComputationVariable
{

    var $cbm;
    var $itemWeight;
    var $valFeeCharge;
    var $codFeeCharge;
    var $ownPackaging;
    var $shipRate;

    public function setCbm($length, $width, $height, $qty)
    {
        $this->cbm = ($length * $width * $height) * $qty;
    }

    public function getCbm()
    {
        return $this->cbm;
    }

    public function setWeight($weight, $qty)
    {
        $this->itemWeight = $weight * $qty;
    }

    public function getWeight()
    {
        return $this->itemWeight;
    }

    public function setValFeeCharge($price, $qty, $fee)
    {
        $this->valFeeCharge = $price * $qty * $fee;
    }

    public function getValFeeCharge()
    {
        return $this->valFeeCharge;
    }

    public function setCodFeeCharge($price, $qty, $fee)
    {
        $this->codFeeCharge = $price * $qty * $fee;
    }

    public function getCodFeeCharge()
    {
        return $this->codFeeCharge;
    }

    public function setOwnPackaging($max, $weight, $computationFee, $amt)
    {
        $this->ownPackaging = ($max - $weight) * $computationFee + $amt;
    }

    public function getOwnPackaging()
    {
        return $this->ownPackaging;
    }

    public function setShipRate($deliveryRate, $valFee, $codFee)
    {
        $this->shipRate = $deliveryRate + $valFee + $codFee;
    }

    public function getShipRate()
    {
        return $this->shipRate;
    }


}
