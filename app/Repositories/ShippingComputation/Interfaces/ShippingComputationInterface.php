<?php
namespace App\Repositories\ShippingComputation\Interfaces;

interface ShippingComputationInterface
{
    public function getComputedFee($param);

}
