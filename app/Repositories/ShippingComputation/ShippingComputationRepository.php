<?php
namespace App\Repositories\ShippingComputation;

use App\Area;
use App\Brand;
use App\PackageType;
use App\ProductPriceColor;
use App\Region;
use App\User;
use App\Cart;
use App\Courier;
use App\Checkout;
use App\CourierArea;
use App\CourierRate;
use App\UserDetail;
use App\DeliveryBoundary;
use App\CourierExcessRate;
use App\ProductCourierSpecial;
use App\CourierSpecial;
use App\Base\BaseRepository;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ShippingComputation\AccessControl\ComputationVariable;
use App\Repositories\ShippingComputation\Interfaces\ShippingComputationInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


class ShippingComputationRepository extends BaseRepository implements ShippingComputationInterface
{

    protected $cartModel;
    protected $checkoutModel;
    protected $courierRateModel;
    protected $computationClass;
    protected $packageTypeModel;
    protected $brand;


    public function __construct(Cart $cartModel,
                                ComputationVariable $computationClass,
                                CourierRate $courierRateModel,
                                Checkout $checkoutModel,
                                PackageType $packageTypeModel,
                                Brand $brand
    ){
        $this->cartModel = $cartModel;
        $this->checkoutModel = $checkoutModel;
        $this->computationClass = $computationClass;
        $this->courierRateModel = $courierRateModel;
        $this->packageTypeModel = $packageTypeModel;
        $this->brand = $brand;
    }

    public function findKeyId($courier, $base){

        $arrayArea          =   CourierArea::where('courier_id', $courier->id)->get();
        $pregBaseStr        =   preg_replace("/[^a-zA-Z]/", " ", $base);
        $explode            =   explode(" ", $pregBaseStr);
        $filterArr          =   array_values(array_filter($explode));

        $checkArr = [];
        foreach ($arrayArea as $val){
            $checkArr[$val->id] = $val['state'];
        }

        $keys = array_keys(array_intersect($checkArr,$filterArr));

        $dataCourierAreaId = "";
        foreach ($keys as $key) {
            $dataCourierAreaId = intval($key);
        }

        return $dataCourierAreaId == "" ? "empty" : $dataCourierAreaId;

    }
    public function findShippingArea($courier){

        $user = User::find(Auth::user()->id);

        if($user['ship_user_detail']){
            $getShipId          =   UserDetail::find($user['ship_user_detail']);
            $regionBase         =   Region::find($getShipId->region_id);
            $areaBase           =   Area::find($regionBase->area_id);

            if($regionBase->id == 14){//default MM and NCR
                $defaultSetUp = ['METRO MANILA', 'NCR'];
                $getShipIdData = CourierArea::where('courier_id', $courier->id)
                    ->whereIn('state', $defaultSetUp)
                    ->first();
                $dataCourierAreaId = $getShipIdData->id;

            }else{
                $isKey = $this::findKeyId($courier, $regionBase->name);
                if($isKey != "empty"){//find in region
                    $dataCourierAreaId = $isKey;

                }else{//find area id
                    $isKey = $this::findKeyId($courier, $areaBase->area);

                    $dataCourierAreaId = $isKey;
                }
            }
        }else{
            $dataCourierAreaId = NULL;
        }

        return $dataCourierAreaId;
    }

    public function getComputedFee($param){
        $batchByBrand = [];
        $getCheckedItems = $this->cartModel->with(
            ['priceColor',
                'product',
                'sizeVariation',
                'sizeType.countrySize',
                'sizeType.size',
                'brand'
            ])
            ->where('for_booking', 0)
            ->where('checked', 1)
            ->where('user_id', Auth::user()->id)
            ->get();

        //parent
        $courier            =   Courier::where('is_basis', 1)->first();
        $getFindShipId      =   $this::findShippingArea($courier);

        if($getFindShipId == NULL || $getFindShipId == "empty"){
            foreach ($getCheckedItems as $data){

                if($param == 'cart'){
                    $cart = Cart::find($data->id);
                    $cart->shipping_fee = NULL;
                    $cart->save();

                }else{
                    $cart = Checkout::find($data->id);
                    $cart->shipping_fee = NULL;
                    $cart->save();
                }
            }//end foreach
        }else{
            $boundRate          =   DeliveryBoundary::find(1);
            $getArea            =   CourierArea::find($getFindShipId);
            $getExcessRate      =   CourierExcessRate::where('courier_id', $courier->id)->first();

            // Valuation Fee Computation - E-Nortel
            if ($getArea->state == 'NCR') {
                // $valuation_fee = "0.01";
                $valuation_fee = "0";
            } else {
                // $valuation_fee = "0.01";
                $valuation_fee = "0";
            }

            /***double check dimensions***/
            $set = [];
            foreach ($getCheckedItems as $data) {
                if($data->priceColor->sale_price == 0){
                    $price = $data->priceColor->price;
                }else{
                    $price = $data->priceColor->sale_price;
                }

                /* GET PRODUCT LEVEL */
                $discount_rate = 0;
                $product_rate = ProductCourierSpecial::where('product_id', $data->product_id)->first();
                if($product_rate) {
                    /* GET COURIER PER PRODUCT */
                    $custom_courier = Courier::find($product_rate->courier_id);
                    /* GET SPECIAL RATE */
                    $special_rate = CourierSpecial::find($product_rate->courierspecial_id);
                    $discount_rate = $special_rate->rate_discount/100;
                }
                else{
                    /* GET COURIER PER MERCHANT */
                    $custom_courier = Courier::where('is_active', 1)
                    ->where('is_default', 1)
                    ->where('owner_id', $data->merchant_detail_id)
                    ->join('merchant_courier', 'couriers.id', '=', 'merchant_courier.courier_id')
                    ->first();                    
                }

                $customArea = null;
                $customExcessRate = null;
                if($custom_courier){
                    $customFindShipId      =   $this::findShippingArea($custom_courier);
                    $customArea            =   CourierArea::find($customFindShipId);
                    $customExcessRate      =   CourierExcessRate::where('courier_id', $custom_courier->id)->first();
                }

                $set[$data->brand_id][] = [
                        'l'         =>  $data->sizeVariation->package_length,
                        'w'         =>  $data->sizeVariation->package_width,
                        'h'         =>  $data->sizeVariation->package_height,
                        'q'         =>  $data->qty,
                        'weight'    =>  $data->sizeVariation->package_weight,
                        'brand_id'  =>  $data->brand_id,
                        'price'     =>  $price,
                        'courier'   =>  $custom_courier ? $custom_courier : $courier,
                        'area_id'   =>  $customArea ? $customArea->id : $getArea->id,
                        'excess_rates' => $customExcessRate ? $customExcessRate: $getExcessRate,
                        'discount_rate' => $discount_rate,
                ];
            }

            /***compress and check each brand id***/
            $compressData = [];
            foreach ($set as $key => $data){
                $sumL = 0;
                $sumW = 0;
                $sumH = 0;
                $sumQ = 0;
                $sumWeight = 0;
                $sumPrice = 0;
                foreach ($data as $row){
                    $compressData[$key] = [
                        'length'    => $sumL+=$row['l'],
                        'width'     => $sumW+=$row['w'],
                        'height'    => $sumH+=$row['h'],
                        'quantity'  => $sumQ+=$row['q'],
                        'weight'    => $sumWeight+=$row['weight'] * $row['q'],
                        'brand_id'  => $row['brand_id'],
                        'price_result' => $sumPrice+=$row['price'] * $row['q'],
                        'valuation_fee' => $valuation_fee,
                        'area_id'   =>  $row['area_id'],
                        'excess_rates' => $row['excess_rates'],
                        'discount_rate' => $row['discount_rate'],
                    ];
                }
            }

            /***compare lxh dimension and save depends on rate***/
            foreach ($compressData as $key => $item){

                $this->computationClass->setCbm($item['length'],
                    $item['width'],
                    $item['height'],
                    $item['quantity']);

                $compare = [
                    'weight'    => number_format($item['weight'],2,".", ""),
                ];

                $rate = $this->packageTypeModel->getMaxWeightByAreaId($item['area_id']);
                $getPackageType = $this->packageTypeModel->getPackageRate(max($compare), $item['area_id']);

                if($rate->max_weight > max($compare) ){
                    $this::normalRate($item,$getPackageType);
                }else{
                    $this::ownPackage($item,$item['excess_rates'], max($compare));
                }

                $comp = Cart::where('brand_id', $key)
                    ->where('user_id', Auth::user()->id)
                    ->where('checked', 1)
                    ->groupBy('brand_id')
                    ->first();

                $batchByBrand[$key] = $comp ? $comp->shipping_fee : null;
            }
        }

        $data = [
            'totalShipByBrand'      =>    $batchByBrand,
            'totalShipping'         =>    array_sum($batchByBrand)
        ];
        return $data;
    }

    public function normalRate($item,$getPackageType){
        $valFeeCharge = ($item['price_result']) * $item['valuation_fee'];

        if ($item['weight'] > 3) {
            $excess = CourierExcessRate::where('courier_area_id', $getPackageType->courier_area_id)->first();
            $excess_item_weight = $item['weight'] - 3;
            $excess_charge = $excess->computation_rate * (abs($excess_item_weight));
            $finalShipFee = $getPackageType->delivery_rate + $excess_charge + $valFeeCharge;
        } else {
            $finalShipFee = $getPackageType->delivery_rate + $valFeeCharge;
        }

        if($item['discount_rate']) {
            $finalShipFee = $finalShipFee - ($finalShipFee*$item['discount_rate']);
        }

        Cart::where('user_id', Auth::user()->id)
        ->where('brand_id',$item['brand_id'])
        ->update([
            'package_id'    => $getPackageType->package_id,
            'shipping_fee'  => number_format($finalShipFee, 2, ".", ","),
        ]);
    }

    public function ownPackage($item, $getExcessRate, $packageW){

        $valFeeCharge = $item['price_result'] * $item['quantity'] * $item['valuation_fee'];

        $this->computationClass->setOwnPackaging(
            $packageW,
            $getExcessRate->weight,
            $getExcessRate->computation_rate,
            $getExcessRate->amount
        );

        $excessPackRate = $this->computationClass->getOwnPackaging();
        $finalShipFee = $excessPackRate + $valFeeCharge;

        Cart::where('user_id', Auth::user()->id)
        ->where('brand_id',$item['brand_id'])
        ->update([
            'package_id'    => $getExcessRate->package_id,
            'shipping_fee'  => $finalShipFee,
        ]);
    }

}
