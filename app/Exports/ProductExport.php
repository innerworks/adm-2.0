<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductExport implements FromCollection, WithHeadings
{
    public $products;

    public function __construct($products)
    {
        $this->products = $products;
    }
    /**
    * Headings
    */
    public function headings(): array
    {
        return [
            'name',
            'sku',
            'description',
            'status',
            'merchant name',
            'category',
            'brand',
            'base price',
            'selling price',
            'package_weight',
            'package_length',
            'package_width',
            'package_height',
            'primary_photo',
            'size',
            'tags',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        foreach ($this->products as $product) {
            $responseData[] = [
                'name' => $product['name'],
                'sku' => $product['sku'],
                'description' => $product['description'],
                'status' => $product['status'],
                'merchant name' => $product['store_name'],
                'category' => $product['category_name'],
                'brand' => $product['brand_name'],
                'base price' => $product['base_price'],
                'selling price' => $product['selling_price'],
                'package_weight' => $product['weight'],
                'package_length' => $product['length'],
                'package_width' => $product['width'],
                'package_height' => $product['height'],
                'primary_photo' => $product['primary_photo'],
                'size' => $product->sizes_csv,
                'tags' => ($product['tags']) ? $product['tags'] : null,
            ];
        }

        return collect($responseData);
    }
}
