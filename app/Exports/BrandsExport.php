<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\Models\Brand;

class BrandsExport implements FromCollection, WithHeadings
{
    /**
    * Headings
    */
	public function headings(): array
    {
        return [
            'Name',
            'Slug Name',
            'Logo',
            'Filename',
            'Distribution Type',
            'Active'           
        ];
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $brands = Brand::all();

        $responseData = [];

        foreach ($brands as $brand)
        {
            $responseData[] = [
                'name' => $brand->name,
                'slug_name' => $brand->slug_name,
                'logo' => $brand->logo,
                'filename' => $brand->filename,
                'distribution_type' => $brand->distribution_type,
                'active' => $brand->active
            ];
        }

        return collect($responseData);     
    }
}
