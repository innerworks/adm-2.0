<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\Models\Category;

class CategoryExport implements FromCollection, WithHeadings
{
	/**
    * Headings
    */
	public function headings(): array
    {
        return [
            'Parent ID',
            'Category',
            'Slug Name',
            'Sort Order',
            'Image',
            'File Name',
            'Merchant ID'           
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $categories = Category::all();

        $responseData = [];

        foreach ($categories as $category)
        {
            $responseData[] = [
                'parent_id' => $category->parent_id,
                'name' => $category->name,
                'slug_name' => $category->slug_name,
                'sort_order' => $category->sort_order,
                'image' => $category->image,
                'filename' => $category->filename,
                'merchant_id' => $category->merchant_id
            ];
        }

        return collect($responseData);    
    }
}
