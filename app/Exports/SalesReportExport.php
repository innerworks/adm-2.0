<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SalesReportExport implements FromCollection, WithHeadings
{
    public $sales;

    public function __construct($sales)
    {
        $this->sales = $sales;
    }
    /**
    * Headings
    */
    public function headings(): array
    {
        return [
            'Order Number',
            'Order Date',
            'Delivery Date',
            'Product Name',
            'Product Size',
            'Merchant Name',
            'Brand Name',
            'Category Name',
            'Order Status',
            'Quantity',
            'Product Price',
            'Price Purchased',
            'Commission Rate',
            'Customer Name',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        foreach ($this->sales as $sale) {
            $responseData[] = [
                'Order Number' => $sale['order_number'],
                'Order Date' => $sale['created_at'],
                'Delivery Date' => $sale['received_date'],
                'Product Name' => $sale['product_name'],
                'Product Size' => $sale['product_size'],
                'Merchant Name' => $sale['merchant_name'],
                'Brand Name' => $sale['brand_name'],
                'Category Name' => $sale['category_name'],
                'Order Status' => $sale['order_status'],
                'Quantity' => $sale['qty'],
                'Product Price' => $sale['base_price'],
                'Price Purchased' => $sale['total_price'],
                'Commission Rate' => $sale['commission_rate'],
                'Customer Name' => $sale['customer_name']
            ];
        }

        return collect($responseData);
    }
}
