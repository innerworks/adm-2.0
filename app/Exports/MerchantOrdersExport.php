<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\Models\OrderDetail;
use Carbon\Carbon;

class MerchantOrdersExport implements FromCollection, WithHeadings
{
    public $dateFrom;
    public $dateTo;
    public $order;
    public $merchantId;

    public function __construct(string $dateFrom, string $dateTo, int $merchantId = 0)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->merchantId = $merchantId;
    }
	/**
    * Headings
    */
	public function headings(): array
    {
        return [
            'Order ID',
            'Order Date',
            'Buyer Name',
            'Buyer Email',
            'Buyer Mobile Number',
            'Product Name',
            'Size',
            'Variant',
            'Add Ons',
            'Brand',
            'Merchant Name',
            'Payment type',
            'Status',           
            'Qty',           
            'Total Price',           
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $orderItems = OrderDetail::when($this->merchantId, function($query){
            return $query->where('order_details.merchant_id', $this->merchantId);
        })
        ->where('orders.paid_date','>=', Carbon::create($this->dateFrom)->format('Y-m-d'))
        ->where('orders.paid_date','<=', Carbon::create($this->dateTo)->addDays(1)->format('Y-m-d'))
        ->join('orders', 'order_details.order_id', '=', 'orders.id')
        ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
        ->leftJoin('categories', 'order_details.category_id', '=', 'categories.id')
        ->leftJoin('order_status', 'order_details.order_status_id', '=', 'order_status.id')            
        ->leftJoin('payment_types', 'orders.payment_type_id', '=', 'payment_types.id')            
        ->selectRaw('order_details.*, orders.order_number, orders.order_date , CONCAT(customers.first_name, " " ,customers.last_name) AS customer_name, customers.email, order_status.status, payment_types.payment_type')
        ->get();

        $orderVariants = [];
        $orderAdons = [];
        $orderCustomer = [];
        $responseData = [];

        foreach ($orderItems as $item) {
            $orderVariants = $item->variants;
            $orderAddons = $item->addOns;
            $orderCustomer = $item->customer_address;

            $variants = '';
            foreach ($orderVariants as $variant) {
                if($variants)
                    $variants .= ' | '.$variant->variant;
                $variants .= $variant->variant;
            }

            $addons = '';
            foreach ($orderAddons as $addon) {
                if($addons)
                    $addons .= ' | '.$addon->addon;
                $addons .= $addon->addon;
            }

            $responseData[] = [
                'order_id' => "'".$item->order_number."'",
                'order_date' => $item->order_date,
                'buyer_name' => $item->customer_name,
                'buyer_email' => $item->email,
                'buyer_mobile_number' => $orderCustomer['mobile_number'],
                'product_name' => $item->product_name,
                'product_size' => $item->size_name,
                'variants' => $variants,
                'add_ons' => $addons,
                'brand' => $item->brand['name'],
                'merchant' => $item->merchant['store_name'],
                'payment_type' => $item->payment_type,
                'status' => $item->status,           
                'qty' => $item->qty,           
                'total_price' => $item->total_price,
            ];
        }

        return collect($responseData);
    }
}
