<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Order;
use App\Models\ProductSize;
use App\Models\MerchantMeta;
use App\Models\Category;
use App\Models\Brand;
use App\Models\CommissionRate;
use App\Models\OrderStatus;
use App\Models\Customer;

class OrderDetailsViewModel extends Model
{
    use SoftDeletes;

     /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'order_details';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:M d, Y H:i:s',
        'received_date' => 'datetime:M d, Y H:i:s',
    ];

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'order_number',
        'product_size',
        'merchant_name',
        'brand_name',
        'category_name',
        'commission_rate',
        'order_status',
        'customer_name',
    ];

    public function getOrderNumberAttribute()
    {
        return Order::find($this->order_id)->order_number;
    }

    public function getProductSizeAttribute()
    {
        return ProductSize::find($this->size_id)->size;
    }

    public function getMerchantNameAttribute()
    {
        return MerchantMeta::where('merchant_id', $this->merchant_id)
        ->where('meta_key', 'store_name')
        ->first()->meta_value;
    }

    public function getBrandNameAttribute()
    {
        $brand = Brand::find($this->brand_id);
        if($brand)
            return $brand->name;

        return null;
    }

    public function getCategoryNameAttribute()
    {
        $category = Category::find($this->category_id);
        if($category)                
            return $category->name;
        return null;
    }

    public function getCommissionRateAttribute()
    {
        $commission_rate = CommissionRate::where('product_id', $this->product_id)->first();
        if($commission_rate)
            return $commission_rate->product_rate;

        $commission_rate = CommissionRate::where('category_id', $this->category_id)->first();
        if($commission_rate)
            return $commission_rate->category_rate;

        $commission_rate = CommissionRate::where('merchant_id', $this->merchant_id)->first();
        if($commission_rate)
            return $commission_rate->merchant_rate;

        return 0;
    }

    public function getOrderStatusAttribute()
    {
        return OrderStatus::find($this->order_status_id)->status;
    }

    public function getCustomerNameAttribute()
    {
        return Customer::find(Order::find($this->order_id)->customer_id)->name;
    }
}
