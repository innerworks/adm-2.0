<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Models\Brand;

use Storage;

class BrandsImport implements ToCollection, WithHeadingRow 
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            Brand::updateOrCreate(
            	[
                	'name' => $row['name']
            	],	
            	[
	                'slug_name' => str_slug($row['name'], "_"),
	                'distribution_type' => strtolower($row['distribution_type']),
	                'active' => $row['active'],
                    // 'image_url' => $this->uploadImageFromUrl($row['logo']),
        	    ]
        	);
        }
    }

    public function uploadImageFromUrl($url) {
        if(!$url)
            return null;
        return $url;
        $contents = file_get_contents($url);
        $name = substr($url, strrpos($url, '/') + 1);
        Storage::put('public/brands/'.$name, $contents);
        return asset('storage/brands/'. $name);
    }
}
