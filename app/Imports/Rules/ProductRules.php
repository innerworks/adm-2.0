<?php

namespace App\Imports\Rules;

class ProductRules 
{

    public static function rules(){
        return [
            '*.name' => 'required',   
            '*.sku' => 'required',
            '*.description' => 'required',  
            '*.status' => 'required',    
            '*.merchant_name' => 'required|exists:merchant_meta,meta_value',    
            '*.category' => 'required|exists:categories,name',    
            '*.brand' => 'nullable|exists:brands,name',    
            '*.base_price' => 'required', 
            '*.selling_price' => 'required',   
            '*.package_weight' => 'required',  
            '*.package_length' => 'nullable',    
            '*.package_width' => 'nullable',    
            '*.package_height' => 'nullable',  
            '*.primary_photo' => 'nullable',  
            '*.size' => 'nullable',  
            '*.tags' => 'nullable',  
            // '*.size' => 'nullable|exists:product_sizes,size',  
            // '*.tags' => 'nullable|exists:tags,tag_name',  
        ];
    }

    public static function messages(){
        return [
            '*.name' => 'Product name is required',
            '*.sku' => 'Product sku is required',
            '*.description' => 'Product description is required',  
            '*.status' => 'Product status must be 0 or 1', 
            '*.merchant_name' => 'Merchant name is required',    
            '*.category' => 'Category is required',    
            '*.brand' => 'Brand is required',    
            '*.base_price' => 'Base price is required', 
            '*.selling_price' => 'Selling price must be 0 or greater than 0',   
            '*.package_weight' => 'Package weight must be 0 or greater than 0',  
            '*.package_length' => 'Package length must be 0 or greater than 0',    
            '*.package_width' => 'Package width must be 0 or greater than 0',    
            '*.package_height' => 'Package height must be 0 or greater than 0',  
        ];
    }
    
}