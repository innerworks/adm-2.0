<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Auth; 
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\ProductTag;
use App\Models\MerchantMeta;
use App\Models\Tag;
use App\Models\TagSearch;

use App\Imports\Rules\ProductRules;
use Carbon\Carbon;

use Storage;

class ProductImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        Validator::make(
            $rows->toArray(), 
            ProductRules::rules(),
            ProductRules::messages()
        )->validate();

         foreach ($rows as $row) {

            // CHECK MERCHANT IF EXIST
            $merchant = MerchantMeta::where('meta_value', 'like', '%'.$row['merchant_name'].'%')
                                    ->where('meta_key', 'store_name')
                                    ->first();

            // CHECK CATEGORY IF EXIST
            $category = Category::where('name', 'like', '%'.$row['category'].'%')->whereNull('deleted_at')->first();

            // CHECK BRAND IF EXIST
            $brand = Brand::where('name', 'like', '%'.$row['brand'].'%')->whereNull('deleted_at')->first();

            // CHECK TAGS IF EXIST
            $tags = [];        
            if($row['tags']){
                $array_tags = array_map('trim', explode(',', $row['tags']));                
                $this->saveTagTable($array_tags);
                $tags = Tag::whereIn('tag_name', $array_tags)->get()->toArray();
            }

            // CHECK IF PRODUCT EXIST
            $product = Product::where('name', 'like', '%'.$row['name'].'%')->whereNull('deleted_at')->first();

            $product_size = ProductSize::where('size', 'like', '%'.$row['size'].'%')->whereNull('deleted_at')->first();
            if(!$product_size)
                $product_size = ProductSize::find(1);

            $product_data = [
                'sku' => $row['sku'],
                'name' => $row['name'],
                'description' => $row['description'],
                'permalink' => $this->generateLinkToken($row['name']),
                'status' => $row['status'],
                'merchant_id' => ($merchant) ? $merchant->merchant_id : null,
                'category_id' => ($category) ? $category->id : null,
                'brand_id' => ($brand) ? $brand->id : null,
                'base_price' => $row['base_price'],
                'selling_price' => $row['selling_price'],
                'weight' => $row['package_weight'],
                'length' => $row['package_length'],
                'width' => $row['package_width'],
                'height' => $row['package_height'],
                'primary_photo' => $row['primary_photo'],
            ];

            if(!$product) {
                // ADD NEW PRODUCT
                $new_product = Product::create($product_data);

                $this->saveTagSearch($row['tags'], $new_product->id);

                // ADD PRODUCT TAGS
                if($tags)
                    $new_product->addTag($tags);

                // ADD PRODUCT META
                $product_sizes = array();                
                $product_sizes[] = $product_size;
                $details = [
                    'sizes' => json_encode($product_sizes),
                    'tags' => json_encode($tags),
                    'gallery' => json_encode(array())                
                ];
                $new_product->addMeta($details);             
            }
            else {
                $product->update($product_data);

                $this->saveTagSearch($row['tags'], $product->id);

                // ADD PRODUCT TAGS
                if($tags)
                    $product->addTag($tags);

                // ADD PRODUCT META
                $product_sizes = array();                
                $product_sizes[] = $product_size;
                $details = [
                    'sizes' => json_encode($product_sizes),
                    'tags' => json_encode($tags),
                    'gallery' => json_encode(array())                
                ];
                $product->addMeta($details);
            }
         }
    }

    public function generateLinkToken($name) {
        $token = str_replace(' ', '-', strtolower($name)) . '-' . str_random(10);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $token);
    }

    public function saveTagSearch($tags, $product_id)
    {
        // ADD TAG SEARCH
        if($tags) {
            $tag_data = [
                'product_id' => $product_id,
                'search_tag' => $tags
            ];

            $tag_search = TagSearch::where('product_id', $product_id)->first();
            if($tag_search) {
                $tag_search->update($tag_data);
            }
            else {
                TagSearch::create($tag_data);
            }
        }
    }

    public function saveTagTable($tags = array())
    {
        if(count($tags) > 0) 
            foreach($tags as $index => $tag) {
                $tag = str_replace(",","",trim($tag));
                $tag_name = Tag::where('tag_name', $tag)->first();
                if(!$tag_name && $tag) 
                    Tag::create(['tag_name' => $tag]);
            }
    }
}
