<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Models\Category;
use App\Models\MediaLibrary;

use Storage;

class CategoryImport implements ToCollection, WithHeadingRow 
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            Category::updateOrCreate(
            	[
                    'parent_id' => $row['parent_id'],
                	'name' => $row['name'],
                    'merchant_id' => $row['merchant_id']
            	],	
            	[
	                'slug_name' => str_slug(strtolower($row['name']), "_"),
	                'sort_order' => $row['sort_order'],
	                'parent_id' => $row['parent_id'],
	                'merchant_id' => $row['merchant_id'],
	                // 'image_url' => $this->uploadImageFromUrl($row['image'])
        	    ]
        	);
        }
    }

    public function uploadImageFromUrl($url) {
        if(!$url)
            return null;
        $contents = file_get_contents($url);
        $name = substr($url, strrpos($url, '/') + 1);
        Storage::put('public/media_library/'.$name, $contents);
        return asset('storage/media_library/'. $name);       
    }
}
