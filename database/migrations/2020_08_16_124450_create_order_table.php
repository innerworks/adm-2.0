<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->longText('order_number')->nullable();
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('payment_type_id')->nullable();
            $table->decimal('voucher_discount_price',  8, 2)->nullable();
            $table->integer('points_generated')->nullable();
            $table->integer('points_consumed')->nullable();
            $table->integer('checkout')->nullable();
            $table->bigInteger('status_id')->nullable();
            $table->dateTime('order_date')->nullable();
            $table->dateTime('paid_date')->nullable();
            $table->dateTime('pending_payment_date')->nullable();
            $table->dateTime('failed_date')->nullable();
            $table->boolean('is_process')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
