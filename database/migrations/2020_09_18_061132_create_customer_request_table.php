<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_request', function (Blueprint $table) {
            $table->engine = "InnoDB";
                        
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('order_detail_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('merchant_id')->unsigned();
            $table->bigInteger('question_id')->unsigned();
            $table->bigInteger('type_id')->unsigned();
            $table->bigInteger('address_id')->unsigned();
            $table->longText('details')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_request');
    }
}
