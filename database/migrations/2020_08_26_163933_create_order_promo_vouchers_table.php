<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPromoVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_promo_vouchers', function (Blueprint $table) {
            $table->engine = "InnoDB";
                        
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable();
            $table->bigInteger('promo_id')->nullable();
            $table->bigInteger('brand_id')->nullable();
            $table->bigInteger('voucher_id')->nullable();
            $table->decimal('discount_percent', 10, 2)->nullable();
            $table->decimal('discount_price', 10, 2)->nullable();
            $table->string('brand_name')->nullable();
            $table->string('promo_title')->nullable();
            $table->string('promo_description')->nullable();
            $table->boolean('is_shipping')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_promo_vouchers');
    }
}
