<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesMunicipalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_municipalities', function (Blueprint $table) {
            $table->engine = "InnoDB";
            
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('city_municipality_name');
            $table->string('region_id', 10)->index();
            $table->string('province_id', 10)->index();
            $table->string('city_id', 10)->index();

            $table->index(
                ['province_id', 'region_id'], 
                'cities_province_regions'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities_municipalities');
    }
}
