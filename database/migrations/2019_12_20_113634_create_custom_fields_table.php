<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_fields', function (Blueprint $table) {
            $table->engine = "InnoDB";
            
            $table->bigIncrements('id');
            $table->bigInteger('cf_author')->nullable()->unsigned();
            $table->bigInteger('cf_parent')->nullable()->unsigned()->index();
            $table->string('cf_title');
            $table->string('cf_field_name');
            $table->longText('cf_params');
            $table->boolean('cf_status')->default(false)->index();
            $table->integer('cf_order')->default(0);
            $table->string('cf_name');
            $table->string('cf_type')->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cf_author')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_fields');
    }
}
