<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTrackingStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_tracking_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('order_detail_id')->unsigned();
            $table->bigInteger('order_status_id')->unsigned();
            $table->bigInteger('merchant_id')->unsigned();
            $table->bigInteger('courier_id')->unsigned();
            $table->longText('tracking_number')->nullable();
            $table->longText('remark')->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('order_detail_id')->references('id')->on('order_details');
            $table->foreign('order_status_id')->references('id')->on('order_status');
            $table->foreign('merchant_id')->references('id')->on('merchants');
            $table->foreign('courier_id')->references('id')->on('couriers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_tracking_status');
    }
}
