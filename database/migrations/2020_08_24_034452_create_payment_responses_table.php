<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_responses', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('merchant_id')->nullable();
            $table->bigInteger('payment_type_id')->nullable();
            $table->longText('request_id')->nullable();
            $table->longText('response_id')->nullable();
            $table->longText('response_code')->nullable();
            $table->longText('response_message')->nullable();
            $table->longText('response_advise')->nullable();
            $table->longText('processor_response_id')->nullable();
            $table->longText('processor_response_authcode')->nullable();
            $table->longText('processor_response_code')->nullable();
            $table->longText('processor_response_mess')->nullable();
            $table->longText('request_sign')->nullable();
            $table->longText('response_sign')->nullable();
            $table->dateTime('process_date')->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_responses');
    }
}
