<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_rate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('merchant_id')->unsigned()->nullable();
            $table->decimal('merchant_rate',  8, 3)->nullable();
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->decimal('category_rate',  8, 3)->nullable();
            $table->bigInteger('product_id')->unsigned()->nullable();
            $table->decimal('product_rate',  8, 3)->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('merchant_id')->references('id')->on('merchants');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_rate');
    }
}
