<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('merchant_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();
            $table->bigInteger('brand_id')->nullable();
            $table->bigInteger('shipping_address_id')->unsigned();
            $table->bigInteger('order_status_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->longText('product_name')->nullable();
            $table->longText('product_image')->nullable();
            $table->bigInteger('size_id')->unsigned();
            $table->longText('variant_ids')->nullable();
            $table->longText('add_ons_ids')->nullable();
            $table->decimal('base_price',  8, 2)->nullable();
            $table->decimal('selling_price',  8, 2)->nullable();
            $table->decimal('variants_price',  8, 2)->nullable();
            $table->decimal('add_ons_price',  8, 2)->nullable();
            $table->integer('qty')->unsigned();
            $table->decimal('total_price',  8, 2)->nullable();
            $table->decimal('shipping_fee',  8, 2)->nullable();
            $table->decimal('commision_rate',  8, 2)->nullable();
            $table->string('mrspeedy_order_id')->nullable();
            $table->integer('delivery_status_id')->nullable();
            $table->dateTime('shipped_date')->nullable();
            $table->dateTime('received_date')->nullable();
            $table->mediumText('special_instructions')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('merchant_id')->references('id')->on('merchants')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('shipping_address_id')->references('id')->on('customer_addresses');
            $table->foreign('order_status_id')->references('id')->on('order_status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
