<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->engine = "InnoDB";
                        
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('code')->nullable();
            $table->string('description')->nullable();
            $table->decimal('discount_percent', 10, 2)->nullable();
            $table->decimal('discount_price', 10, 2)->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->bigInteger('merchant_id')->unsigned()->nullable();
            $table->bigInteger('voucher_stock')->unsigned()->nullable();
            $table->boolean('is_unlimited')->default(0)->nullable();
            $table->boolean('active')->default(0)->nullable();            
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
