<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMrspeedyOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mrspeedy_order', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable()->index();
            $table->string('order_name')->nullable();
            $table->integer('vehicle_type_id')->nullable();
            $table->string('status')->nullable();
            $table->string('status_description')->nullable();
            $table->longText('matter')->nullable();
            $table->longText('points')->nullable();
            $table->decimal('payment_amount')->nullable();
            $table->decimal('delivery_fee_amount')->nullable();
            $table->decimal('intercity_delivery_fee_amount')->nullable();            
            $table->decimal('weight_fee_amount')->nullable();
            $table->decimal('insurance_amount')->nullable();
            $table->decimal('insurance_fee_amount')->nullable();
            $table->decimal('loading_fee_amount')->nullable();
            $table->decimal('money_transfer_fee_amount')->nullable();
            $table->decimal('suburban_delivery_fee_amount')->nullable();
            $table->decimal('overnight_fee_amount')->nullable();
            $table->decimal('discount_amount')->nullable();
            $table->decimal('backpayment_amount')->nullable();
            $table->decimal('cod_fee_amount')->nullable();
            $table->string('payment_method')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mrspeedy_order');
    }
}
