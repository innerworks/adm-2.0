<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->engine = "InnoDB";
            
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->string('name')->index();
            $table->string('description');
            $table->timestamps();
            $table->softDeletes()->index();
    
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('role_permissions', function (Blueprint $table) {
            $table->engine = "InnoDB";
            
            $table->bigInteger('role_id')->unsigned();
            $table->bigInteger('module_id')->unsigned();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes()->index();

            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('module_id')->references('id')->on('modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permissions');
        Schema::dropIfExists('modules');
    }
}
