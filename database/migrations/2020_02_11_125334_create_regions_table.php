<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('country_code');
            $table->string('country_name');
        });
                
        Schema::create('regions', function (Blueprint $table) {
            $table->engine = "InnoDB";
            
            $table->increments('id');
            $table->string('code', 9)->unique();
            $table->string('name');
            $table->string('region_id', 2)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
        Schema::dropIfExists('countries');
    }
}
