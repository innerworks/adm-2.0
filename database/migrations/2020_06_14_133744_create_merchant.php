<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('merchant_status', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('merchants', function (Blueprint $table) {
            $table->engine = "InnoDB";

            $table->bigIncrements('id');
            $table->string('email')->index();
            $table->string('username')->nullable();            
            $table->string('password')->nullable();
            $table->string('salt')->nullable();
            $table->integer('login_attempt')->default(0);
            $table->integer('is_blocked')->default(0);
            $table->boolean('category_id')->default(0);
            $table->boolean('status')->default(1)->index();
            $table->string('activation_token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

       Schema::create('merchant_meta', function (Blueprint $table) {
            $table->engine = "InnoDB";
            
            $table->bigIncrements('id');
            $table->bigInteger('merchant_id')->nullable()->unsigned();
            $table->string('meta_key')->nullable()->index();
            $table->longText('meta_value')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('merchant_id')->references('id')->on('merchants');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('merchant_types');
        Schema::dropIfExists('merchant_meta');
        Schema::dropIfExists('merchants');
        Schema::dropIfExists('merchant_status');
    }
}
