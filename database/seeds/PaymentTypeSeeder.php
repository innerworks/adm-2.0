<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentType;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $input['payment_type'] = 'Paypal';
        $input['payment_acronym'] = 'pp';

		PaymentType::create($input);

        $input['payment_type'] = 'GCash';
        $input['payment_acronym'] = 'gc';

		PaymentType::create($input);

        $input['payment_type'] = 'Express Booked';
        $input['payment_acronym'] = 'book';

		PaymentType::create($input);

        $input['payment_type'] = 'Credit Card';
        $input['payment_acronym'] = 'cc';

		PaymentType::create($input);

        $input['payment_type'] = 'COD';
        $input['payment_acronym'] = 'cod';

		PaymentType::create($input);
    }
}
