<?php

use Illuminate\Database\Seeder;
use App\Services\Helpers\PasswordHelper;
use App\Models\User;
use App\Models\Customer;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salt = bcrypt(PasswordHelper::generateSalt());

        // $input['name'] = 'Administrator';
        // $input['email'] = 'bautistael23@gmail.com';
        // $input['password'] = 'm@h@lk0t009';
        // $input['salt'] = $salt;
        // $input['password'] = PasswordHelper::generate($salt, $input['password']);
        // $input['active'] = 1;

		//User::create($input);

        // $input['name'] = 'Admin';
        // $input['email'] = 'adm_admin@gmail.com';
        // $input['password'] = '@d0b0m@ll@dm1n';
        // $input['salt'] = $salt;
        // $input['password'] = PasswordHelper::generate($salt, $input['password']);
        // $input['active'] = 1;

        User::create($input);

        $input['name'] = 'Merchant';
        $input['email'] = 'adm_merchant@gmail.com';
        $input['password'] = '@dmm3rch@nt';
        $input['salt'] = $salt;
        $input['password'] = PasswordHelper::generate($salt, $input['password']);
        $input['active'] = 1;

        User::create($input);

        // $input['name'] = 'Customer';
        // $input['email'] = 'elbautista@innerworksinternational.com';
        // $input['password'] = 'password';
        // $input['salt'] = $salt;
        // $input['password'] = PasswordHelper::generate($salt, $input['password']);
        // $input['active'] = 1;

        // Customer::create($input);        
    }
}
