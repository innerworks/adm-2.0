<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Role::create([
		    'user_id' => 1,
		    'name' => 'Administrator',
		    'description' => 'System Administrator',
		    'active' => 1
		]);

		Role::create([
		    'user_id' => 1,
		    'name' => 'Admin',
		    'description' => 'Admin User',
		    'active' => 1
		]);

		Role::create([
		    'user_id' => 1,
		    'name' => 'Merchant',
		    'description' => 'Merchant Admin User',
		    'active' => 1
		]);

		Role::create([
		    'user_id' => 1,
		    'name' => 'Sales',
		    'description' => 'Sales User',
		    'active' => 1
		]);

		Role::create([
		    'user_id' => 1,
		    'name' => 'Guest',
		    'description' => 'Guest User',
		    'active' => 1
		]);

    }
}
