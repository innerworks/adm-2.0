<?php

use Illuminate\Database\Seeder;
use App\Models\OrderStatus;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [            
	        [
	        	'id' => 1, 
				'status' => 'Booked',
				'status_title' => 'Pending to process',
				'status_message' => 'Your order will be processed soon as possible. Thank you very much',
	    	],
	    	[
	        	'id' => 2, 
				'status' => 'Processing',
				'status_title' => 'Packed by seller',
				'status_message' => 'Your order is packed and ready to be picked up by our delivery partner.',
	    	],
	    	[
	        	'id' => 3, 
				'status' => 'Shipped',
				'status_title' => 'Shipped',
				'status_message' => 'Your order has been shipped',
	    	],
	    	[
	        	'id' => 4, 
				'status' => 'Delivered',
				'status_title' => 'Delivered',
				'status_message' => 'Your parcel is successfully delivered. Thank you!',
	    	],
	    	[
	        	'id' => 5, 
				'status' => 'Refunded',
				'status_title' => 'Refunded',
				'status_message' => 'Your payment has been refunded.',
	    	],
	    	[
	        	'id' => 6, 
				'status' => 'Returned',
				'status_title' => 'Returned',
				'status_message' => 'Your order has been returned.',
	    	],
	    	[
	        	'id' => 7, 
				'status' => 'To Pay',
				'status_title' => '',
				'status_message' => 'Thank you for shopping at Adobomall! An email will be sent shortly once your order is successfully placed with the selected payment method. Orders with no payment method will be cancelled automatically after 72 hours or when the item goes out of stock.',
			],
			[
	        	'id' => 8, 
				'status' => 'On Hold',
				'status_title' => 'On Hold',
				'status_message' => 'Your order is currently on-hold',
			],
			[
	        	'id' => 9, 
				'status' => 'Failed',
				'status_title' => 'Failed',
				'status_message' => 'Your order has failed',
			],
			[
	        	'id' => 10, 
				'status' => 'Request for Warranty',
				'status_title' => 'On Process for Warranty',
				'status_message' => 'Your order is being processed for return',
			],
			[
	        	'id' => 11, 
				'status' => 'Processing Item to Return',
				'status_title' => 'Approved to Return',
				'status_message' => 'Your order’s return has been approved.',
			],
			[
	        	'id' => 12, 
				'status' => 'Successfully Returned',
				'status_title' => 'On merchant warehouse',
				'status_message' => 'Your order has been successfully returned',
			],
			[
	        	'id' => 13, 
				'status' => 'Request for Cancellation',
				'status_title' => 'On Process to cancel',
				'status_message' => 'Your request for cancellation is being processed.',
			],
			[
	        	'id' => 14, 
				'status' => 'Cancelled',
				'status_title' => 'Successfully Cancelled',
				'status_message' => 'Your order has been cancelled.',
			],
			[
	        	'id' => 15, 
				'status' => 'Rejected to Cancel',
				'status_title' => 'Rejected to Cancel',
				'status_message' => 'Your request for cancellation has been declined.',
			],
			[
	        	'id' => 16, 
				'status' => 'Rejected Warranty',
				'status_title' => 'Rejected',
				'status_message' => 'Your warranty claim has been declined.',
			],
			[
	        	'id' => 17, 
				'status' => 'Ready for Picked Up',
				'status_title' => 'Ready for Picked Up',
				'status_message' => 'Your order has been picked up by our delivery partner',
	    	],
			[
	        	'id' => 18, 
				'status' => 'invalid',
				'status_title' => 'Mr. Speedy Invalid',
				'status_message' => 'Invalid draft delivery',
	    	],
			[
	        	'id' => 19, 
				'status' => 'draft',
				'status_title' => 'Mr. Speedy Draft',
				'status_message' => 'Draft delivery',
	    	],
			[
	        	'id' => 20, 
				'status' => 'planned',
				'status_title' => 'Mr. Speedy Planned',
				'status_message' => 'Planned delivery (No courier assigned)',
	    	],
			[
	        	'id' => 21, 
				'status' => 'active',
				'status_title' => 'Mr. Speedy Active',
				'status_message' => 'Delivery in process (Courier on the way)',
	    	],
			[
	        	'id' => 22, 
				'status' => 'finished',
				'status_title' => 'Mr. Speedy Finished',
				'status_message' => 'Delivery finished (Courier delivered the parcel)',
	    	],
			[
	        	'id' => 23, 
				'status' => 'canceled',
				'status_title' => 'Mr. Speedy Canceled',
				'status_message' => 'Delivery canceled',
	    	],
			[
	        	'id' => 24, 
				'status' => 'delayed',
				'status_title' => 'Mr. Speedy Delayed',
				'status_message' => 'Delivery delayed',
	    	],
			[
	        	'id' => 25, 
				'status' => 'failed',
				'status_title' => 'Mr. Speedy Failed',
				'status_message' => 'Delivery failed (Courier could not find a customer)',
	    	],
			[
	        	'id' => 26, 
				'status' => 'courier_assigned',
				'status_title' => 'Mr. Speedy Courier Assigned',
				'status_message' => 'Courier assigned, but still not departed',
	    	],
			[
	        	'id' => 27, 
				'status' => 'courier_departed',
				'status_title' => 'Mr. Speedy Courier Departed',
				'status_message' => 'Courier departed to the pick-up point',
	    	],
			[
	        	'id' => 28, 
				'status' => 'parcel_picked_up',
				'status_title' => 'Mr. Speedy Parcel Picked Up',
				'status_message' => 'Courier took parcel at the pick-up point',
	    	],	            
			[
	        	'id' => 29, 
				'status' => 'courier_arrived',
				'status_title' => 'Mr. Speedy Courier Arrived',
				'status_message' => 'Courier has arrived and is waiting for a customer',
	    	],	
			[
	        	'id' => 30, 
				'status' => 'deleted',
				'status_title' => 'Mr. Speedy Deleted',
				'status_message' => 'Delivery deleted',
	    	],	
			[
	        	'id' => 31, 
				'status' => 'In Complete',
				'status_title' => 'In Complete',
				'status_message' => 'In Complete',
	    	],	
	    ];

        foreach ($status as $key => $value) {
        	OrderStatus::create($value);
        }
    }
}
