<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Category::create([
		    'name' => 'Food',
		    'slug_name' => 'food'
		]);

		Category::create([
		    'name' => 'Electronic',
		    'slug_name' => 'electronic'
		]);

		Category::create([
		    'name' => 'Home & Living',
		    'slug_name' => 'home_living'
		]);

		Category::create([
		    'name' => 'Health & Beauty',
		    'slug_name' => 'health_beauty'
		]);

		Category::create([
		    'name' => 'Toys & Hobbies',
		    'slug_name' => 'babies_hobbies'
		]);

		Category::create([
		    'name' => 'Fashion',
		    'slug_name' => 'fashion'
		]);

		Category::create([
		    'name' => 'Groceries',
		    'slug_name' => 'groceries'
		]);

		Category::create([
		    'name' => 'Home Services',
		    'slug_name' => 'home_services'
		]);
    }
}
