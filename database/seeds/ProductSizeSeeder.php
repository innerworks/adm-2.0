<?php

use Illuminate\Database\Seeder;
use App\Models\ProductSize;

class ProductSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sizes = [
            'XS','S','M','L','XXL','XXXL',
			'45','44','43','42','41','40','39','38','37','36','35','34','33','32','31','30','29','28','27','26',
			'25','24','23','22','21','20',
			'US 4','US 5','US 6','US 7','US 8','US 9','US 10','US 11','US 12',
			'UK 4','UK 5','UK 6','UK 7','UK 8','UK 9','UK 10','UK 11','UK 12',
			'EU 4','EU 5','EU 6','EU 7','EU 8','EU 9','EU 10','EU 11','EU 12'
	    ];

        foreach ($sizes as $key => $value) {
	        $input['size'] = $value;
	        ProductSize::create($input);
        }
    }
}
