<?php

use Illuminate\Database\Seeder;
use App\Models\ProductCountrySize;

class ProductCountrySizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $input['country_size'] = 'US';
        ProductCountrySize::create($input);

        $input['country_size'] = 'UK';
        ProductCountrySize::create($input);

        $input['country_size'] = 'EU';
        ProductCountrySize::create($input);

        $input['country_size'] = 'JAPAN';
        ProductCountrySize::create($input);

    }
}
