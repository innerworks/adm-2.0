<?php

use Illuminate\Database\Seeder;
use App\Models\Courier;

class CouriersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$couriers = [            
	        [
	        	'id' => 1, 
	        	'name' => 'Mr. Speedy',
	        	'is_basis' => '0',
	        	'valuation_fee' => '0',
	        	'cod_fee' => '0'
	    	],
	        [
	        	'id' => 2, 
	        	'name' => 'LBC',
	        	'is_basis' => '0',
	        	'valuation_fee' => '0.010',
	        	'cod_fee' => '0.010'
	    	],
	    	[
	        	'id' => 3, 
	        	'name' => 'Grab',
	        	'is_basis' => '0',
	        	'valuation_fee' => '33.000',
	        	'cod_fee' => '33.000'
	    	],
	    	[
	        	'id' => 4, 
	        	'name' => '2GO',
	        	'is_basis' => '0',
	        	'valuation_fee' => '22.000',
	        	'cod_fee' => '22.000'
	    	],
	    	[
	        	'id' => 5, 
	        	'name' => 'Xend',
	        	'is_basis' => '0',
	        	'valuation_fee' => '44.000',
	        	'cod_fee' => '44.000'
	    	],  
	    ];

	    
        foreach ($couriers as $key => $value) {
        	Courier::create($value);
        }
    }
}
