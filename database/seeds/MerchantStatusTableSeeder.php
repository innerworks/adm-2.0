<?php

use Illuminate\Database\Seeder;
use App\Models\MerchantStatus;

class MerchantStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MerchantStatus::create([
		    'name' => 'Request',
		    'description' => 'New Merchant Request'
		]);

        MerchantStatus::create([
		    'name' => 'Pending',
		    'description' => 'Pending'
		]);

		MerchantStatus::create([
		    'name' => 'Active',
		    'description' => 'Active Merchant'
		]);

		MerchantStatus::create([
		    'name' => 'Inactive',
		    'description' => 'Inactive Merchant'
		]);

		MerchantStatus::create([
		    'name' => 'On Hold',
		    'description' => 'On Hold Merchant'
		]);
	}
}
