<?php

use Illuminate\Database\Seeder;
use App\Models\Module;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Module::create([
            'user_id' => 1,
		    'name' => 'Admin Users',
		    'description' => 'Admin User Management'
		]);

        Module::create([
            'user_id' => 1,
            'name' => 'Roles',
            'description' => 'Role Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Categories',
            'description' => 'Categories Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Brands',
            'description' => 'Brands Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Sizes',
            'description' => 'Sizes Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Merchants',
            'description' => 'Merchants Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Products',
            'description' => 'Products Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Inventories',
            'description' => 'Inventories Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Delivery Rates',
            'description' => 'Delivery Rates Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Commission Rates',
            'description' => 'Commission Rates Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Carts',
            'description' => 'Carts Management, for cart cleaning'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Orders',
            'description' => 'Orders Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Customers',
            'description' => 'Customers Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Banners',
            'description' => 'Banners Management'
        ]);        

        Module::create([
            'user_id' => 1,
            'name' => 'Comments',
            'description' => 'Comments Management'
        ]);
        
        Module::create([
            'user_id' => 1,
            'name' => 'Social Media Feed',
            'description' => 'Social Media Feed Management'
        ]);        

        Module::create([
            'user_id' => 1,
            'name' => 'Stories',
            'description' => 'Stories Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Vouchers',
            'description' => 'Vouchers Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Loyalty Points',
            'description' => 'Loyalty Points Management'
        ]);

        Module::create([
            'user_id' => 1,
            'name' => 'Reports',
            'description' => 'Reports Management'
        ]);        
    }
}
