<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () 
{

    // Route::get('fetch-data', 'Api\CustomerController@fetchData');
    // Route::put('update-data', 'Api\CustomerController@updateData')->name('api.v1.update-data');

    /*
    |--------------------------------------------------------------------------
    | Contact Us Routes
    |--------------------------------------------------------------------------
    */
    Route::post('contact-us', 'Api\CustomerController@sendInquiry')->name('api.v1.contactUs');

    /*
    |--------------------------------------------------------------------------
    | Login Routes
    |--------------------------------------------------------------------------
    */    
    Route::post('login', 'Api\AuthController@login')->name('api.v1.auth.login');
    Route::post('signup', 'Api\AuthController@register')->name('api.v1.auth.signup');
    Route::post('refresh', 'Api\AuthController@refresh')->name('api.v1.auth.refresh');
    Route::get('signup/activate/{token}', 'Api\AuthController@signupActivate')->name('api.v1.auth.activate');

    /*
    |--------------------------------------------------------------------------
    | Password Routes
    |--------------------------------------------------------------------------
    */
    Route::post('password/create', 'Api\PasswordResetController@create')->name('api.v1.password.create');
    Route::get('password/find/{token}', 'Api\PasswordResetController@find')->name('api.v1.password.find');
    Route::post('password/reset', 'Api\PasswordResetController@reset')->name('api.v1.password.reset');

    /*
    |--------------------------------------------------------------------------
    | Merchant Sign-Up
    |--------------------------------------------------------------------------
    */    
    Route::post('merchant/login', 'Merchant\MerchantDashboardController@login')->name('api.v1.merchant.login');
    // Route::post('merchant/signup', 'Api\MerchantController@register')->name('api.v1.merchant.signup');

    /*
    |--------------------------------------------------------------------------
    | Look-Ups
    |--------------------------------------------------------------------------
    */    
    Route::get('lookup/getCountries', 'Api\LookUpController@getCountries')->name('api.v1.lookup.getCountries');
    Route::get('lookup/getRegions', 'Api\LookUpController@getRegions')->name('api.v1.lookup.getRegions');
    Route::post('lookup/getProvinces', 'Api\LookUpController@getProvinces')->name('api.v1.lookup.getProvinces');
    Route::post('lookup/getCities', 'Api\LookUpController@getCities')->name('api.v1.lookup.getCities');
    Route::post('lookup/getBarangays', 'Api\LookUpController@getBarangays')->name('api.v1.lookup.getBarangays');
    Route::post('lookup/getCitiesByProvince', 'Api\LookUpController@getCitiesByProvince')->name('api.v1.lookup.getCitiesByProvince');
    Route::post('lookup/getBarangaysByCity', 'Api\LookUpController@getBarangaysByCity')->name('api.v1.lookup.getBarangaysByCity');
    Route::post('lookup/checkZipCode', 'Api\LookUpController@checkZipCode')->name('api.v1.lookup.checkZipCode');
    Route::get('lookup/getMerchants', 'Api\LookUpController@getMerchants')->name('api.v1.lookup.getMerchants');
    Route::get('lookup/getCouriers', 'Api\LookUpController@getCouriers')->name('api.v1.lookup.getCouriers');

    /*
    |--------------------------------------------------------------------------
    | Customer Routes
    |--------------------------------------------------------------------------
    */    
    Route::post('customer/registerByMobile', 'Api\CustomerController@registerByMobile')->name('api.v1.customer.registerByMobile');
    Route::post('customer/validateByMobile', 'Api\CustomerController@validateByMobile')->name('api.v1.customer.validateByMobile');
    Route::post('customer/registerByEmail', 'Api\CustomerController@registerByEmail')->name('api.v1.customer.registerByEmail');
    Route::post('customer/validateByEmail', 'Api\CustomerController@validateByEmail')->name('api.v1.customer.validateByEmail');
    Route::put('customer/setAccount', 'Api\CustomerController@setAccount')->name('api.v1.customer.setAccount');
    Route::post('customer/login', 'Api\CustomerController@login')->name('api.v1.customer.login');
    Route::post('customer/social', 'Api\CustomerController@social')->name('api.v1.customer.social');
    
    Route::post('/customer/password/create', 'Api\PasswordResetController@createPasswordToken')->name('api.v1.customer.password-create');
    // Route::post('/customer/password/find/{token}', 'Api\PasswordResetController@findPasswordToken')->name('api.v1.customer.password-find');
    Route::post('/customer/password/reset', 'Api\PasswordResetController@resetPasswordToken')->name('api.v1.customer.password-reset');

    /*
    |--------------------------------------------------------------------------
    | User App
    |--------------------------------------------------------------------------
    */    
    Route::get('user-app/getAds', 'Api\UserAppController@getAds')->name('api.v1.user-app.getAds');
    Route::get('user-app/smart-search', 'Api\UserAppController@smartSearch')->name('api.v1.user-app.smart-search');
    Route::get('user-app/search', 'Api\UserAppController@search')->name('api.v1.user-app.search');
    Route::get('user-app/getStories', 'Api\UserAppController@getStories')->name('api.v1.user-app.getStories');
    Route::get('user-app/getBanners', 'Api\UserAppController@getBanners')->name('api.v1.user-app.getBanners');
    Route::get('user-app/getCategories', 'Api\UserAppController@getCategories')->name('api.v1.user-app.getCategories');
    Route::get('user-app/getTopPicks', 'Api\UserAppController@getTopPicks')->name('api.v1.user-app.getTopPicks');
    Route::get('user-app/getFlashSale', 'Api\UserAppController@getFlashSale')->name('api.v1.user-app.getFlashSale');
    Route::get('user-app/getStores', 'Api\UserAppController@getStores')->name('api.v1.user-app.getStores');
    Route::get('user-app/getStoreDetails', 'Api\UserAppController@getStoreDetails')->name('api.v1.user-app.getStoreDetails');
    Route::get('user-app/storesNearYou', 'Api\UserAppController@storesNearYou')->name('api.v1.user-app.storesNearYou');
    Route::get('user-app/getRandomProducts', 'Api\UserAppController@getRandomProducts')->name('api.v1.user-app.getRandomProducts');
    Route::get('user-app/getProducts', 'Api\UserAppController@getProducts')->name('api.v1.user-app.getProducts');
    Route::get('user-app/getProductDetails', 'Api\UserAppController@getProductDetails')->name('api.v1.user-app.getProductDetails');

    /*
    |--------------------------------------------------------------------------
    | Media Library Routes
    |--------------------------------------------------------------------------
    */
    Route::get('media-library/{id}', 'Api\MediaLibraryController@getMediaDetails')->where('id', '[0-9]+')->name('api.v1.media-library.show');
    Route::post('media-library/store', 'Api\MediaLibraryController@store')->name('api.v1.media-library.store');
    Route::put('media-library/update', 'Api\MediaLibraryController@update')->name('api.v1.media-library.update');
    Route::get('media-library/delete/{id}', 'Api\MediaLibraryController@delete')->where('id', '[0-9]+')->name('api.v1.media-library.delete');
    Route::get('media-library/list', 'Api\MediaLibraryController@list')->name('api.v1.media-library.list');

    /*
    |--------------------------------------------------------------------------
    | Mr Speedy API
    |--------------------------------------------------------------------------
    */
    Route::get('mr-speedy/placeOrder', 'MrSpeedy\MrSpeedyController@placeOrder')->name('api.v1.mr-speedy.placeOrder');
    Route::post('mr-speedy/callBack', 'MrSpeedy\MrSpeedyController@callBack')->name('api.v1.mr-speedy.callBack');
    Route::get('mr-speedy/get-courier', 'MrSpeedy\MrSpeedyController@getCourierLocation')->name('api.v1.mr-speedy.get-courier');
    Route::get('mr-speedy/get-courier-sample', 'MrSpeedy\MrSpeedyController@getCourierLocationSample')->name('api.v1.mr-speedy.get-courier-sample');

    /*
    |--------------------------------------------------------------------------
    | View Tracker API
    |--------------------------------------------------------------------------
    */
    Route::get('tracker/view', 'Api\ViewTrackerController@viewVisit')->name('api.v1.tracker.view');
    Route::get('tracker/add-view', 'Api\ViewTrackerController@home')->name('api.v1.tracker.add-view');
    Route::get('tracker/category-view', 'Api\ViewTrackerController@category')->name('api.v1.tracker.category-view');
    Route::get('tracker/brand-view', 'Api\ViewTrackerController@brand')->name('api.v1.tracker.brand-view');
    Route::get('tracker/merchant-view', 'Api\ViewTrackerController@merchant')->name('api.v1.tracker.merchant-view');
    Route::get('tracker/product-view', 'Api\ViewTrackerController@product')->name('api.v1.tracker.product-view');
});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api_customer']], function () 
{
    /*
    |--------------------------------------------------------------------------
    | Customer Routes
    |--------------------------------------------------------------------------
    */
    Route::get('customer/getProfile', 'Api\CustomerController@getProfile')->name('api.v1.customer.getProfile');    
    Route::post('customer/editProfile', 'Api\CustomerController@editProfile')->name('api.v1.customer.editProfile');
    Route::post('customer/saveAddress', 'Api\CustomerController@saveAddress')->name('api.v1.customer.saveAddress');
    Route::get('customer/getAddresses', 'Api\CustomerController@getAddresses')->name('api.v1.customer.getAddresses');

    Route::post('customer/getAddressById', 'Api\CustomerController@getAddressById')->name('api.v1.customer.getAddressById');

    Route::get('customer/deleteAddress', 'Api\CustomerController@deleteAddress')->name('api.v1.customer.deleteAddress');
    Route::get('customer/getOrders', 'Api\CustomerController@getOrders')->name('api.v1.customer.getOrders');
    Route::get('customer/getDetails', 'Api\CustomerController@getDetails')->name('api.v1.customer.getDetails');
    Route::get('customer/getLikes', 'Api\CustomerController@getLikes')->name('api.v1.customer.getLikes');
    Route::post('customer/storeRequest', 'Api\CustomerRequestController@storeRequest')->name('api.v1.customer.storeRequest');
    Route::get('customer/request-list', 'Api\CustomerRequestController@list')->name('api.v1.customer.request-list');

    /*
    |--------------------------------------------------------------------------
    | User App
    |--------------------------------------------------------------------------
    */    
    Route::post('user-app/addRating', 'Api\UserAppController@addRating')->name('api.v1.user-app.addRating');
    Route::post('user-app/storeToCart', 'Api\UserAppController@storeToCart')->name('api.v1.user-app.storeToCart');
    Route::post('user-app/checkUncheckCart', 'Api\UserAppController@checkUncheckCart')->name('api.v1.user-app.checkUncheckCart');
    Route::get('user-app/clearCart', 'Api\UserAppController@clearCart')->name('api.v1.user-app.clearCart');
    Route::get('user-app/getCarts', 'Api\UserAppController@getCarts')->name('api.v1.user-app.getCarts');
    Route::get('user-app/deleteCart', 'Api\UserAppController@deleteCart')->name('api.v1.user-app.deleteCart');
    Route::get('user-app/voucher', 'Api\UserAppController@voucher')->name('api.v1.user-app.voucher');
    Route::post('user-app/addVoucher', 'Api\UserAppController@addVoucher')->name('api.v1.user-app.addVoucher');

    /*
    |--------------------------------------------------------------------------
    | Checkout
    |--------------------------------------------------------------------------
    */    
    Route::get('checkout', 'Api\CheckoutController@index')->name('api.v1.checkout');
    Route::get('checkout/placeOrder', 'Api\CheckoutController@placeOrder')->name('api.v1.checkout.placeOrder');

    /*
    |--------------------------------------------------------------------------
    | Wishlist
    |--------------------------------------------------------------------------
    */    
    Route::post('wishlist/like', 'Api\WishlistController@likeProduct')->name('api.v1.wishlist.like');
    Route::get('wishlist/unlike/{id}', 'Api\WishlistController@unlikeProduct')->where('id', '[0-9]+')->name('api.v1.wishlist.unlike');
});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api']], function () 
{
    /*
    |--------------------------------------------------------------------------
    | Dashboard Overview
    |--------------------------------------------------------------------------
    */
    Route::get('/dashboard-count', 'Api\OrderController@fetchOrdersCount')->name('api.v1.dashboard.count');
    Route::get('/dashboard-report/{filter}', 'Api\OrderController@getMonthlySalesReport')->name('api.v1.dashboard.orders');

    /*
    |--------------------------------------------------------------------------
    | Logout
    |--------------------------------------------------------------------------
    */

    Route::get('logout', 'Api\AuthController@logout')->name('api.v1.auth.logout');
    /*
    |--------------------------------------------------------------------------
    | Roles Routes
    |--------------------------------------------------------------------------
    */
    Route::post('role/create', 'Api\RoleController@create')->name('api.v1.role.create');
    Route::get('role/{roleId}', 'Api\RoleController@show')->where('roleId', '[0-9]+')->name('api.v1.role.show');
    Route::put('role/update', 'Api\RoleController@update')->name('api.v1.role.update');
    Route::get('role/delete/{roleId}', 'Api\RoleController@delete')->where('moduleId', '[0-9]+')->name('api.v1.role.delete');
    Route::get('role/list', 'Api\RoleController@list')->name('api.v1.role.list');

    /*
    |--------------------------------------------------------------------------
    | Module Routes
    |--------------------------------------------------------------------------
    */
    Route::post('module/create', 'Api\ModuleController@create')->name('api.v1.module.create');
    Route::get('module/{moduleId}', 'Api\ModuleController@show')->where('moduleId', '[0-9]+')->name('api.v1.module.show');
    Route::post('module/update', 'Api\ModuleController@update')->name('api.v1.module.update');
    Route::get('module/delete/{moduleId}', 'Api\ModuleController@delete')->where('moduleId', '[0-9]+')->name('api.v1.module.delete');
    Route::get('module/list', 'Api\ModuleController@list')->name('api.v1.module.list');

    /*
    |--------------------------------------------------------------------------
    | Permission Routes
    |--------------------------------------------------------------------------
    */
    Route::post('permission/grant_permission', 'Api\PermissionController@grantPermission')->name('api.v1.permission.grant');

    /*
    |--------------------------------------------------------------------------
    | User Routes
    |--------------------------------------------------------------------------
    */
    Route::get('user/{id}', 'Api\UserController@getUserDetails')->where('id', '[0-9]+')->name('api.v1.user.show');
    Route::post('user/store', 'Api\UserController@store')->name('api.v1.user.store');
    Route::put('user/update', 'Api\UserController@update')->name('api.v1.user.update');
    Route::get('user/delete/{id}', 'Api\UserController@delete')->where('id', '[0-9]+')->name('api.v1.user.delete');
    Route::get('user/list', 'Api\UserController@list')->name('api.v1.user.list');

    /*
    |--------------------------------------------------------------------------
    | User Roles
    |--------------------------------------------------------------------------
    */
    Route::post('user/assign_roles', 'Api\UserRoleController@setUserRoles')->name('api.v1.user.roles');
    Route::get('user/get_roles/{id}', 'Api\UserRoleController@getUserRoles')->where('id', '[0-9]+')->name('api.v1.user.role');

    /*
    |--------------------------------------------------------------------------
    | Categories Routes
    |--------------------------------------------------------------------------
    */
    Route::get('category/{id}', 'Api\CategoryController@getCategory')->where('id', '[0-9]+')->name('api.v1.category.show');
    Route::post('category/store', 'Api\CategoryController@store')->name('api.v1.category.store');
    Route::post('category/update', 'Api\CategoryController@update')->name('api.v1.category.update');
    Route::get('category/delete/{id}', 'Api\CategoryController@delete')->where('id', '[0-9]+')->name('api.v1.category.delete');
    Route::get('category/list', 'Api\CategoryController@list')->name('api.v1.category.list');
    Route::get('category/getAllCategories', 'Api\CategoryController@getAllCategories')->name('api.v1.category.getAllCategories');
    Route::get('category/getDropdown', 'Api\CategoryController@getDropdown')->name('api.v1.category.getDropdown');
    Route::post('category/batchUpload', 'Api\CategoryController@batchUpload')->name('api.v1.category.batchUpload');    
    Route::get('category/downloadCsv', 'Api\CategoryController@downloadCsv')->name('api.v1.category.downloadCsv');    

    /*
    |--------------------------------------------------------------------------
    | Brands Routes
    |--------------------------------------------------------------------------
    */
    Route::get('brand/{id}', 'Api\BrandController@getBrand')->where('id', '[0-9]+')->name('api.v1.brand.show');
    Route::post('brand/store', 'Api\BrandController@store')->name('api.v1.brand.store');
    Route::put('brand/update', 'Api\BrandController@update')->name('api.v1.brand.update');
    Route::get('brand/delete/{id}', 'Api\BrandController@delete')->where('id', '[0-9]+')->name('api.v1.brand.delete');
    Route::get('brand/list', 'Api\BrandController@list')->name('api.v1.brand.list');
    Route::get('brand/getDropdown', 'Api\BrandController@getDropdown')->name('api.v1.brand.getDropdown');    
    Route::post('brand/batchUpload', 'Api\BrandController@batchUpload')->name('api.v1.brand.batchUpload');    
    Route::get('brand/downloadCsv', 'Api\BrandController@downloadCsv')->name('api.v1.brand.downloadCsv');    

    /*
    |--------------------------------------------------------------------------
    | Merchant Routes
    |--------------------------------------------------------------------------
    */
    Route::get('merchant/{id}', 'Api\MerchantController@getMerchantDetails')->where('id', '[0-9]+')->name('api.v1.merchant.show');
    Route::get('merchant/list', 'Api\MerchantController@list')->name('api.v1.merchant.list');
    Route::post('merchant/store', 'Api\MerchantController@store')->name('api.v1.merchant.store');
    Route::post('merchant/update', 'Api\MerchantController@update')->name('api.v1.merchant.update');
    Route::get('merchant/delete/{id}', 'Api\MerchantController@delete')->where('id', '[0-9]+')->name('api.v1.merchant.delete');
    Route::get('merchant/getStatus', 'Api\MerchantController@getStatus')->name('api.v1.merchant.getStatus');
    Route::get('merchant/getDropdown', 'Api\MerchantController@getDropdown')->name('api.v1.merchant.getDropdown');
    Route::get('merchant/local-migration', 'Api\MerchantController@localMigration')->name('api.v1.merchant.localMigration');

    /*
    |--------------------------------------------------------------------------
    | Product Size Routes
    |--------------------------------------------------------------------------
    */
    Route::get('product-size/{id}', 'Api\ProductSizeController@getSize')->where('id', '[0-9]+')->name('api.v1.product-size.show');
    Route::post('product-size/store', 'Api\ProductSizeController@store')->name('api.v1.product-size.store');
    Route::put('product-size/update', 'Api\ProductSizeController@update')->name('api.v1.product-size.update');
    Route::get('product-size/delete/{id}', 'Api\ProductSizeController@delete')->where('id', '[0-9]+')->name('api.v1.product-size.delete');
    Route::get('product-size/list', 'Api\ProductSizeController@list')->name('api.v1.product-size.list');
    Route::get('product-size/getDropdown', 'Api\ProductSizeController@getDropdown')->name('api.v1.product-size.getDropdown');    
    Route::post('product-size/batchUpload', 'Api\ProductSizeController@batchUpload')->name('api.v1.product-size.batchUpload');    

    /*
    |--------------------------------------------------------------------------
    | Product Routes
    |--------------------------------------------------------------------------
    */
    Route::get('product/{id}', 'Api\ProductController@getProduct')->where('id', '[0-9]+')->name('api.v1.product.show');
    Route::post('product/store', 'Api\ProductController@store')->name('api.v1.product.store');
    Route::put('product/update', 'Api\ProductController@update')->name('api.v1.product.update');
    Route::get('product/delete/{id}', 'Api\ProductController@delete')->where('id', '[0-9]+')->name('api.v1.product.delete');
    Route::get('product/list', 'Api\ProductController@list')->name('api.v1.product.list');
    Route::post('product/batchUpload', 'Api\ProductController@batchUpload')->name('api.v1.product.batchUpload');    
    Route::get('product/downloadCsv', 'Api\ProductController@downloadCsv')->name('api.v1.product.downloadCsv');    
    Route::get('product/getProductSizes', 'Api\ProductController@getProductSizes')->name('api.v1.product.getProductSizes');  
    Route::get('product/getAll', 'Api\ProductController@getAll')->name('api.v1.product.getAll');  
    Route::get('product/product-meta-migration', 'Api\ProductController@productMetaMigration')->name('api.v1.product.productMetaMigration');  
    Route::put('product/batchUpdate', 'Api\ProductController@batchUpdate')->name('api.v1.product.batchUpdate');    

    /*
    |--------------------------------------------------------------------------
    | Tags Routes
    |--------------------------------------------------------------------------
    */
    Route::get('product/best-seller', 'Api\ProductController@getBestSeller')->name('api.v1.product.best-seller');    

    /*
    |--------------------------------------------------------------------------
    | Tags Routes
    |--------------------------------------------------------------------------
    */
    Route::get('tags/list/{tag?}', 'Api\TagController@list')->name('api.v1.tags.list');
    Route::post('tagger/store', 'Api\TagController@storeTag')->name('api.v1.tags.store');
    Route::post('tagger/reconstruct', 'Api\TagController@reconstruct')->name('api.v1.tags.reconstruct');

    /*
    |--------------------------------------------------------------------------
    | Banner Ads Routes
    |--------------------------------------------------------------------------
    */
    Route::get('banner-ads/{id}', 'Api\BannerAdController@getBannerDetails')->where('id', '[0-9]+')->name('api.v1.banner-ads.show');
    Route::post('banner-ads/store', 'Api\BannerAdController@store')->name('api.v1.banner-ads.store');
    Route::put('banner-ads/update', 'Api\BannerAdController@update')->name('api.v1.banner-ads.update');
    Route::get('banner-ads/delete/{id}', 'Api\BannerAdController@delete')->where('id', '[0-9]+')->name('api.v1.banner-ads.delete');
    Route::get('banner-ads/list', 'Api\BannerAdController@list')->name('api.v1.banner-ads.list');

    /*
    |--------------------------------------------------------------------------
    | Banner Ads Routes
    |--------------------------------------------------------------------------
    */
    Route::get('ads/{id}', 'Api\AdsController@getAdsDetails')->where('id', '[0-9]+')->name('api.v1.ads.show');
    Route::post('ads/store', 'Api\AdsController@store')->name('api.v1.ads.store');
    Route::put('ads/update', 'Api\AdsController@update')->name('api.v1.ads.update');
    Route::get('ads/delete/{id}', 'Api\AdsController@delete')->where('id', '[0-9]+')->name('api.v1.ads.delete');
    Route::get('ads/list', 'Api\AdsController@list')->name('api.v1.ads.list');

    /*
    |--------------------------------------------------------------------------
    | Flash Sale Routes
    |--------------------------------------------------------------------------
    */
    Route::get('rush-sale/{id}', 'Api\FlashSaleController@getFlashSaleDetails')->where('id', '[0-9]+')->name('api.v1.rush-sale.show');
    Route::post('rush-sale/store', 'Api\FlashSaleController@store')->name('api.v1.rush-sale.store');
    Route::put('rush-sale/update', 'Api\FlashSaleController@update')->name('api.v1.rush-sale.update');
    Route::get('rush-sale/delete/{id}', 'Api\FlashSaleController@delete')->where('id', '[0-9]+')->name('api.v1.rush-sale.delete');
    Route::get('rush-sale/list', 'Api\FlashSaleController@list')->name('api.v1.rush-sale.list');

    /*
    |--------------------------------------------------------------------------
    | View Tracker Routes
    |--------------------------------------------------------------------------
    */
    Route::get('view-tracker/list', 'Api\ViewTrackerController@list')->name('api.v1.view-tracker.list');    


    /*
    |--------------------------------------------------------------------------
    | Best Saler Routes
    |--------------------------------------------------------------------------
    */
    Route::get('best-saler/generate', 'Api\BestSellerController@generate')->name('api.v1.best-saler.generate');
    Route::get('best-saler/list', 'Api\BestSellerController@list')->name('api.v1.best-saler.list');
    Route::get('best-saler/delete', 'Api\BestSellerController@delete')->name('api.v1.best-saler.delete');

    /*
    |--------------------------------------------------------------------------
    | Voucher Routes
    |--------------------------------------------------------------------------
    */
    Route::get('voucher/{id}', 'Api\VoucherController@getVoucherDetails')->where('id', '[0-9]+')->name('api.v1.voucher.show');
    Route::post('voucher/store', 'Api\VoucherController@store')->name('api.v1.voucher.store');
    Route::put('voucher/update', 'Api\VoucherController@update')->name('api.v1.voucher.update');
    Route::get('voucher/delete/{id}', 'Api\VoucherController@delete')->where('id', '[0-9]+')->name('api.v1.voucher.delete');
    Route::get('voucher/list', 'Api\VoucherController@list')->name('api.v1.voucher.list');

    /*
    |--------------------------------------------------------------------------
    | Commission Rate Routes
    |--------------------------------------------------------------------------
    */
    Route::get('commission-rate/{id}', 'Api\CommissionRateController@getCommissionRateDetails')->where('id', '[0-9]+')->name('api.v1.commission-rate.show');
    Route::post('commission-rate/store', 'Api\CommissionRateController@store')->name('api.v1.commission-rate.store');
    Route::put('commission-rate/update', 'Api\CommissionRateController@update')->name('api.v1.commission-rate.update');
    Route::get('commission-rate/delete/{id}', 'Api\CommissionRateController@delete')->where('id', '[0-9]+')->name('api.v1.commission-rate.delete');
    Route::get('commission-rate/list', 'Api\CommissionRateController@list')->name('api.v1.commission-rate.list');

    /*
    |--------------------------------------------------------------------------
    | Orders Routes
    |--------------------------------------------------------------------------
    */
    Route::get('order/{id}', 'Api\OrderController@getOrderDetails')->where('id', '[0-9]+')->name('api.v1.order.show');
    Route::get('orders/list-statuses', 'Api\OrderController@getDropdownStatus')->name('api.v1.orders.getDropdownStatus');
    Route::put('order/update', 'Api\OrderController@update')->name('api.v1.order.update');
    Route::get('order/list', 'Api\OrderController@list')->name('api.v1.order.list');
    Route::get('order/item-list', 'Api\OrderController@orderItems')->name('api.v1.order.item-list');
    Route::post('order/downloadCsv', 'Api\OrderController@downloadCsv')->name('api.v1.order.downloadCsv');
    Route::post('orders/update-statuses', 'Api\OrderController@updateBatchOrder')->name('api.v1.orders.updateBatchOrder');

    /*
    |--------------------------------------------------------------------------
    | Questions Route 
    |--------------------------------------------------------------------------
    */
    Route::get('question/{id}', 'Api\QuestionController@getQuestion')->where('id', '[0-9]+')->name('api.v1.question.show');
    Route::post('question/store', 'Api\QuestionController@store')->name('api.v1.question.store');
    Route::put('question/update', 'Api\QuestionController@update')->name('api.v1.question.update');
    Route::get('question/delete/{id}', 'Api\QuestionController@delete')->where('id', '[0-9]+')->name('api.v1.question.delete');
    Route::get('question/list', 'Api\QuestionController@list')->name('api.v1.question.list');

    /*
    |--------------------------------------------------------------------------
    | Reports Routes
    |--------------------------------------------------------------------------
    */
    Route::get('report/sales', 'Api\ReportController@sales')->name('api.v1.report.sales');
    Route::get('report/download-csv', 'Api\ReportController@downloadCsv')->name('api.v1.report.download-csv');
    Route::get('report/sales-summary', 'Api\ReportController@sales')->name('api.v1.report.sales-summary');
    // Route::get('order/{id}', 'Api\OrderController@getOrderDetails')->where('id', '[0-9]+')->name('api.v1.order.show');
    // Route::get('orders/list-statuses', 'Api\OrderController@getDropdownStatus')->name('api.v1.orders.getDropdownStatus');
    // Route::put('order/update', 'Api\OrderController@update')->name('api.v1.order.update');
    // Route::get('order/item-list', 'Api\OrderController@orderItems')->name('api.v1.order.item-list');
    // Route::post('order/downloadCsv', 'Api\OrderController@downloadCsv')->name('api.v1.order.downloadCsv');
    // Route::post('orders/update-statuses', 'Api\OrderController@updateBatchOrder')->name('api.v1.orders.updateBatchOrder');

    /*
    |--------------------------------------------------------------------------
    | Courier Routes
    |--------------------------------------------------------------------------
    */
    Route::get('courier/get-area', 'Api\CourierControlller@getAreas')->name('api.v1.courier.get-area');

    Route::get('courier/get-package/{courier_id}', 'Api\CourierControlller@getPackageDetails')->where('courier_id', '[0-9]+')->name('api.v1.courier.get-package');
    Route::get('courier/get-excess-rate/{courier_id}', 'Api\CourierControlller@getExcessDetails')->where('id', '[0-9]+')->name('api.v1.courier.get-excess-rate');

    Route::get('courier/get-details/{id}', 'Api\CourierControlller@getDetails')->where('id', '[0-9]+')->name('api.v1.courier.get-details');
    Route::get('courier/list', 'Api\CourierControlller@list')->name('api.v1.courier.list');
    Route::post('courier/store', 'Api\CourierControlller@store')->name('api.v1.courier.store');
    Route::get('courier/delete/{id}', 'Api\CourierControlller@delete')->where('id', '[0-9]+')->name('api.v1.courier.delete');
    
    Route::post('courier/package/store', 'Api\CourierControlller@storePackage')->name('api.v1.courier.package.store');
    Route::get('courier/package/delete/{id}', 'Api\CourierControlller@deletePackage')->where('id', '[0-9]+')->name('api.v1.courier.package.delete');
    
    Route::post('courier/rate/store', 'Api\CourierControlller@storeRate')->name('api.v1.courier.rate.store');
    Route::get('courier/rate/delete/{id}', 'Api\CourierControlller@deleteRate')->where('id', '[0-9]+')->name('api.v1.courier.rate.delete');

    Route::post('courier/excess-rate/store', 'Api\CourierControlller@storeExcessRate')->name('api.v1.courier.excess-rate.store');
    Route::get('courier/excess-rate/delete/{id}', 'Api\CourierControlller@deleteExcessRate')->where('id', '[0-9]+')->name('api.v1.courier.excess-rate.delete');

    Route::get('courier/special-rate/list', 'Api\CourierControlller@specialRateList')->name('api.v1.courier.special-rate.list');
    Route::post('courier/special-rate/store', 'Api\CourierControlller@storeSpecialRate')->name('api.v1.courier.special-rate.store');
    Route::get('courier/special-rate/delete/{id}', 'Api\CourierControlller@deleteSpecialRate')->where('id', '[0-9]+')->name('api.v1.courier.special-rate.delete');

});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api_merchant']], function () 
{
   /*
    |--------------------------------------------------------------------------
    | Logout
    |--------------------------------------------------------------------------
    */
    Route::get('logout', 'Merchant\MerchantDashboardController@logout')->name('api.v1.auth.logout');

    /*
    |--------------------------------------------------------------------------
    | Dashboard Overview
    |--------------------------------------------------------------------------
    */
    Route::get('merchant/dashboard-count', 'Merchant\MerchantDashboardController@fetchOrdersCount')->name('api.v1.merchant.dashboard.count');
    Route::get('merchant/dashboard-report/{filter}', 'Merchant\MerchantDashboardController@getMonthlySalesReport')->name('api.v1.merchant.dashboard.orders');

    /*
    |--------------------------------------------------------------------------
    | Categories Routes
    |--------------------------------------------------------------------------
    */
    Route::get('merchant/category/{id}', 'Merchant\CategoryController@getCategory')->where('id', '[0-9]+')->name('api.v1.merchant.category.show');
    Route::post('merchant/category/store', 'Merchant\CategoryController@store')->name('api.v1.merchant.category.store');
    Route::post('merchant/category/update', 'Merchant\CategoryController@update')->name('api.v1.merchant.category.update');
    Route::get('merchant/category/delete/{id}', 'Merchant\CategoryController@delete')->where('id', '[0-9]+')->name('api.v1.merchant.category.delete');
    Route::get('merchant/category/list', 'Merchant\CategoryController@list')->name('api.v1.merchant.category.list');
    Route::get('merchant/category/getAllCategories', 'Merchant\CategoryController@getAllCategories')->name('api.v1.merchant.category.getAllCategories');
    Route::get('merchant/category/getDropdown', 'Merchant\CategoryController@getDropdown')->name('api.v1.merchant.category.getDropdown');
    Route::post('merchant/category/batchUpload', 'Merchant\CategoryController@batchUpload')->name('api.v1.merchant.category.batchUpload');    
    Route::get('merchant/category/downloadCsv', 'Merchant\CategoryController@downloadCsv')->name('api.v1.merchant.category.downloadCsv');    

    /*
    |--------------------------------------------------------------------------
    | Product Size Routes
    |--------------------------------------------------------------------------
    */
    Route::get('merchant/product-size/{id}', 'Merchant\ProductSizeController@getSize')->where('id', '[0-9]+')->name('api.v1.merchant.product-size.show');
    Route::post('merchant/product-size/store', 'Merchant\ProductSizeController@store')->name('api.v1.merchant.product-size.store');
    Route::put('merchant/product-size/update', 'Merchant\ProductSizeController@update')->name('api.v1.merchant.product-size.update');
    Route::get('merchant/product-size/delete/{id}', 'Merchant\ProductSizeController@delete')->where('id', '[0-9]+')->name('api.v1.merchant.product-size.delete');
    Route::get('merchant/product-size/list', 'Merchant\ProductSizeController@list')->name('api.v1.merchant.product-size.list');
    Route::get('merchant/product-size/getDropdown', 'Merchant\ProductSizeController@getDropdown')->name('api.v1.merchant.product-size.getDropdown');    
    Route::post('merchant/product-size/batchUpload', 'Merchant\ProductSizeController@batchUpload')->name('api.v1.merchant.product-size.batchUpload');    

    /*
    |--------------------------------------------------------------------------
    | Product Routes
    |--------------------------------------------------------------------------
    */
    Route::get('merchant/product/{id}', 'Merchant\ProductController@getProduct')->where('id', '[0-9]+')->name('api.v1.merchant.product.show');
    Route::post('merchant/product/store', 'Merchant\ProductController@store')->name('api.v1.merchant.product.store');
    Route::put('merchant/product/update', 'Merchant\ProductController@update')->name('api.v1.merchant.product.update');
    Route::get('merchant/product/delete/{id}', 'Merchant\ProductController@delete')->where('id', '[0-9]+')->name('api.v1.merchant.product.delete');
    Route::get('merchant/product/list', 'Merchant\ProductController@list')->name('api.v1.merchant.product.list');
    Route::post('merchant/product/batchUpload', 'Merchant\ProductController@batchUpload')->name('api.v1.merchant.product.batchUpload');    
    Route::get('merchant/product/downloadCsv', 'Merchant\ProductController@downloadCsv')->name('api.v1.merchant.product.downloadCsv');    
    Route::get('merchant/product/getProductSizes', 'Merchant\ProductController@getProductSizes')->name('api.v1.merchant.product.getProductSizes');  

    Route::get('merchant/product/brands', 'Merchant\ProductController@getBrands')->name('api.v1.merchant.product.brands');  
    Route::get('merchant/product/tags', 'Merchant\ProductController@getTags')->name('api.v1.merchant.product.tags');  

    /*
    |--------------------------------------------------------------------------
    | Merchant Orders Routes
    |--------------------------------------------------------------------------
    */
    Route::get('merchant/orders/list', 'Merchant\OrderController@list')->name('api.v1.merchant.orders.list');
    Route::get('merchant/orders/list-statuses', 'Merchant\OrderController@getDropdownStatus')->name('api.v1.merchant.orders.getDropdownStatus');
    Route::get('merchant/orders/{id}', 'Merchant\OrderController@getDetails')->name('api.v1.merchant.orders.details');
    Route::post('merchant/orders/update', 'Merchant\OrderController@updateOrder')->name('api.v1.merchant.orders.updateOrder');
    Route::post('merchant/orders/update-statuses', 'Merchant\OrderController@updateBatchOrder')->name('api.v1.merchant.orders.updateBatchOrder');
    Route::post('merchant/orders/downloadCsv', 'Merchant\OrderController@downloadCsv')->name('api.v1.merchant.orders.downloadCsv');

    /*
    |--------------------------------------------------------------------------
    | Merchant Routes
    |--------------------------------------------------------------------------
    */
    Route::get('merchant-settings', 'Merchant\MerchantDashboardController@getMerchantDetails')->where('id', '[0-9]+')->name('api.v1.merchant-settings.show');
    Route::post('merchant-settings/update', 'Merchant\MerchantDashboardController@update')->name('api.v1.merchant-settings.update');

    /*
    |--------------------------------------------------------------------------
    | Courier Routes
    |--------------------------------------------------------------------------
    */
    Route::get('merchant-courier/get-area', 'Api\CourierControlller@getAreas')->name('api.v1.merchant-courier.get-area');

    Route::get('merchant-courier/get-package/{courier_id}', 'Api\CourierControlller@getPackageDetails')->where('courier_id', '[0-9]+')->name('api.v1.merchant-courier.get-package');
    Route::get('merchant-courier/get-excess-rate/{courier_id}', 'Api\CourierControlller@getExcessDetails')->where('id', '[0-9]+')->name('api.v1.merchant-courier.get-excess-rate');

    Route::get('merchant-courier/get-details/{id}', 'Api\CourierControlller@getDetails')->where('id', '[0-9]+')->name('api.v1.merchant-courier.get-details');
    Route::get('merchant-courier/list', 'Api\CourierControlller@list')->name('api.v1.merchant-courier.list');
    Route::post('merchant-courier/store', 'Api\CourierControlller@store')->name('api.v1.merchant-courier.store');
    Route::get('merchant-courier/delete/{id}', 'Api\CourierControlller@delete')->where('id', '[0-9]+')->name('api.v1.merchant-courier.delete');
    
    Route::post('merchant-courier/package/store', 'Api\CourierControlller@storePackage')->name('api.v1.merchant-courier.package.store');
    Route::get('merchant-courier/package/delete/{id}', 'Api\CourierControlller@deletePackage')->where('id', '[0-9]+')->name('api.v1.merchant-courier.package.delete');
    
    Route::post('merchant-courier/rate/store', 'Api\CourierControlller@storeRate')->name('api.v1.merchant-courier.rate.store');
    Route::get('merchant-courier/rate/delete/{id}', 'Api\CourierControlller@deleteRate')->where('id', '[0-9]+')->name('api.v1.merchant-courier.rate.delete');

    Route::post('merchant-courier/excess-rate/store', 'Api\CourierControlller@storeExcessRate')->name('api.v1.merchant-courier.excess-rate.store');
    Route::get('merchant-courier/excess-rate/delete/{id}', 'Api\CourierControlller@deleteExcessRate')->where('id', '[0-9]+')->name('api.v1.merchant-courier.excess-rate.delete');

    Route::get('merchant-courier/special-rate/list', 'Api\CourierControlller@specialRateList')->name('api.v1.merchant-courier.special-rate.list');
    Route::post('merchant-courier/special-rate/store', 'Api\CourierControlller@storeSpecialRate')->name('api.v1.merchant-courier.special-rate.store');
    Route::get('merchant-courier/special-rate/delete/{id}', 'Api\CourierControlller@deleteSpecialRate')->where('id', '[0-9]+')->name('api.v1.merchant-courier.special-rate.delete');

    /*
    |--------------------------------------------------------------------------
    | Reports Routes
    |--------------------------------------------------------------------------
    */
    Route::get('merchant-report/sales', 'Api\MerchantReportController@sales')->name('api.v1.merchant-report.sales');
    Route::get('merchant-report/download-csv', 'Api\MerchantReportController@downloadCsv')->name('api.v1.merchant-report.download-csv');
    Route::get('merchant-report/sales-summary', 'Api\MerchantReportController@getSales')->name('api.v1.merchant-report.sales-summary');
    Route::get('merchant-report/brand/getDropdown', 'Api\MerchantReportController@getDropdown')->name('api.v1.merchant-report.brand-getDropdown');

});