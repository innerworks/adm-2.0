<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'web', 'middleware' => ['auth:api_customer']], function () {

	Route::post('/addWishList', 'Web\WebController@addWishList')->name('web.addWishList');
	Route::get('/getWishlists/{id}', 'Web\WebController@getWishlists')->name('web.getWishlists');
	Route::get('/getWishItem/{id}', 'Web\WebController@getWishItem')->name('web.getWishItem');
	Route::get('/deleteWishlist/{id}', 'Web\WebController@deleteWishlist')->name('web.deleteWishlist');

	Route::get('/getDefaultShipping', 'Web\WebController@getDefaultShipping')->name('web.getDefaultShipping');
	Route::get('/getDefaultBilling', 'Web\WebController@getDefaultBilling')->name('web.getDefaultBilling');

	// Route::post('/addItemHistory', 'Web\WebController@addItemHistory')->name('web.addItemHistory');

	Route::get('/countCart', 'Web\WebController@countCart')->name('web.countCart');
	Route::post('/storeToCart', 'Web\WebController@storeToCart')->name('web.storeToCart');
	Route::get('/getCarts', 'Web\WebController@getCarts')->name('web.getCarts');
	Route::post('/updateCartQty', 'Web\WebController@updateCartQty')->name('web.updateCartQty');
	Route::post('/setItemQty', 'Web\WebController@setItemQty')->name('web.setItemQty');
	Route::get('/deleteCart/{cart_id}', 'Web\WebController@deleteCart')->name('web.deleteCart');
	Route::post('/selectThisItem', 'Web\WebController@selectThisItem')->name('web.selectThisItem');
	Route::post('/selectThisItems', 'Web\WebController@selectThisItems')->name('web.selectThisItems');
	// Route::get('/getShippingCheckout/{permalink}', 'Web\WebController@getShippingCheckout')->name('web.getShippingCheckout');
	Route::post('/updateCourier', 'Web\WebController@updateCourier')->name('web.updateCourier');
	Route::get('/getCheckout', 'Web\WebController@getCheckout')->name('web.getCheckout');
	Route::get('/getCheckoutByProduct/{product_id}', 'Web\WebController@getCheckoutByProduct')->name('web.getCheckoutByProduct');
	Route::get('/checkout/getPaymentType', 'Web\WebController@getPaymentType')->name('web.getPaymentType');
	Route::post('/checkout/placeOrder', 'Web\WebController@placeOrder')->name('web.placeOrder');
	Route::post('/checkout/placeOrderSingleProduct', 'Web\WebController@placeOrderSingleProduct')->name('web.placeOrderSingleProduct');
	Route::post('/paymentStatus', 'Web\PaymentStatusController@index')->name('web.paymentStatus');

	Route::post('/getProfile', 'Web\WebController@getProfile')->name('web.getProfile');  
	Route::post('/editProfile', 'Web\WebController@editProfile')->name('web.editProfile');  

	Route::get('/getOrders', 'Web\WebController@getOrders')->name('web.getOrders'); 
	Route::post('/getOrderDetail', 'Web\WebController@getOrderDetail')->name('web.getOrderDetail'); 
	Route::get('/getOrderByStatus/{status_id}', 'Web\WebController@getOrderByStatus')->name('web.getOrderByStatus'); 
	Route::post('/getOrderTrackingStatus', 'Web\WebController@getOrderTrackingStatus')->name('web.getOrderTrackingStatus'); 

	Route::post('/addRating', 'Web\WebController@addRating')->name('web.addRating');
	Route::post('/ratingChecker', 'Web\WebController@ratingChecker')->name('web.ratingChecker');
});

Route::group(['prefix' => 'web'], function () {
	// Route::get('/generate-elastic', 'Web\WebController@setupDataElastic')->name('web.setupElastic');

	Route::get('elastic-search/create/batch-products/{index_name}', 'Web\ElasticController@createBatchProducts')->name('web.createBatchProducts');
	Route::get('elastic-search/create/index/{index_name}', 'Web\ElasticController@createIndex')->name('web.createIndex');
	Route::get('elastic-search/list/index/{index_name}', 'Web\ElasticController@getIndex')->name('web.getIndex');
	Route::get('elastic-search/list/index/', 'Web\ElasticController@getIndices')->name('web.getIndices');
	Route::get('elastic-search/delete/index/{index_name}', 'Web\ElasticController@deleteIndex')->name('web.deleteIndex');
	Route::get('elastic-search/stats/index/{index_name}', 'Web\ElasticController@getSearchFields')->name('web.getSearchFields');
	Route::get('elastic-search/category/{category}/search/{search}', 'Web\ElasticController@getSearch')->name('web.getSearch');

	// Route::get('/search', 'Web\WebController@getElasticSearch')->name('web.search'); // aws elasticsearch
	Route::get('/search', 'Web\ElasticController@getSearch')->name('web.customSearch'); // custom elasticsearch
	Route::get('/search-all', 'Web\WebController@searchAll')->name('web.searchAll');

	Route::get('/getOnSaleProducts', 'Web\WebController@getOnSaleProducts')->name('web.getOnSaleProducts');
	Route::get('/getAllCategories', 'Web\WebController@getAllCategories')->name('web.getAllCategories');
	Route::get('/getParentCategories', 'Web\WebController@getParentCategories')->name('web.getParentCategories');
	Route::get('/getBrands', 'Web\WebController@getBrands')->name('web.getBrands');
	Route::get('/getBrand/{id}', 'Web\WebController@getBrand')->name('web.getBrand');
	Route::get('/getFlashSale', 'Web\WebController@getFlashSale')->name('web.getFlashSale');
	Route::get('/getBestSellers', 'Web\WebController@getBestSellers')->name('web.getBestSellers');
	Route::get('/getBestSellersRandom', 'Web\WebController@getBestSellersRandom')->name('web.getBestSellersRandom');
	Route::get('/getNewProducts', 'Web\WebController@getNewProducts')->name('web.getNewProducts');
	Route::get('/getProductsByBrand', 'Web\WebController@getProductsByBrand')->name('web.getProductsByBrand');
	Route::get('/getProductsByCategory', 'Web\WebController@getProductsByCategory')->name('web.getProductsByCategory');

	Route::get('/getProduct/{id}', 'Web\WebController@getProduct')->name('web.getProduct');
	Route::post('/getRelatedProducts', 'Web\WebController@getRelatedProducts')->name('web.getRelatedProducts');
	Route::get('/getProductByLink/{permalink}', 'Web\WebController@getProductByLink')->name('web.getProductByLink');
	Route::get('/getMerchantByLink/{permalink}', 'Web\WebController@getMerchantByLink')->name('web.getMerchantByLink');
	Route::get('/getProductsByMerchant', 'Web\WebController@getProductsByMerchant')->name('web.getProductsByMerchant');
	Route::get('/getMerchantBanner', 'Web\WebController@getMerchantBanner')->name('web.getMerchantBanner');

});

Route::get('/admin/{any}', 'SpaController@index')->where('any', '.*');
Route::get('/merchant/{any}', 'SpaController@merchant')->where('any', '.*');
Route::get('/{any}', 'SpaController@customer')->where('any', '^((?!login/).)*');

// Route::get('/payment-status', 'Web\PaymentStatusController@index')->name('payment-status');

Route::namespace('Socialite')->prefix('login/facebook')->group(function(){
	// Facebook Login
	Route::get('/','LoginController@redirectToProviderFacebook')->name('login.facebook');
	Route::get('/callback','LoginController@handleProviderCallbackFacebook');
});

Route::namespace('Socialite')->prefix('login/google')->group(function(){
	// Google Login
	Route::get('/','LoginController@redirectToProviderGoogle')->name('login.google');
	Route::get('/callback','LoginController@handleProviderCallbackGoogle');
});