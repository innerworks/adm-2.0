/* CATEGORIES MIGRATION */
DELETE FROM adobomall_v2.categories

INSERT INTO adobomall_v2.categories (id, parent_id, NAME, slug_name, created_at, updated_at)
SELECT category_id, parent_id, category_name, REPLACE(category_name,' ','_'), SYSDATE(), SYSDATE()
FROM adobomall_v1.categories WHERE is_active = 1

SELECT * FROM adobomall_v2.categories

/* BRANDS MIGRATION */
DELETE FROM adobomall_v2.brands

INSERT INTO adobomall_v2.brands (id, NAME, slug_name, image_url, distribution_type, active, created_at, updated_at)
SELECT brand_id, brand_name, brand_slug_name, 
CONCAT('https://ammediaserver.s3.amazonaws.com/beta/media/',brand_logo),
'outright', 1, SYSDATE(), SYSDATE()
FROM adobomall_v1.brands

SELECT * FROM adobomall_v2.brands

/* MERCHANTS MIGRATION */
SELECT 
m.merchant_id, store_name, slug_name, merchant_category, username, merchant_lname, merchant_fname,
merchant_mname, email_address, phone_number,  mobile_number, 
CONCAT('https://ammediaserver.s3.amazonaws.com/beta/media/',primary_img) AS primary_img, 
CONCAT('https://ammediaserver.s3.amazonaws.com/beta/media/',primary_mobile_img) AS primary_mobile_img,  
CONCAT('https://ammediaserver.s3.amazonaws.com/beta/media/',profile_img_uri) AS profile_img_uri,  
business_name, business_permit_number, tin, m.status, profile_details,
commission_rate, store_contact_person, store_contact_number, store_address1, 
store_address2, store_city_code, store_prov_code, store_brgy_code, store_zip_code
FROM adobomall_v1.merchants AS m
LEFT JOIN adobomall_v1.merchant_stores AS ms ON m.merchant_id = ms.merchant_id
WHERE m.merchant_id > 10
GROUP BY m.merchant_id
ORDER BY m.merchant_id
LIMIT 20

SELECT COUNT(*) FROM adobomall_v1.merchants
SELECT COUNT(*) FROM adobomall_v2.merchants

/* PRODUCTS MIGRATION */
SELECT * FROM adobomall_v1.products

INSERT INTO adobomall_v2.products (id, sku, NAME, description, permalink, STATUS, merchant_id, category_id, brand_id, base_price, 
weight, LENGTH, width, height, other_information, primary_photo, created_at, updated_at)
SELECT p.product_id, p.product_sku, p.product_name, p.product_desc, 
CONCAT(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(p.product_name), '-', ''), ' ', '-'), '(', ''), ')', ''),'/', '-'),',',''), '-', p.product_id) AS permalink,
p.is_available, p.merchant_id, 
p.category_id, p.brand_id, p.srp, p.product_height, p.product_length, p.product_width, p.product_height,
p.product_specs,
(SELECT CONCAT('https://ammediaserver.s3.amazonaws.com/beta/media/',pm.uri) FROM adobomall_v1.product_media AS pm WHERE pm.product_id = p.product_id LIMIT 1) AS primary_photo,
SYSDATE(), SYSDATE()
FROM adobomall_v1.products AS p
ORDER BY p.product_id

SELECT * FROM adobomall_v2.products

/* PRODUCTS SIZES MIGRATION */
SELECT DISTINCT(size) FROM adobomall_v1.products WHERE size != ''

INSERT INTO adobomall_v2.product_sizes (size, created_at, updated_at)
SELECT DISTINCT(size), SYSDATE(), SYSDATE() 
FROM adobomall_v1.products WHERE size != ''

/* TAGS MIGRATION */
SELECT * FROM adobomall_v1.product_keyword

INSERT INTO tags (tag_name, created_at, updated_at)
SELECT DISTINCT(key_name), SYSDATE(), SYSDATE() FROM adobomall_v1.product_keyword
ORDER BY key_id


/* PRODUCT SIZE MIGRATION */
SELECT product_id, (CASE WHEN size = '' THEN 'FREE SIZE/NO SIZE' ELSE size END) AS size  
FROM adobomall_v1.products

SELECT (product_id-1) FROM adobomall_v2.product_meta ORDER BY id DESC LIMIT 1



